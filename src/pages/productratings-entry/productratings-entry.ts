import { Component,ViewChild,ElementRef } from '@angular/core';
import { DomSanitizer } from "@angular/platform-browser";
import { IonicPage, NavController, NavParams, LoadingController, AlertController,ToastController,ActionSheetController } from 'ionic-angular';
import * as _ from 'lodash';
import * as async from 'async';

import { Camera, CameraOptions } from "@ionic-native/camera";
import { File } from "@ionic-native/file";
import { FilePath } from "@ionic-native/file-path";
import { FileTransferObject } from "@ionic-native/file-transfer";

import { environment } from '@app/env';

import { ProductsServiceProvider } from '../../providers/service/products';
import { OrdersServiceProvider } from '../../providers/service/orders';

@IonicPage()
@Component({
	selector: 'page-productratings-entry',
	templateUrl: 'productratings-entry.html',
})
export class ProductratingsEntryPage {
	public serverUrl = environment.api_url + 'products';

	public isUploading = false;
	public uploadingProgress = {};
	public uploadingHandler = {};
	public images: any = [];
	public filesToUpload: Array<File> = [];
	protected imagesValue: Array<any>;

	order: any = {};
	product: any = {};
	rating:any = {};
	productsArr: any[] = [];

	rate: number;
	orderId: string;
	type: string;

	@ViewChild('myInput') myInput: ElementRef;

	constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public loadingCtrl: LoadingController, public toastCtrl: ToastController,private sanitization: DomSanitizer, private actionSheetCtrl: ActionSheetController, private camera: Camera,
		private file: File, public products: ProductsServiceProvider, public orders: OrdersServiceProvider) {

		this.rate = 5;
		this.order = navParams.get('item');
		this.type = navParams.get('type');
		console.log('this.order: ', this.order);
		console.log('this.type: ', this.type);

		if (this.order.details && this.order.details.length > 0) {
			this.product = this.order.details[0];

			_.each(this.order.details, (row: any) => {
				this.productsArr.push(row);
			});

			console.log('this.product: ', this.product);
		}
	}

	public uploadImages(productId): Promise<Array<any>> {
		return new Promise((resolve, reject) => {
			this.isUploading = true;
			Promise.all(this.filesToUpload.map(image => {
				return this.uploadImage(image, productId);
			}))
				.then(resolve)
				.catch(reason => {
					this.isUploading = false;
					this.uploadingProgress = {};
					this.uploadingHandler = {};
					reject(reason);
				});

		});
	}

	public abort() {
		if (!this.isUploading)
			return;
		this.isUploading = false;
		for (let key in this.uploadingHandler) {
			this.uploadingHandler[key].abort();
		}
	}

	// ======================================================================
	protected removeImage(image) {
		if (this.isUploading) {
			return;
		}

		this.confirm("Are you sure to remove it?").then(value => {
			if (value) {
				this.removeFromArray(this.imagesValue, image);
				this.removeFromArray(this.images, image.url);
				// this.removeFromArray(this.filesToUpload, image.url);
				/*if (image && image.Id) {
					this.deleteProductImage(image.Id);
				}*/
			}
		});
	}

	protected showAddImage() {
		if (!window['cordova']) {
			let input = document.createElement('input');
			input.type = 'file';
			input.accept = "image/x-png,image/gif,image/jpeg";
			input.click();
			input.onchange = () => {
				let blob = window.URL.createObjectURL(input.files[0]);
				let blob2: any = input.files[0];
				this.images.push(blob);
				this.filesToUpload.push(blob2);
				console.log('this.filesToUpload: ', this.filesToUpload);
				this.trustImages();87
			}
		} else {
			new Promise((resolve, reject) => {
				let actionSheet = this.actionSheetCtrl.create({
					title: 'Add a photo',
					buttons: [
						{
							text: 'From photo library',
							handler: () => {
								resolve(this.camera.PictureSourceType.PHOTOLIBRARY);
							}
						},
						{
							text: 'From camera',
							handler: () => {
								resolve(this.camera.PictureSourceType.CAMERA);
							}
						},
						{
							text: 'Cancel',
							role: 'cancel',
							handler: () => {
								reject();
							}
						}
					]
				});
				actionSheet.present();
			}).then(sourceType => {
				if (!window['cordova'])
					return;
				let options: CameraOptions = {
					quality: 100,
					sourceType: sourceType as number,
					saveToPhotoAlbum: false,
					correctOrientation: true
				};
				this.camera.getPicture(options).then((imagePath) => {
					console.log('CameraOptions imagePath: ',imagePath);
					this.images.push(imagePath);
					this.filesToUpload.push(imagePath);
					console.log('CameraOptions this.filesToUpload: ', this.filesToUpload);
					this.trustImages();
				});
			}).catch(() => {
			});
		}
	}

	private uploadImage(targetPath, productId) {
		return new Promise((resolve, reject) => {
			this.uploadingProgress[targetPath] = 0;
			var uploadUrl = this.serverUrl + '/' + productId + '/image';
			console.log('targetPath: ', targetPath);
			console.log('uploadUrl: ', uploadUrl);

			if (window['cordova']) {
				let options = {
					fileKey: "image",
					fileName: targetPath,
					chunkedMode: false,
					method: 'PUT',
					mimeType: "multipart/form-data",
					headers: {
						'Authorization': 'Bearer ' + localStorage.getItem('app.token')
					}
				};

				const fileTransfer = new FileTransferObject();
				this.uploadingHandler[targetPath] = fileTransfer;

				fileTransfer.upload(targetPath, uploadUrl, options).then(data => {
					resolve(JSON.parse(data.response));
				}).catch(() => {
					askRetry();
				});

				fileTransfer.onProgress(event2 => {
					this.uploadingProgress[targetPath] = event2.loaded * 100 / event2.total;
				});
			} else {
				let formData: FormData = new FormData(),
					xhr2: XMLHttpRequest = new XMLHttpRequest();

				formData.append('image', targetPath);
				this.uploadingHandler[targetPath] = xhr2;

				xhr2.onreadystatechange = () => {
					if (xhr2.readyState === 4) {
						if (xhr2.status === 200)
							resolve(JSON.parse(xhr2.response));
						else
							askRetry();
					}
				};

				xhr2.upload.onprogress = (event) => {
					this.uploadingProgress[targetPath] = event.loaded * 100 / event.total;
				};
				xhr2.open('PUT', uploadUrl, true);
				xhr2.setRequestHeader('Authorization', 'Bearer ' + localStorage.getItem('app.token'));
				xhr2.send(formData);
			}

			let askRetry = () => {
				// might have been aborted
				if (!this.isUploading) return reject(null);
				this.confirm('Do you wish to retry?', 'Upload failed').then(res => {
					if (!res) {
						this.isUploading = false;
						for (let key in this.uploadingHandler) {
							this.uploadingHandler[key].abort();
						}
						return reject(null);
					}
					else {
						if (!this.isUploading) return reject(null);
						this.uploadImage(targetPath, productId).then(resolve, reject);
					}
				});
			};
		});
	}

	private removeFromArray<T>(array: Array<T>, item: T) {
		let index: number = array.indexOf(item);
		if (index !== -1) {
			array.splice(index, 1);
		}
	}

	private confirm(text, title = '', yes = "Yes", no = "No") {
		return new Promise(
			(resolve) => {
				this.alertCtrl.create({
					title: title,
					message: text,
					buttons: [
						{
							text: no,
							role: 'cancel',
							handler: () => {
								resolve(false);
							}
						},
						{
							text: yes,
							handler: () => {
								resolve(true);
							}
						}
					]
				}).present();
			}
		);
	}

	private trustImages() {
		this.imagesValue = this.images.map(
			val => {
				return {
					url: val,
					sanitized: this.sanitization.bypassSecurityTrustStyle("url(" + val + ")")
				}
			}
		);
		console.log('this.imagesValue: ', this.imagesValue);
	}

	private trustImages2() {
		this.imagesValue = this.images.map(
			val => {
				return {
					url: val.url,
					Id: val.Id,
					sanitized: this.sanitization.bypassSecurityTrustStyle("url(" + val.url + ")")
				}
			}
		);
		console.log('this.imagesValue: ', this.imagesValue);
	}

	showToast(text: string) {
		this.toastCtrl.create({
			message: text,
			duration: 5000,
			position: 'bottom'
		}).present();
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad ProductratingsEntryPage');
	}

	initData(ev?: any) {
		let loading = this.loadingCtrl.create();
		if (!_.isEmpty(ev)) {
			loading.present();
		}
		this.orders.getOrderDetails(this.orderId, this.type).then((data: any) => {
			if (data && data.success) {
				this.order = data.result;

				if (this.order.details && this.order.details.length > 0) {
					this.productsArr = [];

					this.product = this.order.details[0];
					_.each(this.order.details, (row: any) => {
						this.productsArr.push(row);
					});
				}
				console.log('this.order: ', this.order);
			}
			if (ev) {
				ev.compelete();
			}
		}, (error) => {
			if (ev) {
				ev.compelete();
			}
		});
	}

	doRefresh(ev){
		this.initData(ev);
	}

	onModelChange(ev) {
		console.log('onModelChange: ', ev);
		this.rate = ev;
		this.rating.rate = ev;
	}


	submitRating() {
		let loading = this.loadingCtrl.create();
		loading.present();
		this.rating.rate = this.rate;
		console.log('rating: ', this.rating);

		async.eachSeries(this.productsArr,(item:any,callback)=>{
			this.products.rateProduct(item.productId, this.rating).then((data:any)=>{
				console.log('error: ',data);
				if(data && data.success){
					callback();
				}
			}).catch((error)=>{
				console.log('error: ',error);
				callback();
			});
		},()=>{
			loading.dismiss();
			let alert = this.alertCtrl.create({
				title: 'Success',
				message: 'Product successfully rated!',
				buttons:['OK']
			});
			alert.onDidDismiss(()=>{
				this.navCtrl.pop();
			});
			alert.present();
		});
	}
}
