import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Ionic2RatingModule } from 'ionic2-rating';

import { ProductratingsEntryPage } from './productratings-entry';

@NgModule({
  declarations: [
    ProductratingsEntryPage,
  ],
  imports: [
  	Ionic2RatingModule,
    IonicPageModule.forChild(ProductratingsEntryPage),
  ],
})
export class ProductratingsEntryPageModule {}
