import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';

import { AuthProvider } from '../../providers/service/authentication';

@IonicPage()
@Component({
	selector: 'page-forgot',
	templateUrl: 'forgot.html',
})
export class ForgotPage {
	item: any = {};
	errArr:any[] = [];

	constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public auths: AuthProvider, public alertCtrl: AlertController) {
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad ForgotPage');
	}

	continue() {
		let loading = this.loadingCtrl.create();
		loading.present();
		this.auths.forgotPassword(this.item).then((data: any) => {
			if (data && data.success) {
				console.log('data: ', data);
				loading.dismiss();

				let alert = this.alertCtrl.create({
					title: 'Success',
					subTitle: data.msg,
					buttons: ['OK']
				});
				alert.onDidDismiss(() => {
					this.navCtrl.pop();
				});
				alert.present();
			} else {
				loading.dismiss();

				let alert = this.alertCtrl.create({
					title: 'Error',
					subTitle: data.msg,
					buttons: ['OK']
				});
				alert.present();
				return;
			}
		}, (error) => {
			loading.dismiss();
			console.log('error: ', error);
			let err = error.data;
			this.errArr = err.result;
			setTimeout(() => {
				this.errArr = [];
			}, 3000);
		});
	}

}
