import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { Ionic2RatingModule } from "ionic2-rating";

import { ShopprofilePage, ShopProfilePopoverPage } from "./shopprofile";

import { PipesModule } from "../../pipes/pipes.module";
import { ComponentsModule } from "../../components/components.module";

@NgModule({
  declarations: [ShopprofilePage, ShopProfilePopoverPage],
  imports: [
    PipesModule,
    ComponentsModule,
    Ionic2RatingModule,
    IonicPageModule.forChild(ShopprofilePage)
  ],
  entryComponents: [ShopProfilePopoverPage]
})
export class ShopprofilePageModule {}
