import { Component, NgZone } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  App,
  MenuController,
  PopoverController,
  ViewController,
  AlertController,
  LoadingController
} from "ionic-angular";
import * as _ from "lodash";
import * as async from "async";

import { DetailsPage } from "../details/details";
import { MyfollowerFollowingPage } from "../myfollower-following/myfollower-following";
import { MyratingPage } from "../myrating/myrating";
import { SearchresultPage } from "../searchresult/searchresult";

import { UsersServiceProvider } from "../../providers/service/users";
import { SellerServiceProvider } from "../../providers/service/seller";
import { SellerVouchersService } from "../../providers/service/seller-vouchers";
import { ServiceProvider } from "../../providers/service/service";

@Component({
  template: `
    <ion-list>
      <button ion-item (click)="close()">Share</button>
      <button ion-item (click)="close()">Report this user</button>
      <button ion-item (click)="close()">Block this user</button>
    </ion-list>
  `
})
export class ShopProfilePopoverPage {
  constructor(public viewCtrl: ViewController) {}

  close() {
    this.viewCtrl.dismiss();
  }
}
@IonicPage()
@Component({
  selector: "page-shopprofile",
  templateUrl: "shopprofile.html"
})
export class ShopprofilePage {
  imageUrl: string = "assets/imgs/banner_def.jpg";
  mode: string = "shop";
  action: string = "popular";
  isLow: boolean = false;

  showLoading: boolean = true;
  showFollow: boolean = true;
  showChat: boolean = false;
  following: boolean = false;
  isLogin: boolean = false;

  profile: any = {};
  details: any = {};
  user: any = {};
  currency: any = {};

  userLikesArr: any[] = [];
  productsArr: any[] = [];
  productsArrCopy: any[] = [];
  sellerVouchersArr: any[] = [];

  followerCount: number = 0;
  followingCount: number = 0;

  constructor(
    public app: App,
    public navCtrl: NavController,
    public navParams: NavParams,
    public menuCtrl: MenuController,
    public popoverCtrl: PopoverController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public zone: NgZone,
    public users: UsersServiceProvider,
    public sellers: SellerServiceProvider,
    public sellerVouchers: SellerVouchersService,
    public serviceProvider: ServiceProvider
  ) {
    this.profile = navParams.get("profile");
    this.user = JSON.parse(localStorage.getItem("app.user")) || {};
    this.currency = this.serviceProvider.defaultCurrency();
    console.log("this.profile: ", this.profile);

    if (!_.isEmpty(this.user)) {
      this.isLogin = true;
    } else {
      this.isLogin = false;
    }
  }

  initData(ev?: any) {
    this.showLoading = true;

    if (this.profile.uuid == this.user.uuid) {
      this.showFollow = false;
      this.showChat = false;
    }else{
      this.showChat = true;
    }

    this.sellers.getShopProducts(this.profile.uuid).then(
      (data: any) => {
        if (data && data.success) {
          _.each(data.result, (row: any) => {
            row.date_created_timestamp = new Date(row.date_created).getTime();
          });

          this.productsArr = data.result;
          this.productsArrCopy = _.cloneDeep(data.result);
          console.log("seller products: ", this.productsArr);
        }

        if (!_.isEmpty(this.user)) {
          this.users.getCurrentUserLike().then(
            (data: any) => {
              if (data && data.success) {
                this.userLikesArr = data.result;
                _.each(this.productsArr, (row: any) => {
                  let res = _.find(this.userLikesArr, {
                    productId: row.productId
                  });
                  if (res) {
                    row.isLiked = true;
                  } else {
                    row.isLiked = false;
                  }
                });
                this.productsArrCopy = _.cloneDeep(this.productsArr);
              }
            },
            error => {
              console.log("getCurrentUserLike: ", error);
            }
          );
        }
      },
      error => {
        console.log("error: ", error);
      }
    );

    this.sellerVouchers.getSellerVouchers(this.profile.uuid).then(
      (data: any) => {
        if (data && data.success) {
          this.sellerVouchersArr = data.result;
        }
      },
      error => {
        console.log("error: ", error);
      }
    );

    async.waterfall(
      [
        callback => {
          this.users.getCurrentUserFollowing().then(
            (data: any) => {
              if (data && data.success) {
                this.followingCount = _.size(data.result);
                this.user.following = data.result;
                callback();
              } else {
                callback();
              }
            },
            error => {
              this.user.following = [];
              this.showLoading = false;
              if (ev) {
                ev.complete();
              }
              callback();
            }
          );
        },
        callback => {
          if (_.find(this.user.following, { uuid: this.profile.uuid })) {
            this.following = true;
          } else {
            this.following = false;
          }

          this.users.getShopProfile(this.profile.uuid).then(
            (data: any) => {
              if (data && data.success) {
                this.details = data.result;
                this.imageUrl = _.isEmpty(this.details.banner_path)
                  ? "./assets/imgs/banner_def.jpg"
                  : this.details.banner_path;

                this.sellers
                  .getShopRatings(this.profile.uuid)
                  .then((data1: any) => {
                    if (data1 && data1.success) {
                      if (data1.result) {
                        this.details.rating = data1.result.average;
                      } else {
                        this.details.rating = 0;
                      }
                    }
                  });

                console.log("this.details: ", this.details);
              }
              callback();
            },
            error => {
              console.log("error: ", error);
              this.showLoading = false;
              if (ev) {
                ev.complete();
              }
              callback();
            }
          );
        },
        callback => {
          async.parallel(
            [
              cb => {
                this.sellers.getSellerFollowers(this.profile.uuid).then(
                  (data: any) => {
                    if (data && data.success) {
                      this.followerCount = _.size(data.result);
                    }
                    cb();
                  },
                  error => {
                    console.log("error: ", error);
                    cb();
                  }
                );
              },
              cb => {
                this.sellers.getSellerFollowing(this.profile.uuid).then(
                  (data: any) => {
                    if (data && data.success) {
                      this.followingCount = _.size(data.result);
                    }
                    cb();
                  },
                  error => {
                    console.log("error: ", error);
                    cb();
                  }
                );
              }
            ],
            callback
          );
        }
      ],
      () => {
        this.showLoading = false;
        if (ev) {
          ev.complete();
        }
      }
    );
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad ShopprofilePage");
    this.initData();
  }

  ionViewDidEnter() {
    console.log("ionViewDidLoad ionViewDidEnter");
    // this.initData();
  }

  gotoDetails(item) {
    this.app.getRootNav().push(DetailsPage, {
      product: item
    });
  }

  doRefresh(ev) {
    this.initData(ev);
  }

  getFollowers() {
    this.sellers.getSellerFollowers(this.profile.uuid).then(
      (data: any) => {
        if (data && data.success) {
          this.followerCount = _.size(data.result);
        }
      },
      error => {
        console.log("getSellerFollowers error: ", error);
      }
    );
  }

  getFollowing() {
    this.sellers.getSellerFollowing(this.profile.uuid).then(
      (data: any) => {
        if (data && data.success) {
          this.followingCount = _.size(data.result);
        }
      },
      error => {
        console.log("getSellerFollowing error: ", error);
      }
    );
  }

  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(ShopProfilePopoverPage);
    popover.present({
      ev: myEvent
    });
  }

  viewMyFollower() {
    this.navCtrl.push(MyfollowerFollowingPage, {
      title: "Followers",
      action: "follower",
      mode: "shop",
      params: this.profile
    });
  }

  viewMyFollowing() {
    this.navCtrl.push(MyfollowerFollowingPage, {
      title: "Following",
      action: "following",
      mode: "shop",
      params: this.profile
    });
  }

  follow() {
    let loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    loading.present();
    this.users
      .followSeller({
        userId: this.profile.uuid
      })
      .then(
        (data: any) => {
          if (data && data.success) {
            this.following = true;
            this.getFollowers();
          } else if (data && !data.success) {
            this.alertCtrl
              .create({
                title: "WARNING",
                message: data.msg,
                buttons: ["OK"]
              })
              .present();
          }
          loading.dismiss();
        },
        error => {
          loading.dismiss();
          console.log("Error: ", error);
        }
      );
  }

  unfollow() {
    let loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    loading.present();
    this.users
      .unfollowSeller({
        userId: this.profile.uuid
      })
      .then(
        (data: any) => {
          if (data && data.success) {
            this.following = false;
            this.getFollowing();
          }
          loading.dismiss();
        },
        error => {
          loading.dismiss();
          console.log("Error: ", error);
        }
      );
  }

  showShopRating() {
    this.app.getRootNav().push(MyratingPage, {
      mode: "buyer",
      profile: this.details
    });
  }

  segmentChanged(ev: any) {
    console.log("this.action: ", this.action);
    let loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    loading.present();

    if (this.action == "popular") {
      this.productsArr = _.orderBy(this.productsArr, "rating", "desc");
    } else if (this.action == "latest") {
      this.productsArr = _.orderBy(
        this.productsArr,
        "date_created_timestamp",
        "asc"
      );
    } else if (this.action == "top") {
      this.productsArr = _.orderBy(this.productsArr, "orderscount", "desc");
    } else if (this.action == "price") {
      if (this.isLow) {
        this.productsArr = _.orderBy(this.productsArr, "productPrice", "asc");
      } else {
        this.productsArr = _.orderBy(this.productsArr, "productPrice", "desc");
      }
    }
    loading.dismiss();
  }

  sortPrice() {
    this.isLow = !this.isLow;
    if (this.isLow) {
      this.zone.run(() => {
        this.productsArr = _.orderBy(this.productsArr, "productPrice", "asc");
      });
    } else {
      this.zone.run(() => {
        this.productsArr = _.orderBy(this.productsArr, "productPrice", "desc");
      });
    }
    console.log("sortPrice: ", this.productsArr);
  }

  viewCategories(item) {
    this.navCtrl.push(SearchresultPage, {
      action: "shop_category",
      item: item
    });
  }
}
