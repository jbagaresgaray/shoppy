import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import * as _ from 'lodash';
import * as async from 'async';

import { ProductsServiceProvider } from '../../providers/service/products';
import { SellerServiceProvider } from '../../providers/service/seller';
import { ServiceProvider } from '../../providers/service/service';

@IonicPage()
@Component({
	selector: 'page-shoppingcart-selectshippingoption',
	templateUrl: 'shoppingcart-selectshippingoption.html',
})
export class ShoppingcartSelectshippingoptionPage {
	shippingArr: any[] = [];
	tmpShippingArr: any[] = [];
	order: any = {};
	currency: any = {};

	callback: any;
	constructor(public navCtrl: NavController, public navParams: NavParams,
		public products: ProductsServiceProvider, public sellers: SellerServiceProvider, public services: ServiceProvider) {

		this.order = navParams.get('order');
		this.tmpShippingArr = navParams.get('tmpShippingArr');
		this.callback = navParams.get('callback');
		this.currency = this.services.defaultCurrency();

		console.log('this.order: ', this.order);
	}

	ionViewWillEnter() {
		this.order = this.navParams.get('order');

		if (!_.isEmpty(this.tmpShippingArr)) {
			_.each(this.tmpShippingArr, (row: any) => {
				if (row.shippingCourierId == this.order.selected_shipping.shippingCourierId) {
					row.selected = true;
					row.storeId = this.order.storeId;
				} else {
					row.selected = false;
				}
			});
		}
		this.shippingArr = this.tmpShippingArr;
		console.log('this.shippingArr: ',this.shippingArr);
	}

	selectItem(item) {
		_.each(this.shippingArr, (row: any) => {
			if (item.shippingCourierId == row.shippingCourierId) {
				row.selected = true;
				row.storeId = this.order.storeId;
				setTimeout(() => {
					this.callback(row).then(() => { this.navCtrl.pop() });
				}, 800);
			} else {
				row.selected = false;
			}
		});
	}
}
