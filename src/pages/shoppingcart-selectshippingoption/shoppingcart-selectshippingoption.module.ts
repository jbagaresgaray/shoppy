import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShoppingcartSelectshippingoptionPage } from './shoppingcart-selectshippingoption';

@NgModule({
  declarations: [
    ShoppingcartSelectshippingoptionPage,
  ],
  imports: [
    IonicPageModule.forChild(ShoppingcartSelectshippingoptionPage),
  ],
})
export class ShoppingcartSelectshippingoptionPageModule {}
