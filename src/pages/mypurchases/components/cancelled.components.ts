import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
	selector: 'cancelled',
	templateUrl: 'cancelled.html',
})
export class CancelledMyPurchasesComponent {
	@Input() showList: boolean;
	@Input() orderList: any[] = [];
	@Input() action: string;

	@Output() _viewOrderDetails = new EventEmitter<any>();
	@Output() _viewCancelReason = new EventEmitter<any>();

	viewOrderDetails(order, action){
		this._viewOrderDetails.emit({
			order: order,
			action: action
		});
	}

	viewCancelReason(order,ev:Event){
		this._viewCancelReason.emit({
			order:order,
			ev: ev
		});
	}
}