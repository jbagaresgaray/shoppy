import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
	selector: 'topay',
	templateUrl: 'topay.html',
})
export class ToPayMyPurchasesComponent {
	@Input() showList: boolean;
	@Input() orderList: any[] = [];

	@Output() _viewOrderDetails = new EventEmitter<any>();
	@Output() _contactSeller = new EventEmitter<any>();

	viewOrderDetails(order, action){
		console.log('action: ',action);
		console.log('order: ',order);
		this._viewOrderDetails.emit({
			order: order,
			action: action
		});
	}

	contactSeller(ev){
		this._contactSeller.emit(ev);
	}
}