import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
	selector: 'receive',
	templateUrl: 'receive.html',
})
export class ReceivedMyPurchasesComponent {
	// console.log('ReceivedMyPurchasesComponent');

	@Input() showList: boolean;
	@Input() orderList: any[] = [];

	@Output() _viewOrderDetails = new EventEmitter<any>();
	@Output() _contactSeller = new EventEmitter<any>();
	@Output() _orderReceived = new EventEmitter<any>();
	@Output() _orderReturnRefund = new EventEmitter<any>();

	viewOrderDetails(order, action){
		this._viewOrderDetails.emit({
			order: order,
			action: action
		});
	}

	contactSeller(ev){
		this._contactSeller.emit(ev);
	}

	orderReceived(order,ev:Event){
		console.log('orderReceived');
		this._orderReceived.emit({
			order: order,
			ev: ev
		});
	}

	orderReturnRefund(order,ev:Event){
		this._orderReturnRefund.emit({
			order: order,
			ev: ev
		});
	}
}