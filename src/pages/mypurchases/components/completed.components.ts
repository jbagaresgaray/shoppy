import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
	selector: 'completed',
	templateUrl: 'completed.html',
})
export class CompletedMyPurchasesComponent {
	@Input() showList: boolean;
	@Input() orderList: any[] = [];
	@Input() action: string;

	@Output() _viewOrderDetails = new EventEmitter<any>();
	@Output() _contactSeller = new EventEmitter<any>();

	viewOrderDetails(order, action){
		this._viewOrderDetails.emit({
			order: order,
			action: action
		});
	}

	contactSeller(ev){
		this._contactSeller.emit(ev);
	}
}