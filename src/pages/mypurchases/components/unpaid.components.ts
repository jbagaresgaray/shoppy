import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
	selector: 'unpaid',
	templateUrl: 'unpaid.html',
})
export class UnpaidMyPurchasesComponent {
	@Input() showList: boolean;
	@Input() orderList: any[] = [];

	@Output() _viewOrderDetails = new EventEmitter<any>();
	@Output() _contactSeller = new EventEmitter<any>();

	viewOrderDetails(order, action){
		this._viewOrderDetails.emit({
			order: order,
			action: action
		});
	}

	contactSeller(ev){
		this._contactSeller.emit(ev);
	}
}