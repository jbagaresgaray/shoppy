import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
	selector: 'toship',
	templateUrl: 'toship.html',
})
export class ToShipMyPurchasesComponent {
	@Input() showList: boolean;
	@Input() orderList: any[] = [];

	@Output() _viewOrderDetails = new EventEmitter<any>();
	@Output() _contactSeller = new EventEmitter<any>();

	viewOrderDetails(order, action){
		this._viewOrderDetails.emit({
			order: order, 
			action: action
		});
	}

	contactSeller(ev){
		this._contactSeller.emit(ev);
	}
}

@Component({
	selector: 'toship-selller',
	templateUrl: 'toshipseller.html',
})
export class ToShipSellerMyPurchasesComponent {
	@Input() showList: boolean;
	@Input() orderList: any[] = [];

	@Output() _viewOrderDetails = new EventEmitter<any>();
	@Output() _arrangeShipment = new EventEmitter<any>();
	@Output() _viewPickupDetails = new EventEmitter<any>();

	viewOrderDetails(order, action){
		this._viewOrderDetails.emit({
			order: order, 
			action: action
		});
	}

	arrangeShipment(item,ev:Event){
		this._arrangeShipment.emit({
			item: item,
			ev: ev
		});
	}

	viewPickupDetails(item, ev: Event){
		this._viewPickupDetails.emit({
			item: item,
			ev: ev
		});
	}
}