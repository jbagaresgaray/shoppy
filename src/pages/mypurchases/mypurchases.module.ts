import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NgPipesModule } from 'ngx-pipes';

import { MypurchasesPage } from './mypurchases';
import { UnpaidMyPurchasesComponent } from './components/unpaid.components';
import { ToPayMyPurchasesComponent } from './components/topay.components';
import { ToShipMyPurchasesComponent, ToShipSellerMyPurchasesComponent } from './components/toship.components';
import { ShippingMyPurchasesComponent } from './components/shipping.components';
import { ReceivedMyPurchasesComponent } from './components/receive.components';
import { CompletedMyPurchasesComponent } from './components/completed.components';
import { CancelledMyPurchasesComponent } from './components/cancelled.components';
import { ReturnMyPurchasesComponent } from './components/return.components';


@NgModule({
	declarations: [
		MypurchasesPage,
		UnpaidMyPurchasesComponent,
		ToPayMyPurchasesComponent,
		ToShipMyPurchasesComponent, ToShipSellerMyPurchasesComponent,
		ShippingMyPurchasesComponent,
		ReceivedMyPurchasesComponent,
		CompletedMyPurchasesComponent,
		CancelledMyPurchasesComponent,
		ReturnMyPurchasesComponent
	],
	imports: [
		NgPipesModule,
		IonicPageModule.forChild(MypurchasesPage),
	],
	entryComponents: [
		UnpaidMyPurchasesComponent,
		ToPayMyPurchasesComponent,
		ToShipMyPurchasesComponent, ToShipSellerMyPurchasesComponent,
		ShippingMyPurchasesComponent,
		ReceivedMyPurchasesComponent,
		CompletedMyPurchasesComponent,
		CancelledMyPurchasesComponent,
		ReturnMyPurchasesComponent
	]
})
export class MypurchasesPageModule { }
