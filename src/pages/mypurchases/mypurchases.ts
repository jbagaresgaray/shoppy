import { Component, ViewChild } from "@angular/core";
import {
	IonicPage,
	NavController,
	NavParams,
	ModalController,
	Slides,
	LoadingController,
	ActionSheetController,
	Events,
	Platform,
	AlertController
} from "ionic-angular";
import * as _ from "lodash";

import { ChatPage } from "../../pages/chat/chat";
import { MakepaymentPage } from "../../pages/shoppingcart-makepayment/makepayment";
import { ChatdetailPage } from "../../pages/chatdetail/chatdetail";
import { OrderdetailsPage } from "../../pages/orderdetails/orderdetails";
import { ProductratingsEntryPage } from "../../pages/productratings-entry/productratings-entry";
import { AllFormEntryPage } from "../../pages/all-form-entry/all-form-entry";

import { OrdersServiceProvider } from "../../providers/service/orders";
import { ChatService } from "../../providers/service/chat";
import { ServiceProvider } from "../../providers/service/service";

declare let safari: any;

@IonicPage()
@Component({
	selector: "page-mypurchases",
	templateUrl: "mypurchases.html"
})
export class MypurchasesPage {
	user: any = {};
	@ViewChild("mySlider") slider: Slides;
	selectedSegment: string;
	slides: any;
	action: any;
	apptitle: string;
	currency: any = {};
	orderList: any[] = [];

	showList: boolean = false;
	isSafari: boolean = false;
	unreadCount: number = 0;

	constructor(
		public platform: Platform,
		public navCtrl: NavController,
		public navParams: NavParams,
		public modalCtrl: ModalController,
		public loadingCtrl: LoadingController,
		public actionSheetCtrl: ActionSheetController,
		public events: Events,
		public alertCtrl: AlertController,
		public orders: OrdersServiceProvider,
		public chatService: ChatService,
		public services: ServiceProvider
	) {
		this.action = this.navParams.get("action");
		this.currency = this.services.defaultCurrency();

		this.isSafari = /constructor/i.test(window["HTMLElement"]) || (function(p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window["safari"] || safari.pushNotification);
		this.user = JSON.parse(localStorage.getItem("app.user")) || {};

		if (this.action == "buyer") {
			this.apptitle = "My Purchases";
			this.slides = [
				{
					id: "pay"
				},
				{
					id: "ship"
				},
				{
					id: "receive"
				},
				{
					id: "completed"
				},
				{
					id: "cancelled"
				},
				{
					id: "return"
				}
			];
		} else if (this.action == "seller") {
			this.apptitle = "My Sales";
			this.slides = [
				{
					id: "unpaid"
				},
				{
					id: "ship"
				},
				{
					id: "shipping"
				},
				{
					id: "receive"
				},
				{
					id: "completed"
				},
				{
					id: "cancelled"
				},
				{
					id: "return"
				}
			];
		}

		// ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
		// ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
		// ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
		events.unsubscribe("onMessageReceived");
		events.unsubscribe("onMessageUpdated");
		events.unsubscribe("getTotalUnreadMessageCount");

		events.subscribe("onMessageReceived", () => {
			this.unreadCount = this.chatService.unreadCount;
		});

		events.subscribe("onMessageUpdated", () => {
			this.unreadCount = this.chatService.unreadCount;
		});

		events.subscribe("getTotalUnreadMessageCount", () => {
			this.unreadCount = this.chatService.unreadCount;
		});
	}

	doRefresh(ev) {
		this.getOrderList(ev);
	}

	getOrderList(ev?: any) {
		let loading = this.loadingCtrl.create();
		if (_.isEmpty(ev)) {
			loading.present();
		}
		this.orderList = [];
		this.showList = false;
		console.log("this.action: ", this.action);
		this.orders.getOrderList(this.selectedSegment, this.action).then(
			(data: any) => {
				if (data && data.success) {
					this.orderList = data.result;
					_.each(this.orderList, (row: any) => {
						if (this.platform.is("ios")) {
							if (row.shippingDeadline) {
								if (this.isSafari) {
									row.shippingDeadline = new Date(
										row.shippingDeadline.replace(/-/g, "/")
									);
								} else {
									row.shippingDeadline = new Date(
										row.shippingDeadline.replace(/-/g, "/")
									);
								}
							}

							if (row.deliveryDeadline) {
								if (this.isSafari) {
									row.deliveryDeadline = new Date(
										row.deliveryDeadline.replace(/-/g, "/")
									);
								} else {
									row.deliveryDeadline = new Date(
										row.deliveryDeadline.replace(/-/g, "/")
									);
								}
							}
						}

						if (row.checkpoint) {
							row.isOrderDelivered = row.checkpoint.tag == "Delivered" ? true : false;
						} else {
							row.isOrderDelivered = false;
						}

						row.isOrderReceived = row.isReceived == 1 || !_.isEmpty(row.receivedDateTime) ? true : false;

						if (!_.isEmpty(row.cancelledRequested)) {
							row.isOrderCancelRequested = true;
							row.isOrderCancelWithdraw = false;
							row.isOrderCancelRejected = false;

							if (
								row.isWithdrawCancelled == 1 &&
								row.isCancelled == 0
							) {
								row.isOrderCancelWithdraw = true;
								row.isOrderCancelled = false;
							} else if (
								row.isRejectCancelled == 1 &&
								row.isCancelled == 0
							) {
								row.isOrderCancelRejected = true;
								row.isOrderCancelled = false;
							} else {
								if (row.isCancelled == 1 && !_.isEmpty(row.cancelledDateTime)) {
									row.isOrderCancelled = true;
								} else {
									row.isOrderCancelled = false;
								}
							}
						} else {
							row.isOrderCancelRequested = false;
							row.isOrderCancelWithdraw = false;
							row.isOrderCancelRejected = false;
							row.isOrderCancelled = false;
						}

						if (
							row.order_return_info &&
							!_.isNull(row.order_return_info)
						) {
							row.isOrderReturnRequest = true;

							if (row.order_return_info.isReturnRequestAccepted == 1 || !_.isEmpty(row.order_return_info.returnRequestedAccepted)) {
								row.isOrderReturnRequestAccepted = true;
							}

							if (!_.isEmpty(row.order_return_info.cancelReturnRequest)) {
								row.isOrderReturnRequestCancelled = true;
							}

							if (row.order_return_info.isReturned == 1 || !_.isEmpty(row.order_return_info.returnedDateTime)) {
								row.isOrderReturned = true;
							} else {
								row.isOrderReturned = false;
							}
						} else {
							row.isOrderReturnRequest = false;
							row.isOrderReturned = false;
							row.isOrderReturnDispute = false;
							row.isOrderReturnRequestCancelled = false;
							row.isOrderReturnRequestAccepted = false;
						}

						if (row.orderAmt) {
							row.orderAmt = this.currency.symbol + " " + row.orderAmt.toFixed(this.currency.decimal_digits);
						}

						if (row.shippingFee) {
							row.shippingFee = this.currency.symbol + " " + row.shippingFee.toFixed(this.currency.decimal_digits);
						}

						_.each(row.details, (rows: any) => {
							rows.subTotal = rows.detailPrice * rows.detailQty;

							if (row.detailPrice) {
								rows.detailPrice = this.currency.symbol + " " + rows.detailPrice.toFixed(this.currency.decimal_digits);
							}
							if (row.subTotal) {
								rows.subTotal = this.currency.symbol + " " + rows.subTotal.toFixed(this.currency.decimal_digits);
							}
						});
					});
					console.log("this.orderList: ", this.orderList);
				}

				if (!_.isEmpty(this.orderList)) {
					this.showList = true;
				} else {
					this.showList = false;
				}
				loading.dismiss();
				if (ev) {
					ev.complete();
				}
			},
			error => {
				loading.dismiss();
				if (ev) {
					ev.complete();
				}
			}
		);
	}

	ionViewDidLoad() {
		console.log("ionViewDidLoad MypurchasesPage");
		this.unreadCount = this.chatService.unreadCount;
	}

	ionViewDidEnter() {
		console.log("ionViewDidEnter MypurchasesPage");

		if (this.action == "seller") {
			if (this.navParams.get("mode")) {
				this.selectedSegment = this.navParams.get("mode");
				this.onSegmentChanged({
					value: this.selectedSegment
				});
			} else {
				this.selectedSegment = "unpaid";
			}
		} else {
			if (this.navParams.get("mode")) {
				this.selectedSegment = this.navParams.get("mode");
				this.onSegmentChanged({
					value: this.selectedSegment
				});
			} else {
				this.selectedSegment = "pay";
			}
		}
		console.log("this.selectedSegment: ", this.selectedSegment);
	}

	ionViewWillEnter() {
		this.isSafari =
			/constructor/i.test(window["HTMLElement"]) ||
			(function(p) {
				return p.toString() === "[object SafariRemoteNotification]";
			})(!window["safari"] || safari.pushNotification);
		// this.getOrderList();
	}

	private getOrderCallback = resp => {
		return new Promise((resolve, reject) => {
			if (resp == "save") {
				this.getOrderList();
				resolve();
			}
		});
	};

	onSegmentChanged(segmentButton) {
		console.log("segmentButton: ", segmentButton.value);
		const selectedIndex = this.slides.findIndex(slide => {
			return slide.id == segmentButton.value;
		});
		console.log("selectedIndex: ", selectedIndex);
		this.slider.slideTo(selectedIndex);
		this.getOrderList();
	}

	onSlideChanged(slider) {
		console.log("onSlideChanged: ", slider.getActiveIndex());
		const currentSlide = this.slides[slider.getActiveIndex()];
		console.log("currentSlide: ", currentSlide);
		if (currentSlide) {
			this.selectedSegment = currentSlide.id;
		}
	}

	viewChat() {
		let modal = this.modalCtrl.create(ChatPage);
		modal.present();
	}

	payNow($event) {
		$event.stopPropagation();
		$event.preventDefault();
		this.navCtrl.push(MakepaymentPage);
	}

	contactSeller($event) {
		$event.stopPropagation();
		$event.preventDefault();
		this.navCtrl.push(ChatdetailPage, {
			user: {
				Id: "210000198410281948",
				name: "Cassio Zen",
				email: "cassiozen@gmail.com"
			}
		});
	}

	viewOrderDetails(order, action) {
		console.log("action: ", action);
		this.navCtrl.push(OrderdetailsPage, {
			orderId: order.orderId,
			status: action,
			usertype: this.action
		});
	}

	orderReceived(order, $event) {
		console.log("orderReceived 2:");
		$event.stopPropagation();
		$event.preventDefault();

		let vm = this;

		function orderReceived() {
			let loading = vm.loadingCtrl.create({
				dismissOnPageChange: true
			});
			loading.present();

			let orderReceviedObj: any = {
				receivedDateTime: new Date()
			};

			if (order.isCOD == 1) {
				orderReceviedObj.isPaid = 1;
				orderReceviedObj.paymentDateTime = new Date();
			}

			vm.orders.receivedOrder(order.orderId, orderReceviedObj).then(
				(data: any) => {
					if (data && data.success) {
						vm.getOrderList();
					} else {
						let alert = vm.alertCtrl.create({
							title: "Warning!",
							subTitle: data.msg,
							buttons: ["OK"]
						});
						alert.onDidDismiss(() => {
							vm.getOrderList();
						});
						alert.present();
					}
					loading.dismiss();
				},
				error => {
					loading.dismiss();
				}
			);
		}

		const confirm = this.alertCtrl.create({
			title: "Release " + order.orderAmt + " to seller",
			message:
				"I confirm I have received the products and accept their condition. There is no refund or return after I confirm?",
			buttons: [
				{
					text: "Not Now",
					handler: () => {
						console.log("Disagree clicked");
					}
				},
				{
					text: "Confirm",
					handler: () => {
						orderReceived();
					}
				}
			]
		});
		confirm.present();
	}

	orderReturnRefund(order, event) {
		event.preventDefault();
		event.stopPropagation();
		this.navCtrl.push(AllFormEntryPage, {
			action: "return",
			orders: order,
			callback: this.getOrderCallback
		});
	}

	viewCancelReason(order, ev) {
		ev.preventDefault();
		ev.stopPropagation();
		this.navCtrl.push(AllFormEntryPage, {
			orders: order,
			action: "cancel_view",
			usertype: this.action
		});
	}

	rateProduct(item, type, event) {
		event.preventDefault();
		event.stopPropagation();

		this.navCtrl.push(ProductratingsEntryPage, {
			item: item,
			type: type
		});
	}

	arrangeShipment(item, event) {
		console.log("arrangeShipment: ", item);
		event.preventDefault();
		event.stopPropagation();

		const loading = this.loadingCtrl.create({
			dismissOnPageChange: true
		});
		loading.present();
		this.orders.isOrderArrangeShipment(item.orderId, item.slug).then(
			(data: any) => {
				if (data && data.success) {
					loading.dismiss().then(() => {
						this.navCtrl.push(AllFormEntryPage, {
							action: "shipping",
							orders: item,
							callback: this.getOrderCallback
						});
					});
				} else if (data && !data.success) {
					loading.dismiss().then(() => {
						this.alertCtrl
							.create({
								title: "WARNING",
								message: data.msg,
								buttons: ["OK"]
							})
							.present();
					});
					return;
				}
			},
			(error: any) => {
				console.log("isOrderArrangeShipment err: ", error);
				loading.dismiss();
			}
		);
	}
}
