import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HelpCentrePage } from './help-centre';

@NgModule({
  declarations: [
    HelpCentrePage,
  ],
  imports: [
    IonicPageModule.forChild(HelpCentrePage),
  ],
})
export class HelpCentrePageModule {}
