import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { DomSanitizer } from '@angular/platform-browser';

import { environment } from '@app/env';

@IonicPage()
@Component({
	selector: 'page-help-centre',
	templateUrl: 'help-centre.html',
})
export class HelpCentrePage {
	helpcenter: any;
	base_url: string = environment.help_url;

	constructor(public navCtrl: NavController, public navParams: NavParams, private sanitizer: DomSanitizer, public http: HttpClient, public loadingCtrl: LoadingController) {
	}

	ionViewDidLoad() {
		/*console.log('ionViewDidLoad HelpCentrePage');
		let loading = this.loadingCtrl.create({
			dismissOnPageChange: true
		});
		loading.present();
		this.http.get(this.base_url, { responseType: 'text' as 'text' })
			.subscribe((html: any) => {
			console.log('html: ',html);
			if(html){
				this.helpcenter = this.sanitizer.bypassSecurityTrustHtml(html);
			}
			loading.dismiss();
		}, (error: any) => {
			console.log('error: ', error);
			loading.dismiss();
		});*/
	}

	dismiss(){
		this.navCtrl.pop();
	}

	urlpaste() {
		console.log('this.base_url: ',this.base_url);
		let url = this.sanitizer.bypassSecurityTrustResourceUrl(this.base_url);
		console.log('url: ',url);
		return url;
	}

}
