import { Component } from '@angular/core';
import { LoadingController, Platform } from 'ionic-angular';
import { AppVersion } from '@ionic-native/app-version';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  appInfo: any = {};
  constructor(public platform: Platform, public loadingCtrl: LoadingController,
    private appVersion: AppVersion) {

    platform.ready().then(() => {
      if (platform.is('cordova')) {
        this.appVersion.getAppName().then((appName:string)=>{
            this.appInfo.AppName = appName;
        });
        this.appVersion.getVersionNumber().then((versionCode:string)=>{
          this.appInfo.VersionNumber = versionCode;
        });
      }else{
         this.appInfo = {
          AppName: 'ShoppyApp',
          VersionNumber: '0.0.0-dev',
        }
      }
    });
  }


}
