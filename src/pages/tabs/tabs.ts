import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, App, ModalController, Events, ViewController } from 'ionic-angular';
import * as _ from 'lodash';
// import { Socket } from 'ng-socket-io';

import { HomePage } from '../home/home';
import { FeedPage } from '../feed/feed';
import { MyprofilePage } from '../myprofile/myprofile';
import { NotificationPage } from '../notification/notification';
import { MylikesPage } from '../mylikes/mylikes';

import { NotificationsServiceProvider } from '../../providers/service/notifications';

@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = FeedPage;
  tabHeart = MylikesPage;
  tab4Root = NotificationPage;
  tab5Root = MyprofilePage;

  user: any = {};
  mapTabEnabled: boolean = true;
  notifCount: number = 0;

  notification: any[] = [];
  notif: any = {};

  constructor(public navCtrl: NavController, public navParams: NavParams, public app: App, public modalCtrl: ModalController, public viewCtrl: ViewController, public events: Events, public zone: NgZone,
    public notifications: NotificationsServiceProvider) {
    this.user = JSON.parse(localStorage.getItem('app.user')) || {};
    if (_.isEmpty(this.user)) {
      this.mapTabEnabled = false;
    } else {
      this.mapTabEnabled = true;
      this.loadNotifications();
    }

    this.events.subscribe('app.login', () => {
      this.mapTabEnabled = true;
    });

    this.events.subscribe('approve-seller', () => {
      console.log('approve-seller');
      this.notifCount = this.notifCount + 1;
    });

    this.notif = {
      promotionCount: 0,
      appupdatesCount: 0,
      sellerupdatesCount: 0,
      walletupdatesCount: 0,
      orderupdatesCount: 0
    };

    events.unsubscribe('reloadAppNotifications');
    events.subscribe('reloadAppNotifications', () => {
      console.log('reloadAppNotifications: ');
      this.loadNotifications();
    });
  }

  myMethod($event) {
    this.user = JSON.parse(localStorage.getItem('app.user')) || {};
    if (_.isEmpty(this.user)) {
      this.mapTabEnabled = false;
    } else {
      this.mapTabEnabled = true;
    }
  }

  ionViewWillEnter() {
    this.viewCtrl.showBackButton(false);
  }

  loadNotifications() {
    this.notifications.getUpdatesNotifications().then((data: any) => {
      if (data && data.success) {
        this.notification = data.result;

        let promotions = _.filter(this.notification, { 'notifTypeId': 1, 'isRead': 0 });
        let sellerupdates = _.filter(this.notification, { 'notifTypeId': 2, 'isRead': 0 });
        let walletupdates = _.filter(this.notification, { 'notifTypeId': 3, 'isRead': 0 });
        let appupdates = _.filter(this.notification, { 'notifTypeId': 4, 'isRead': 0 });
        let orderupdates = _.filter(this.notification, { 'notifTypeId': 5, 'isRead': 0 });
        let sellerupdates2 = _.filter(this.notification, { 'notifTypeId': 6, 'isRead': 0 });

        this.notif = {
          promotionCount: _.size(promotions),
          appupdatesCount: _.size(appupdates),
          sellerupdatesCount: _.size(sellerupdates) + _.size(sellerupdates2),
          walletupdatesCount: _.size(walletupdates),
          orderupdatesCount: _.size(orderupdates)
        };

        this.zone.run(() => {
          this.notifCount = this.notif.promotionCount + this.notif.appupdatesCount + this.notif.sellerupdatesCount + this.notif.walletupdatesCount + this.notif.orderupdatesCount;
        });
        console.log('notifCount: ', this.notifCount);
      }
    }, (error) => {
      console.log('notification err: ', error);
    });
  }

}
