import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProductShippingEntryPage } from './product-shipping-entry';

@NgModule({
  declarations: [
    ProductShippingEntryPage,
  ],
  imports: [
    IonicPageModule.forChild(ProductShippingEntryPage),
  ],
})
export class ProductShippingEntryPageModule {}
