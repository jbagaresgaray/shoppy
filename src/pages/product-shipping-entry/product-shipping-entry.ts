import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import * as _ from 'lodash';

import { ServiceProvider } from '../../providers/service/service';

@IonicPage()
@Component({
  selector: 'page-product-shipping-entry',
  templateUrl: 'product-shipping-entry.html',
})
export class ProductShippingEntryPage {
  shippingArr: any[] = [];
  selectedArr: any[] = [];
  callback: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public services: ServiceProvider) {
    if (this.navParams.get('shippingArr')) {
      this.selectedArr = this.navParams.get('shippingArr');
      this.callback = this.navParams.get('callback');
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductShippingEntryPage');
    this.services.getAllShippingCouriers().then((data: any) => {
      if (data && data.success) {
        console.log('data: ', data);
        this.shippingArr = data.result;
        _.each(this.shippingArr, (row: any) => {
          row.enable = false;
          row.isCover = false;
        });
      }
    }, (error) => {
      console.log('error: ', error);
    });
  }

  saveProduct() {
    _.each(this.shippingArr, (row: any) => {
      if (row.enable) {
        this.selectedArr.push(row);
      }
    });
    this.callback(this.selectedArr).then(() => { this.navCtrl.pop() });
  }

  doRefresh(ev) {
    this.services.getAllShippingCouriers().then((data: any) => {
      if (data && data.success) {
        this.shippingArr = data.result;
        _.each(this.shippingArr, (row: any) => {
          row.enable = false;
          row.isCover = false;
        });
        ev.complete();
      }
    }, (error) => {
      console.log('error: ', error);
      ev.complete();
    });
  }

}
