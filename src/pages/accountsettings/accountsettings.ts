import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ActionSheetController, LoadingController, Events, AlertController, Platform } from 'ionic-angular';
import { AuthService } from "angular4-social-login";
// import { Socket } from 'ng-socket-io';
import * as _ from 'lodash';
import * as async from 'async';
import * as SendBird from 'SendBird';

import { Device } from '@ionic-native/device';
import { OneSignal } from '@ionic-native/onesignal';
import { Storage } from '@ionic/storage';

import { environment } from '@app/env';

import { ChatPage } from '../../pages/chat/chat';
import { MyaddressPage } from '../../pages/myaddress/myaddress';
import { UserprofilePage } from '../../pages/userprofile/userprofile';
import { MybankcardsPage } from '../../pages/mybankcards/mybankcards';
import { AboutPage } from '../../pages/about/about';
import { CountryCurrencyPage } from '../../pages/country-currency/country-currency';
import { HelpCentrePage } from '../../pages/help-centre/help-centre';

import { TabsPage } from '../../pages/tabs/tabs';
import { ContentFormPage } from '../../pages/content-form/content-form';
import { ShopprofileSettingsPage } from '../../pages/shopprofile-settings/shopprofile-settings';

import { UsersServiceProvider } from '../../providers/service/users';
import { ServiceProvider } from '../../providers/service/service';
import { ChatService } from '../../providers/service/chat';
import { AuthProvider } from '../../providers/service/authentication';

@IonicPage()
@Component({
  selector: 'page-accountsettings',
  templateUrl: 'accountsettings.html',
})
export class AccountsettingsPage {

  user: any = {};
  country: any = {};
  currency: any = {};
  sendBird: any;

  unreadCount: number = 0;
  user_player_ids: string;

  constructor(public platform: Platform, public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController, public actionSheetCtrl: ActionSheetController, public loadingCtrl: LoadingController, public events: Events, public alertCtrl: AlertController,
    public users: UsersServiceProvider, public services: ServiceProvider, private authService: AuthService, public chatService: ChatService,
    private device: Device, private oneSignal: OneSignal, public auth: AuthProvider,public storage: Storage) {

    this.sendBird = new SendBird({
      appId: environment.SendBird
    });

    // ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
    // ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
    // ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
    events.unsubscribe('onMessageReceived');
    events.unsubscribe('onMessageUpdated');
    events.unsubscribe('getTotalUnreadMessageCount');

    events.subscribe('onMessageReceived', () => {
      this.unreadCount = this.chatService.unreadCount;
    });

    events.subscribe('onMessageUpdated', () => {
      this.unreadCount = this.chatService.unreadCount;
    });

    events.subscribe('getTotalUnreadMessageCount', () => {
      this.unreadCount = this.chatService.unreadCount;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AccountsettingsPage');
    this.user = JSON.parse(localStorage.getItem('app.user')) || {};

    this.loadingDeviceId();
  }

  ionViewWillEnter() {
    this.user = JSON.parse(localStorage.getItem('app.user')) || {};

    this.loadingDeviceId();

    if (!_.isEmpty(this.user)) {
      this.users.getCurrentUser().then((data: any) => {
        if (data && data.success) {
          this.user = data.result;
          console.log('this.user: ', this.user);
        }
      }).then(() => {
        async.parallel([
          (callback) => {
            this.services.getRestCountries().then((data: any) => {
              if (data) {
                this.country = _.find(data, { 'alpha3Code': this.user.countryId }) || {};
              }
              callback();
            }, (error) => {
              console.log('getRestCountries: ', error);
            });
          },
          (callback) => {
            this.services.getCurrencies().then((data: any) => {
              if (data && data.success) {
                this.currency = _.find(data.result, { 'code': this.user.currencyId }) || {};
                console.log('currency: ', this.currency);
              }
              callback();
            }, (error) => {
              console.log('getRestCountries: ', error);
            });
          }
        ]);
      }, (error) => {
        console.log('error: ', error);
      });
    }
    this.unreadCount = this.chatService.unreadCount;
  }

  loadingDeviceId() {
    this.platform.ready().then(() => {
      if (window['cordova']) {
        this.oneSignal.getIds().then((data: any) => {
          console.log('getIds: ', data);
          this.user_player_ids = data.userId;
        });
      }
    });
  }

  viewChat() {
    let modal = this.modalCtrl.create(ChatPage);
    modal.present();
  }

  viewMyAddressess() {
    this.navCtrl.push(MyaddressPage);
  }

  viewProfile() {
    this.navCtrl.push(UserprofilePage);
  }

  viewBankCards() {
    this.navCtrl.push(MybankcardsPage);
  }

  viewAbout() {
    this.navCtrl.push(AboutPage);
  }

  viewHelpCentre() {
    this.navCtrl.push(HelpCentrePage);
  }

  viewDetails(type) {
    this.navCtrl.push(CountryCurrencyPage, { type: type });
  }

  viewPolicies(action) {
    let title: string;

    if (action == 'terms') {
      title = 'Terms of Services';
    } else if (action == 'privacy') {
      title = 'Privacy Policy';
    } else if (action == 'items_policy') {
      title = 'Prohibited and Restricted Items Policy';
    } else if (action == 'return_policy') {
      title = 'Return and Refunds Policy';
    }

    this.navCtrl.push(ContentFormPage, {
      action: action,
      title: title
    });
  }

  viewNotificationSettings() {
    this.navCtrl.push(ShopprofileSettingsPage, {
      action: 'notification'
    });
  }

  viewBlockUsers() {
    this.navCtrl.push(ContentFormPage, {
      action: 'block'
    });
  }

  requestForDelete() {
    const confirm = this.alertCtrl.create({
      message: 'We are sad that you want to leave us, but please note that account deletion is irreversable.',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'OK',
          handler: () => {
            console.log('Agree clicked');
          }
        }
      ]
    });
    confirm.present();
  }

  private logoutApp() {
    let loader = this.loadingCtrl.create();
    loader.onDidDismiss(() => {
      this.navCtrl.setRoot(TabsPage, null, { animate: true, direction: 'back' });
    });
    loader.present();

    this.auth.logoutApp({
      player_id: this.user_player_ids,
      device_uuid: this.device.uuid
    }).then((data: any) => {
      if (data && data.success) {
        localStorage.removeItem('app.user');
        localStorage.removeItem('app.token');

        this.storage.remove('search');
        this.storage.remove('app.cart');

        this.sendBird.disconnect(function() {
          console.log('You are disconnected from SendBird.: ');
          loader.dismiss();
        });
      }
    }, (error: any) => {
      console.log('Error: ', error);;
    })
  }

  logout() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Your profile will be saved. Can\'t wait to have you back',
      buttons: [
        {
          text: 'Logout',
          role: 'destructive',
          handler: () => {
            this.logoutApp();
          }
        }, {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

}
