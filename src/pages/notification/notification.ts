import { Component } from "@angular/core";
import {
	IonicPage,
	NavController,
	NavParams,
	ModalController,
	App,
	Events,
	Platform
} from "ionic-angular";
import * as _ from "lodash";
import * as async from "async";
// import { Socket } from 'ng-socket-io';
import * as SendBird from "SendBird";
import { OneSignal } from "@ionic-native/onesignal";

import { environment } from "@app/env";

import { LoginPage } from "../../pages/login/login";
import { SignupPage } from "../../pages/signup/signup";
import { ShoppingcartPage } from "../../pages/shoppingcart/shoppingcart";
import { ChatPage } from "../../pages/chat/chat";
import { ContentFormPage } from "../../pages/content-form/content-form";
import { OrderdetailsPage } from "../../pages/orderdetails/orderdetails";

import { ShoppingCartProvider } from "../../providers/service/shopping-cart";
import { UsersServiceProvider } from "../../providers/service/users";
import { NotificationsServiceProvider } from "../../providers/service/notifications";
import { ChatService } from "../../providers/service/chat";

@IonicPage()
@Component({
	selector: "page-notification",
	templateUrl: "notification.html"
})
export class NotificationPage {
	user: any = {};
	mapTabEnabled: boolean = true;
	showLoading: boolean = true;
	cartCount: number;
	unreadCount: number = 0;
	notification: any[] = [];
	notif: any = {};
	user_player_ids: string;

	constructor(
		public platform: Platform,
		public app: App,
		public navCtrl: NavController,
		public navParams: NavParams,
		public modalCtrl: ModalController,
		public shoppingCart: ShoppingCartProvider,
		public users: UsersServiceProvider,
		public notifications: NotificationsServiceProvider,
		public events: Events,
		public chatService: ChatService,
		private oneSignal: OneSignal
	) {
		this.user = JSON.parse(localStorage.getItem("app.user")) || {};
		if (_.isEmpty(this.user)) {
			this.mapTabEnabled = false;
		} else {
			this.mapTabEnabled = true;
		}

		this.shoppingCart.getCart().then((data: any) => {
			this.cartCount = !_.isEmpty(data) ? data.length : 0;
		});

		this.notif = {
			promotions: [],
			promotionCount: 0,
			appupdates: [],
			appupdatesCount: 0,
			sellerupdates: [],
			sellerupdatesCount: 0,
			walletupdates: [],
			walletupdatesCount: 0,
			orderupdates: [],
			orderupdatesCount: 0
		};

		// ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
		// ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
		// ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //

		events.unsubscribe("update:cart");
		events.unsubscribe("onMessageReceived");
		events.unsubscribe("onMessageUpdated");
		events.unsubscribe("getTotalUnreadMessageCount");
		events.unsubscribe("reloadAppNotifications");

		events.subscribe("update:cart", () => {
			this.cartCount = this.shoppingCart.cartCount();
		});

		events.subscribe("onMessageReceived", () => {
			this.unreadCount = this.chatService.unreadCount;
		});

		events.subscribe("onMessageUpdated", () => {
			this.unreadCount = this.chatService.unreadCount;
		});

		events.subscribe("getTotalUnreadMessageCount", () => {
			this.unreadCount = this.chatService.unreadCount;
		});

		events.subscribe("reloadAppNotifications", () => {
			console.log("reloadAppNotifications");
			this.initData();
		});

		platform.ready().then(() => {
			if (platform.is("cordova")) {
				this.oneSignal.getIds().then((data: any) => {
					console.log("getIds: ", data);
					this.user_player_ids = data.userId;
				});
			}
		});
	}

	initData(ev?: any) {
		console.log("initData");
		this.user = JSON.parse(localStorage.getItem("app.user")) || {};
		this.showLoading = true;
		if (_.isEmpty(this.user)) {
			this.mapTabEnabled = false;
			if (ev) {
				ev.complete();
			}
		} else {
			this.mapTabEnabled = true;
			this.notifications
				.getUpdatesNotifications()
				.then(
					(data: any) => {
						if (data && data.success) {
							this.notif = {
								promotions: [],
								promotionCount: 0,
								appupdates: [],
								appupdatesCount: 0,
								sellerupdates: [],
								sellerupdatesCount: 0,
								walletupdates: [],
								walletupdatesCount: 0,
								orderupdates: [],
								orderupdatesCount: 0
							};

							this.notification = data.result;

							let promotions = _.filter(this.notification, {
								notifTypeId: 1,
								isRead: 0
							});
							let sellerupdates = _.filter(this.notification, {
								notifTypeId: 2,
								isRead: 0
							});
							let walletupdates = _.filter(this.notification, {
								notifTypeId: 3,
								isRead: 0
							});
							let appupdates = _.filter(this.notification, {
								notifTypeId: 4,
								isRead: 0
							});
							let orderupdates = _.filter(this.notification, {
								notifTypeId: 5,
								isRead: 0
							});
							let sellerupdates2 = _.filter(this.notification, {
								notifTypeId: 6,
								isRead: 0
							});

							let newsellerupdates: any[] = [];
							_.each(sellerupdates, (row: any) => {
								newsellerupdates.push(row);
							});
							_.each(sellerupdates2, (row: any) => {
								newsellerupdates.push(row);
							});

							newsellerupdates = _.orderBy(
								newsellerupdates,
								["datetime"],
								["desc"]
							);
							orderupdates = _.orderBy(
								orderupdates,
								["datetime"],
								["desc"]
							);

							this.notif = {
								promotions: promotions,
								promotionCount: _.size(promotions),
								appupdates: appupdates,
								appupdatesCount: _.size(appupdates),
								sellerupdatesCount:
									_.size(sellerupdates) +
									_.size(sellerupdates2),
								sellerupdates: newsellerupdates,
								walletupdates: walletupdates,
								walletupdatesCount: _.size(walletupdates),
								orderupdates: orderupdates,
								orderupdatesCount: _.size(orderupdates)
							};

							console.log("notification: ", this.notif);
						}
						this.showLoading = false;
						if (ev) {
							ev.complete();
						}
					},
					error => {
						console.log("notification error: ", error);
						this.showLoading = false;
						if (ev) {
							ev.complete();
						}
					}
				)
				.then(() => {
					this.cartCount = this.shoppingCart.cartCount();
					this.unreadCount = this.chatService.unreadCount;
				});
		}
	}

	ionViewDidLoad() {
		console.log("ionViewDidLoad NotificationPage");
		this.initData();
	}

	ionViewWillEnter() {
		this.cartCount = this.shoppingCart.cartCount();
		this.unreadCount = this.chatService.unreadCount;
	}

	getCurrentUser() {
		this.user = JSON.parse(localStorage.getItem("app.user")) || {};
		if (!_.isEmpty(this.user)) {
			this.mapTabEnabled = true;
			this.users
				.getCurrentUser()
				.then(
					(data: any) => {
						if (data && data.success) {
							localStorage.setItem(
								"app.user",
								JSON.stringify(data.result)
							);
							var sb = new SendBird({
								appId: environment.SendBird
							});
							sb.connect(
								data.result.uuid,
								data.result.sendBirdToken,
								(user, error) => {}
							);

							this.initData();
							this.events.publish("reloadAppNotifications");
						}
					},
					error => {
						console.log("error: ", error);
					}
				)
				.then(() => {
					if (!_.isEmpty(this.user)) {
						this.user =
							JSON.parse(localStorage.getItem("app.user")) || {};
						async.parallel(
							[
								callback => {
									this.users.getCurrentUserFollowers().then(
										(data: any) => {
											if (data && data.success) {
												console.log(
													"getCurrentUserFollowers: ",
													data
												);
												this.user.followers =
													data.result;
											}
											callback();
										},
										error => {
											console.log("error: ", error);
											this.user.followers = [];
											callback();
										}
									);
								},
								callback => {
									this.users.getCurrentUserFollowing().then(
										(data: any) => {
											if (data && data.success) {
												console.log(
													"getCurrentUserFollowing: ",
													data
												);
												this.user.following =
													data.result;
											}
											callback();
										},
										error => {
											console.log("error: ", error);
											this.user.following = [];
											callback();
										}
									);
								}
							],
							() => {
								localStorage.setItem(
									"app.user",
									JSON.stringify(this.user)
								);
							}
						);
					}
				});
		} else {
			this.mapTabEnabled = false;
		}
	}

	doRefresh(ev) {
		this.initData(ev);
	}

	openLogin() {
		// TODO: Check for authentication
		let modal = this.modalCtrl.create(LoginPage);
		modal.onDidDismiss(res => {
			if (res == "login") {
				this.getCurrentUser();
			}
		});
		modal.present();
	}

	openSignUp() {
		// TODO: Check for authentication
		console.log("openSignUp");
		let modal = this.modalCtrl.create(SignupPage);
		modal.present();
	}

	viewChat() {
		if (_.isEmpty(this.user)) {
			let modal = this.modalCtrl.create(LoginPage);
			modal.present();
			return;
		}

		let modal = this.modalCtrl.create(ChatPage);
		modal.present();
	}

	viewCart() {
		// TODO: Check if Authenticated
		if (_.isEmpty(this.user)) {
			let modal = this.modalCtrl.create(LoginPage);
			modal.present();
			return;
		}

		this.app.getRootNav().push(ShoppingcartPage);
	}

	viewNotifications() {
		this.app.getRootNav().push(ContentFormPage, {
			action: "notification",
			params: this.notif.promotions,
			title: "Promotions"
		});
	}

	viewSellerNotifications() {
		this.app.getRootNav().push(ContentFormPage, {
			action: "sellers",
			params: this.notif.sellerupdates,
			title: "Seller Updates"
		});
	}

	viewOrderNotification(order) {
		console.log("viewOrderNotification: ", order);
		if (order) {
			if (order.link) {
				const link: any = JSON.parse(order.link);

				if (link.action === "order_ship") {
					link.action = "ship";
				} else if (link.action === "order_receive") {
					link.action = "receive";
				}
				this.app.getRootNav().push(OrderdetailsPage, {
					orderId: link.orderId,
					status: link.action,
					usertype: "buyer"
				});
			}
		}
	}
}
