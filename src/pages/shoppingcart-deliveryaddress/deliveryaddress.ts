import { Component } from "@angular/core";
import {
	IonicPage,
	NavController,
	NavParams,
	LoadingController
} from "ionic-angular";
import * as _ from "lodash";

import { MyaddressPage } from "../../pages/myaddress/myaddress";
import { AllFormEntryPage } from "../../pages/all-form-entry/all-form-entry";
import { ShippingoptionPage } from "../../pages/shoppingcart-shippingoption/shippingoption";

import { UserAddressessServiceProvider } from "../../providers/service/user-address";

@IonicPage()
@Component({
	selector: "page-deliveryaddress",
	templateUrl: "deliveryaddress.html"
})
export class DeliveryaddressPage {
	userAddressArr: any[] = [];
	order: any = {};
	orderAddress: any = {};
	callback: any;

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public loadingCtrl: LoadingController,
		public addressess: UserAddressessServiceProvider
	) {
		this.orderAddress = JSON.parse(localStorage.getItem("app.orders")) || {};
		this.callback = navParams.get("callback");
	}

	ionViewDidLoad() { }

	ionViewDidEnter() {
		this.orderAddress = JSON.parse(localStorage.getItem("app.orders")) || {};
		this.initData();
	}

	initData() {
		let loading = this.loadingCtrl.create({
			dismissOnPageChange: true
		});
		loading.present();

		this.addressess
			.getAllUserAddress()
			.then(
				(data: any) => {
					if (data && data.success) {
						this.userAddressArr = data.result;
						_.each(this.userAddressArr, (row: any) => {
							row.selected = row.isDefaultAddress;

							if (row.isDefaultAddress) {
								this.order.addressId = row._id;
								this.order.orderShipAddress = row.detailAddress;
								this.order.orderShipCity = row.city;
								this.order.orderShipState = row.state;
								this.order.orderShipZipCode = row.zipcode;
								this.order.orderShipCountry = row.country;
								this.order.orderShipSuburb = row.suburb;
								this.order.orderPhone = row.mobile;
								this.order.orderShipName = row.name;
							}
						});
					}
					loading.dismiss();
				},
				error => {
					loading.dismiss();
				}
			)
			.then(() => {
				if (!_.isEmpty(this.orderAddress)) {
					_.each(this.userAddressArr, (row: any) => {
						row.selected =
							row._id == this.orderAddress.addressId
								? true
								: false;
					});
				}
			});
	}

	openMyAddress() {
		this.navCtrl.push(MyaddressPage);
	}

	addNewAddress() {
		this.navCtrl.push(AllFormEntryPage, {
			action: 'my_address_entry'
		});
	}

	selectItem(item) {
		_.each(this.userAddressArr, (row: any) => {
			if (item._id == row._id) {
				row.selected = true;

				this.order.addressId = row._id;
				this.order.orderShipAddress = row.detailAddress;
				this.order.orderShipCity = row.city;
				this.order.orderShipState = row.state;
				this.order.orderShipZipCode = row.zipcode;
				this.order.orderShipCountry = row.country;
				this.order.orderShipSuburb = row.suburb;
				this.order.orderPhone = row.mobile;
				this.order.orderShipName = row.name;

				setTimeout(() => {
					this.callback(row).then(() => { this.navCtrl.pop() });
				}, 800);
			} else {
				row.selected = false;
			}
		});
	}

	proceedNext() {
		console.log("proceedNext");
		localStorage.setItem("app.orders", JSON.stringify(this.order));
		this.navCtrl.push(ShippingoptionPage, {
			order: this.order
		});
	}
}
