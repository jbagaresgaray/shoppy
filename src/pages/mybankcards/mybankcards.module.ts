import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MybankcardsPage } from './mybankcards';

@NgModule({
  declarations: [
    MybankcardsPage,
  ],
  imports: [
    IonicPageModule.forChild(MybankcardsPage),
  ],
})
export class MybankcardsPageModule {}
