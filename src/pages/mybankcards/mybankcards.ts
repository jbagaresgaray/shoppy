import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, LoadingController, AlertController, Events } from 'ionic-angular';
import * as _ from 'lodash';

import { ChatPage } from '../../pages/chat/chat';
import { AllFormEntryPage } from '../../pages/all-form-entry/all-form-entry';

import { UserBanksServiceProvider } from '../../providers/service/user-bank';
import { ChatService } from '../../providers/service/chat';

@IonicPage()
@Component({
  selector: 'page-mybankcards',
  templateUrl: 'mybankcards.html',
})
export class MybankcardsPage {

  banksArr: any[] = [];
  unreadCount: number = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController, public alertCtrl: AlertController, public loadingCtrl: LoadingController, public userBanks: UserBanksServiceProvider,
    public events: Events, public chatService: ChatService) {

    // ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
    // ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
    // ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
    events.unsubscribe('onMessageReceived');
    events.unsubscribe('onMessageUpdated');
    events.unsubscribe('getTotalUnreadMessageCount');


    events.subscribe('onMessageReceived',()=>{
      this.unreadCount = this.chatService.unreadCount;
    });

    events.subscribe('onMessageUpdated',()=>{
      this.unreadCount = this.chatService.unreadCount;
    });

    events.subscribe('getTotalUnreadMessageCount',()=>{
      this.unreadCount = this.chatService.unreadCount;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MybankcardsPage');
    this.unreadCount = this.chatService.unreadCount;
  }

  ionViewDidEnter() {
    let loading = this.loadingCtrl.create();
    loading.present();

    this.userBanks.getAllUserBanks().then((data: any) => {
      if (data && data.success) {
        this.banksArr = data.result;
        loading.dismiss();
      }
    }, (error) => {
      loading.dismiss();
    });
  }

  viewChat() {
    let modal = this.modalCtrl.create(ChatPage);
    modal.present();
  }

  addNewAccount() {
    this.navCtrl.push(AllFormEntryPage, {
      action: 'bank_cards_entry',
      mode: 'entry'
    });
  }

  viewAccount(item) {
    this.navCtrl.push(AllFormEntryPage, {
      bankId: item._id,
      action: 'bank_cards_entry',
      mode: 'view'
    });
  }

}
