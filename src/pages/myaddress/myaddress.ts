import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, LoadingController, Events } from 'ionic-angular';
import * as _ from 'lodash';

import { ChatPage } from '../../pages/chat/chat';
import { AllFormEntryPage } from '../../pages/all-form-entry/all-form-entry';

import { UserAddressessServiceProvider } from '../../providers/service/user-address';
import { ChatService } from '../../providers/service/chat';

@IonicPage()
@Component({
  selector: 'page-myaddress',
  templateUrl: 'myaddress.html',
})
export class MyaddressPage {
  useraddress: any[] = [];
  unreadCount: number = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController, public loadingCtrl: LoadingController, public addressess: UserAddressessServiceProvider, public events: Events, public chatService: ChatService) {
    // ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
    // ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
    // ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
    events.unsubscribe('onMessageReceived');
    events.unsubscribe('onMessageUpdated');
    events.unsubscribe('getTotalUnreadMessageCount');

    events.subscribe('onMessageReceived', () => {
      this.unreadCount = this.chatService.unreadCount;
    });

    events.subscribe('onMessageUpdated', () => {
      this.unreadCount = this.chatService.unreadCount;
    });

    events.subscribe('getTotalUnreadMessageCount', () => {
      this.unreadCount = this.chatService.unreadCount;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyaddressPage');
  }

  ionViewDidEnter() {
    let loading = this.loadingCtrl.create();
    loading.present();
    this.addressess.getAllUserAddress().then((data: any) => {
      if (data && !_.isEmpty(data.result)) {
        this.useraddress = data.result;
      }
    }, (error) => {
      loading.dismiss();
    }).then(() => {
      loading.dismiss();
    });

    this.unreadCount = this.chatService.unreadCount;
  }

  viewChat() {
		let modal = this.modalCtrl.create(ChatPage);
		modal.present();
	}

	addNewAddress() {
		this.navCtrl.push(AllFormEntryPage, {
      action: 'my_address_entry'
    });
	}

  viewAddress(item) {
    this.navCtrl.push(AllFormEntryPage, {
      action: 'my_address_entry',
      addressId: item._id
    });
  }

}
