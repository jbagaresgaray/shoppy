import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProductPackagingEntryPage } from './product-packaging-entry';

@NgModule({
  declarations: [
    ProductPackagingEntryPage,
  ],
  imports: [
    IonicPageModule.forChild(ProductPackagingEntryPage),
  ],
})
export class ProductPackagingEntryPageModule {}
