import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ProductPackagingEntryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-product-packaging-entry',
  templateUrl: 'product-packaging-entry.html',
})
export class ProductPackagingEntryPage {
  package: any = {};

  callback: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.callback = this.navParams.get('callback');

    if (this.navParams.get('package')) {
      this.package = this.navParams.get('package');
    } else {
      this.package = {
        width: 0,
        length: 0,
        height: 0
      };
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductPackagingEntryPage');
  }


  saveProduct() {
    this.callback(this.package).then(() => { this.navCtrl.pop() });
  }
}
