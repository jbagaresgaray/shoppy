import { Component,ViewChild  } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, Content } from 'ionic-angular';
import * as _ from 'lodash';

/**
 * Generated class for the ProductWholesaleEntryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-product-wholesale-entry',
  templateUrl: 'product-wholesale-entry.html',
})
export class ProductWholesaleEntryPage {
  wholesaleArr: any[] = [];
  demoArr: any[] = [];
  item: any = {};
  price: any;
  callback: any;

  @ViewChild(Content) content: Content;
  constructor(public navCtrl: NavController, public navParams: NavParams, public toastCtrl: ToastController) {
    this.callback = this.navParams.get('callback');
    this.price = this.navParams.get('price');

    if (this.navParams.get('wholesaleArr')) {
      this.wholesaleArr = this.navParams.get('wholesaleArr');
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductWholesaleEntryPage');
  }

  addWholeSale() {
    if (_.isEmpty(this.item.minAmt)) {
      let toast = this.toastCtrl.create({
        message: 'Please enter min order!',
        duration: 1000
      });
      toast.present();
      return;
    }

    if (_.isEmpty(this.item.maxAmt)) {
      let toast = this.toastCtrl.create({
        message: 'Please enter max order!',
        duration: 1000
      });
      toast.present();
      return;
    }

    if (_.isEmpty(this.item.price)) {
      let toast = this.toastCtrl.create({
        message: 'Please enter price!',
        duration: 1000
      });
      toast.present();
      return;
    }

    this.wholesaleArr.push(this.item);
    this.item = {
      minAmt: "",
      maxAmt: "",
      price: ""
    };
    this.content.scrollToBottom();
    console.log('this.wholesaleArr: ', this.wholesaleArr);
  }

  saveWholeSale() {
    if (this.item.minAmt) {
      this.wholesaleArr.push(this.item);
    }

    this.callback(this.wholesaleArr).then(() => { this.navCtrl.pop() });
  }

  dismiss() {
    this.navCtrl.pop();
  }

}
