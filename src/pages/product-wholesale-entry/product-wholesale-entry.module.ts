import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProductWholesaleEntryPage } from './product-wholesale-entry';

@NgModule({
  declarations: [
    ProductWholesaleEntryPage,
  ],
  imports: [
    IonicPageModule.forChild(ProductWholesaleEntryPage),
  ],
})
export class ProductWholesaleEntryPageModule {}
