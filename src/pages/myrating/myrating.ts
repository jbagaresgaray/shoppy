import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import * as _ from "lodash";
import * as async from "async";

import { SellerServiceProvider } from "../../providers/service/seller";

@IonicPage()
@Component({
  selector: "page-myrating",
  templateUrl: "myrating.html"
})
export class MyratingPage {
  pet: any;
  profile: any = {};
  user: any = {};
  title: string;

  ratingsArr: any[] = [];
  ratingsCopyArr: any[] = [];
  ratingsCopy2Arr: any[] = [];
  pageStart: number;
  perPage: number;
  totalPage: number;

  fiveRate: number;
  fourRate: number;
  threeRate: number;
  twoRate: number;
  oneRate: number;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public sellerService: SellerServiceProvider
  ) {
    this.pet = navParams.get("mode");
    this.profile = navParams.get("profile");
    this.user = JSON.parse(localStorage.getItem("app.user")) || {};
    console.log("this.profile: ", this.profile);

    this.perPage = 20;
    this.pageStart = 0;

    this.fiveRate = 0;
    this.fourRate = 0;
    this.threeRate = 0;
    this.twoRate = 0;
    this.oneRate = 0;

    if (this.profile) {
      if (this.profile.userId === this.user.userId) {
        this.title = "My Rating";
        this.pet = "buyer";

        this.initRatings(this.user.userId);
      } else {
        this.title = this.profile.sellerName + " Rating";
        this.pet = "shop";

        this.initRatings(this.profile.userId);
      }
    } else {
      this.title = "My Rating";
      this.pet = "buyer";

      this.initRatings(this.user.userId);
    }
  }

  private initRatings(userId, ev?: any) {
    async.parallel(
      [
        callback => {
          this.sellerService.getShopRatingSummary(userId).then(
            (data: any) => {
              if (data && data.success) {
                this.user.rating = data.result;
              }
              console.log("this.profile.rating: ", this.user.rating);
              callback();
            },
            error => {
              console.log("error: ", error);
              callback();
            }
          );
        },
        callback => {
          this.sellerService.getShopRatings(userId).then(
            (data: any) => {
              if (data && data.success) {
                console.log("data: ", data.result);
                this.pageStart = parseInt(data.result.startAt);
                this.totalPage = parseInt(data.result.page_size);

                for (let i = 0; i < _.size(data.result.result); i++) {
                  data.result.result[i].selected = false;
                  this.ratingsCopyArr.push(data.result.result[i]);
                }
              }
              callback();
            },
            error => {
              console.log("error: ", error);
              callback();
            }
          );
        }
      ],
      () => {
        this.segmentChanged(null);
        if (ev) {
          ev.complete();
        }
      }
    );
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad MyratingPage");
  }

  segmentChanged(ev: any) {
    if (this.pet === "shop") {
      this.ratingsArr = _.cloneDeep(this.ratingsCopyArr);

      this.fiveRate = _.size(_.filter(this.ratingsCopyArr, { rating: 5 }));
      this.fourRate = _.size(_.filter(this.ratingsCopyArr, { rating: 4 }));
      this.threeRate = _.size(_.filter(this.ratingsCopyArr, { rating: 3 }));
      this.twoRate = _.size(_.filter(this.ratingsCopyArr, { rating: 2 }));
      this.oneRate = _.size(_.filter(this.ratingsCopyArr, { rating: 1 }));
    } else {
      this.ratingsArr = _.cloneDeep(this.ratingsCopy2Arr);

      this.fiveRate = _.size(_.filter(this.ratingsCopy2Arr, { rating: 5 }));
      this.fourRate = _.size(_.filter(this.ratingsCopy2Arr, { rating: 4 }));
      this.threeRate = _.size(_.filter(this.ratingsCopy2Arr, { rating: 3 }));
      this.twoRate = _.size(_.filter(this.ratingsCopy2Arr, { rating: 2 }));
      this.oneRate = _.size(_.filter(this.ratingsCopy2Arr, { rating: 1 }));
    }
  }

  doRefresh(ev) {
    if (this.profile) {
      if (this.profile.userId === this.user.userId) {
        this.initRatings(this.user.userId, ev);
      } else {
        this.initRatings(this.profile.userId, ev);
      }
    } else {
      this.initRatings(this.user.userId, ev);
    }
  }

  filterRatings(value) {
    if (_.isNumber(value)) {
      this.ratingsArr = _.filter(this.ratingsCopyArr, { rating: value });
    } else {
      this.ratingsArr = _.cloneDeep(this.ratingsCopyArr);
    }
  }
}
