import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Ionic2RatingModule } from "ionic2-rating";

import { MyratingPage } from './myrating';

@NgModule({
  declarations: [
    MyratingPage,
  ],
  imports: [
    IonicPageModule.forChild(MyratingPage),
    Ionic2RatingModule
  ],
})
export class MyratingPageModule {}
