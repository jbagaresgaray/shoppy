import { Component, NgZone } from '@angular/core';
import { DomSanitizer } from "@angular/platform-browser";
import { IonicPage, NavController, NavParams, LoadingController, AlertController, ToastController, ActionSheetController } from 'ionic-angular';
import * as _ from 'lodash';
import * as async from 'async';

import { UserprofileEntryPage } from '../../pages/userprofile-entry/userprofile-entry';

import { UsersServiceProvider } from '../../providers/service/users';
import { SellerServiceProvider } from '../../providers/service/seller';

import { environment } from '@app/env';

import { Camera, CameraOptions } from "@ionic-native/camera";
import { FileTransfer, FileTransferObject, FileUploadOptions } from "@ionic-native/file-transfer";
import { File } from '@ionic-native/file';
import { Crop } from '@ionic-native/crop';

@IonicPage()
@Component({
	selector: 'page-shopprofile-entry',
	templateUrl: 'shopprofile-entry.html',
})
export class ShopprofileEntryPage {
	user: any = {};
	action: string;

	public isUploading: boolean = false;
	public isUploadingBanner: boolean = false;

	isAvatarChange: boolean = false;
	isBannerChange: boolean = false;

	public uploadingProgress: any;
	public uploadingBannerProgress: any;
	public uploadingHandler: any;
	public uploadingBannerHandler: any;
	public images: any = [];
	public filesToUpload: Array<File> = [];
	public bannerToUpload: Array<File> = [];

	protected imagesValue: Array<any> = [];
	protected bannerValue: Array<any> = [];

	serverUrl: string = environment.api_url;

	constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public alertCtrl: AlertController, public toastCtrl: ToastController, public actionSheetCtrl: ActionSheetController,
		public users: UsersServiceProvider, private sanitization: DomSanitizer, public zone: NgZone, public sellers: SellerServiceProvider,
		private camera: Camera, private file: File, public transfer: FileTransfer, private crop: Crop) {

		this.isAvatarChange = false;
		this.isBannerChange = false;
	}

	public uploadAvatarImage(): Promise<Array<any>> {
		return new Promise((resolve, reject) => {
			Promise.all(this.filesToUpload.map((image: any) => {
				this.isUploading = true;
				return this.uploadAvatar(image);
			})).then(() => {
				resolve();
				this.filesToUpload = [];
				this.images = [];
				this.imagesValue = [];
				this.isUploading = false;

				this.getCurrentUser();
			}).catch((reason: any) => {
				console.log('reasons: ', reason);
				this.isUploading = false;

				this.uploadingProgress = {};
				this.uploadingHandler = {};
				// reject(reason);
			});
		});
	}

	public uploadBannerImage(): Promise<Array<any>> {
		return new Promise((resolve, reject) => {
			Promise.all(this.bannerToUpload.map((image: any) => {
				this.isUploadingBanner = true;
				return this.uploadBanner(image);
			})).then(() => {
				resolve();
				this.bannerToUpload = [];
				this.images = [];
				this.bannerValue = [];
				this.isUploadingBanner = false;

				this.getCurrentUser();
			}).catch((reason: any) => {
				console.log('reasons: ', reason);
				this.isUploadingBanner = false;

				this.uploadingBannerProgress = {};
				this.uploadingBannerHandler = {};
				// reject(reason);
			});
		});
	}

	public abortAvatar() {
		if (!this.isUploading)
			return;

		this.isUploading = false;
		this.uploadingHandler.abort();
	}


	public abortBanner() {
		if (!this.isUploadingBanner)
			return;

		this.isUploadingBanner = false;
		this.uploadingBannerHandler.abort();
	}

	// ======================================================================
	protected removeImage(image) {
		if (this.isUploading) {
			return;
		}

		this.confirm("Are you sure to remove it?").then(value => {
			if (value) {
				this.removeFromArray(this.imagesValue, image);
				this.removeFromArray(this.images, image.url);
			}
		});
	}

	private removeFromArray<T>(array: Array<T>, item: T) {
		let index: number = array.indexOf(item);
		if (index !== -1) {
			array.splice(index, 1);
		}
	}

	private confirm(text, title = '', yes = "Yes", no = "No") {
		return new Promise(
			(resolve) => {
				this.alertCtrl.create({
					title: title,
					message: text,
					buttons: [
						{
							text: no,
							role: 'cancel',
							handler: () => {
								resolve(false);
							}
						},
						{
							text: yes,
							handler: () => {
								resolve(true);
							}
						}
					]
				}).present();
			}
		);
	}

	private trustImages() {
		this.imagesValue = this.images.map(
			val => {
				return {
					url: val,
					sanitized: this.sanitization.bypassSecurityTrustUrl(val)
				}
			}
		);
		this.zone.run(() => {
			this.isAvatarChange = true;
			this.user.img_path = this.imagesValue[0].sanitized;
			console.log('this.user.img_path: ', this.user.img_path);
		});
	}

	private trustBanner() {
		this.bannerValue = this.images.map(
			val => {
				return {
					url: val,
					sanitized: this.sanitization.bypassSecurityTrustStyle("url(" + val + ")")
				}
			}
		);
		this.zone.run(() => {
			this.isBannerChange = true;
			this.user.banner_path = this.bannerValue[0].sanitized;
			console.log('this.user.banner_path: ', this.user.banner_path);
		});
	}

	showToast(text: string) {
		this.toastCtrl.create({
			message: text,
			duration: 5000,
			position: 'bottom'
		}).present();
	}

	private uploadAvatar(targetPath) {
		return new Promise((resolve, reject) => {
			this.uploadingProgress = 0;
			var uploadUrl = this.serverUrl + 'users/avatar';

			if (window['cordova']) {
				let options: FileUploadOptions = {
					fileKey: "avatar",
					chunkedMode: false,
					headers: {
						'Authorization': 'Bearer ' + localStorage.getItem('app.token')
					}
				};

				const fileTransfer: FileTransferObject = this.transfer.create();
				this.uploadingHandler = fileTransfer;

				this.file.resolveLocalFilesystemUrl(targetPath).then((info: any) => {
					options.fileName = info.name;
				}).then(() => {
					fileTransfer.upload(targetPath, uploadUrl, options, false).then(data => {
						resolve(JSON.parse(data.response));
					}).catch((error) => {
						console.log('error: ', error);
						if (error) {
							let err = JSON.parse(error.body);
							console.log('err: ', err);
						}
						askRetry();
					});

					fileTransfer.onProgress(event2 => {
						let progressVal = event2.loaded * 100 / event2.total;
						console.log('progressVal: ', progressVal);
						this.uploadingProgress = progressVal;
					});
				});

			} else {
				let formData: FormData = new FormData();
				let xhr2: XMLHttpRequest = new XMLHttpRequest();

				formData.append('avatar', targetPath);
				this.uploadingHandler = xhr2;

				xhr2.onreadystatechange = () => {
					if (xhr2.readyState === 4) {
						if (xhr2.status === 200)
							resolve(JSON.parse(xhr2.response));
						else
							askRetry();
					}
				};

				xhr2.upload.onprogress = (event) => {
					let progressVal = event.loaded * 100 / event.total;
					console.log('progressVal: ', progressVal);

					this.uploadingProgress = progressVal;
				};
				xhr2.open('POST', uploadUrl, true);
				xhr2.setRequestHeader('Authorization', 'Bearer ' + localStorage.getItem('app.token'));
				xhr2.send(formData);
			}

			let askRetry = () => {
				// might have been aborted
				if (!this.isUploading) return reject(null);
				this.confirm('Do you wish to retry?', 'Upload failed').then(res => {
					if (!res) {
						this.isUploading = false;
						this.uploadingHandler.abort();
						return reject(null);
					}
					else {
						if (!this.isUploading) return reject(null);
						this.uploadAvatar(targetPath).then(resolve, reject);
					}
				});
			};
		});
	}

	private uploadBanner(targetPath) {
		return new Promise((resolve, reject) => {
			this.uploadingBannerProgress = 0;
			var uploadUrl = this.serverUrl + 'users/banner';

			if (window['cordova']) {
				let options: FileUploadOptions = {
					fileKey: "banner",
					chunkedMode: false,
					headers: {
						'Authorization': 'Bearer ' + localStorage.getItem('app.token')
					}
				};

				const fileTransfer: FileTransferObject = this.transfer.create();
				this.uploadingBannerHandler = fileTransfer;

				this.file.resolveLocalFilesystemUrl(targetPath).then((info: any) => {
					options.fileName = info.name;
				}).then(() => {
					fileTransfer.upload(targetPath, uploadUrl, options, false).then(data => {
						resolve(JSON.parse(data.response));
					}).catch((error) => {
						console.log('error: ', error);
						if (error) {
							let err = JSON.parse(error.body);
							console.log('err: ', err);
						}
						askRetry();
					});

					fileTransfer.onProgress(event2 => {
						let progressVal = event2.loaded * 100 / event2.total;
						console.log('progressVal: ', progressVal);
						this.uploadingBannerProgress = progressVal;
					});
				});

			} else {
				let formData: FormData = new FormData();
				let xhr2: XMLHttpRequest = new XMLHttpRequest();

				formData.append('banner', targetPath);
				this.uploadingBannerHandler = xhr2;
				xhr2.onreadystatechange = () => {
					if (xhr2.readyState === 4) {
						if (xhr2.status === 200)
							resolve(JSON.parse(xhr2.response));
						else
							askRetry();
					}
				};

				xhr2.upload.onprogress = (event) => {
					let progressVal = event.loaded * 100 / event.total;
					console.log('progressVal: ', progressVal);
					this.uploadingBannerProgress = progressVal;
				};
				xhr2.open('POST', uploadUrl, true);
				xhr2.setRequestHeader('Authorization', 'Bearer ' + localStorage.getItem('app.token'));
				xhr2.send(formData);
			}

			let askRetry = () => {
				// might have been aborted
				if (!this.isUploadingBanner) return reject(null);
				this.confirm('Do you wish to retry?', 'Upload failed').then(res => {
					if (!res) {
						this.isUploadingBanner = false;
						this.uploadingBannerHandler.abort();
						return reject(null);
					}
					else {
						if (!this.isUploadingBanner) return reject(null);
						this.uploadBanner(targetPath).then(resolve, reject);
					}
				});
			};
		});
	}

	updateProfilePic(action) {
		let sText: any;
		this.action = action;
		if (action == 'cover') {
			sText = 'Edit Background';

			if (this.isUploadingBanner) {
				return;
			}
		} else if (action == 'profile') {
			sText = 'Edit Profile Picture';

			if (this.isUploading) {
				return;
			}
		}

		if (!window['cordova']) {
			new Promise((resolve, reject) => {
				let actionSheet = this.actionSheetCtrl.create({
					title: sText,
					buttons: [
						{
							text: 'Browse Photo',
							handler: () => {
								resolve();
							}
						}, {
							text: 'Cancel',
							role: 'cancel',
							handler: () => {
								reject();
							}
						}
					]
				});
				actionSheet.present();
			}).then(() => {
				let input = document.createElement('input');
				input.type = 'file';
				input.accept = "image/x-png,image/gif,image/jpeg";
				input.click();
				input.onchange = () => {
					let blob = window.URL.createObjectURL(input.files[0]);
					let blob2: any = input.files[0];

					this.images = [];
					this.images.push(blob);
					
					if (this.action == 'profile') {
						this.filesToUpload = [];
						this.filesToUpload.push(blob2);
						console.log('this.filesToUpload: ', this.filesToUpload);
						this.trustImages();
					} else if (this.action == 'cover') {
						this.bannerToUpload = [];
						this.bannerToUpload.push(blob2);
						console.log('this.bannerToUpload: ', this.bannerToUpload);
						this.trustBanner();
					}
				}
			}).catch(() => {
			});
		} else {
			new Promise((resolve, reject) => {
				let actionSheet = this.actionSheetCtrl.create({
					title: sText,
					buttons: [
						{
							text: 'Photos',
							handler: () => {
								resolve(this.camera.PictureSourceType.PHOTOLIBRARY);
							}
						},
						{
							text: 'Camera',
							handler: () => {
								resolve(this.camera.PictureSourceType.CAMERA);
							}
						},
						{
							text: 'Cancel',
							role: 'cancel',
							handler: () => {
								reject();
							}
						}
					]
				});
				actionSheet.present();
			}).then(sourceType => {
				if (!window['cordova'])
					return;
				let options: CameraOptions = {
					quality: 100,
					sourceType: sourceType as number,
					saveToPhotoAlbum: false,
					correctOrientation: true
				};
				this.camera.getPicture(options).then((imagePath) => {
					console.log('CameraOptions imagePath: ', imagePath);
					this.images = [];
					this.images.push(imagePath);

					if (this.action == 'cover') {
						this.bannerToUpload = [];
						this.bannerToUpload.push(imagePath);
						console.log('CameraOptions this.bannerToUpload: ', this.bannerToUpload);

						this.trustBanner();
					} else {
						this.crop.crop(imagePath, { quality: 100, targetWidth: 600, targetHeight: 600 }).then((newImage: any) => {
							console.log('new image path is: ' + newImage);

							this.images.push(newImage);
							this.filesToUpload.push(newImage);
							console.log('CameraOptions this.filesToUpload: ', this.filesToUpload);

							this.trustImages();
						}, (error) => {
							console.error('Error cropping image', error)
						});
					}
				});
			}).catch(() => {
			});
		}
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad ShopprofileEntryPage');
		this.getCurrentUser();
	}

	getCurrentUser() {
		let loading = this.loadingCtrl.create();
		loading.present();

		this.user = JSON.parse(localStorage.getItem('app.user')) || {};
		if (!_.isEmpty(this.user)) {
			this.users.getCurrentUser().then((data: any) => {
				if (data && data.success) {
					this.user = data.result;

					this.user.img_path = (_.isEmpty(this.user.img_path)) ? './assets/imgs/user.png' : this.user.img_path;
					this.user.banner_path = (_.isEmpty(this.user.banner_path)) ? './assets/imgs/user.png' : "url(" + this.user.banner_path + ")";
					if (this.user.email) {
						let i = this.user.email.indexOf('@');
						let email = this.user.email.split('@');
						this.user.emailMask = this.user.email.substr(0, 2) + this.user.email.substr(i).replace(/[\S]/g, "*") + '@' + email[1];
					} else {
						this.user.emailMask = '';
					}

					if (this.user.phone) {
						this.user.phoneMask = this.user.phone.substr(0, (this.user.phone.length) - 2).replace(/[\S]/g, "*") + this.user.phone.slice(-2);
					} else {
						this.user.phoneMask = '';
					}

					if (this.user.fax) {
						this.user.faxMask = this.user.fax.substr(0, (this.user.fax.length) - 2).replace(/[\S]/g, "*") + this.user.fax.slice(-2);
					} else {
						this.user.faxMask = '';
					}

					console.log('this.user: ', this.user);
					loading.dismiss();
				}
			}, (error) => {
				loading.dismiss();
			});
		}
	}

	updateEmail() {
		this.navCtrl.push(UserprofileEntryPage, {
			type: 'validate',
			next: 'email',
			user: this.user
		});
	}

	updatePhone() {
		this.navCtrl.push(UserprofileEntryPage, {
			type: 'validate',
			next: 'phone',
			user: this.user
		});
	}

	updateFax() {
		this.navCtrl.push(UserprofileEntryPage, {
			type: 'validate',
			next: 'fax',
			user: this.user
		});
	}

	saveShop() {
		let loading = this.loadingCtrl.create({
			content: 'Saving...'
		});
		loading.present();
		this.sellers.updateStoreInformation({
			sellerName: this.user.sellerName,
			sellerDescription: this.user.sellerDescription
		}).then((data: any) => {
			if (data && data.success) {
				/*let alert = this.alertCtrl.create({
					title: 'Success!',
					subTitle: data.msg,
					buttons: ['OK']
				});
				alert.onDidDismiss(() => {
					this.navCtrl.pop();
				});
				alert.present();*/

				async.parallel([
					(callback) => {
						if (this.isAvatarChange) {
							this.uploadAvatarImage().then((resp: any) => {
								callback();
							}, (error) => {
								console.log('ERROR uploadAvatarImage: ', error);
							});
						} else {
							callback();
						}
					},
					(callback) => {
						if (this.isBannerChange) {
							this.uploadBannerImage().then((resp: any) => {
								callback();
							}, (error) => {
								console.log('ERROR uploadBannerImage: ', error);
							});
						} else {
							callback();
						}
					}
				], () => {
					loading.dismiss();
					this.navCtrl.pop();
				});
			} else {
				loading.dismiss();
			}
		}, (error) => {
			loading.dismiss();
		});
	}
}
