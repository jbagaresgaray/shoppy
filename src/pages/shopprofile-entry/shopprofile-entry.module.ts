import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShopprofileEntryPage } from './shopprofile-entry';

@NgModule({
  declarations: [
    ShopprofileEntryPage,
  ],
  imports: [
    IonicPageModule.forChild(ShopprofileEntryPage),
  ],
})
export class ShopprofileEntryPageModule {}
