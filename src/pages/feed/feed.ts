import { Component } from "@angular/core";
import {
	IonicPage,
	NavController,
	NavParams,
	ModalController,
	App,
	Events
} from "ionic-angular";
import * as _ from "lodash";
import * as async from "async";

import { ShoppingcartPage } from "../../pages/shoppingcart/shoppingcart";
import { ChatPage } from "../../pages/chat/chat";
import { LoginPage } from "../../pages/login/login";
import { SignupPage } from "../../pages/signup/signup";

import { ShoppingCartProvider } from "../../providers/service/shopping-cart";
import { UsersServiceProvider } from "../../providers/service/users";
import { ChatService } from "../../providers/service/chat";

@IonicPage()
@Component({
	selector: "page-feed",
	templateUrl: "feed.html"
})
export class FeedPage {
	user: any = {};
	showSignUp: boolean = true;
	cartCount: number;
	unreadCount: number = 0;

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public app: App,
		public modalCtrl: ModalController,
		public events: Events,
		public shoppingCart: ShoppingCartProvider,
		public chatService: ChatService,
		public users: UsersServiceProvider
	) {
		this.user = JSON.parse(localStorage.getItem("app.user")) || {};
		if (_.isEmpty(this.user)) {
			this.showSignUp = true;
		} else {
			this.showSignUp = false;
		}

		this.shoppingCart.getCart().then((data: any) => {
			this.cartCount = !_.isEmpty(data) ? data.length : 0;
		});

		// ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
		// ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
		// ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
		events.unsubscribe("update:cart");
		events.unsubscribe("onMessageReceived");
		events.unsubscribe("onMessageUpdated");
		events.unsubscribe("getTotalUnreadMessageCount");

		events.subscribe("update:cart", () => {
			this.cartCount = this.shoppingCart.cartCount();
		});

		events.subscribe("onMessageReceived", () => {
			this.unreadCount = this.chatService.unreadCount;
		});

		events.subscribe("onMessageUpdated", () => {
			this.unreadCount = this.chatService.unreadCount;
		});

		events.subscribe("getTotalUnreadMessageCount", () => {
			this.unreadCount = this.chatService.unreadCount;
		});
	}

	ionViewDidLoad() {
		console.log("ionViewDidLoad FeedPage");
	}

	ionViewWillEnter() {
		this.user = JSON.parse(localStorage.getItem("app.user")) || {};
		if (_.isEmpty(this.user)) {
			this.showSignUp = true;
		} else {
			this.showSignUp = false;
		}
		this.cartCount = this.shoppingCart.cartCount();
		this.unreadCount = this.chatService.unreadCount;
	}

	getCurrentUser() {
		this.user = JSON.parse(localStorage.getItem("app.user")) || {};
		if (!_.isEmpty(this.user)) {
			this.users
				.getCurrentUser()
				.then(
					(data: any) => {
						if (data && data.success) {
							localStorage.setItem(
								"app.user",
								JSON.stringify(data.result)
							);
							this.chatService.connect();
							this.events.publish("reloadAppNotifications");
						}
					},
					error => {
						console.log("error: ", error);
					}
				)
				.then(() => {
					if (!_.isEmpty(this.user)) {
						this.user =
							JSON.parse(localStorage.getItem("app.user")) || {};
						async.parallel(
							[
								callback => {
									this.users.getCurrentUserFollowers().then(
										(data: any) => {
											if (data && data.success) {
												console.log(
													"getCurrentUserFollowers: ",
													data
												);
												this.user.followers =
													data.result;
											}
											callback();
										},
										error => {
											console.log("error: ", error);
											this.user.followers = [];
											callback();
										}
									);
								},
								callback => {
									this.users.getCurrentUserFollowing().then(
										(data: any) => {
											if (data && data.success) {
												console.log(
													"getCurrentUserFollowing: ",
													data
												);
												this.user.following =
													data.result;
											}
											callback();
										},
										error => {
											console.log("error: ", error);
											this.user.following = [];
											callback();
										}
									);
								}
							],
							() => {
								localStorage.setItem(
									"app.user",
									JSON.stringify(this.user)
								);
							}
						);
					}
				});
		}
	}

	viewCart() {
		// TODO: Check if Authenticated
		if (_.isEmpty(this.user)) {
			let modal = this.modalCtrl.create(LoginPage);
			modal.onDidDismiss(res => {
				if (res == "login") {
					this.getCurrentUser();
					this.showSignUp = false;
				}
			});
			modal.present();
			return;
		}

		this.app.getRootNav().push(ShoppingcartPage);
	}

	viewChat() {
		if (_.isEmpty(this.user)) {
			let modal = this.modalCtrl.create(LoginPage);
			modal.present();
			return;
		}

		let modal = this.modalCtrl.create(ChatPage);
		modal.present();
	}

	openSignUp() {
		// TODO: Check for authentication
		let modal = this.modalCtrl.create(SignupPage);
		modal.present();
	}

	openLogin() {
		// TODO: Check for authentication
		let modal = this.modalCtrl.create(LoginPage);
		modal.onDidDismiss(res => {
			if (res == "login") {
				this.getCurrentUser();
			}
		});
		modal.present();
	}
}
