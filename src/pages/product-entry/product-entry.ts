import { Component, ViewChild, ElementRef } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import {
  IonicPage,
  NavController,
  NavParams,
  ModalController,
  ViewController,
  Platform,
  LoadingController,
  ToastController,
  ActionSheetController,
  AlertController,
  normalizeURL
} from "ionic-angular";
// import { MultiImageUploadComponent } from "../../components/multi-image-upload/multi-image-upload";
import * as _ from "lodash";
import * as async from "async";

import { Camera, CameraOptions } from "@ionic-native/camera";
import { File, FileEntry, IFile } from "@ionic-native/file";
import { FileTransfer, FileTransferObject } from "@ionic-native/file-transfer";
import { Crop } from "@ionic-native/crop";

import { environment } from "@app/env";

import { ProductWholesaleEntryPage } from "../../pages/product-wholesale-entry/product-wholesale-entry";
import { ProductVariationEntryPage } from "../../pages/product-variation-entry/product-variation-entry";
import { ProductPackagingEntryPage } from "../../pages/product-packaging-entry/product-packaging-entry";
import { ProductShippingEntryPage } from "../../pages/product-shipping-entry/product-shipping-entry";
import { ContentFormPage } from "../../pages/content-form/content-form";

import { ProductsServiceProvider } from "../../providers/service/products";

@IonicPage()
@Component({
  selector: "page-product-entry",
  templateUrl: "product-entry.html"
})
export class ProductEntryPage {
  product: any = {};
  public serverUrl = environment.api_url + "products";

  public isUploading = false;
  public uploadingProgress = {};
  public uploadingHandler = {};
  public images: any = [];
  public filesToUpload: Array<File> = [];
  protected imagesValue: Array<any>;

  callback: any;
  title: string;
  action: string;
  productId: string;
  item: any = {};
  errArr: any[] = [];

  // @ViewChild(MultiImageUploadComponent) multiImageUpload: MultiImageUploadComponent;
  @ViewChild("myInput") myInput: ElementRef;
  constructor(
    public platform: Platform,
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public viewCtrl: ViewController,
    public products: ProductsServiceProvider,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    private sanitization: DomSanitizer,
    private actionSheetCtrl: ActionSheetController,
    private camera: Camera,
    private alertCtrl: AlertController,
    private crop: Crop,
    private file: File,
    private transfer: FileTransfer
  ) {
    this.product.wholesale = [];
    this.product.variations = [];
    this.product.shipping = [];
    this.product.package = {};
    this.product.condition = "new";
    this.product.preOrderDays = 7;

    this.action = navParams.get("action");
    this.item = navParams.get("product");
    this.callback = navParams.get("callback");
  }

  public uploadImages(productId): Promise<Array<any>> {
    return new Promise((resolve, reject) => {
      this.isUploading = true;
      Promise.all(
        this.filesToUpload.map(image => {
          return this.uploadImage(image, productId);
        })
      )
        .then(resolve)
        .catch(reason => {
          this.isUploading = false;
          this.uploadingProgress = {};
          this.uploadingHandler = {};
          reject(reason);
        });
    });
  }

  public abort() {
    if (!this.isUploading) return;
    this.isUploading = false;
    for (let key in this.uploadingHandler) {
      this.uploadingHandler[key].abort();
    }
  }

  // ======================================================================
  protected removeImage(image) {
    if (this.isUploading) {
      return;
    }

    this.confirm("Are you sure to remove it?").then(value => {
      if (value) {
        _.remove(this.images, {
          uuid: image.uuid
        });
        _.remove(this.filesToUpload, (row: any) => {
          return row.uuid == image.uuid;
        });

        this.removeFromArray(this.imagesValue, image);
        // this.removeFromArray(this.filesToUpload, image.url);
        if (image && image.Id) {
          this.deleteProductImage(image.Id);
        }
      }
    });
  }

  protected showAddImage() {
    const uuid = parseInt(new Date().getTime().toString());
    if (!this.platform.is("cordova")) {
      let input = document.createElement("input");
      input.type = "file";
      input.accept = "image/x-png,image/gif,image/jpeg";
      input.click();
      input.onchange = () => {
        let blob = window.URL.createObjectURL(input.files[0]);
        let blob2: any = input.files[0];
        blob2.uuid = uuid;

        this.images.push({
          url: blob,
          uuid: uuid
        });
        this.filesToUpload.push(blob2);

        console.log("this.filesToUpload: ", this.filesToUpload);
        this.trustImages();
      };
    } else {
      new Promise((resolve, reject) => {
        let actionSheet = this.actionSheetCtrl.create({
          title: "Add a photo",
          buttons: [
            {
              text: "From photo library",
              handler: () => {
                resolve(this.camera.PictureSourceType.PHOTOLIBRARY);
              }
            },
            {
              text: "From camera",
              handler: () => {
                resolve(this.camera.PictureSourceType.CAMERA);
              }
            },
            {
              text: "Cancel",
              role: "cancel",
              handler: () => {
                reject();
              }
            }
          ]
        });
        actionSheet.present();
      })
        .then(sourceType => {
          if (!this.platform.is("cordova")) return;

          let options: CameraOptions = {
            quality: 100,
            sourceType: sourceType as number,
            saveToPhotoAlbum: false,
            correctOrientation: true
          };

          this.camera.getPicture(options).then(imagePath => {
            this.crop
              .crop(imagePath, {
                quality: 100,
                targetWidth: 600,
                targetHeight: 600
              })
              .then(
                (newImage: any) => {
                  console.log("new image path is: " + newImage);
                  this.file.resolveLocalFilesystemUrl(imagePath).then(
                    (entry: FileEntry) => {
                      entry.file(
                        (meta: IFile) => {
                          if (this.platform.is("ios")) {
                            this.images.push({
                              url: normalizeURL(imagePath),
                              file: meta,
                              size: meta.size,
                              name: meta.name,
                              uuid: uuid
                            });
                          } else {
                            this.images.push({
                              url: imagePath,
                              file: meta,
                              size: meta.size,
                              name: meta.name,
                              uuid: uuid
                            });
                          }
                          this.filesToUpload.push(imagePath);
                          this.trustImages();
                        },
                        error => {
                          console.error("error getting fileentry file!", error);
                        }
                      );
                    },
                    error => {
                      console.error("error getting file! ", error);
                    }
                  );
                },
                error => {
                  console.error("Error cropping image", error);
                }
              );
          });
        })
        .catch(() => {});
    }
  }

  private uploadImage(targetPath, productId) {
    return new Promise((resolve, reject) => {
      this.uploadingProgress[targetPath] = 0;
      var uploadUrl = this.serverUrl + "/" + productId + "/image";
      if (this.platform.is("cordova")) {
        let options: any = {
          fileKey: "image",
          chunkedMode: false,
          httpMethod: "PUT",
          headers: {
            Authorization: "Bearer " + localStorage.getItem("app.token")
          }
        };

        const fileTransfer: FileTransferObject = this.transfer.create();
        this.uploadingHandler[targetPath] = fileTransfer;

        this.file
          .resolveLocalFilesystemUrl(targetPath)
          .then((info: any) => {
            console.log("file info: ", info);
            options.fileName = info.name;
          })
          .then(() => {
            fileTransfer
              .upload(targetPath, uploadUrl, options, false)
              .then(data => {
                resolve(JSON.parse(data.response));
              })
              .catch(error => {
                console.log("error: ", error);
                if (error) {
                  let err = JSON.parse(error.body);
                  console.log("err: ", err);
                }
                askRetry();
              });

            fileTransfer.onProgress(event2 => {
              this.uploadingProgress[targetPath] =
                (event2.loaded * 100) / event2.total;
            });
          });
      } else {
        let formData: FormData = new FormData(),
          xhr2: XMLHttpRequest = new XMLHttpRequest();

        formData.append("image", targetPath);
        this.uploadingHandler[targetPath] = xhr2;

        xhr2.onreadystatechange = () => {
          if (xhr2.readyState === 4) {
            if (xhr2.status === 200) resolve(JSON.parse(xhr2.response));
            else askRetry();
          }
        };

        xhr2.upload.onprogress = event => {
          const loadingProgress = (event.loaded * 100) / event.total;
          console.log("loadingProgress: ", loadingProgress);
          this.uploadingProgress[targetPath] = loadingProgress;
          console.log("this.uploadingProgress: ", this.uploadingProgress);
        };
        xhr2.open("PUT", uploadUrl, true);
        xhr2.setRequestHeader(
          "Authorization",
          "Bearer " + localStorage.getItem("app.token")
        );
        xhr2.send(formData);
      }

      let askRetry = () => {
        // might have been aborted
        if (!this.isUploading) return reject(null);
        this.confirm("Do you wish to retry?", "Upload failed").then(res => {
          if (!res) {
            this.isUploading = false;
            for (let key in this.uploadingHandler) {
              this.uploadingHandler[key].abort();
            }
            return reject(null);
          } else {
            if (!this.isUploading) return reject(null);
            this.uploadImage(targetPath, productId).then(resolve, reject);
          }
        });
      };
    });
  }

  private removeFromArray<T>(array: Array<T>, item: T) {
    let index: number = array.indexOf(item);
    if (index !== -1) {
      array.splice(index, 1);
    }
  }

  private confirm(text, title = "", yes = "Yes", no = "No") {
    return new Promise(resolve => {
      this.alertCtrl
        .create({
          title: title,
          message: text,
          buttons: [
            {
              text: no,
              role: "cancel",
              handler: () => {
                resolve(false);
              }
            },
            {
              text: yes,
              handler: () => {
                resolve(true);
              }
            }
          ]
        })
        .present();
    });
  }

  private trustImages() {
    this.imagesValue = this.images.map(val => {
      return {
        url: val.url,
        uuid: val.uuid,
        sanitized: this.sanitization.bypassSecurityTrustStyle(
          "url(" + val.url + ")"
        )
      };
    });
    console.log("this.imagesValue: ", this.imagesValue);
  }

  private trustImages2() {
    this.imagesValue = this.images.map(val => {
      return {
        url: val.url,
        Id: val.Id,
        sanitized: this.sanitization.bypassSecurityTrustStyle(
          "url(" + val.url + ")"
        )
      };
    });
    console.log("this.imagesValue: ", this.imagesValue);
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad ProductEntryPage");
    if (this.action == "update") {
      this.title = "Update Product";

      let loading = this.loadingCtrl.create();
      loading.present();
      this.products.getProductInfo(this.item.uuid).then(
        (data: any) => {
          if (data && data.success) {
            this.product = data.result;

            this.product.wholesale = [];
            this.product.variations = [];
            this.product.shipping = [];
            this.product.package = {};

            this.product.name = _.clone(this.product.productName);
            this.product.desc = _.clone(this.product.productDesc);
            this.product.category = {
              category: this.product.category,
              subcategory: this.product.subcategory
            };
            this.product.categoryId = this.product.category.category.categoryId;
            this.product.subcategoryId = this.product.category.subcategory.subcategoryId;
            this.product.brandId = this.product.brand.brandId;
            this.product.price = _.clone(this.product.productPrice);
            this.product.stock = _.clone(this.product.productStock);
            this.product.weight = _.clone(this.product.productWeight);
            this.product.condition = _.clone(this.product.productCondition);

            this.product.variations = _.clone(this.product.variants);

            this.product.package.width = _.clone(this.product.productWidth);
            this.product.package.height = _.clone(this.product.productHeight);
            this.product.package.length = _.clone(this.product.productLength);

            this.product.length = _.clone(this.product.productLength);
            this.product.width = _.clone(this.product.productWidth);
            this.product.height = _.clone(this.product.productHeight);

            this.product.shipping = _.clone(this.product.shippingFee);

            if (this.platform.is("ios")) {
              this.product.date_created = new Date(
                this.product.date_created.replace(/-/g, "/")
              );
            }

            _.each(this.product.images, (row: any) => {
              this.images.push({
                url: row.img_path,
                Id: row.productImagesId
              });
            });
            this.trustImages2();
            console.log("product: ", this.product);
          }
          loading.dismiss();
        },
        error => {
          let alert = this.alertCtrl.create({
            subTitle: "The product failed to load!",
            buttons: ["OK"]
          });
          alert.present();
          loading.dismiss();
        }
      );
    } else if (this.action == "create") {
      this.title = "Add Product";
    }
  }

  ionViewWillEnter() {
    this.viewCtrl.showBackButton(false);
  }

  resize() {
    // this.myInput.nativeElement.style.height = this.myInput.nativeElement.scrollHeight + 'px';
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  openWholeSale() {
    console.log("openWholeSale");
    if (_.isEmpty(this.product.price)) {
      let toast = this.toastCtrl.create({
        message: "Please provide Product price",
        duration: 2000
      });
      toast.present();
      return;
    }

    this.navCtrl.push(ProductWholesaleEntryPage, {
      price: this.product.price,
      wholesaleArr: this.product.wholesale,
      callback: this.getWholeSaleData
    });
  }

  openVariation() {
    this.navCtrl.push(ProductVariationEntryPage, {
      variantsArr: this.product.variations,
      callback: this.getVariationData
    });
  }

  openPackaging() {
    this.navCtrl.push(ProductPackagingEntryPage, {
      callback: this.getPackagingData,
      package: this.product.package
    });
  }

  openShippingRate() {
    if (_.isEmpty(this.product.weight)) {
      let toast = this.toastCtrl.create({
        message: "Please provide Product weight",
        duration: 2000
      });
      toast.present();
      return;
    }

    this.navCtrl.push(ProductShippingEntryPage, {
      callback: this.getShippingData,
      shippingArr: this.product.shipping
    });
  }

  openCategory() {
    this.navCtrl.push(ContentFormPage, {
      action: 'category',
      isProduct: true,
      callback: this.getProductCategory
    });
  }

  openBrands() {
    this.navCtrl.push(ContentFormPage, {
      action: 'brands',
      isProduct: true,
      callback: this.getProductBrand,
      selected: this.product.brand
    });
  }

  getProductCategory = (data: any) => {
    let ctrl = this;
    console.log("data: ", data);
    return new Promise((resolve, reject) => {
      ctrl.product.category = data;

      if (data.category) {
        ctrl.product.categoryId = data.category.categoryId;
      }
      if (data.subcategory) {
        ctrl.product.subcategoryId = data.subcategory.subcategoryId;
      }
      resolve();
    });
  };

  getProductBrand = data => {
    let ctrl = this;
    console.log("data: ", data);
    return new Promise((resolve, reject) => {
      ctrl.product.brand = data;
      ctrl.product.brandId = data.brandId;
      resolve();
    });
  };

  getWholeSaleData = data => {
    console.log("data: ", data);
    let ctrl = this;
    return new Promise((resolve, reject) => {
      ctrl.product.wholesale = data;
      resolve();
    });
  };

  getVariationData = data => {
    console.log("data: ", data);
    let ctrl = this;
    return new Promise((resolve, reject) => {
      ctrl.product.variations = data;
      resolve();
    });
  };

  getPackagingData = data => {
    let ctrl = this;
    return new Promise((resolve, reject) => {
      ctrl.product.package = data;
      ctrl.product.length = data.length;
      ctrl.product.width = data.width;
      ctrl.product.height = data.height;
      resolve();
    });
  };

  getShippingData = data => {
    console.log("data: ", data);
    let ctrl = this;
    return new Promise((resolve, reject) => {
      ctrl.product.shipping = data;
      resolve();
    });
  };

  private saveProduct() {
    console.log("product: ", this.product);
    let loading = this.loadingCtrl.create({
      content: "Saving..."
    });
    loading.present();

    let productId: any;

    async.waterfall(
      [
        callback => {
          this.products.saveProduct(this.product).then(
            (data: any) => {
              if (data && data.success) {
                productId = data.result;
                console.log("products: ", data.msg);
                callback(null, productId);
              }
            },
            error => {
              console.log("saveProduct error: ", error);
              loading.dismiss();
              let err = error.data;
              console.log("saveProduct err: ", err);
              this.errArr = err.result;
              console.log("saveProduct errArr: ", this.errArr);
              /*setTimeout(() => {
						this.errArr = [];
					}, 3000);*/
              return;
            }
          );
        },
        (productId, callback) => {
          console.log("productId: ", productId);
          this.uploadImages(productId).then(
            (data: any) => {
              console.log("uploadImages: ", data);
              callback();
            },
            error => {
              console.log("saveProduct error: ", error);
              loading.dismiss();
              return;
            }
          );
        },
        callback => {
          // VARIATIONS
          console.log("VARIATIONS productId: ", productId);
          this.products.mailCreatedProduct(productId).then(
            (data: any) => {
              if (data && data.success) {
                console.log("Mail success: ", data.msg);
              }
            },
            error => {
              console.log("Mail Error: ", error);
            }
          );

          if (!_.isEmpty(this.product.variations)) {
            async.eachSeries(
              this.product.variations,
              (item, cb) => {
                this.products.saveProductVariants(productId, item).then(
                  (data: any) => {
                    if (data && data.success) {
                      console.log("variants: ", data.msg);
                      cb();
                    }
                  },
                  error => {
                    console.log("saveProductVariants error: ", error);
                    loading.dismiss();
                    return;
                  }
                );
              },
              () => {
                callback();
              }
            );
          } else {
            callback();
          }
        },
        callback => {
          // WHOLESALE
          if (!_.isEmpty(this.product.wholesale)) {
            async.eachSeries(
              this.product.wholesale,
              (item, cb) => {
                this.products.saveProductWholesale(productId, item).then(
                  (data: any) => {
                    if (data && data.success) {
                      console.log("wholesale: ", data.msg);
                      cb();
                    }
                  },
                  error => {
                    console.log("saveProductWholesale error: ", error);
                    loading.dismiss();
                    return;
                  }
                );
              },
              () => {
                callback();
              }
            );
          } else {
            callback();
          }
        },
        callback => {
          // SHIPPING
          if (!_.isEmpty(this.product.shipping)) {
            async.eachSeries(
              this.product.shipping,
              (item, cb) => {
                this.products.saveProductShipping(productId, item).then(
                  (data: any) => {
                    if (data && data.success) {
                      console.log("shipping: ", data.msg);
                      cb();
                    }
                  },
                  error => {
                    console.log("saveProductShipping error: ", error);
                    loading.dismiss();
                    return;
                  }
                );
              },
              () => {
                callback();
              }
            );
          } else {
            callback();
          }
        }
      ],
      () => {
        loading.dismiss();
        // this.viewCtrl.dismiss("save");
        this.callback("save").then(() => {
          this.viewCtrl.dismiss("save");
        });
      }
    );
  }

  private updateProduct() {
    let loading = this.loadingCtrl.create({
      content: "Updating..."
    });
    loading.present();

    let productId: any;
    async.waterfall(
      [
        callback => {
          productId = this.product.productId;
          this.products
            .updateProduct(this.product.productId, this.product)
            .then(
              (data: any) => {
                if (data && data.success) {
                  console.log("products: ", data.msg);
                  callback();
                }
              },
              error => {
                console.log("updateProduct error: ", error);
                loading.dismiss();
                let err = error.data;
                console.log("updateProduct err: ", err);
                this.errArr = err.result;
                console.log("updateProduct this.errArr: ", this.errArr);

                if (this.errArr) {
                  this.alertCtrl
                    .create({
                      title: "WARNING",
                      message: "An error occur while updating.",
                      buttons: ["OK"]
                    })
                    .present();
                }
                return;
              }
            );
        },
        callback => {
          this.uploadImages(this.product.productId)
            .then(
              (data: any) => {
                console.log("uploadImages: ", data);
                callback();
              },
              error => {
                console.log("updateProduct error: ", error);
                loading.dismiss();
                return;
              }
            )
            .catch(error => {
              console.log("updateProduct error: ", error);
              loading.dismiss();
              return;
            });
        },
        callback => {
          // VARIATIONS
          if (!_.isEmpty(this.product.variations)) {
            async.waterfall(
              [
                callback1 => {
                  this.products
                    .deleteAllProductVariations(this.product.productId)
                    .then(
                      (data: any) => {
                        if (data && data.success) {
                          console.log("deleteAllProductVariations: ", data.msg);
                          callback1();
                        }
                      },
                      error => {
                        console.log("saveProductVariants error: ", error);
                        loading.dismiss();
                        return;
                      }
                    );
                },
                callback1 => {
                  async.eachSeries(
                    this.product.variations,
                    (item, cb) => {
                      this.products
                        .saveProductVariants(this.product.productId, item)
                        .then(
                          (data: any) => {
                            if (data && data.success) {
                              console.log("variants: ", data.msg);
                              cb();
                            }
                          },
                          error => {
                            console.log("saveProductVariants error: ", error);
                            loading.dismiss();
                            return;
                          }
                        );
                    },
                    () => {
                      callback1();
                    }
                  );
                }
              ],
              () => {
                callback();
              }
            );
          } else {
            callback();
          }
        },
        callback => {
          // WHOLESALE
          if (!_.isEmpty(this.product.wholesale)) {
            async.waterfall(
              [
                callback1 => {
                  this.products
                    .deleteAllProductWholeSale(this.product.productId)
                    .then(
                      (data: any) => {
                        if (data && data.success) {
                          console.log("wholesale: ", data.msg);
                          callback1();
                        }
                      },
                      error => {
                        console.log("saveProductVariants error: ", error);
                        loading.dismiss();
                        return;
                      }
                    );
                },
                callback1 => {
                  async.eachSeries(
                    this.product.wholesale,
                    (item, cb) => {
                      this.products
                        .saveProductWholesale(this.product.productId, item)
                        .then(
                          (data: any) => {
                            if (data && data.success) {
                              console.log("wholesale: ", data.msg);
                              cb();
                            }
                          },
                          error => {
                            console.log("saveProductWholesale error: ", error);
                            loading.dismiss();
                            return;
                          }
                        );
                    },
                    () => {
                      callback1();
                    }
                  );
                }
              ],
              () => {
                callback();
              }
            );
          } else {
            callback();
          }
        },
        callback => {
          // SHIPPING
          if (!_.isEmpty(this.product.shipping)) {
            async.waterfall(
              [
                callback1 => {
                  this.products
                    .deleteProductShipping(this.product.productId)
                    .then(
                      (data: any) => {
                        if (data && data.success) {
                          console.log("shipping: ", data.msg);
                          callback1();
                        }
                      },
                      error => {
                        console.log("saveProductShipping error: ", error);
                        loading.dismiss();
                        return;
                      }
                    );
                },
                callback1 => {
                  async.eachSeries(
                    this.product.shipping,
                    (item, cb) => {
                      this.products
                        .saveProductShipping(this.product.productId, item)
                        .then(
                          (data: any) => {
                            if (data && data.success) {
                              console.log("shipping: ", data.msg);
                              cb();
                            }
                          },
                          error => {
                            console.log("saveProductShipping error: ", error);
                            loading.dismiss();
                            return;
                          }
                        );
                    },
                    () => {
                      callback1();
                    }
                  );
                }
              ],
              () => {
                callback();
              }
            );
          } else {
            callback();
          }
        }
      ],
      () => {
        loading.dismiss();
        // this.viewCtrl.dismiss("save");
        this.callback("save").then(() => {
          this.viewCtrl.dismiss("save");
        });
      }
    );
  }

  saveEntry() {
    const confirm = this.alertCtrl.create({
      title: "Save product entry?",
      message: "Do you want to save or update the product entry?",
      buttons: [
        {
          text: "Cancel",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Confirm",
          handler: () => {
            if (this.action == "update") {
              this.updateProduct();
            } else if (this.action == "create") {
              this.saveProduct();
            }
          }
        }
      ]
    });
    confirm.present();
  }

  deleteProductImage(imageId) {
    let loading = this.loadingCtrl.create({
      content: "Deleting..."
    });
    loading.present();
    this.products.deleteProductImage(this.product.productId, imageId).then(
      (data: any) => {
        if (data && data.success) {
          console.log("data: ", data);
        }
        loading.dismiss();
      },
      error => {
        console.log("deleteProductImage error: ", error);
        loading.dismiss();
      }
    );
  }
}
