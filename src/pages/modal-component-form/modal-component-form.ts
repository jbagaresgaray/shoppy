import { Component, EventEmitter, ViewEncapsulation } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController
} from "ionic-angular";
import * as _ from "lodash";

import { ServiceProvider } from "../../providers/service/service";

@IonicPage()
@Component({
  selector: "page-modal-component-form",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "modal-component-form.html"
})
export class ModalComponentFormPage {
  modalIsInset: boolean;
  enableBuying: boolean;
  hasSelected: boolean;

  isLow: boolean = true;

  title: string;
  priceTitle: string;
  stockCount: string;
  action: string;
  searchMode: string = "popular";

  image: any = {};
  details: any = {};
  user: any = {};
  currency: any = {};
  variants: any[] = [];
  wholesale: any[] = [];
  shippingFee: any[] = [];

  refreshNotification: EventEmitter<any> = new EventEmitter();
  createNotification: EventEmitter<any> = new EventEmitter();
  deleteNotification: EventEmitter<any> = new EventEmitter();

  getItemsNotification: EventEmitter<any> = new EventEmitter();
  clearItemsNotification: EventEmitter<any> = new EventEmitter();
  segmentChangedNotification: EventEmitter<any> = new EventEmitter();
  sortPriceNotification: EventEmitter<any> = new EventEmitter();

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public services: ServiceProvider
  ) {
    this.user = JSON.parse(localStorage.getItem("app.user")) || {};
    this.currency = this.services.defaultCurrency();

    this.modalIsInset = navParams.get("modalInset");
    this.action = navParams.get("action");

    if (navParams.get("image")) {
      this.image = navParams.get("image");
    }

    if (navParams.get("details")) {
      this.details = navParams.get("details");
      console.log("this.details: ", this.details);

      if (!_.isEmpty(this.user)) {
        if (this.details.userId == this.user.userId) {
          this.enableBuying = false;
        } else {
          this.enableBuying = true;
        }
      }

      if (this.details) {
        this.details.offerPrice = _.clone(this.details.price);
      }
    }

    if (navParams.get("variants")) {
      this.variants = navParams.get("variants");

      const minPrice: any = _.minBy(this.variants, (row: any) => {
        return row.price;
      });
      const maxPrice: any = _.maxBy(this.variants, (row: any) => {
        return row.price;
      });
      const stockCount: any = _.sumBy(this.variants, (row: any) => {
        return row.stock;
      });
      this.stockCount = stockCount;

      if (this.action == "product_variants_modal") {
        if (minPrice.price == maxPrice.price) {
          this.priceTitle = minPrice.price;
        } else {
          this.priceTitle = minPrice.price + "-" + maxPrice.price;
        }
      } else if (this.action == "product_vouchers_modal") {
        this.priceTitle = this.details.productName;
      } else if (this.action == "make_offer_modal") {
        if (minPrice.price == maxPrice.price) {
          this.priceTitle = minPrice.price;
        } else {
          this.priceTitle = minPrice.price + "-" + maxPrice.price;
        }
      }
    }

    if (navParams.get("wholesale")) {
      this.wholesale = navParams.get("wholesale");
    }

    if (navParams.get("shippingFee")) {
      this.shippingFee = navParams.get("shippingFee");
      console.log("this.shippingFee: ", this.shippingFee);
    }
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad ModalComponentFormPage");
    switch (this.action) {
      case "product_picker_modal":
        this.title = "Selected Products";
        break;
      case "make_offer_modal":
        this.title = "Make Offer";
        break;
      case "product_vouchers_modal":
        this.title = "Shop Vouchers";
        break;
      case "product-wholesale-modal":
        this.title = "Wholesale Price";
        break;
      case "product-shipping-modal":
        this.title = "Shipping Fee Information";
        break;
      default:
        break;
    }
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  doRefresh(ev: any) {
    this.refreshNotification.emit(ev);
  }

  lessQuantity() {}

  addQuantity() {}

  addToCart() {}

  makeOffer() {}

  confirm() {
    this.createNotification.emit();
  }

  getItems(ev: any) {
    this.getItemsNotification.emit(ev);
  }

  onClear() {
    this.clearItemsNotification.emit(true);
  }

  segmentChanged(ev) {
    this.segmentChangedNotification.emit(ev);
  }

  sortPrice() {
    this.isLow = !this.isLow;
    this.sortPriceNotification.emit();
  }

  close() {
    this.viewCtrl.dismiss();
  }
}
