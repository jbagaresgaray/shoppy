import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { ComponentsModule } from "../../components/components.module";

import { ModalComponentFormPage } from "./modal-component-form";

@NgModule({
  declarations: [ModalComponentFormPage],
  imports: [ComponentsModule, IonicPageModule.forChild(ModalComponentFormPage)]
})
export class ModalComponentFormPageModule {}
