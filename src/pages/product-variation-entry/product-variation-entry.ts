import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, Content } from 'ionic-angular';
import * as _ from 'lodash';

/**
 * Generated class for the ProductVariationEntryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-product-variation-entry',
  templateUrl: 'product-variation-entry.html',
})
export class ProductVariationEntryPage {
  variantsArr: any = [];
  item: any = {};

  callback: any;
  @ViewChild(Content) content: Content;
  constructor(public navCtrl: NavController, public navParams: NavParams, public toastCtrl: ToastController) {
    this.callback = this.navParams.get('callback');

    if (this.navParams.get('variantsArr')) {
      this.variantsArr = this.navParams.get('variantsArr');
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductVariationEntryPage');
  }

  addVariants() {
    if (_.isEmpty(this.item.name)) {
      let toast = this.toastCtrl.create({
        message: 'Please enter variant name!',
        duration: 1000
      });
      toast.present();
      return;
    }

    if (_.isEmpty(this.item.price)) {
      let toast = this.toastCtrl.create({
        message: 'Please enter price!',
        duration: 1000
      });
      toast.present();
      return;
    }

    if (_.isEmpty(this.item.stock)) {
      let toast = this.toastCtrl.create({
        message: 'Please enter stock amount!',
        duration: 1000
      });
      toast.present();
      return;
    }

    this.variantsArr.push(this.item);
    this.item = {
      name: "",
      price: "",
      stock: ""
    };
    this.content.scrollToBottom();
    console.log('this.variantsArr: ', this.variantsArr);
  }

  removeVariant(index) {
    this.variantsArr.splice(index, 1);
  }

  saveProduct() {
    if (!_.isEmpty(this.item.name)) {
      this.variantsArr.push(this.item);
    }

    this.callback(this.variantsArr).then(() => { this.navCtrl.pop() });
  }

  dismiss() {
    this.navCtrl.pop();
  }
}
