import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProductVariationEntryPage } from './product-variation-entry';

@NgModule({
  declarations: [
    ProductVariationEntryPage,
  ],
  imports: [
    IonicPageModule.forChild(ProductVariationEntryPage),
  ],
})
export class ProductVariationEntryPageModule {}
