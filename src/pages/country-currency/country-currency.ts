import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import * as _ from 'lodash';

import { ServiceProvider } from '../../providers/service/service';
import { UsersServiceProvider } from '../../providers/service/users';

@IonicPage()
@Component({
	selector: 'page-country-currency',
	templateUrl: 'country-currency.html',
})
export class CountryCurrencyPage {
	itemArr: any[] = [];
	itemArrCopy: any[] = [];

	groupedArr: any[] = [];
	fakeArr: any[] = [];

	user: any = {};

	type: any;
	title: string;
	myInput: any;

	showContent: boolean = false;

	constructor(public navCtrl: NavController, public navParams: NavParams, public services: ServiceProvider, public users: UsersServiceProvider, public loadingCtrl: LoadingController, public alertCtrl: AlertController) {
		this.type = navParams.get('type');
		for (var i = 0; i < 12; ++i) {
			this.fakeArr.push(i);
		}
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad CountryCurrencyPage');
		if (this.type === 'country') {
			this.title = 'Country/Region';
		} else if (this.type === 'currency') {
			this.title = 'Currency';
		}

		this.user = JSON.parse(localStorage.getItem('app.user')) || {};
		if (!_.isEmpty(this.user)) {
			this.users.getCurrentUser().then((data: any) => {
				if (data && data.success) {
					this.user = data.result;
				}
			});
		}

		this.initData();
	}

	initData(ev?: any) {
		this.showContent = false;
		if (this.type === 'country') {
			this.services.getRestCountries().then((data: any) => {
				if (data) {
					this.itemArr = data;
					this.itemArrCopy = data;

					_.each(this.itemArr, (row: any) => {
						row.selected = false;
						if (row.alpha3Code === this.user.countryId) {
							row.selected = true;
						}
					});

					_.each(this.itemArrCopy, (row: any) => {
						row.selected = false;
						if (row.alpha3Code === this.user.countryId) {
							row.selected = true;
						}
					});

					this.groupContacts(this.itemArr);
				}
				this.showContent = true;
				if (ev) {
					ev.complete();
				}
			}, (error) => {
				this.showContent = true;
				if (ev) {
					ev.complete();
				}
			});
		} else if (this.type === 'currency') {
			this.services.getCurrencies().then((data: any) => {
				if (data && data.success) {
					this.itemArr = data.result;
					this.itemArrCopy = data.result;

					_.each(this.itemArr, (row: any) => {
						row.selected = false;
						if (row.code === this.user.currencyId) {
							row.selected = true;
						}
					});

					_.each(this.itemArrCopy, (row: any) => {
						row.selected = false;
						if (row.code === this.user.currencyId) {
							row.selected = true;
						}
					});

					this.groupContacts(this.itemArr);
				}
				this.showContent = true;
				if (ev) {
					ev.complete();
				}
			}, (error) => {
				this.showContent = true;
				if (ev) {
					ev.complete();
				}
			});
		}
	}


	doRefresh(ev) {
		this.initData(ev);
	}

	groupContacts(contacts) {
		// let sortedContacts = contacts.sort();
		let sortedContacts = _.orderBy(contacts, ['name'], ['asc']);
		let currentLetter = false;
		let currentContacts = [];

		sortedContacts.forEach((value, index) => {

			if (value.name.charAt(0) != currentLetter) {

				currentLetter = value.name.charAt(0);

				let newGroup = {
					letter: currentLetter,
					itemArr: []
				};

				currentContacts = newGroup.itemArr;
				this.groupedArr.push(newGroup);
			}

			currentContacts.push(value);

		});
	}

	private setCountry(value) {
		let loading = this.loadingCtrl.create({
			dismissOnPageChange: true,
			content: 'Setting country...'
		});
		loading.present();
		this.users.updateUserCountry({
			country: value
		}).then((data: any) => {
			if (data && data.success) {
				loading.dismiss();

				let alert = this.alertCtrl.create({
					title: 'Success!',
					subTitle: data.msg,
					buttons: ['OK']
				});
				alert.onDidDismiss(() => {
					this.navCtrl.pop();
				});
				alert.present();
			} else {
				loading.dismiss();
				let alert = this.alertCtrl.create({
					title: 'Error!',
					subTitle: data.msg,
					buttons: ['OK']
				});
				alert.present();
			}
		}, (error) => {
			loading.dismiss();
			console.log('error: ', error);
		});
	}

	private setCurrency(value) {
		let loading = this.loadingCtrl.create({
			dismissOnPageChange: true,
			content: 'Setting currency...'
		});
		loading.present();
		this.users.updateUserCurrency({
			currency: value
		}).then((data: any) => {
			if (data && data.success) {
				loading.dismiss();

				let alert = this.alertCtrl.create({
					title: 'Success!',
					subTitle: data.msg,
					buttons: ['OK']
				});
				alert.onDidDismiss(() => {
					this.navCtrl.pop();
				});
				alert.present();
			} else {
				loading.dismiss();
				let alert = this.alertCtrl.create({
					title: 'Error!',
					subTitle: data.msg,
					buttons: ['OK']
				});
				alert.present();
			}
		}, (error) => {
			loading.dismiss();
			console.log('error: ', error);
		});
	}

	select(item: any) {
		if (this.type === 'country') {
			this.setCountry(item.alpha3Code);
		} else if (this.type === 'currency') {
			this.setCurrency(item.code);
		}
	}

	onCancel(ev) {
		this.groupContacts(this.itemArrCopy);
	}

	getItems(ev) {
		// set val to the value of the searchbar
		let val = ev.target.value;

		// if the value is an empty string don't filter the items
		if (val && val.trim() != '') {
			this.itemArr = this.itemArrCopy.filter((item) => {
				return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
			})
		} else {
			this.itemArr = this.itemArrCopy;
		}
		this.groupedArr = [];
		this.groupContacts(this.itemArr);
	}

}
