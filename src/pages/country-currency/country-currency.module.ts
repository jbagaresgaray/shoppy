import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CountryCurrencyPage } from './country-currency';

@NgModule({
  declarations: [
    CountryCurrencyPage,
  ],
  imports: [
    IonicPageModule.forChild(CountryCurrencyPage),
  ],
})
export class CountryCurrencyPageModule {}
