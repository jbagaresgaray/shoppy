import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { SignupPage } from "./signup";

import { ComponentsModule } from "../../components/components.module";
import { PipesModule } from "../../pipes/pipes.module";

@NgModule({
	declarations: [SignupPage],
	imports: [
		IonicPageModule.forChild(SignupPage),
		ComponentsModule,
		PipesModule
	]
})
export class SignupPageModule {}
