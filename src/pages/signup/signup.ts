import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController, LoadingController, AlertController, Events } from 'ionic-angular';
import { AuthService } from "angular4-social-login";
import { FacebookLoginProvider, GoogleLoginProvider } from "angular4-social-login";
import { GooglePlus } from '@ionic-native/google-plus';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';

import { environment } from '@app/env';

import { UsersServiceProvider } from '../../providers/service/users';
import { AuthProvider } from '../../providers/service/authentication';

import { TabsPage } from '../../pages/tabs/tabs';

@IonicPage()
@Component({
	selector: 'page-signup',
	templateUrl: 'signup.html',
})
export class SignupPage {
	user: any = {};
	errArr: any[] = [];

	constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public users: UsersServiceProvider, public toastCtrl: ToastController, public loadingCtrl: LoadingController,
		public alertCtrl: AlertController, private authService: AuthService, private fb: Facebook, private googlePlus: GooglePlus, public events: Events, public auth: AuthProvider, ) {
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad SignupPage');
	}

	save() {
		let loading = this.loadingCtrl.create();
		loading.present();
		this.users.signUpUser(this.user).then((data: any) => {
			if (data && data.success) {
				console.log('data: ', data);
				loading.dismiss();

				let alert = this.alertCtrl.create({
					title: 'Success',
					subTitle: data.msg,
					buttons: ['OK']
				});
				alert.onDidDismiss(() => {
					this.navCtrl.setRoot(TabsPage, null, { animate: true, direction: 'back' });
				});
				alert.present();
			} else {
				loading.dismiss();

				let alert = this.alertCtrl.create({
					title: 'Error',
					subTitle: data.msg,
					buttons: ['OK']
				});
				alert.present();
				return;
			}
		}, (error) => {
			loading.dismiss();
			console.log('error: ', error);
			let err = error.data;
			this.errArr = err.result;
			setTimeout(() => {
				this.errArr = [];
			}, 3000);
		});
	}

	close() {
		this.viewCtrl.dismiss();
	}

	private socialAuth(resp: any, loading?: any) {
		let vm = this;
		vm.auth.socialAuth({
			provider: resp.provider,
			Id: resp.id
		}).then((data: any) => {
			if (data && data.success) {
				localStorage.setItem('app.user', JSON.stringify(data.result.user));
				localStorage.setItem('app.token', data.result.token);

				vm.events.publish('app.login');

				loading.dismiss();
				setTimeout(() => {
					vm.viewCtrl.dismiss('login');
				}, 1000);
			} else if (data && !data.success) {
				loading.dismiss();

				let alert = vm.alertCtrl.create({
					title: 'Error',
					subTitle: data.msg,
					buttons: ['OK']
				});
				alert.present();
				return;
			}
		}, (error) => {
			console.log('error: ', error);
			loading.dismiss();
		});
	}

	signUpWithGoogle() {
		let loading = this.loadingCtrl.create();
		loading.present();
		if (window['cordova']) {
			this.googlePlus.login({
				webClientId: environment.GoogleClientID
			}).then(profile => {
				console.log('user: ', profile);
				if (profile) {
					this.users.socialSignUp({
						provider: 'google',
						email: profile.email,
						username: profile.email,
						firstname: profile.givenName,
						lastname: profile.familyName || profile.name,
						img_path: profile.imageUrl,
						Id: profile.userId
					}).then((resp: any) => {
						if (resp && resp.success) {
							let alert = this.alertCtrl.create({
								title: 'Success',
								subTitle: resp.msg,
								buttons: ['OK']
							});
							alert.onDidDismiss(() => {
								this.navCtrl.setRoot(TabsPage, null, { animate: true, direction: 'back' });
							});
							alert.present();
						} else {
							let alert = this.alertCtrl.create({
								title: 'Warning',
								subTitle: resp.msg,
								buttons: ['OK']
							});
							alert.present();
						}
						loading.dismiss();
					}, (error) => {
						loading.dismiss();
					});
				} else {
					loading.dismiss();
				}
			}).catch(err => {
				loading.dismiss();
				console.error(err);
			});
		} else {
			this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then((data: any) => {
				console.log('signInWithGoogle: ', data);
				if (data) {
					this.users.socialSignUp({
						provider: data.provider,
						email: data.email,
						username: data.email,
						firstname: data.givenName || data.name,
						lastname: data.familyName || data.name,
						img_path: data.photoUrl,
						Id: data.id
					}).then((resp: any) => {
						if (resp && resp.success) {
							let alert = this.alertCtrl.create({
								title: 'Success',
								subTitle: resp.msg,
								buttons: ['OK']
							});
							alert.onDidDismiss(() => {
								this.navCtrl.setRoot(TabsPage, null, { animate: true, direction: 'back' });
							});
							alert.present();
						} else {
							let alert = this.alertCtrl.create({
								title: 'Warning',
								subTitle: resp.msg,
								buttons: ['OK']
							});
							alert.present();
						}
						loading.dismiss();
					}, (error) => {
						loading.dismiss();
					});
				} else {
					loading.dismiss();
				}
			}).catch((err) => {
				loading.dismiss();
			});
		}
	}

	signUpWithFB() {
		let loading = this.loadingCtrl.create();
		loading.present();
		if (window['cordova']) {
			this.fb.login(['public_profile', 'user_friends', 'email']).then((res: FacebookLoginResponse) => {
				this.fb.api('me?fields=id,name,email,first_name,last_name,picture.width(720).height(720).as(picture_large)', []).then(profile => {
					console.log('profile: ', profile);
					if (profile) {
						this.users.socialSignUp({
							provider: 'facebook',
							email: profile.email,
							username: profile.email,
							firstname: profile.first_name,
							lastname: profile.last_name,
							img_path: profile['picture_large']['data']['url'],
							Id: profile.id
						}).then((resp: any) => {
							if (resp && resp.success) {
								let alert = this.alertCtrl.create({
									title: 'Success',
									subTitle: resp.msg,
									buttons: ['OK']
								});
								alert.onDidDismiss(() => {
									this.navCtrl.setRoot(TabsPage, null, { animate: true, direction: 'back' });
								});
								alert.present();
							} else {
								let alert = this.alertCtrl.create({
									title: 'Warning',
									subTitle: resp.msg,
									buttons: ['OK']
								});
								alert.present();
							}
							loading.dismiss();
						}, (error) => {
							loading.dismiss();
						});
					} else {
						loading.dismiss();
					}
				}).catch(error => {
					console.log('Error logging into fb.api', error);
					loading.dismiss();
				});
			}).catch(e => {
				console.log('Error logging into Facebook', e);
				loading.dismiss();
			});
		} else {
			this.authService.signIn(FacebookLoginProvider.PROVIDER_ID).then((data: any) => {
				console.log('signInWithFB: ', data);
				if (data) {
					this.users.socialSignUp({
						provider: data.provider,
						email: data.email,
						username: data.email,
						firstname: data.firstName,
						lastname: data.lastName,
						img_path: data.photoUrl,
						Id: data.id
					}).then((resp: any) => {
						if (resp && resp.success) {
							let alert = this.alertCtrl.create({
								title: 'Success',
								subTitle: resp.msg,
								buttons: ['OK']
							});
							alert.onDidDismiss(() => {
								this.navCtrl.setRoot(TabsPage, null, { animate: true, direction: 'back' });
							});
							alert.present();
						} else {
							let alert = this.alertCtrl.create({
								title: 'Warning',
								subTitle: resp.msg,
								buttons: ['OK']
							});
							alert.present();
						}
						loading.dismiss();
					}, (error) => {
						loading.dismiss();
					});
				} else {
					loading.dismiss();
				}
			}).catch((err) => {
				loading.dismiss();
			});
		}
	}
}
