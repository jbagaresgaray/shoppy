import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController, Events } from 'ionic-angular';
import * as _ from 'lodash';

import { environment } from '@app/env';

import { ChatdetailPage } from '../../pages/chatdetail/chatdetail';

import { UsersServiceProvider } from '../../providers/service/users';
import { ChatService } from '../../providers/service/chat';

@IonicPage()
@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html',
})
export class ChatPage {
  pet: any;
  contactsArr: any[] = [];
  contactsArrCopy: any[] = [];
  fakeArr: any[] = [];

  showContent: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public loadingCtrl: LoadingController, public zone: NgZone, public events: Events,
    public users: UsersServiceProvider, public chatService: ChatService) {
    this.pet = 'all';
    for (var i = 0; i < 20; ++i) {
      this.fakeArr.push(i);
    }
  }

  private initializeData(ev?: any) {
    let vm = this;
    let vuser = JSON.parse(localStorage.getItem('app.user')) || {};
    this.showContent = false;

    this.chatService.connect().then((user: any) => {
      this.createMyGroupChannelListQuery(user);
      this.chatService.ChannelHandler.onMessageReceived = (channel, message) => {
        this.createMyGroupChannelListQuery(user);
      };
      this.chatService.ChannelHandler.onMessageUpdated = (channel, message) => {
        this.createMyGroupChannelListQuery(user);
      };
      this.chatService.ChannelHandler.onReadReceiptUpdated = (channel, message) => {
        vm.events.publish('onMessageUpdated');
      };

      this.chatService.sendBird.addChannelHandler('UNIQUE_HANDLER_ID2', this.chatService.ChannelHandler);

      vm.showContent = true;
      if (ev) {
        ev.complete();
      }
    }, () => {
      vm.showContent = true;
      if (ev) {
        ev.complete();
      }
    });
  }

  private createMyGroupChannelListQuery(user) {
    var channelListQuery = this.chatService.sendBird.GroupChannel.createMyGroupChannelListQuery();
    channelListQuery.includeEmpty = true;
    channelListQuery.limit = 20;

    if (channelListQuery.hasNext) {
      channelListQuery.next((channelList, error) => {
        if (error) {
          console.error("channelListQuery: ", error);
          return;
        }
        if (channelList) {
          this.zone.run(() => {
            this.contactsArr = [];
            this.contactsArrCopy = [];

            _.each(channelList, (row: any) => {
              let invited = _.find(row.members, (o: any) => { return o.userId != user.userId; });
              if (invited) {
                row.invited = {
                  name: invited.nickname,
                  id: invited.userId,
                  avatar: invited.profileUrl,
                  connectionStatus: invited.connectionStatus
                }
              }
              if (row.lastMessage) {
                this.contactsArr.push(row);
              }
            });
            this.contactsArrCopy = _.clone(this.contactsArr);
          });
        }
      });
    }
  }

  doRefresh(ev) {
    this.initializeData(ev);
  }


  ionViewWillEnter() {
    this.initializeData();
  }

  ionViewWillLeave() {
    this.chatService.sendBird.removeChannelHandler('UNIQUE_HANDLER_ID2');
  }

  close() {
    this.viewCtrl.dismiss();
  }

  getItems(ev: any) {
    let val = ev.target.value;
    if (val && val.trim() != '') {
      this.contactsArr = this.contactsArrCopy.filter((item: any) => {
        return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      });
    } else {
      this.contactsArr = this.contactsArrCopy;
    }
  }

  delete() {

  }

  unread() {

  }

  gotoDetail(user: any) {
    this.navCtrl.push(ChatdetailPage, {
      user: {
        "user_id": user.invited.id,
        "name": user.invited.name,
        "avatar": user.invited.avatar,
        "connectionStatus": user.invited.connectionStatus
      },
    });
  }
}
