import { Component, NgZone } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import {
	IonicPage,
	NavController,
	NavParams,
	ActionSheetController,
	LoadingController,
	AlertController,
	ToastController
} from "ionic-angular";
import { AuthService } from "angular4-social-login";
import {
	FacebookLoginProvider,
	GoogleLoginProvider
} from "angular4-social-login";
import * as _ from "lodash";

import { GooglePlus } from "@ionic-native/google-plus";
import { Facebook, FacebookLoginResponse } from "@ionic-native/facebook";
import { Camera, CameraOptions } from "@ionic-native/camera";
import {
	FileTransfer,
	FileTransferObject,
	FileUploadOptions
} from "@ionic-native/file-transfer";
import { File } from "@ionic-native/file";
import { Crop } from "@ionic-native/crop";

import { environment } from "@app/env";

import { UserprofileEntryPage } from "../../pages/userprofile-entry/userprofile-entry";

import { UsersServiceProvider } from "../../providers/service/users";

declare let $: any;

@IonicPage()
@Component({
	selector: "page-userprofile",
	templateUrl: "userprofile.html"
})
export class UserprofilePage {
	imageUrl: string = "url(assets/imgs/banner_def.jpg)";
	user: any = {};
	action: string;

	public isUploading: boolean = false;
	public isUploadingBanner: boolean = false;

	public uploadingProgress: any;
	public uploadingBannerProgress: any;
	public uploadingHandler: any;
	public uploadingBannerHandler: any;
	public images: any = [];
	public filesToUpload: Array<File> = [];

	protected imagesValue: Array<any> = [];
	protected bannerValue: Array<any> = [];

	serverUrl: string = environment.api_url;

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public actionSheetCtrl: ActionSheetController,
		public loadingCtrl: LoadingController,
		public alertCtrl: AlertController,
		public toastCtrl: ToastController,
		private sanitization: DomSanitizer,
		public zone: NgZone,
		private authService: AuthService,
		public users: UsersServiceProvider,
		private fb: Facebook,
		private googlePlus: GooglePlus,
		private camera: Camera,
		private file: File,
		public transfer: FileTransfer,
		private crop: Crop
	) { }

	public uploadImages(): Promise<Array<any>> {
		return new Promise((resolve, reject) => {
			Promise.all(
				this.filesToUpload.map((image: any) => {
					console.log("this.action: ", this.action);
					if (this.action == "cover") {
						this.isUploadingBanner = true;
						return this.uploadBanner(image);
					} else if (this.action == "profile") {
						this.isUploading = true;
						return this.uploadAvatar(image);
					}
				})
			)
				.then(() => {
					resolve();
					this.filesToUpload = [];
					this.images = [];
					this.imagesValue = [];
					this.isUploading = false;
					this.isUploadingBanner = false;

					$("#circle")
						.find("canvas")
						.remove();

					this.getCurrentUser();
				})
				.catch((reason: any) => {
					console.log("reasons: ", reason);
					this.isUploading = false;
					this.isUploadingBanner = false;

					this.uploadingProgress = {};
					this.uploadingBannerProgress = {};
					this.uploadingHandler = {};
					this.uploadingBannerHandler = {};

					$("#circle")
						.find("canvas")
						.remove();
					// reject(reason);
				});
		});
	}

	public abortAvatar() {
		if (!this.isUploading) return;

		this.isUploading = false;
		this.uploadingHandler.abort();
	}

	public abortBanner() {
		if (!this.isUploadingBanner) return;

		this.isUploadingBanner = false;
		this.uploadingBannerHandler.abort();
	}

	// ======================================================================
	protected removeImage(image) {
		if (this.isUploading) {
			return;
		}

		this.confirm("Are you sure to remove it?").then(value => {
			if (value) {
				this.removeFromArray(this.imagesValue, image);
				this.removeFromArray(this.images, image.url);
			}
		});
	}

	private removeFromArray<T>(array: Array<T>, item: T) {
		let index: number = array.indexOf(item);
		if (index !== -1) {
			array.splice(index, 1);
		}
	}

	private confirm(text, title = "", yes = "Yes", no = "No") {
		return new Promise(resolve => {
			this.alertCtrl
				.create({
					title: title,
					message: text,
					buttons: [
						{
							text: no,
							role: "cancel",
							handler: () => {
								resolve(false);
							}
						},
						{
							text: yes,
							handler: () => {
								resolve(true);
							}
						}
					]
				})
				.present();
		});
	}

	private trustImages() {
		this.imagesValue = this.images.map(val => {
			return {
				url: val,
				sanitized: this.sanitization.bypassSecurityTrustStyle(
					"url(" + val + ")"
				)
			};
		});
		console.log("this.imagesValue: ", this.imagesValue);
	}

	private trustBanner() {
		this.bannerValue = this.images.map(val => {
			return {
				url: val,
				sanitized: this.sanitization.bypassSecurityTrustStyle(
					"url(" + val + ")"
				)
			};
		});
		this.zone.run(() => {
			this.imageUrl = this.bannerValue[0].sanitized;
			console.log("this.imageUrl: ", this.imageUrl);
		});
	}

	showToast(text: string) {
		this.toastCtrl
			.create({
				message: text,
				duration: 5000,
				position: "bottom"
			})
			.present();
	}

	private uploadAvatar(targetPath) {
		return new Promise((resolve, reject) => {
			this.uploadingProgress = 0;
			var uploadUrl = this.serverUrl + "users/avatar";

			if (window["cordova"]) {
				let options: FileUploadOptions = {
					fileKey: "avatar",
					chunkedMode: false,
					headers: {
						Authorization:
							"Bearer " + localStorage.getItem("app.token")
					}
				};

				const fileTransfer: FileTransferObject = this.transfer.create();
				this.uploadingHandler = fileTransfer;

				this.file
					.resolveLocalFilesystemUrl(targetPath)
					.then((info: any) => {
						options.fileName = info.name;
					})
					.then(() => {
						var circle = $("#circle").circleProgress({
							size: 110,
							thickness: 5,
							fill: { gradient: ["#488aff", "#488aff"] }
						});

						fileTransfer
							.upload(targetPath, uploadUrl, options, false)
							.then(data => {
								resolve(JSON.parse(data.response));
							})
							.catch(error => {
								console.log("error: ", error);
								if (error) {
									let err = JSON.parse(error.body);
									console.log("err: ", err);
								}
								askRetry();
							});

						fileTransfer.onProgress(event2 => {
							let progressVal =
								(event2.loaded * 100) / event2.total;
							console.log("progressVal: ", progressVal);
							this.uploadingProgress = progressVal;
							circle.circleProgress("value", progressVal);
						});
					});
			} else {
				let formData: FormData = new FormData();
				let xhr2: XMLHttpRequest = new XMLHttpRequest();

				formData.append("avatar", targetPath);
				this.uploadingHandler = xhr2;

				var circle = $("#circle").circleProgress({
					size: 110,
					thickness: 5,
					fill: { gradient: ["#488aff", "#488aff"] }
				});

				xhr2.onreadystatechange = () => {
					if (xhr2.readyState === 4) {
						if (xhr2.status === 200)
							resolve(JSON.parse(xhr2.response));
						else askRetry();
					}
				};

				xhr2.upload.onprogress = event => {
					let progressVal = (event.loaded * 100) / event.total;
					console.log("progressVal: ", progressVal);

					this.uploadingProgress = progressVal;
					circle.circleProgress("value", progressVal);
				};
				xhr2.open("POST", uploadUrl, true);
				xhr2.setRequestHeader(
					"Authorization",
					"Bearer " + localStorage.getItem("app.token")
				);
				xhr2.send(formData);
			}

			let askRetry = () => {
				// might have been aborted
				if (!this.isUploading) return reject(null);
				this.confirm("Do you wish to retry?", "Upload failed").then(
					res => {
						if (!res) {
							this.isUploading = false;
							for (let key in this.uploadingHandler) {
								this.uploadingHandler[key].abort();
							}
							return reject(null);
						} else {
							if (!this.isUploading) return reject(null);
							this.uploadAvatar(targetPath).then(resolve, reject);
						}
					}
				);
			};
		});
	}

	private uploadBanner(targetPath) {
		return new Promise((resolve, reject) => {
			this.uploadingBannerProgress = 0;
			var uploadUrl = this.serverUrl + "users/banner";

			if (window["cordova"]) {
				let options: FileUploadOptions = {
					fileKey: "banner",
					chunkedMode: false,
					headers: {
						Authorization:
							"Bearer " + localStorage.getItem("app.token")
					}
				};

				const fileTransfer: FileTransferObject = this.transfer.create();
				this.uploadingBannerHandler = fileTransfer;

				this.file
					.resolveLocalFilesystemUrl(targetPath)
					.then((info: any) => {
						options.fileName = info.name;
					})
					.then(() => {
						fileTransfer
							.upload(targetPath, uploadUrl, options, false)
							.then(data => {
								resolve(JSON.parse(data.response));
							})
							.catch(error => {
								console.log("error: ", error);
								if (error) {
									let err = JSON.parse(error.body);
									console.log("err: ", err);
								}
								askRetry();
							});

						fileTransfer.onProgress(event2 => {
							let progressVal =
								(event2.loaded * 100) / event2.total;
							console.log("progressVal: ", progressVal);
							this.uploadingBannerProgress = progressVal;
						});
					});
			} else {
				let formData: FormData = new FormData();
				let xhr2: XMLHttpRequest = new XMLHttpRequest();

				formData.append("banner", targetPath);
				this.uploadingBannerHandler = xhr2;
				xhr2.onreadystatechange = () => {
					if (xhr2.readyState === 4) {
						if (xhr2.status === 200)
							resolve(JSON.parse(xhr2.response));
						else askRetry();
					}
				};

				xhr2.upload.onprogress = event => {
					let progressVal = (event.loaded * 100) / event.total;
					console.log("progressVal: ", progressVal);
					this.uploadingBannerProgress = progressVal;
				};
				xhr2.open("POST", uploadUrl, true);
				xhr2.setRequestHeader(
					"Authorization",
					"Bearer " + localStorage.getItem("app.token")
				);
				xhr2.send(formData);
			}

			let askRetry = () => {
				// might have been aborted
				if (!this.isUploadingBanner) return reject(null);
				this.confirm("Do you wish to retry?", "Upload failed").then(
					res => {
						if (!res) {
							this.isUploadingBanner = false;
							this.uploadingBannerHandler.abort();
							return reject(null);
						} else {
							if (!this.isUploadingBanner) return reject(null);
							this.uploadBanner(targetPath).then(resolve, reject);
						}
					}
				);
			};
		});
	}

	/*ionViewDidLoad() {
		console.log('ionViewWillEnter MyprofilePage');
		console.log('imagesValue: ', this.imagesValue);
		this.getCurrentUser();
	}
*/
	ionViewDidEnter() {
		console.log("ionViewDidEnter MyprofilePage");
		console.log("imagesValue: ", this.imagesValue);
		this.getCurrentUser();
	}

	getCurrentUser() {
		let loading = this.loadingCtrl.create({
			dismissOnPageChange: true
		});
		loading.present();
		this.user = JSON.parse(localStorage.getItem("app.user")) || {};
		if (!_.isEmpty(this.user)) {
			this.users.getCurrentUser().then(
				(data: any) => {
					if (data && data.success) {
						this.user = data.result;
						this.imageUrl = _.isEmpty(this.user.banner_path)
							? "url(assets/imgs/banner_def.jpg)"
							: "url(" + this.user.banner_path + ")";
						if (this.user.email) {
							let i = this.user.email.indexOf("@");
							let email = this.user.email.split("@");
							this.user.emailMask =
								this.user.email.substr(0, 2) +
								this.user.email
									.substr(i)
									.replace(/[\S]/g, "*") +
								"@" +
								email[1];
						} else {
							this.user.emailMask = "";
						}

						if (this.user.phone) {
							this.user.phoneMask =
								this.user.phone
									.substr(0, this.user.phone.length - 2)
									.replace(/[\S]/g, "*") +
								this.user.phone.slice(-2);
						} else {
							this.user.phoneMask = "";
						}

						if (this.user.fax) {
							this.user.faxMask =
								this.user.fax
									.substr(0, this.user.fax.length - 2)
									.replace(/[\S]/g, "*") +
								this.user.fax.slice(-2);
						} else {
							this.user.faxMask = "";
						}

						console.log("this.user: ", this.user);
					}
					loading.dismiss();
				},
				error => {
					console.log("Error: ", error);
					loading.dismiss();
				}
			);
		}
	}

	updateProfilePic(action) {
		let sText: any;
		this.action = action;
		if (action == "cover") {
			sText = "Edit Background";

			if (this.isUploadingBanner) {
				return;
			}
		} else if (action == "profile") {
			sText = "Edit Profile Picture";

			if (this.isUploading) {
				return;
			}
		}

		if (!window["cordova"]) {
			new Promise((resolve, reject) => {
				let actionSheet = this.actionSheetCtrl.create({
					title: sText,
					buttons: [
						{
							text: "Browse Photo",
							handler: () => {
								resolve();
							}
						},
						{
							text: "Cancel",
							role: "cancel",
							handler: () => {
								reject();
							}
						}
					]
				});
				actionSheet.present();
			})
				.then(() => {
					let input = document.createElement("input");
					input.type = "file";
					input.accept = "image/x-png,image/gif,image/jpeg";
					input.click();
					input.onchange = () => {
						let blob = window.URL.createObjectURL(input.files[0]);
						let blob2: any = input.files[0];
						this.images.push(blob);
						this.filesToUpload.push(blob2);
						console.log("this.filesToUpload: ", this.filesToUpload);

						if (this.action == "profile") {
							this.trustImages();
						} else if (this.action == "cover") {
							this.trustBanner();
						}

						setTimeout(() => {
							this.uploadImages();
						}, 1000);
					};
				})
				.catch(() => { });
		} else {
			new Promise((resolve, reject) => {
				let actionSheet = this.actionSheetCtrl.create({
					title: sText,
					buttons: [
						{
							text: "Photos",
							handler: () => {
								resolve(
									this.camera.PictureSourceType.PHOTOLIBRARY
								);
							}
						},
						{
							text: "Camera",
							handler: () => {
								resolve(this.camera.PictureSourceType.CAMERA);
							}
						},
						{
							text: "Cancel",
							role: "cancel",
							handler: () => {
								reject();
							}
						}
					]
				});
				actionSheet.present();
			})
				.then(sourceType => {
					if (!window["cordova"]) return;
					let options: CameraOptions = {
						quality: 100,
						sourceType: sourceType as number,
						saveToPhotoAlbum: false,
						correctOrientation: true
					};
					this.camera.getPicture(options).then(imagePath => {
						console.log("CameraOptions imagePath: ", imagePath);

						if (this.action == "cover") {
							this.images.push(imagePath);
							this.filesToUpload.push(imagePath);
							console.log(
								"CameraOptions this.filesToUpload: ",
								this.filesToUpload
							);

							this.trustBanner();

							setTimeout(() => {
								this.uploadImages();
							}, 1000);
						} else {
							this.crop
								.crop(imagePath, {
									quality: 100,
									targetWidth: 600,
									targetHeight: 600
								})
								.then(
									(newImage: any) => {
										console.log(
											"new image path is: " + newImage
										);
										this.images.push(newImage);
										this.filesToUpload.push(newImage);
										console.log(
											"CameraOptions this.filesToUpload: ",
											this.filesToUpload
										);
										this.trustImages();

										setTimeout(() => {
											this.uploadImages();
										}, 1000);
									},
									error => {
										console.error(
											"Error cropping image",
											error
										);
									}
								);
						}
					});
				})
				.catch(() => { });
		}
	}

	saveProfile() {
		let loading = this.loadingCtrl.create({
			dismissOnPageChange: true,
			content: "Updating..."
		});
		loading.present();
		this.users.updateUserProfile(this.user).then(
			(data: any) => {
				if (data && data.success) {
					this.getCurrentUser();
				}
				loading.dismiss();
			},
			error => {
				loading.dismiss();
			}
		);
	}

	updatePassword() {
		this.navCtrl.push(UserprofileEntryPage, {
			type: "validate",
			next: "password",
			user: this.user
		});
	}

	updateEmail() {
		this.navCtrl.push(UserprofileEntryPage, {
			type: "validate",
			next: "email",
			user: this.user
		});
	}

	updatePhone() {
		this.navCtrl.push(UserprofileEntryPage, {
			type: "validate",
			next: "phone",
			user: this.user
		});
	}

	updateFax() {
		this.navCtrl.push(UserprofileEntryPage, {
			type: "validate",
			next: "fax",
			user: this.user
		});
	}

	signInWithGoogle(): void {
		let loading = this.loadingCtrl.create();
		loading.present();

		if (window["cordova"]) {
			this.googlePlus
				.login({
					webClientId: environment.GoogleClientID
				})
				.then(res => {
					console.log("googlePlus: ", res);
					/*this.displayName = res.displayName;
				this.email = res.email;
				this.familyName = res.familyName;
				this.givenName = res.givenName;
				this.userId = res.userId;
				this.imageUrl = res.imageUrl;*/

					this.users
						.linkGoogleAccount({
							Id: res.userId,
							img_path: res.imageUrl
						})
						.then(
							(resp: any) => {
								if (resp && resp.success) {
									this.getCurrentUser();
								}
								loading.dismiss();
							},
							error => {
								loading.dismiss();
							}
						);
				})
				.catch(err => {
					loading.dismiss();
					console.error(err);
				});
		} else {
			this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then(
				(data: any) => {
					console.log("signInWithGoogle: ", data);
					this.users
						.linkGoogleAccount({
							Id: data.id,
							img_path: data.photoUrl
						})
						.then(
							(resp: any) => {
								if (resp && resp.success) {
									this.getCurrentUser();
								}
								loading.dismiss();
							},
							error => {
								loading.dismiss();
							}
						);
				},
				error => {
					loading.dismiss();
				}
			);
		}
	}

	signInWithFB(): void {
		let loading = this.loadingCtrl.create();
		loading.present();
		if (window["cordova"]) {
			this.fb
				.login(["public_profile", "user_friends", "email"])
				.then((res: FacebookLoginResponse) => {
					console.log("Logged into Facebook!", res);
					this.fb
						.api(
							"me?fields=id,name,email,first_name,last_name,picture.width(720).height(720).as(picture_large)",
							[]
						)
						.then(profile => {
							console.log("profile: ", profile);
							if (profile) {
								this.users
									.linkFacebookAccount({
										Id: profile.id,
										img_path:
											profile["picture_large"]["data"][
											"url"
											]
									})
									.then(
										(resp: any) => {
											if (resp && resp.success) {
												this.getCurrentUser();
											}
											loading.dismiss();
										},
										error => {
											loading.dismiss();
										}
									);
							} else {
								loading.dismiss();
							}
						})
						.catch(e => {
							console.log("Error logging into Facebook", e);
							loading.dismiss();
						});
				})
				.catch(error => {
					console.log("Error logging into fb.api", error);
					loading.dismiss();
				});
		} else {
			this.authService.signIn(FacebookLoginProvider.PROVIDER_ID).then(
				(data: any) => {
					console.log("signInWithFB: ", data);
					this.users.linkFacebookAccount({ Id: data.id }).then(
						(resp: any) => {
							if (resp && resp.success) {
								this.getCurrentUser();
							}
							loading.dismiss();
						},
						error => {
							loading.dismiss();
						}
					);
				},
				error => {
					loading.dismiss();
				}
			);
		}
	}

	unlinkFacebook() {
		let loading = this.loadingCtrl.create();
		let actionSheet = this.actionSheetCtrl.create({
			title: "Unlink Facebook to you account?",
			buttons: [
				{
					text: "Unlink",
					role: "destructive",
					handler: () => {
						loading.present();
						this.users.unlinkFacebookAccount().then(
							(data: any) => {
								if (data && data.success) {
									this.getCurrentUser();
								}
								loading.dismiss();
							},
							error => {
								loading.dismiss();
							}
						);
					}
				},
				{
					text: "Cancel",
					role: "cancel",
					handler: () => {
						console.log("Cancel clicked");
					}
				}
			]
		});
		actionSheet.present();
	}

	unlinkGoogle() {
		let loading = this.loadingCtrl.create();
		let actionSheet = this.actionSheetCtrl.create({
			title: "Unlink Google to you account?",
			buttons: [
				{
					text: "Unlink",
					role: "destructive",
					handler: () => {
						loading.present();
						this.users.unlinkGoogleAccount().then(
							(data: any) => {
								if (data && data.success) {
									this.getCurrentUser();
								}
								loading.dismiss();
							},
							error => {
								loading.dismiss();
							}
						);
					}
				},
				{
					text: "Cancel",
					role: "cancel",
					handler: () => {
						console.log("Cancel clicked");
					}
				}
			]
		});
		actionSheet.present();
	}
}
