import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { LoginPage } from "./login";

import { ComponentsModule } from "../../components/components.module";
import { PipesModule } from "../../pipes/pipes.module";

@NgModule({
	declarations: [LoginPage],
	imports: [
		IonicPageModule.forChild(LoginPage),
		ComponentsModule,
		PipesModule
	]
})
export class LoginPageModule {}
