import { Component } from "@angular/core";
import {
	Platform,
	IonicPage,
	NavController,
	NavParams,
	ViewController,
	AlertController,
	LoadingController,
	Events
} from "ionic-angular";
import * as _ from "lodash";
import { AuthService } from "angular4-social-login";
import {
	FacebookLoginProvider,
	GoogleLoginProvider
} from "angular4-social-login";

import { GooglePlus } from "@ionic-native/google-plus";
import { Facebook, FacebookLoginResponse } from "@ionic-native/facebook";
import { OneSignal } from "@ionic-native/onesignal";
import { Device } from "@ionic-native/device";

// import { Socket } from 'ng-socket-io';

import { environment } from "@app/env";

import { AuthProvider } from "../../providers/service/authentication";
import { ForgotPage } from "../../pages/forgot/forgot";
import { SignupPage } from "../../pages/signup/signup";

@IonicPage()
@Component({
	selector: "page-login",
	templateUrl: "login.html"
})
export class LoginPage {
	user: any = {};
	user_player_ids: string;

	constructor(
		public platform: Platform,
		public navCtrl: NavController,
		public navParams: NavParams,
		public viewCtrl: ViewController,
		public auth: AuthProvider,
		public alertCtrl: AlertController,
		public loadingCtrl: LoadingController,
		public events: Events,
		private oneSignal: OneSignal,
		private authService: AuthService,
		private fb: Facebook,
		private googlePlus: GooglePlus,
		private device: Device
	) {}

	ionViewDidLoad() {
		this.loadingDeviceId();
	}

	ionViewWillEnter() {
		this.loadingDeviceId();
	}

	loadingDeviceId() {
		this.platform.ready().then(() => {
			if (window["cordova"]) {
				this.oneSignal.getIds().then((data: any) => {
					console.log("getIds: ", data);
					this.user_player_ids = data.userId;
				});
			}
		});
	}

	close() {
		this.viewCtrl.dismiss();
	}

	forgot() {
		console.log("forgot: ");
		this.navCtrl.push(ForgotPage);
	}

	openSignUp() {
		this.navCtrl.push(SignupPage);
	}

	login() {
		if (_.isEmpty(this.user)) {
			let alert = this.alertCtrl.create({
				title: "Invalid Username",
				subTitle: "Invalid username and password!",
				buttons: ["OK"]
			});
			alert.present();
			return;
		}

		if (_.isEmpty(this.user.username)) {
			let alert = this.alertCtrl.create({
				title: "Invalid Username",
				subTitle: "Invalid username and password!",
				buttons: ["OK"]
			});
			alert.present();
			return;
		} else if (_.isEmpty(this.user.password)) {
			let alert = this.alertCtrl.create({
				title: "Invalid Password",
				subTitle: "Invalid username and password!",
				buttons: ["OK"]
			});
			alert.present();
			return;
		}

		let loading = this.loadingCtrl.create();
		loading.present();

		if (this.user_player_ids) {
			this.user.player_id = this.user_player_ids;
			this.user.device_model = this.device.model;
			this.user.device_version = this.device.version;
			this.user.device_manufacturer = _.toUpper(this.device.manufacturer);
			this.user.device_uuid = this.device.uuid;
			this.user.device_serial = this.device.serial;
		}
		this.auth.authenticate(this.user).then(
			(data: any) => {
				console.log("data: ", data);
				if (data && data.success) {
					localStorage.setItem(
						"app.user",
						JSON.stringify(data.result.user)
					);
					localStorage.setItem("app.token", data.result.token);

					this.events.publish("app.login");

					// this.socket.connect();
					// this.socket.emit('create-room', this.user.uuid);

					loading.dismiss();
					setTimeout(() => {
						this.viewCtrl.dismiss("login");
					}, 1000);
				} else if (data && !data.success) {
					loading.dismiss();

					let alert = this.alertCtrl.create({
						title: "Error",
						subTitle: data.msg,
						buttons: ["OK"]
					});
					alert.present();
					return;
				}
			},
			error => {
				console.log("error: ", error);
				loading.dismiss();
			}
		);
	}

	private socialAuth(resp: any, loading?: any) {
		let vm = this;
		vm.auth
			.socialAuth({
				provider: resp.provider,
				Id: resp.id
			})
			.then(
				(data: any) => {
					if (data && data.success) {
						localStorage.setItem(
							"app.user",
							JSON.stringify(data.result.user)
						);
						localStorage.setItem("app.token", data.result.token);

						vm.events.publish("app.login");

						loading.dismiss();
						setTimeout(() => {
							vm.viewCtrl.dismiss("login");
						}, 1000);
					} else if (data && !data.success) {
						loading.dismiss();

						let alert = vm.alertCtrl.create({
							title: "Error",
							subTitle: data.msg,
							buttons: ["OK"]
						});
						alert.present();
						return;
					}
				},
				error => {
					console.log("error: ", error);
					loading.dismiss();
				}
			);
	}

	signInWithGoogle() {
		let loading = this.loadingCtrl.create();
		loading.present();
		if (window["cordova"]) {
			this.googlePlus
				.login({
					webClientId: environment.GoogleClientID
				})
				.then(res => {
					console.log(res);
					res.id = res.userId;
					res.provider = "google";
					this.socialAuth(res, loading);
				})
				.catch(err => {
					loading.dismiss();
					console.error(err);
				});
		} else {
			this.authService
				.signIn(GoogleLoginProvider.PROVIDER_ID)
				.then((data: any) => {
					console.log("signInWithGoogle: ", data);
					if (data) {
						this.socialAuth(data, loading);
					} else {
						loading.dismiss();
					}
				})
				.catch(err => {
					loading.dismiss();
				});
		}
	}

	signInWithFB() {
		let loading = this.loadingCtrl.create();
		loading.present();
		if (window["cordova"]) {
			this.fb
				.login(["public_profile", "user_friends", "email"])
				.then((res: FacebookLoginResponse) => {
					this.fb
						.api(
							"me?fields=id,name,email,first_name,last_name,picture.width(720).height(720).as(picture_large)",
							[]
						)
						.then(profile => {
							console.log("profile: ", profile);
							if (profile) {
								profile.provider = "facebook";
								this.socialAuth(profile, loading);
							} else {
								loading.dismiss();
							}
						})
						.catch(error => {
							console.log("Error logging into fb.api", error);
							loading.dismiss();
						});
				})
				.catch(e => {
					console.log("Error logging into Facebook", e);
					loading.dismiss();
				});
		} else {
			this.authService
				.signIn(FacebookLoginProvider.PROVIDER_ID)
				.then((data: any) => {
					console.log("signInWithFB: ", data);
					if (data) {
						this.socialAuth(data, loading);
					} else {
						loading.dismiss();
					}
				})
				.catch(err => {
					loading.dismiss();
				});
		}
	}
}
