import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PipesModule } from "../../pipes/pipes.module";
import { ComponentsModule } from "../../components/components.module";
import { NgPipesModule } from 'ngx-pipes';

import { AllFormEntryPage } from './all-form-entry';



@NgModule({
  declarations: [
    AllFormEntryPage,
  ],
  imports: [
    IonicPageModule.forChild(AllFormEntryPage),
    NgPipesModule,
    PipesModule,
    ComponentsModule
  ],
})
export class AllFormEntryPageModule {}
