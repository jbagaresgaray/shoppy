import { Component, NgZone, EventEmitter } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  AlertController,
  ToastController,
  ActionSheetController,
  normalizeURL,
  Platform
} from "ionic-angular";
import * as _ from "lodash";
import * as async from "async";

import { MypurchasesPage } from "../../pages/mypurchases/mypurchases";
import { ShoppingcartSelectshippingoptionPage } from "../../pages/shoppingcart-selectshippingoption/shoppingcart-selectshippingoption";

import { OrdersServiceProvider } from "../../providers/service/orders";
import { SellerServiceProvider } from "../../providers/service/seller";
import { ServiceProvider } from "../../providers/service/service";

import { environment } from "@app/env";

@IonicPage()
@Component({
  selector: "page-all-form-entry",
  templateUrl: "all-form-entry.html"
})
export class AllFormEntryPage {
  action: string;
  mode: string;
  title: string;
  usertype: string;
  buttonTitle: string = "Submit";

  order: any = {};
  pickup: any = {};
  return: any = {};
  currency: any = {};
  sellerShope: any = {};
  discount: any = {};

  userAddressArr: any[] = [];
  reasonArr: any[] = [];
  returnArr: any[] = [];

  tmpShippingArr: any[] = [];

  callback: any;

  // public serverUrl = ((environment.production == true) ? environment.api_prod_url : environment.api_url) + 'orders';

  refreshNotification: EventEmitter<any> = new EventEmitter();
  createNotification: EventEmitter<any> = new EventEmitter();
  deleteNotification: EventEmitter<any> = new EventEmitter();
  saveEntryNotification: EventEmitter<any> = new EventEmitter();
  updateNotification: EventEmitter<any> = new EventEmitter();

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    private sanitization: DomSanitizer,
    public toastCtrl: ToastController,
    public actionSheetCtrl: ActionSheetController,
    public platform: Platform,
    public zone: NgZone,
    public orders: OrdersServiceProvider,
    public sellers: SellerServiceProvider,
    public services: ServiceProvider
  ) {
    this.action = navParams.get("action");
    this.usertype = navParams.get("usertype");
    this.callback = navParams.get("callback");

    this.currency = this.services.defaultCurrency();

    this.reasonArr = [
      {
        id: 1,
        reason: "Seller is not responsive to my inquiries"
      },
      {
        id: 2,
        reason: "Seller ask me to cancel"
      },
      {
        id: 3,
        reason: "Modify existing order (color,size, address, voucher, etc.)"
      },
      {
        id: 4,
        reason:
          "Product has bad reviews (not authentic, not as description, etc.)"
      },
      {
        id: 5,
        reason: "Seller takes too long to ship my order"
      },
      {
        id: 6,
        reason: "Seller is untrustworthy (asks to transact out of Shopee, etc.)"
      },
      {
        id: 7,
        reason: "Others (changed mind, found cheaper price, etc.)"
      }
    ];

    this.returnArr = [
      {
        id: 1,
        return:
          "Received an incomplete product (missing quantity or accessories)"
      },
      {
        id: 2,
        return:
          "Received a wrong product(s) (e.g. wrong size, wrong colour, different item)"
      },
      {
        id: 3,
        return: "Received a product with physical damaged"
      },
      {
        id: 4,
        return: "Received a product with functional damaged"
      },
      {
        id: 5,
        return: "I did not receive the order"
      }
    ];

    _.each(this.returnArr, (row: any) => {
      row.selected = false;
    });

    _.each(this.reasonArr, (row: any) => {
      row.selected = false;
    });

    this.initContentForm();
  }

  private initContentForm() {
    if (this.action == "shipment") {
      this.order = this.navParams.get("orders");
      this.title = "Order Tracking Entry";
      this.buttonTitle = "Request Pickup";
    } else if (this.action == "shipping") {
      this.order = this.navParams.get("orders");
      this.title = "Arrange Pickup";
    } else if (this.action == "cancel") {
      this.title = "Choose Cancellation Reason";
      this.order = this.navParams.get("orders");
      this.buttonTitle = "Cancel Order";
      console.log("this.order: ", this.order);
    } else if (this.action == "cancel_view") {
      this.title = "Cancellation Details";
      this.order = this.navParams.get("orders");
      console.log("this.order: ", this.order);
    } else if (this.action == "return") {
      this.title = "Return/Refund";
      this.order = this.navParams.get("orders");
      console.log("this.order: ", this.order);
      this.return = {};
      if (!_.isEmpty(this.order.details) && _.size(this.order.details) > 0) {
        _.each(this.order.details, (row: any) => {
          row.selected = false;
        });
      }
    } else if (this.action == "return_view") {
      this.title = "Return/Refund Details";
      this.order = this.navParams.get("orders");
    } else if (this.action == "return_shipment_view") {
      this.title = "Return Shipment Details";
      this.return = this.navParams.get("return");
      console.log("this.return: ", this.return);
    } else if (this.action == "return_shipment") {
      this.title = "Return Shipping";
      this.buttonTitle = "Ship";
      this.order = this.navParams.get("orders");
      this.return = {};
      this.return = this.navParams.get("return");
      console.log("this.return: ", this.return);
    } else if (this.action == "return_shipment_shipping") {
      this.title = "Return Shipping";
      this.order = this.navParams.get("orders");
      this.return = this.navParams.get("return");
    } else if (this.action == "my_address_entry") {
      this.title = "New Address";
    } else if (this.action == "discount_promotions") {
      this.title = "Create Discount Promotions";
      this.buttonTitle = "Save";
    } else if (this.action == "seller_vouchers") {
      this.title = "Create Vouchers";
      this.buttonTitle = "Save";
      this.mode = this.navParams.get("mode");
    } else if (this.action == "seller_toppicks") {
      this.title = "Create Collections";
      this.buttonTitle = "Save";
    } else if (this.action == "bank_cards_entry") {
      this.mode = this.navParams.get("mode");
      this.title = "Add Bank Account";
      if (this.mode == "view") {
        this.buttonTitle = "Set as Default Account";
      } else if (this.mode == "entry") {
        this.buttonTitle = "Save";
      }
    } else if (this.action == "discount_promotions_products") {
      this.title = "Promotion Details";
      this.buttonTitle = "Submit";
    }
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad AllFormEntryPage");
    this.initContentForm();
  }

  saveEntry() {
    if (this.action == "bank_cards_entry") {
      if (this.mode == "view") {
        this.createNotification.emit("view");
      } else if (this.mode == "entry") {
        this.createNotification.emit("entry");
      }
    } else if (this.action == "discount_promotions_products") {
      this.saveEntryNotification.emit();
    } else {
      this.createNotification.emit();
    }
  }

  create() {
    this.createNotification.emit();
  }

  delete() {
    this.deleteNotification.emit();
  }

  update() {
    if (this.action == "discount_promotions_products") {
      if (
        _.isEmpty(this.discount.discount) ||
        _.isEmpty(this.discount.purchase_limit)
      ) {
        this.alertCtrl
          .create({
            message:
              "Please provide either purchase limit or discount percentage!",
            buttons: ["OK"]
          })
          .present();
        return;
      }
      this.updateNotification.emit(this.discount);
    } else {
      this.updateNotification.emit();
    }
  }
}
