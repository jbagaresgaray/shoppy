import { Component, NgZone, ViewEncapsulation } from "@angular/core";
import {
  NavController,
  NavParams,
  LoadingController,
  AlertController,
  ModalController,
  ViewController,
  PopoverController,
  App,
  Events,
  Platform
} from "ionic-angular";
import * as _ from "lodash";

import { ShoppingcartPage } from "../shoppingcart/shoppingcart";
import { ChatdetailPage } from "../chatdetail/chatdetail";
import { ProductratingsPage } from "../productratings/productratings";
import { ShopprofilePage } from "../shopprofile/shopprofile";
import { ProductcommentPage } from "../productcomment/productcomment";
import { LoginPage } from "../login/login";
import { SearchresultPage } from "../searchresult/searchresult";
import { ProductEntryPage } from "../product-entry/product-entry";
import { MyratingPage } from "../myrating/myrating";
import { ModalComponentFormPage } from "../modal-component-form/modal-component-form";

import { ProductsServiceProvider } from "../../providers/service/products";
import { UsersServiceProvider } from "../../providers/service/users";
import { SellerServiceProvider } from "../../providers/service/seller";
import { ServiceProvider } from "../../providers/service/service";
import { ShoppingCartProvider } from "../../providers/service/shopping-cart";
import { SellerVouchersService } from "../../providers/service/seller-vouchers";

import { SocialSharing } from "@ionic-native/social-sharing";

declare let safari: any;

@Component({
  template: `
    <ion-list>
      <ion-item (click)="viewChat()"
        ><ion-icon name="ios-chatbubbles-outline" item-start></ion-icon>
        <h2 class="typo-r14">Chat Now</h2></ion-item
      >
      <ion-item (click)="close()"
        ><ion-icon name="ios-share-outline" item-start></ion-icon>
        <h2 clas="typo-r14">Share</h2></ion-item
      >
      <ion-item (click)="close()"
        ><ion-icon name="ios-home-outline" item-start></ion-icon>
        <h2 class="typo-r14">Back to Home</h2></ion-item
      >
      <ion-item (click)="close()"
        ><ion-icon name="ios-information-circle-outline" item-start></ion-icon>
        <h2 class="typo-r14">Report this product</h2></ion-item
      >
    </ion-list>
  `
})
export class DetailPopoverPage {
  user: any = {};
  isLogin: boolean = false;
  shopUser: any = {};

  constructor(
    public navParams: NavParams,
    public events: Events,
    public viewCtrl: ViewController,
    public navCtrl: NavController,
    public modalCtrl: ModalController
  ) {
    this.shopUser = navParams.get("shopUser");
    this.user = JSON.parse(localStorage.getItem("app.user")) || {};
    console.log("shopUser: ", this.shopUser);

    if (!_.isEmpty(this.user)) {
      this.isLogin = true;
    }
  }

  close() {
    this.viewCtrl.dismiss();
  }

  viewChat() {
    // TODO: Check if Authenticated
    if (_.isEmpty(this.user)) {
      let modal = this.modalCtrl.create(LoginPage);
      modal.present();
      return;
    }

    this.events.publish("chat:now");
    this.viewCtrl.dismiss();
  }
}
@Component({
  selector: "page-details",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "details.html"
})
export class DetailsPage {
  user: any = {};
  item: any = {};
  details: any = {};
  shop: any = {};
  currency: any = {};

  cartCount: number;

  productsArr: any[] = [];
  userLikesArr: any[] = [];
  sellerTopPicksArr: any[] = [];
  sellerVouchersArr: any[] = [];
  productVouchersArr: any[] = [];

  enableBuying: boolean = true;
  isLogin: boolean = false;
  showMoreProducts: boolean = false;
  isSafari: boolean = false;
  showLoading: boolean = true;

  callback: any;

  constructor(
    public app: App,
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public popoverCtrl: PopoverController,
    public events: Events,
    public zone: NgZone,
    public platform: Platform,
    public products: ProductsServiceProvider,
    public users: UsersServiceProvider,
    public shoppingCart: ShoppingCartProvider,
    public sellerService: SellerServiceProvider,
    public sellerVouchers: SellerVouchersService,
    public services: ServiceProvider,
    private socialSharing: SocialSharing
  ) {
    this.user = JSON.parse(localStorage.getItem("app.user")) || {};
    this.item = navParams.get("product");
    this.isSafari =
      /constructor/i.test(window["HTMLElement"]) ||
      (function(p) {
        return p.toString() === "[object SafariRemoteNotification]";
      })(!window["safari"] || safari.pushNotification);
    this.callback = navParams.get("callback");

    this.shoppingCart.getCart().then((data: any) => {
      this.cartCount = !_.isEmpty(data) ? data.length : 0;
    });

    if (!_.isEmpty(this.user)) {
      this.isLogin = true;

      if (this.item.userId === this.user.userId) {
        this.enableBuying = false;
      }
    }

    this.currency = this.services.defaultCurrency();

    this.details.moreproducts = [];

    // ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
    // ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
    // ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
    events.unsubscribe("update:cart");
    events.unsubscribe("chat:now");

    events.subscribe("update:cart", () => {
      this.cartCount = this.shoppingCart.cartCount();
    });
    events.subscribe("chat:now", () => {
      this.viewChat(this.details.shopInfo);
    });
  }

  private refreshAfterCallback = resp => {
    console.log("refreshAfterCallback");
    return new Promise((resolve, reject) => {
      if (resp == "save") {
        this.initializeData(null);
        resolve();
      }
    });
  };

  initializeData(refresher?: any) {
    let vm = this;

    if (this.item) {
      if (this.item.userId === this.user.userId) {
        this.enableBuying = false;
      }
      this.showMoreProducts = false;
      this.products
        .getProductInfo(this.item.productId)
        .then(
          (data: any) => {
            if (data && data.success) {
              this.details = data.result;
              this.details.isLiked = false;

              if (this.details.images) {
                this.details.image = this.details.images[0];
              }

              if (this.platform.is("ios")) {
                if (this.isSafari) {
                  this.details.date_created = new Date(
                    this.details.date_created.replace(/-/g, "/")
                  );
                } else {
                  this.details.date_created = new Date(
                    this.details.date_created.replace(/-/g, "/")
                  );
                }
              }

              if (!_.isEmpty(this.details.shippingFee)) {
                const minPrice: any = _.minBy(
                  this.details.shippingFee,
                  (row: any) => {
                    return row.fee;
                  }
                );
                const maxPrice: any = _.maxBy(
                  this.details.shippingFee,
                  (row: any) => {
                    return row.fee;
                  }
                );
                this.details.shippingFeeObj = {
                  minPrice: minPrice.fee,
                  maxPrice: maxPrice.fee
                };
              }

              if (!_.isEmpty(this.details.wholesale)) {
                const maxPrice: any = _.maxBy(
                  this.details.wholesale,
                  (row: any) => {
                    return row.price;
                  }
                );
                this.details.wholeSaleObj = maxPrice;
              }

              this.products
                .getProductDiscount(this.details.productId)
                .then((data: any) => {
                  if (data && data.success) {
                    console.log("discount: ", data.result);
                  }
                });

              /* this.products.getAllProductRatings(this.details.uuid).then(
                (data: any) => {
                  if (data && data.success) {
                  }
                },
                error => {
                  console.log("getAllProductRatings: ", error);
                }
              ); */

              console.log("this.details: ", this.details);
              if (!_.isEmpty(this.user)) {
                this.users.getCurrentUserLike().then(
                  (data: any) => {
                    if (data && data.success) {
                      this.userLikesArr = data.result;
                      let res = _.find(this.userLikesArr, {
                        productId: this.item.productId
                      });
                      if (res) {
                        this.details.isLiked = true;
                      }
                    }
                  },
                  error => {
                    console.log("getCurrentUserLike: ", error);
                  }
                );
              }
            }
            this.showLoading = false;
            if (refresher) {
              refresher.complete();
            }
          },
          error => {
            this.showLoading = false;
            if (refresher) {
              refresher.complete();
            }
            const alert = this.alertCtrl.create({
              subTitle: "The product failed to load. Please reload.",
              buttons: [
                {
                  text: "RELOAD",
                  handler: data => {
                    console.log("RELOAD clicked");
                  }
                }
              ]
            });
            alert.present();
          }
        )
        .then(() => {
          this.shoppingCart.getCart().then((data: any) => {
            if (data && data.length) {
              this.cartCount = data.length;
            }
          });

          this.users.getShopProfile(this.item.userId).then(
            (data: any) => {
              if (data && data.success) {
                console.log("storeInfo: ", data.result);
                this.shop = data.result;

                this.sellerService
                  .getShopRatingSummary(this.shop.userId)
                  .then((data: any) => {
                    if (data && data.success) {
                      console.log("shop summary: ", data.result);
                      this.shop.rating = data.result.total_avg_rating;
                    }
                  });

                this.sellerService
                  .getSellerActiveTopPickCollection(this.item.userId)
                  .then(
                    (data: any) => {
                      if (data && data.success) {
                        this.sellerTopPicksArr = data.result.products;
                      }
                    },
                    error => {
                      console.log("error: ", error);
                    }
                  );

                this.sellerVouchers.getSellerVouchers(this.item.userId).then(
                  (data: any) => {
                    if (data && data.success) {
                      console.log("_sellerVouchers: ", data.result);
                      this.sellerVouchersArr = data.result;
                    }
                  },
                  error => {
                    console.log("error: ", error);
                  }
                );
              }
            },
            error => {
              console.log("error: ", error);
            }
          );

          this.products.getProductOnSameShop(this.item.productId).then(
            (data: any) => {
              if (data && data.success) {
                this.zone.run(() => {
                  vm.details.moreproducts = data.result;
                });

                _.each(this.details.moreproducts, (row: any) => {
                  let res = _.find(this.userLikesArr, {
                    productId: row.productId
                  });
                  if (res) {
                    row.isLiked = true;
                  } else {
                    row.isLiked = false;
                  }
                });
              }
              this.showMoreProducts = true;
            },
            error => {
              this.showMoreProducts = true;
            }
          );

          this.products.getAllProductVouchers(this.item.productId).then(
            (data: any) => {
              if (data && data.success) {
                console.log("productVouchersArr: ", data.result);
                this.productVouchersArr = data.result;
              }
            },
            error => {
              console.log("error: ", error);
            }
          );
        });
    }
  }

  ionViewDidLoad() {
    this.showLoading = true;
    this.initializeData();
  }

  ionViewWillEnter() {
    this.isSafari =
      /constructor/i.test(window["HTMLElement"]) ||
      (function(p) {
        return p.toString() === "[object SafariRemoteNotification]";
      })(!window["safari"] || safari.pushNotification);
  }

  doRefresh(refresher) {
    this.initializeData(refresher);
  }

  likeItem() {
    if (this.details.isLiked) {
      this.products.unlikeProduct(this.details.uuid).then(
        (data: any) => {
          if (data && data.success) {
            this.initializeData();
          }
        },
        error => {
          console.log("likeProduct: ", error);
        }
      );
    } else {
      this.products.likeProduct(this.details.uuid).then(
        (data: any) => {
          if (data && data.success) {
            this.initializeData();
          }
        },
        error => {
          console.log("likeProduct: ", error);
        }
      );
    }
  }

  presentPopover(shop, myEvent) {
    console.log("shop: ", shop);
    let popover = this.popoverCtrl.create(DetailPopoverPage, {
      shopUser: shop
    });
    popover.present({
      ev: myEvent
    });
  }

  viewCart() {
    // TODO: Check if Authenticated
    if (_.isEmpty(this.user)) {
      let modal = this.modalCtrl.create(LoginPage);
      modal.present();
      return;
    }

    this.navCtrl.push(ShoppingcartPage);
  }

  viewProductRating() {
    if (_.isEmpty(this.user)) {
      let modal = this.modalCtrl.create(LoginPage);
      modal.present();
      return;
    }

    this.navCtrl.push(ProductratingsPage, {
      productId: this.item.productId,
      product: this.item
    });
  }

  viewShopProfile(item) {
    this.navCtrl.push(ShopprofilePage, {
      profile: item
    });
  }

  viewShopRating(item) {
    if (_.isEmpty(this.user)) {
      let modal = this.modalCtrl.create(LoginPage);
      modal.present();
      return;
    }

    this.navCtrl.push(MyratingPage, {
      profile: item
    });
  }

  viewProductComment() {
    if (_.isEmpty(this.user)) {
      let modal = this.modalCtrl.create(LoginPage);
      modal.present();
      return;
    }
    this.navCtrl.push(ProductcommentPage, {
      product: this.details
    });
  }

  viewProductCategory(item, type, category?) {
    if (type === "category") {
      console.log("1");
      this.app.getRootNav().push(SearchresultPage, {
        action: type,
        item: item
      });
    } else if (type === "subcategory") {
      console.log("2");
      this.app.getRootNav().push(SearchresultPage, {
        action: type,
        item: category,
        sub_item: item
      });
    }
  }

  shareProduct() {
    if (this.platform.is("cordova")) {
      this.socialSharing
        .share(
          "Tell your friends and family about your finds",
          "Share this listing"
        )
        .then(() => {
          // Success!
        })
        .catch(error => {
          console.log("socialSharing Error! ", error);
        });
    } else {
      this.alertCtrl
        .create({
          title: "WARNING",
          subTitle: "Social sharing only available on the actual device!",
          buttons: ["OK"]
        })
        .present();
      return;
    }
  }

  gotoDetails(item) {
    this.navCtrl.push(DetailsPage, {
      product: item
    });
  }

  viewBrand(item, type) {
    console.log("item: ", item);
    this.app.getRootNav().push(SearchresultPage, {
      action: "brands",
      item: item
    });
  }

  viewChat(shop: any) {
    console.log("shop: ", shop);
    if (_.isEmpty(this.user)) {
      let modal = this.modalCtrl.create(LoginPage);
      modal.present();
      return;
    }

    this.navCtrl.push(ChatdetailPage, {
      user: {
        user_id: this.shop.uuid,
        name: this.shop.sellerName,
        avatar: this.shop.img_path
      },
      isLink: true,
      product: {
        uuid: this.details.uuid,
        name: this.details.productName,
        description: this.details.productDesc,
        image: this.details.image,
        images: this.details.images,
        price: this.details.productPrice,
        variants: this.details.variants,
        productInfo: this.details
      }
    });
  }

  showVariants() {
    const modal = this.modalCtrl.create(
      ModalComponentFormPage,
      {
        action: "product_variants_modal",
        variants: this.details.variants,
        image: this.details.image,
        details: this.details,
        modalInset: true
      },
      { cssClass: "inset-modal" }
    );

    modal.onDidDismiss(resp => {
      if (resp) {
        this.shoppingCart
          .getCart()
          .then((data: any) => {
            this.cartCount = data.length;
          })
          .then(() => {
            this.events.publish("update:cart");
          });
      }
    });

    modal.present();
  }

  showVouchers() {
    const modal = this.modalCtrl.create(
      ModalComponentFormPage,
      {
        action: "product_vouchers_modal",
        variants: this.details.variants,
        image: this.details.image,
        details: this.details,
        modalInset: true
      },
      {
        cssClass: "inset-modal2"
      }
    );
    modal.present();
  }

  showWholesale() {
    const modal = this.modalCtrl.create(
      ModalComponentFormPage,
      {
        action: "product-wholesale-modal",
        wholesale: this.details.wholesale,
        image: this.details.image,
        details: this.details,
        modalInset: true
      },
      {
        cssClass: "inset-modal"
      }
    );
    modal.present();
  }

  showShippingFee() {
    const modal = this.modalCtrl.create(
      ModalComponentFormPage,
      {
        action: "product-shipping-modal",
        shippingFee: this.details.shippingFee,
        image: this.details.image,
        details: this.details,
        modalInset: true
      },
      {
        cssClass: "inset-modal"
      }
    );
    modal.present();
  }

  addToCart() {
    if (_.isEmpty(this.user)) {
      let modal = this.modalCtrl.create(LoginPage);
      modal.present();
      return;
    }

    if (this.details) {
      let loading = this.loadingCtrl.create();
      loading.onDidDismiss(() => {
        this.shoppingCart
          .getCart()
          .then((data: any) => {
            this.cartCount = data.length;
          })
          .then(() => {
            this.events.publish("update:cart");
          });
      });

      if (_.size(this.details.variants) > 0) {
        const modal = this.modalCtrl.create(
          ModalComponentFormPage,
          {
            action: "product_variants_modal",
            variants: this.details.variants,
            image: this.details.image,
            details: this.details,
            modalInset: true
          },
          { cssClass: "inset-modal" }
        );

        modal.onDidDismiss(resp => {
          if (resp) {
            this.shoppingCart
              .getCart()
              .then((data: any) => {
                this.cartCount = data.length;
              })
              .then(() => {
                this.events.publish("update:cart");
              });
          }
        });
        modal.present();
      } else {
        loading.present().then(() => {
          this.shoppingCart.addItem(this.details, 1);
        });
        setTimeout(() => {
          loading.dismiss();
        }, 600);
      }
    } else {
      let alert = this.alertCtrl.create({
        subTitle: "The product failed to load. Please reload.",
        buttons: [
          {
            text: "RELOAD",
            handler: data => {
              this.initializeData();
            }
          }
        ]
      });
      alert.present();
    }
  }

  buyNow() {
    if (_.isEmpty(this.user)) {
      let modal = this.modalCtrl.create(LoginPage);
      modal.present();
      return;
    }

    if (this.details) {
      const loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      if (_.size(this.details.variants) > 0) {
        const modal = this.modalCtrl.create(
          ModalComponentFormPage,
          {
            action: "product_variants_modal",
            variants: this.details.variants,
            image: this.details.image,
            details: this.details,
            modalInset: true
          },
          { cssClass: "inset-modal" }
        );
        modal.onDidDismiss(resp => {
          if (resp) {
            this.shoppingCart
              .getCart()
              .then((data: any) => {
                this.cartCount = data.length;
              })
              .then(() => {
                this.events.publish("update:cart");
                this.navCtrl.push(ShoppingcartPage);
              });
          }
        });
        modal.present();
      } else {
        loading.onDidDismiss(() => {
          this.shoppingCart
            .getCart()
            .then((data: any) => {
              this.cartCount = data.length;
            })
            .then(() => {
              this.events.publish("update:cart");
              this.navCtrl.push(ShoppingcartPage);
            });
        });
        loading.present().then(() => {
          this.shoppingCart.addItem(this.details, 1);
        });
        setTimeout(() => {
          loading.dismiss();
        }, 600);
      }
    } else {
      let alert = this.alertCtrl.create({
        subTitle: "The product failed to load. Please reload.",
        buttons: [
          {
            text: "RELOAD",
            handler: data => {
              this.initializeData();
            }
          }
        ]
      });
      alert.present();
    }
  }

  updateProduct() {
    this.navCtrl.push(ProductEntryPage, {
      action: "update",
      product: this.details,
      callback: this.refreshAfterCallback
    });
  }
}
