import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { Ionic2RatingModule } from "ionic2-rating";

import { DetailsPage, DetailPopoverPage } from "./details";

import { PipesModule } from "../../pipes/pipes.module";
import { ComponentsModule } from "../../components/components.module";

@NgModule({
  declarations: [DetailsPage, DetailPopoverPage],
  imports: [
    PipesModule,
    ComponentsModule,
    IonicPageModule.forChild(DetailsPage),
    Ionic2RatingModule
  ],
  entryComponents: [DetailPopoverPage]
})
export class DetailsPageModule {}
