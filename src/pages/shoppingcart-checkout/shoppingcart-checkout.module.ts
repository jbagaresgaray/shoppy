import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShoppingcartCheckoutPage } from './shoppingcart-checkout';

import { ComponentsModule } from "../../components/components.module";
import { PipesModule } from "../../pipes/pipes.module";

@NgModule({
	declarations: [
		ShoppingcartCheckoutPage,
	],
	imports: [
		ComponentsModule,
		PipesModule,
		IonicPageModule.forChild(ShoppingcartCheckoutPage),
	],
})
export class ShoppingcartCheckoutPageModule { }
