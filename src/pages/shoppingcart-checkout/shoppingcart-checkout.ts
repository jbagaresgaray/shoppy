import { Component } from "@angular/core";
import {
	IonicPage,
	NavController,
	NavParams,
	LoadingController,
	AlertController
} from "ionic-angular";
import * as async from "async";
import * as _ from "lodash";

import { UserAddressessServiceProvider } from "../../providers/service/user-address";
import { ShoppingCartProvider } from "../../providers/service/shopping-cart";
import { ServiceProvider } from "../../providers/service/service";
import { UserShippingServiceProvider } from "../../providers/service/user-shipping";
import { ProductsServiceProvider } from "../../providers/service/products";
import { SellerServiceProvider } from "../../providers/service/seller";
import { OrdersServiceProvider } from '../../providers/service/orders';

import { ShoppingcartSelectshippingoptionPage } from '../shoppingcart-selectshippingoption/shoppingcart-selectshippingoption';
import { DeliveryaddressPage } from '../shoppingcart-deliveryaddress/deliveryaddress';
import { OrderdetailsPage } from '../../pages/orderdetails/orderdetails';

/**
 * Generated class for the ShoppingcartCheckoutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: "page-shoppingcart-checkout",
	templateUrl: "shoppingcart-checkout.html"
})
export class ShoppingcartCheckoutPage {
	user: any = {};
	order: any = {};
	orderAddress: any = {};
	currency: any = {};
	payMethod: any = {};

	paymentOption: string = "cod";

	cartArr: any[] = [];
	shippingArr: any[] = [];
	tmpShippingArr: any[] = [];
	userAddressArr: any[] = [];
	paymentOptions: any[] = [];

	total: number = 0;
	shippingTotal: number = 0;
	subTotal: number = 0;
	cartCount: number = 0;
	walletBalance: number = 0;

	disableNext: boolean = false;
	showAddress: boolean = false;

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public loadingCtrl: LoadingController,
		public alertCtrl: AlertController,
		public shoppingCart: ShoppingCartProvider,
		public addressess: UserAddressessServiceProvider,
		public sellershipping: UserShippingServiceProvider,
		public services: ServiceProvider,
		public products: ProductsServiceProvider,
		public sellers: SellerServiceProvider,
		public orderService: OrdersServiceProvider
	) {
		this.user = JSON.parse(localStorage.getItem("app.user")) || {};
		this.currency = this.services.defaultCurrency();
	}

	ionViewDidLoad() {
		console.log("ionViewDidLoad ShoppingcartCheckoutPage");
		this.order = JSON.parse(localStorage.getItem("app.orders")) || {};
		this.order.products = [];
		this.order.orderEmail = this.user.email;

		this.initAddressData().then(() => {
			this.initData();
		});
	}

	async initAddressData() {
		let loading = this.loadingCtrl.create({
			content: "Loading address..."
		});
		loading.present();
		this.showAddress = false;
		await this.addressess
			.getAllUserAddress()
			.then(
				(data: any) => {
					if (data && data.success) {
						this.userAddressArr = data.result;
						_.each(this.userAddressArr, (row: any) => {
							row.selected = row.isDefaultAddress;

							if (row.isDefaultAddress) {
								this.orderAddress = row;

								this.order.addressId = row._id;
								this.order.orderShipAddress = row.detailAddress;
								this.order.orderShipCity = row.city;
								this.order.orderShipState = row.state;
								this.order.orderShipZipCode = row.zipcode;
								this.order.orderShipCountry = row.country;
								this.order.orderShipSuburb = row.suburb;
								this.order.orderPhone = row.mobile;
								this.order.orderShipName = row.name;
							}
						});
					}
					loading.dismiss();
					this.showAddress = true;
					console.log("this.orderAddress: ", this.orderAddress);
					console.log("this.order: ", this.order);
				},
				error => {
					loading.dismiss();
					this.showAddress = true;
				});
	}

	private initData() {
		let vm = this;
		this.shoppingCart.getCart().then((data: any) => {
			this.groupCart(data);
		}).then(() => {
			let loading = vm.loadingCtrl.create({
				dismissOnPageChange: true,
				content: "Loading cart..."
			});
			loading.present();
			async.eachSeries(this.cartArr, (cart, callback) => {
				async.parallel([
					(callback2) => {
						async.eachSeries(cart.details, (item, callback1) => {
							this.products.getProductShipping(item.uuid).then((data: any) => {
								if (data && data.success) {
									item.shipping = data.result;
									if (_.isEmpty(item.shipping)) {
										this.disableNext = true;
									}
								}
								console.log("push");
								this.order.products.push(item);
								callback1();
							}, (error) => {
								callback1();
							});
						}, () => {
							callback2();
						});
					},
					(callback2) => {
						this.sellers.getAllUserSellerShipping(cart.storeId).then((data: any) => {
							if (data && data.success) {
								this.shippingArr = data.result;
							}
							callback2();
						}, (error) => {
							callback2();
						});
					}
				], () => {
					callback();
				})
			}, () => {
				loading.dismiss();

				_.each(this.cartArr, (row: any) => {
					_.each(row.details, (row2: any) => {
						_.each(row2.shipping, (o: any) => {
							this.tmpShippingArr.push(o);
						});
					});

					this.tmpShippingArr = _.uniqBy(this.tmpShippingArr, 'shippingCourierId');
					let selected_shipping: any = _.maxBy(this.tmpShippingArr, (o: any) => { return o.fee; });
					if (_.find(this.shippingArr, { 'shippingId': selected_shipping.shippingCourierId })) {
						row.shippingFee = selected_shipping.fee;
						row.selected_shipping = selected_shipping;
					} else {
						row.shippingFee = 0;
						row.selected_shipping = null;
					}
					row.subTotal = _.sumBy(row.details, (o: any) => { return o.quantity * o.details.productPrice; });
				});
				this.shippingTotal = _.sumBy(this.cartArr, 'shippingFee');
				this.subTotal = _.sumBy(this.cartArr, 'subTotal');
				this.total = _.sumBy(this.cartArr, 'subTotal') + this.shippingTotal;
			});
		});
	}

	private groupCart(carts) {
		let sortedContacts = _.orderBy(carts, ["storeId"], ["asc"]);
		let currentLetter = false;
		let currentContacts = [];
		this.cartArr = [];
		sortedContacts.forEach((value: any, index) => {
			if (value.storeId != currentLetter) {
				currentLetter = value.storeId;
				let newGroup = {
					store: value.store,
					storeId: value.storeId,
					details: []
				};
				currentContacts = newGroup.details;
				this.cartArr.push(newGroup);
			}
			currentContacts.push(value);
		});
	}

	private updateProductShipping = (data: any) => {
		let ctrl = this;
		return new Promise((resolve, reject) => {
			_.each(ctrl.cartArr, (row: any) => {
				if (row.storeId == data.storeId) {
					row.selected_shipping = data;
				}
				row.shippingFee = row.selected_shipping.fee;
				row.subTotal = _.sumBy(row.details, (o: any) => { return o.quantity * o.details.productPrice; });
			});
			this.shippingTotal = _.sumBy(this.cartArr, 'shippingFee');
			this.subTotal = _.sumBy(this.cartArr, 'subTotal');
			this.total = _.sumBy(this.cartArr, 'subTotal') + this.shippingTotal;
			resolve();
		});
	}

	private updateShippingAddress = (data: any) => {
		let ctrl = this;
		return new Promise((resolve, reject) => {
			this.orderAddress = data;

			this.order.addressId = data._id;
			this.order.orderShipAddress = data.detailAddress;
			this.order.orderShipCity = data.city;
			this.order.orderShipState = data.state;
			this.order.orderShipZipCode = data.zipcode;
			this.order.orderShipCountry = data.country;
			this.order.orderShipSuburb = data.suburb;
			this.order.orderPhone = data.mobile;
			this.order.orderShipName = data.name;
			resolve();
		});
	}

	changeShippingAddress(address) {
		this.navCtrl.push(DeliveryaddressPage, {
			callback: this.updateShippingAddress,
		});
	}

	changeShipping(order) {
		this.navCtrl.push(ShoppingcartSelectshippingoptionPage, {
			order: order,
			tmpShippingArr: this.tmpShippingArr,
			callback: this.updateProductShipping,
		});
	}

	proceedCheckOut() {
		if (this.paymentOption == 'wallet') {
			if (this.walletBalance < 1) {
				this.alertCtrl.create({
					title: 'Insufficient Balance',
					subTitle: 'Sorry, you do not have enough balance in your Wallet for this checkout. Please choose another payment method.',
					buttons: ['OK']
				}).present();
				return;
			}
		}

		_.each(this.cartArr, (row: any) => {
			if (_.isEmpty(row.selected_shipping)) {
				this.alertCtrl.create({
					title: 'Warning! No Shipping Provider',
					subTitle: 'Sorry, unable to proceed. Make sure to select Shipping provider to handle the delivery!',
					buttons: ['OK']
				}).present();
				return;
			}
		});

		if (_.isEmpty(this.order.orderEmail)) {
			this.alertCtrl.create({
				title: 'WARNING',
				message: 'Please fill in and confirm your email address.',
				buttons: ['OK']
			}).present();
			return;
		}

		this.order.modePayment = this.paymentOption;
		this.order.orderDateTime = new Date();
		this.order.cartOrder = this.cartArr;
		this.order.orderAmt = this.total;
		this.order.orderShippingFee = this.shippingTotal;

		// localStorage.setItem('app.orders', JSON.stringify(this.order));
		console.log("orders: ", this.order);

		const placeOrder = () => {
			let loading = this.loadingCtrl.create({
				dismissOnPageChange: true,
				content: 'Saving...'
			});
			loading.present();
			this.orderService.createOrder(this.order).then((data: any) => {
				console.log('data: ', data);
				if (data && data.success) {
					this.shoppingCart.emptyCart();
					this.navCtrl.push(OrderdetailsPage, {
						batchId: data.result,
						status: 'topay',
						usertype: 'buyer'
					});
				}
				loading.dismiss();
			}, (error) => {
				loading.dismiss();
			});
		};

		const confirm = this.alertCtrl.create({
			title: 'Checkout Order?',
			message: 'Proceed to place the order?',
			buttons: [
				{
					text: 'Cancell',
					handler: () => {
						console.log('Disagree clicked');
					}
				},
				{
					text: 'Confirm',
					handler: () => {
						console.log('Agree clicked');
						placeOrder();
					}
				}
			]
		});
		confirm.present();



	}
}
