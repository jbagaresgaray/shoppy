import { Component } from "@angular/core";
import {
	IonicPage,
	NavController,
	NavParams,
	LoadingController,
	Events,
	AlertController
} from "ionic-angular";

import { ShopprofileEntryPage } from "../../pages/shopprofile-entry/shopprofile-entry";
import { ShopprofileSettingsPage } from "../../pages/shopprofile-settings/shopprofile-settings";
import { MycustomersPage } from "../../pages/mycustomers/mycustomers";
import { MyproductsPage } from "../../pages/myproducts/myproducts";
import { HelpCentrePage } from "../../pages/help-centre/help-centre";
import { ContentFormPage } from "../../pages/content-form/content-form";

import { UsersServiceProvider } from "../../providers/service/users";
import { SellerServiceProvider } from "../../providers/service/seller";
import { ChatService } from "../../providers/service/chat";

@IonicPage()
@Component({
	selector: "page-manage-shop",
	templateUrl: "manage-shop.html"
})
export class ManageShopPage {
	user: any = {};
	profile: any = {};
	details: any = {};
	unreadCount: number = 0;

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public loadingCtrl: LoadingController,
		public events: Events,
		public alertCtrl: AlertController,
		public users: UsersServiceProvider,
		public chatService: ChatService,
		public sellers: SellerServiceProvider
	) {
		this.user = JSON.parse(localStorage.getItem("app.user")) || {};
		this.profile = navParams.get("profile");
		console.log("this.profile: ", this.profile);

		// ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
		// ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
		// ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //

		events.unsubscribe("onMessageReceived");
		events.unsubscribe("onMessageUpdated");
		events.unsubscribe("getTotalUnreadMessageCount");

		events.subscribe("onMessageReceived", () => {
			this.unreadCount = this.chatService.unreadCount;
		});

		events.subscribe("onMessageUpdated", () => {
			this.unreadCount = this.chatService.unreadCount;
		});

		events.subscribe("getTotalUnreadMessageCount", () => {
			this.unreadCount = this.chatService.unreadCount;
		});
	}

	private initData() {
		const loading = this.loadingCtrl.create({
			dismissOnPageChange: true
		});
		loading.present();
		this.users.getShopProfile(this.profile.uuid).then(
			(data: any) => {
				if (data && data.success) {
					console.log("data: ", data.result);
					this.details = data.result;
				}
				loading.dismiss();
			},
			error => {
				console.log("error: ", error);
				loading.dismiss();
			}
		);
	}

	ionViewDidLoad() {
		console.log("ionViewDidLoad ManageShopPage");
		this.initData();
	}

	ionViewWillEnter() {
		this.unreadCount = this.chatService.unreadCount;
	}

	viewShopProfile() {
		this.navCtrl.push(ShopprofileEntryPage, {
			profile: this.user
		});
	}

	viewMyCustomers() {
		this.navCtrl.push(MycustomersPage, {
			profile: this.user
		});
	}

	viewMyProducts() {
		this.navCtrl.push(MyproductsPage, {
			profile: this.user,
			shop: this.details
		});
	}

	viewShopSettings() {
		this.navCtrl.push(ShopprofileSettingsPage, {
			action: "shop"
		});
	}

	discountPromotions() {
		this.navCtrl.push(ContentFormPage, {
      action: "discount_promotions",
      params: this.details
		});
	}

	sellerVouchers() {
		this.navCtrl.push(ContentFormPage, {
			action: "seller_vouchers"
		});
	}

	sellerTopPicks() {
		this.navCtrl.push(ContentFormPage, {
			action: "seller_toppicks"
		});
	}

	applyForOfficialShop() {
		let vm = this;

		if (this.details.shop_application_date) {
			// Show Page on Evaluation Progress
			this.navCtrl.push(ContentFormPage, {
				action: "official_shop",
				params: {
					shopId: this.profile.uuid
				}
			});
			return;
		}

		if (
			this.details.shop_application_date &&
			this.details.shop_appoved_date
		) {
			// Show page on ShoppyApp Official Store Welcome Page
			this.navCtrl.push(ContentFormPage, {
				action: "official_shop",
				params: {
					shopId: this.profile.uuid
				}
			});
			return;
		}

		function applyOfficialShop() {
			let loading = vm.loadingCtrl.create({
				dismissOnPageChange: true,
				content: "Requesting..."
			});
			loading.present();
			vm.sellers.applyForOfficialShop(vm.profile.uuid).then(
				(data: any) => {
					if (data && data.success) {
						let alert = vm.alertCtrl.create({
							title: "Thank You!",
							message: data.msg,
							buttons: ["OK"]
						});
						alert.onDidDismiss(() => {
							vm.initData();
						});
						alert.present();
					}
					loading.dismiss();
				},
				(error: any) => {
					console.log("Error: ", error);
					loading.dismiss();
				}
			);
		}

		let alert = this.alertCtrl.create({
			message:
				"Official Shops are participating Sellers that have been carefully selected or invited by ShoppyApp that sells their own brand and/or official distributor of a certain brand.",
			buttons: [
				{
					text: "Learn More",
					handler: () => {
						this.navCtrl.push(HelpCentrePage);
					}
				},
				{
					text: "Join",
					handler: () => {
						applyOfficialShop();
					}
				}
			]
		});
		alert.present();
	}
}
