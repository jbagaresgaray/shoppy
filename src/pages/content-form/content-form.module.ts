import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { Ionic2RatingModule } from "ionic2-rating";
import { PipesModule } from "../../pipes/pipes.module";
import { ComponentsModule } from "../../components/components.module";

import { ContentFormPage } from "./content-form";

@NgModule({
  declarations: [ContentFormPage],
  imports: [
    Ionic2RatingModule,
    PipesModule,
    ComponentsModule,
    IonicPageModule.forChild(ContentFormPage)
  ]
})
export class ContentFormPageModule {}
