import { Component, EventEmitter } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  AlertController,
  Platform
} from "ionic-angular";
import { DomSanitizer } from "@angular/platform-browser";
import { HttpClient } from "@angular/common/http";
import * as _ from "lodash";
import * as async from "async";

import { DetailsPage } from "../../pages/details/details";

import { ServiceProvider } from "../../providers/service/service";
import { NotificationsServiceProvider } from "../../providers/service/notifications";
import { ProductsServiceProvider } from "../../providers/service/products";
import { SellerServiceProvider } from "../../providers/service/seller";

declare let safari: any;

@IonicPage()
@Component({
  selector: "page-content-form",
  templateUrl: "content-form.html"
})
export class ContentFormPage {
  action: string;
  title: string;
  sellerMode: string = "seller";
  discountMode: string = "upcoming";
  voucherMode: string = "info";
  content: any;
  params: any;
  navbar_color: any;

  productsArr: any[] = [];
  blockUsers: any[] = [];
  fakeArr: any[] = [];

  showProducts: boolean = false;
  showContent: boolean = false;
  isLoading: boolean = false;

  officialShop: any = {};
  currency: any = {};
  sellerShop: any = {};
  progressCount: number = 0;

  brandGetItems: EventEmitter<any> = new EventEmitter();
  brandClearItems: EventEmitter<boolean> = new EventEmitter();

  refreshNotification: EventEmitter<any> = new EventEmitter();
  createNotification: EventEmitter<any> = new EventEmitter();
  deleteNotification: EventEmitter<any> = new EventEmitter();

  constructor(
    public platform: Platform,
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public navParams: NavParams,
    public services: ServiceProvider,
    private sanitizer: DomSanitizer,
    public http: HttpClient,
    public notifications: NotificationsServiceProvider,
    public loadingCtrl: LoadingController,
    public products: ProductsServiceProvider
  ) {
    this.action = navParams.get("action");
    this.title = this.navParams.get("title");
    this.navbar_color = this.navParams.get("navbar_color");

    this.currency = this.services.defaultCurrency();
    for (var i = 0; i < 20; ++i) {
      this.fakeArr.push(i);
    }

    this.initContentForm();
  }

  private initProducts(ev?: any) {
    this.showProducts = false;
    this.products.getAllProducts().then(
      (data: any) => {
        if (data && data.success) {
          this.productsArr = data.result;
        }
        this.showProducts = true;
        if (ev) {
          ev.complete();
        }
      },
      error => {
        this.showProducts = true;
        console.log("products error: ", error);
        if (ev) {
          ev.complete();
        }
      }
    );
  }

  private initNewsletter(params: any, ev?: any) {
    let loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    if (_.isEmpty(ev)) {
      loading.present();
    }
    this.notifications.getNewsletter(params.link.Id).then(
      (data: any) => {
        if (data && data.success) {
          console.log("data: ", data);
          if (data.result) {
            this.content = data.result.content;
            this.title = data.result.title;
          }
        }
        loading.dismiss();
        if (ev) {
          ev.complete;
        }
      },
      error => {
        loading.dismiss();
        if (ev) {
          ev.complete;
        }
      }
    );
  }

  private initContentForm() {
    if (this.action == "brands") {
      this.title = "Brands";
      this.navbar_color = "shoppyprimary";
    } else if (this.action == "category") {
      this.title = "Categories";
      this.navbar_color = "shoppyprimary";
    } else if (this.action == "terms") {
      this.title = "Terms of Services";
      this.http
        .get("assets/data/terms.html", { responseType: "text" })
        .subscribe((html: any) => {
          this.content = this.sanitizer.bypassSecurityTrustHtml(html);
        });
    } else if (this.action == "privacy") {
      this.title = "Privacy Policy";
      this.http
        .get("assets/data/privacy.html", { responseType: "text" })
        .subscribe((html: any) => {
          this.content = this.sanitizer.bypassSecurityTrustHtml(html);
        });
    } else if (this.action == "items_policy") {
      this.title = "Prohibited and Restricted Items Policy";
      this.http
        .get("assets/data/items_policy.html", { responseType: "text" })
        .subscribe((html: any) => {
          this.content = this.sanitizer.bypassSecurityTrustHtml(html);
        });
    } else if (this.action == "return_policy") {
      this.title = "Return and Refunds Policy";
      this.http
        .get("assets/data/return_policy.html", { responseType: "text" })
        .subscribe((html: any) => {
          this.content = this.sanitizer.bypassSecurityTrustHtml(html);
        });
    } else if (this.action == "notification") {
      this.params = [];
      console.log("notification params: ", this.params);
    } else if (this.action == "newsletter") {
      this.params = {};
      this.params = this.navParams.get("params");
      this.params.link = JSON.parse(this.params.link);
      console.log("newsletter params: ", this.params);
      this.initNewsletter(this.params);
    } else if (this.action == "products") {
      this.initProducts();
    } else if (this.action == "block") {
      this.title = "Block Users";
      this.blockUsers = [];
    } else if (this.action == "order_tracking") {
      this.title = "Shipping Information";
      this.params = {};
      this.params = this.navParams.get("params");
      console.log("tracking params: ", this.params);
    } else if (this.action == "return_instruction") {
      this.title = "Return/Refund Instructions";
      this.params = {};
      this.params = this.navParams.get("params");
      console.log("return_instruction params: ", this.params);
    } else if (this.action == "official_shop") {
      this.title = "Official Shop Application";
      this.params = {};
      this.params = this.navParams.get("params");
    } else if (this.action == "discount_promotions") {
      this.title = "My Discount Promotions";
      this.params = {};
      this.params = this.navParams.get("params");
    } else if (this.action == "seller_vouchers") {
      this.title = "My Vouchers";
    } else if (this.action == "seller_toppicks") {
      this.title = "My Top Picks";
    }else if(this.action == "seller_vouchers_details"){
      this.params = {};
      this.params = this.navParams.get("params");
      this.title = this.params.voucherName;
    }
  }

  ionViewDidLoad() {
    console.log("action: ", this.action);
    this.initContentForm();
  }

  viewDetails(item: any) {
    if (this.action == "notification") {
      this.navCtrl.push(ContentFormPage, {
        action: "newsletter",
        params: item,
        title: item.title
      });
    } else if (this.action == "products") {
      this.navCtrl.push(DetailsPage, {
        product: item
      });
    } else if (this.action == "newsletter") {
      this.navCtrl.push(ContentFormPage, {
        action: "newsletter",
        params: item,
        title: item.title
      });
    }
  }

  doRefresh(ev) {
    if (this.action == "products") {
      this.initProducts(ev);
    } else if (this.action == "newsletter") {
      this.params = {};
      this.params = this.navParams.get("params");
      this.initNewsletter(this.params, ev);
    } else {
      this.refreshNotification.emit(ev);
      setTimeout(() => {
        ev.complete();
      }, 1000);
    }
  }

  createAction() {
    this.createNotification.emit();
  }

  deleteAction() {
    this.deleteNotification.emit();
  }

  doInfinite(ev) {
    setTimeout(() => {
      ev.complete();
    }, 1000);
  }

  onClear() {
    this.brandClearItems.emit(true);
  }

  getItems(ev: any) {
    this.brandGetItems.emit(ev);
  }
}
