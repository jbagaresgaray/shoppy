import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController,
  LoadingController,
  AlertController,
  Platform
} from "ionic-angular";
import * as _ from "lodash";
import * as async from "async";

import { ShopprofilePage } from "../../pages/shopprofile/shopprofile";
import { MypurchasesPage } from "../../pages/mypurchases/mypurchases";
import { ChatdetailPage } from "../../pages/chatdetail/chatdetail";
import { AllFormEntryPage } from "../../pages/all-form-entry/all-form-entry";
import { TabsPage } from "../../pages/tabs/tabs";
import { ContentFormPage } from "../../pages/content-form/content-form";

import { ShoppingCartProvider } from "../../providers/service/shopping-cart";
import { OrdersServiceProvider } from "../../providers/service/orders";
import { ServiceProvider } from "../../providers/service/service";

declare let safari: any;
@IonicPage()
@Component({
  selector: "page-orderdetails",
  templateUrl: "orderdetails.html"
})
export class OrderdetailsPage {
  cartArr: any[] = [];
  order: any = {};
  order_return: any = {};
  currency: any = {};
  total: number = 0;
  batchId: string;
  orderId: string;
  status: string;
  type: string;

  isBatch: boolean = false;
  isSafari: boolean = false;

  isArrangeShipment: boolean = true;
  isOrderDelivered: boolean = false;
  isOrderReceived: boolean = false;

  isOrderReturnRequest: boolean = false;
  isOrderReturnRequestCancelled: boolean = false;
  isOrderReturnRequestAccepted: boolean = false;
  isOrderReturned: boolean = false;
  isOrderReturnDispute: boolean = false;

  isOrderCancelled: boolean = false;
  isOrderCancelRequested: boolean = false;
  isOrderCancelWithdraw: boolean = false;
  isOrderCancelRejected: boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public platform: Platform,
    public shoppingCart: ShoppingCartProvider,
    public orders: OrdersServiceProvider,
    public services: ServiceProvider
  ) {
    this.batchId = this.navParams.get("batchId");
    this.orderId = this.navParams.get("orderId");
    this.status = this.navParams.get("status");
    this.type = this.navParams.get("usertype");

    this.isSafari =
      /constructor/i.test(window["HTMLElement"]) ||
      (function(p) {
        return p.toString() === "[object SafariRemoteNotification]";
      })(!window["safari"] || safari.pushNotification);
    if (!_.isEmpty(this.batchId)) {
      this.isBatch = true;
    }
    this.currency = this.services.defaultCurrency();
    this.order.checkpoint = {};

    console.log("this.batchId: ", this.batchId);
    console.log("this.orderId: ", this.orderId);
    console.log("this.status: ", this.status);
    console.log("this.type: ", this.type);

    console.log("this.isBatch: ", this.isBatch);
  }

  ionViewWillEnter() {
    this.batchId = this.navParams.get("batchId");
    this.orderId = this.navParams.get("orderId");
    this.status = this.navParams.get("status");
    this.type = this.navParams.get("usertype");

    if (this.batchId) {
      this.isBatch = true;
      this.viewCtrl.showBackButton(false);
    }
  }

  ionViewDidEnter() {
    console.log("ionViewDidLoad OrderdetailsPage");
    this.initData();
  }

  initData(ref?: any) {
    let vm = this;

    let loading = vm.loadingCtrl.create({
      dismissOnPageChange: true
    });

    let successResponseData = (data: any) => {
      if (data && data.success) {
        vm.order = data.result;

        if (vm.platform.is("ios")) {
          if (vm.order.shippingDeadline) {
            if (vm.isSafari) {
              vm.order.shippingDeadline = new Date(
                vm.order.shippingDeadline.replace(/-/g, "/")
              );
            } else {
              vm.order.shippingDeadline = new Date(
                vm.order.shippingDeadline.replace(/-/g, "/")
              );
            }
          }

          if (vm.order.deliveryDeadline) {
            if (vm.isSafari) {
              vm.order.deliveryDeadline = new Date(
                vm.order.deliveryDeadline.replace(/-/g, "/")
              );
            } else {
              vm.order.deliveryDeadline = new Date(
                vm.order.deliveryDeadline.replace(/-/g, "/")
              );
            }
          }
        }

        if (vm.order.orderAmt) {
          vm.order.orderAmt = vm.order.orderAmt.toFixed(
            vm.currency.decimal_digits
          );
        }

        if (vm.order.shippingFee) {
          vm.order.shippingFee = vm.order.shippingFee.toFixed(
            vm.currency.decimal_digits
          );
        }

        _.each(vm.order.details, (row: any) => {
          row.subTotal = row.detailPrice * row.detailQty;
          row.detailPrice = row.detailPrice.toFixed(vm.currency.decimal_digits);
          row.subTotal = row.subTotal.toFixed(vm.currency.decimal_digits);
        });

        if (!_.isNull(vm.order.order_return_info)) {
          vm.order_return = vm.order.order_return_info;
          _.each(vm.order_return.return_details, (row: any) => {
            row.subTotal = row.detailPrice * row.detailQty;
            row.detailPrice = row.detailPrice.toFixed(
              vm.currency.decimal_digits
            );
            row.subTotal = row.subTotal.toFixed(vm.currency.decimal_digits);
          });
        }

        async.waterfall(
          [
            callback => {
              if (vm.status == "ship" && vm.type == "seller") {
                vm.orders
                  .isOrderArrangeShipment(vm.order.orderId, vm.order.slug)
                  .then(
                    (data: any) => {
                      if (data && data.success) {
                        vm.isArrangeShipment = true;
                      } else if (data && !data.success) {
                        vm.isArrangeShipment = false;
                      }
                      callback();
                    },
                    (error: any) => {
                      vm.isArrangeShipment = false;
                      callback();
                    }
                  );
              } else {
                callback();
              }
            },
            callback => {
              if (vm.order.courier_slug) {
                vm.orders
                  .getOrderTrackingLastCheckPoint(
                    vm.order.orderId,
                    vm.order.courier_slug
                  )
                  .then(
                    (data: any) => {
                      if (data && data.success) {
                        vm.order.checkpoint = data.result.checkpoint;
                        if (!_.isEmpty(vm.order.checkpoint)) {
                          if (
                            vm.order.isShipped != 1 &&
                            vm.order.isPrepared == 1 &&
                            !_.isEmpty(vm.order.preparedDateTime)
                          ) {
                            vm.order.checkpoint = {
                              slug: vm.order.courier_slug,
                              city: vm.order.orderShipCity,
                              created_at: vm.order.orderDateTime,
                              location: vm.order.orderShipAddress,
                              country_name: null,
                              message: "Order details sent to seller",
                              country_iso3: vm.order.orderShipCountry,
                              tag: "InfoReceived",
                              subtag: "InfoReceived_002",
                              subtag_message: "Info Received",
                              checkpoint_time: vm.order.orderDateTime,
                              coordinates: [],
                              state: vm.order.orderShipState,
                              zip: vm.order.orderShipZipCode
                            };
                          }
                        }
                      } else {
                        vm.order.checkpoint = null;
                      }
                      callback();
                    },
                    (error: any) => {
                      console.log(
                        "getOrderTrackingLastCheckPoint err: ",
                        error
                      );
                      callback();
                    }
                  );
              } else {
                callback();
              }
            }
          ],
          () => {
            loading.dismiss();

            if (vm.order.checkpoint) {
              vm.isOrderDelivered =
                vm.order.checkpoint.tag == "Delivered" ? true : false;
            } else {
              vm.isOrderDelivered = false;
            }
            vm.isOrderReceived =
              vm.order.isReceived == 1 && !_.isEmpty(vm.order.receivedDateTime)
                ? true
                : false;

            if (!_.isEmpty(vm.order.cancelledRequested)) {
              vm.isOrderCancelRequested = true;

              if (
                vm.order.isWithdrawCancelled == 1 &&
                !_.isEmpty(vm.order.WithdrawCancelled)
              ) {
                vm.isOrderCancelWithdraw = true;
              } else {
                vm.isOrderCancelWithdraw = false;
              }

              if (
                vm.order.isRejectCancelled == 1 &&
                !_.isEmpty(vm.order.rejectCancelled)
              ) {
                vm.isOrderCancelRejected = true;
              } else {
                vm.isOrderCancelRejected = false;
              }

              if (
                vm.order.isCancelled == 1 &&
                !_.isEmpty(vm.order.cancelledDateTime)
              ) {
                vm.isOrderCancelled = true;
              } else {
                vm.isOrderCancelled = false;
              }
            } else {
              vm.isOrderCancelRequested = false;
              vm.isOrderCancelWithdraw = false;
              vm.isOrderCancelRejected = false;
              vm.isOrderCancelled = false;
            }

            if (!_.isEmpty(vm.order.order_return_info)) {
              vm.isOrderReturnRequest = true;

              if (
                vm.order_return.isReturnRequestAccepted == 1 ||
                !_.isEmpty(vm.order_return.returnRequestedAccepted)
              ) {
                vm.isOrderReturnRequestAccepted = true;
              }

              if (!_.isEmpty(vm.order_return.cancelReturnRequest)) {
                vm.isOrderReturnRequestCancelled = true;
              }

              if (
                vm.order_return.isReturned == 1 ||
                !_.isEmpty(vm.order.order_return_info.returnedDateTime)
              ) {
                vm.isOrderReturned = true;
              }
            } else {
              vm.isOrderReturnRequest = false;
            }

            console.log("order: ", vm.order);

            console.log("isArrangeShipment: ", vm.isArrangeShipment);
            console.log("isOrderReceived: ", vm.isOrderReceived);
            console.log("isOrderDelivered: ", vm.isOrderDelivered);

            console.log("isOrderCancelled: ", vm.isOrderCancelled);
            console.log("isOrderCancelRequested: ", vm.isOrderCancelRequested);
            console.log("isOrderCancelWithdraw: ", vm.isOrderCancelWithdraw);
            console.log("isOrderCancelRejected: ", vm.isOrderCancelRejected);

            console.log("isOrderReturnRequest: ", vm.isOrderReturnRequest);
            console.log("isOrderReturned: ", vm.isOrderReturned);
            console.log("isOrderReturnDispute: ", vm.isOrderReturnDispute);
            console.log(
              "isOrderReturnRequestCancelled: ",
              vm.isOrderReturnRequestCancelled
            );
            console.log(
              "isOrderReturnRequestAccepted: ",
              vm.isOrderReturnRequestAccepted
            );
            if (ref) {
              ref.complete();
            }
          }
        );
      } else {
        loading.dismiss();
        if (ref) {
          ref.complete();
        }
      }
    };

    let errorResponseData = error => {
      if (ref) {
        ref.complete();
      }
      loading.dismiss();
    };

    if (vm.batchId) {
      vm.isBatch = true;
      loading.present();
      vm.orders
        .getOrderDetailsByBatchNum(vm.batchId)
        .then(successResponseData, errorResponseData);
    } else if (vm.orderId) {
      loading.present();
      vm.orders
        .getOrderDetails(vm.orderId, vm.type)
        .then(successResponseData, errorResponseData);
    }
  }

  groupCart(carts) {
    let sortedContacts = _.orderBy(carts, ["storeId"], ["asc"]);
    let currentLetter = false;
    let currentContacts = [];
    this.cartArr = [];
    sortedContacts.forEach((value: any, index) => {
      if (value.storeId != currentLetter) {
        currentLetter = value.storeId;

        let newGroup = {
          store: value.store,
          details: []
        };
        currentContacts = newGroup.details;
        this.cartArr.push(newGroup);
      }
      currentContacts.push(value);
    });
    _.each(this.cartArr, (row: any) => {
      row.subTotal = _.sumBy(row.details, (o: any) => {
        return o.quantity * o.details.productPrice;
      });
    });

    this.total = _.sumBy(this.cartArr, "subTotal");
  }

  doRefresh(ref) {
    this.initData(ref);
  }

  goHome() {
    this.navCtrl.setRoot(TabsPage, {}, { animate: true, direction: "back" });
  }

  viewShopProfile(item) {
    this.navCtrl.push(ShopprofilePage, {
      profile: item
    });
  }

  gotoPurchases() {
    this.navCtrl.push(MypurchasesPage, {
      action: this.type
    });
  }

  private getOrderDetails = resp => {
    return new Promise((resolve, reject) => {
      if (resp == "save") {
        this.initData();
        resolve();
      }
    });
  };

  cancelOrder() {
    this.navCtrl.push(AllFormEntryPage, {
      orders: this.order,
      action: "cancel",
      callback: this.getOrderDetails
    });
  }

  viewCancelReason() {
    console.log("this.order: ", this.order);
    this.navCtrl.push(AllFormEntryPage, {
      orders: this.order,
      action: "cancel_view",
      usertype: this.type
    });
  }

  approvedOrder() {
    let loading = this.loadingCtrl.create({
      dismissOnPageChange: true,
      content: "Approving request..."
    });
    loading.present();
    var shippingDeadline = new Date();
    shippingDeadline.setDate(shippingDeadline.getDate() + 5);
    this.orders
      .acceptOrder(this.order.orderId, {
        preparedDateTime: new Date(),
        shippingDeadline: shippingDeadline
      })
      .then(
        (data: any) => {
          if (data && data.success) {
            this.navCtrl.popTo(
              this.navCtrl.getByIndex(this.navCtrl.length() - 2)
            );
          } else {
            let alert = this.alertCtrl.create({
              title: "Warning!",
              subTitle: data.msg,
              buttons: ["OK"]
            });
            alert.onDidDismiss(() => {
              this.navCtrl.popTo(
                this.navCtrl.getByIndex(this.navCtrl.length() - 2)
              );
            });
            alert.present();
          }
          loading.dismiss();
        },
        error => {
          loading.dismiss();
        }
      );
  }

  shippedOrder() {
    this.navCtrl.push(AllFormEntryPage, {
      action: "shipping",
      orders: this.order
    });
  }

  orderReceived() {
    let vm = this;

    function orderReceived() {
      let loading = vm.loadingCtrl.create({
        dismissOnPageChange: true
      });
      loading.present();

      let orderReceviedObj: any = {
        receivedDateTime: new Date()
      };

      if (vm.order.isCOD == 1) {
        orderReceviedObj.isPaid = 1;
        orderReceviedObj.paymentDateTime = new Date();
      }

      vm.orders.receivedOrder(vm.order.orderId, orderReceviedObj).then(
        (data: any) => {
          if (data && data.success) {
            vm.navCtrl.popTo(vm.navCtrl.getByIndex(vm.navCtrl.length() - 2));
          } else {
            let alert = vm.alertCtrl.create({
              title: "Warning!",
              subTitle: data.msg,
              buttons: ["OK"]
            });
            alert.onDidDismiss(() => {
              vm.navCtrl.popTo(vm.navCtrl.getByIndex(vm.navCtrl.length() - 2));
            });
            alert.present();
          }
          loading.dismiss();
        },
        error => {
          loading.dismiss();
        }
      );
    }

    const confirm = this.alertCtrl.create({
      title: "Release " + this.order.orderAmt + " to seller",
      message:
        "I confirm I have received the products and accept their condition. There is no refund or return after I confirm?",
      buttons: [
        {
          text: "Not Now",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Confirm",
          handler: () => {
            orderReceived();
          }
        }
      ]
    });
    confirm.present();
  }

  orderReturnRefund() {
    this.navCtrl.push(AllFormEntryPage, {
      action: "return",
      orders: this.order,
      callback: this.getOrderDetails
    });
  }

  contactSeller($event) {
    $event.stopPropagation();
    $event.preventDefault();
    /* this.navCtrl.push(ChatdetailPage, {
      user: {
        Id: "210000198410281948",
        name: "Cassio Zen",
        email: "cassiozen@gmail.com"
      }
    }); */
  }

  contactBuyer($event) {
    $event.stopPropagation();
    $event.preventDefault();
    this.navCtrl.push(ChatdetailPage, {
      user: {
        Id: "210000198410281948",
        name: "Cassio Zen",
        email: "cassiozen@gmail.com"
      }
    });
  }

  viewShippingInformation(order: any) {
    console.log("viewShippingInformation: ", order);
    this.navCtrl.push(ContentFormPage, {
      action: "order_tracking",
      params: order
    });
  }

  withdrawCancellation() {
    let vm = this;

    function withdrawCancellation() {
      let loading = vm.loadingCtrl.create({
        dismissOnPageChange: true
      });
      loading.present();
      vm.orders
        .withdrawCancelOrder(vm.order.orderId, {
          WithdrawCancelled: new Date()
        })
        .then(
          (data: any) => {
            if (data && data.success) {
              vm.navCtrl.popTo(vm.navCtrl.getByIndex(vm.navCtrl.length() - 2));
            } else {
              let alert = vm.alertCtrl.create({
                title: "Warning!",
                subTitle: data.msg,
                buttons: ["OK"]
              });
              alert.onDidDismiss(() => {
                vm.navCtrl.popTo(
                  vm.navCtrl.getByIndex(vm.navCtrl.length() - 2)
                );
              });
              alert.present();
            }
            loading.dismiss();
          },
          error => {
            loading.dismiss();
          }
        );
    }

    const confirm = vm.alertCtrl.create({
      message:
        "You will not be able to cancel this order again once you withdraw this cancellation request. Confirm to withdraw your cancellation request?",
      buttons: [
        {
          text: "Not Now",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Confirm",
          handler: () => {
            withdrawCancellation();
          }
        }
      ]
    });
    confirm.present();
  }

  rejectCancellation() {
    let vm = this;

    function rejectCancellation() {
      let loading = vm.loadingCtrl.create({
        dismissOnPageChange: true
      });
      loading.present();
      vm.orders
        .rejectCancelOrder(vm.order.orderId, {
          rejectCancelled: new Date()
        })
        .then(
          (data: any) => {
            if (data && data.success) {
              vm.navCtrl.popTo(vm.navCtrl.getByIndex(vm.navCtrl.length() - 2));
            } else {
              let alert = vm.alertCtrl.create({
                title: "Warning!",
                subTitle: data.msg,
                buttons: ["OK"]
              });
              alert.onDidDismiss(() => {
                vm.navCtrl.popTo(
                  vm.navCtrl.getByIndex(vm.navCtrl.length() - 2)
                );
              });
              alert.present();
            }
            loading.dismiss();
          },
          error => {
            loading.dismiss();
          }
        );
    }

    const confirm = vm.alertCtrl.create({
      message: "Reject cancellation of order?",
      buttons: [
        {
          text: "Not Now",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Confirm",
          handler: () => {
            rejectCancellation();
          }
        }
      ]
    });
    confirm.present();
  }

  acceptCancellation() {
    let vm = this;

    function cancelOrder() {
      let loading = vm.loadingCtrl.create({
        dismissOnPageChange: true
      });
      loading.present();
      vm.orders
        .cancelOrder(vm.order.orderId, {
          cancelledDateTime: new Date()
        })
        .then(
          (data: any) => {
            if (data && data.success) {
              vm.navCtrl.popTo(vm.navCtrl.getByIndex(vm.navCtrl.length() - 2));
            } else {
              let alert = vm.alertCtrl.create({
                title: "Warning!",
                subTitle: data.msg,
                buttons: ["OK"]
              });
              alert.onDidDismiss(() => {
                vm.navCtrl.popTo(
                  vm.navCtrl.getByIndex(vm.navCtrl.length() - 2)
                );
              });
              alert.present();
            }
            loading.dismiss();
          },
          error => {
            loading.dismiss();
          }
        );
    }

    const confirm = this.alertCtrl.create({
      message: "Confirm of cancellation to order?",
      buttons: [
        {
          text: "Not Now",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Confirm",
          handler: () => {
            cancelOrder();
          }
        }
      ]
    });
    confirm.present();
  }

  inputReturnShippingInformation() {
    if (
      !_.isEmpty(this.order_return.returnTrackingNumber) &&
      this.order_return.isReturnRequestedAcceptedShipping == 1
    ) {
      this.alertCtrl
        .create({
          title: "WARNING",
          message: "Order return request already shipped out!",
          buttons: ["OK"]
        })
        .present();
      return;
    }

    this.navCtrl.push(AllFormEntryPage, {
      action: "return_shipment",
      orders: this.order,
      return: this.order_return,
      callback: this.getOrderDetails
    });
  }

  viewReturnShippingInformation() {
    this.navCtrl.push(AllFormEntryPage, {
      action: "return_shipment_view",
      return: this.order_return
    });
  }

  viewOrderReturnRequestInstruction() {
    this.navCtrl.push(ContentFormPage, {
      action: "return_instruction",
      params: this.order
    });
  }

  cancelOrderReturnRequest() {
    let vm = this;

    function cancelOrderReturnRequest() {
      let returnObj: any = {};
      returnObj.returnId = vm.order_return.returnId;
      returnObj.sellerId = vm.order_return.sellerId;
      returnObj.buyerId = vm.order_return.userId;
      returnObj.userId = vm.order_return.userId;
      returnObj.orderNum = vm.order.orderNum;
      returnObj.cancelReturnRequest = new Date();

      let loading = vm.loadingCtrl.create({
        dismissOnPageChange: true,
        content: "Cancelling..."
      });
      loading.present();
      vm.orders.cancelReturnOrderRequest(vm.order.orderId, returnObj).then(
        (data: any) => {
          if (data && data.success) {
            loading.dismiss();
            vm.navCtrl.popTo(vm.navCtrl.getByIndex(vm.navCtrl.length() - 2));
          } else {
            loading.dismiss();
            let alert = vm.alertCtrl.create({
              title: "Warning!",
              subTitle: data.msg,
              buttons: ["OK"]
            });
            alert.onDidDismiss(() => {
              vm.navCtrl.popTo(vm.navCtrl.getByIndex(vm.navCtrl.length() - 2));
            });
            alert.present();
          }
        },
        (error: any) => {
          console.log("error: ", error);
          loading.dismiss();
        }
      );
    }

    const confirm = vm.alertCtrl.create({
      message:
        "You will not be able to cancel this Return/Refund Order again once you withdraw this cancellation request. Confirm to withdraw your cancellation request?",
      buttons: [
        {
          text: "Not Now",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Confirm",
          handler: () => {
            cancelOrderReturnRequest();
          }
        }
      ]
    });
    confirm.present();
  }

  acceptBuyerRefundRequest() {
    console.log("this.order_return: ", this.order_return);
    let vm = this;

    function acceptBuyerRefundRequest(action) {
      let returnRequestedDeadline = new Date();
      returnRequestedDeadline.setDate(returnRequestedDeadline.getDate() + 3);

      let returnObj: any = {};
      returnObj.returnRequestedDeadline = returnRequestedDeadline;
      returnObj.returnRequestedAccepted = new Date();
      returnObj.returnId = vm.order_return.returnId;
      returnObj.sellerId = vm.order_return.sellerId;
      returnObj.buyerId = vm.order_return.userId;
      returnObj.userId = vm.order_return.userId;

      if (action == "shipment") {
        returnObj.isReturnRequestedAcceptedShipping = 1;
        returnObj.isReturnRequestedAcceptedRefund = 0;
      } else {
        returnObj.isReturnRequestedAcceptedShipping = 0;
        returnObj.isReturnRequestedAcceptedRefund = 1;
        returnObj.returnedDateTime = new Date();
        returnObj.returnedReason = vm.order_return.returnedReason;
        returnObj.returnTransCode = vm.order_return.returnTransCode;
        returnObj.returnTotal = vm.order_return.returnTotal;
        returnObj.orderNum = vm.order.orderNum;
      }

      let loading = vm.loadingCtrl.create({
        dismissOnPageChange: true,
        content: "Accepting..."
      });
      loading.present();
      vm.orders.acceptReturnOrderRequest(vm.order.orderId, returnObj).then(
        (data: any) => {
          if (data && data.success) {
            loading.dismiss();
            vm.navCtrl.popTo(vm.navCtrl.getByIndex(vm.navCtrl.length() - 2));
          } else {
            loading.dismiss();
            let alert = vm.alertCtrl.create({
              title: "Warning!",
              subTitle: data.msg,
              buttons: ["OK"]
            });
            alert.onDidDismiss(() => {
              vm.navCtrl.popTo(vm.navCtrl.getByIndex(vm.navCtrl.length() - 2));
            });
            alert.present();
          }
        },
        (error: any) => {
          console.log("error: ", error);
          loading.dismiss();
        }
      );
    }

    const prompt = this.alertCtrl.create({
      title: "Accept Return",
      message: "Select order return options ",
      cssClass: "alertCustomCss",
      inputs: [
        {
          type: "radio",
          label:
            "Notify buyer to return item(s) to:" +
            this.order_return.seller_address.name +
            " (" +
            this.order_return.seller_address.mobile +
            ") \n" +
            this.order_return.seller_address.detailAddress +
            " \n" +
            this.order_return.seller_address.suburb +
            " \n" +
            this.order_return.seller_address.city +
            " " +
            this.order_return.seller_address.state +
            " " +
            this.order_return.seller_address.country +
            " " +
            this.order_return.seller_address.zipcode,
          value: "shipment",
          checked: true
        },
        {
          type: "radio",
          label:
            "Refund to buyer immediately. \nShoppyApp will release refund to buyer immediately.",
          value: "refund"
        }
      ],
      buttons: [
        {
          text: "Cancel",
          handler: data => {
            console.log("cancel clicked");
          }
        },
        {
          text: "Confirm",
          handler: data => {
            console.log("search clicked: ", data);
            acceptBuyerRefundRequest(data);
          }
        }
      ]
    });
    prompt.present();
  }

  acceptBuyerRefund() {
    let acceptBuyerRefund = () => {
      let loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      loading.present();

      let returnObj: any = {};
      returnObj.returnedDateTime = new Date();
      returnObj.returnId = this.order_return.returnId;
      returnObj.sellerId = this.order_return.sellerId;
      returnObj.buyerId = this.order_return.userId;
      returnObj.userId = this.order_return.userId;

      this.orders.returnOrder(this.order.orderId, returnObj).then(
        (data: any) => {
          if (data && data.success) {
            loading.dismiss();
            this.navCtrl.popTo(
              this.navCtrl.getByIndex(this.navCtrl.length() - 2)
            );
          } else {
            loading.dismiss();
            let alert = this.alertCtrl.create({
              title: "Warning!",
              subTitle: data.msg,
              buttons: ["OK"]
            });
            alert.onDidDismiss(() => {
              this.navCtrl.popTo(
                this.navCtrl.getByIndex(this.navCtrl.length() - 2)
              );
            });
            alert.present();
          }
        },
        (error: any) => {
          console.log("error: ", error);
          loading.dismiss();
        }
      );
    };

    const prompt = this.alertCtrl.create({
      title: "Accept Refund",
      message:
        "This release the . Confirm to withdraw your cancellation request?",
      buttons: [
        {
          text: "Not Now",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Confirm",
          handler: () => {
            acceptBuyerRefund();
          }
        }
      ]
    });
    prompt.present();
  }

  submitDisputeOrderReturn() {
    let vm = this;

    const confirm = vm.alertCtrl.create({
      message:
        "Are you you want to request for Return/Refund dispute?.\nIn any event ShoppyApp reserves the sole right to make the final decision.\n\nConfirm to raise a dispute on the order return request?",
      buttons: [
        {
          text: "Learn More",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Confirm",
          handler: () => {}
        }
      ]
    });
    confirm.present();
  }
}
