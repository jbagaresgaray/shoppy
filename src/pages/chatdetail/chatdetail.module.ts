import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import { PipesModule } from '../../pipes/pipes.module';
import { ComponentsModule } from '../../components/components.module';
import { ChatdetailPage, ChatDetailPopoverPage } from './chatdetail';

@NgModule({
  declarations: [
    ChatdetailPage,
    ChatDetailPopoverPage,
  ],
  imports: [
    PipesModule, ComponentsModule,
    IonicPageModule.forChild(ChatdetailPage),
  ],
  entryComponents: [
    ChatDetailPopoverPage
  ]
})
export class ChatdetailPageModule { }
