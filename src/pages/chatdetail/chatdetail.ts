import { Component, ViewChild } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  PopoverController,
  ViewController,
  Events,
  Content,
  TextInput,
  LoadingController,
  ModalController
} from "ionic-angular";
import * as _ from "lodash";

import {
  ChatService,
  ChatMessage,
  UserInfo
} from "../../providers/service/chat";
import { ShoppingCartProvider } from "../../providers/service/shopping-cart";
import { ServiceProvider } from "../../providers/service/service";

import { ShoppingcartPage } from "../shoppingcart/shoppingcart";
import { ModalComponentFormPage } from "../../pages/modal-component-form/modal-component-form";
import { OrderdetailsPage } from "../../pages/orderdetails/orderdetails";

@Component({
  template: `
    <ion-list>
      <ion-item (click)="close()"
        ><ion-icon name="contact" item-start></ion-icon> View Profile</ion-item
      >
      <ion-item (click)="close()"
        ><ion-icon name="information-circle" item-start></ion-icon> Report this
        user</ion-item
      >
      <ion-item (click)="close()"
        ><ion-icon name="remove-circle" item-start></ion-icon> Block this
        user</ion-item
      >
    </ion-list>
  `
})
export class ChatDetailPopoverPage {
  constructor(public viewCtrl: ViewController) {}

  close() {
    this.viewCtrl.dismiss();
  }
}
@IonicPage()
@Component({
  selector: "page-chatdetail",
  templateUrl: "chatdetail.html"
})
export class ChatdetailPage {
  isLink: boolean = false;
  isOrder: boolean = false;
  currentUser: UserInfo;
  toUser: UserInfo;

  @ViewChild(Content) content: Content;
  @ViewChild("chat_input") messageInput: TextInput;
  // msgList: ChatMessage[] = [];
  msgList: any[] = [];
  usersTyping: any[] = [];
  editorMsg = "";
  showEmojiPicker = false;
  hasVariants = false;

  minPrice: number;
  maxPrice: number;

  channel: any;
  currency: any = {};
  product: any = {};
  order: any = {};

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public popoverCtrl: PopoverController,
    private events: Events,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    private chatService: ChatService,
    private shoppingCart: ShoppingCartProvider,
    private services: ServiceProvider
  ) {
    let user = navParams.get("user");
    this.toUser = {
      id: user.user_id,
      name: user.name,
      avatar: user.avatar,
      connectionStatus: _.upperFirst(user.connectionStatus)
    };
    console.log("this.toUser: ", this.toUser);

    this.isLink = navParams.get("isLink");
    this.isOrder = navParams.get("isOrder");
    console.log("this.isLink: ", this.isLink);
    console.log("this.isOrder: ", this.isOrder);

    this.product = navParams.get("product");
    if (this.product) {
      if (!_.isEmpty(this.product.variants)) {
        this.hasVariants = true;
      }
      this.minPrice = _.minBy(this.product.variants, (row: any) => {
        return row.price;
      });
      this.maxPrice = _.maxBy(this.product.variants, (row: any) => {
        return row.price;
      });
    }
    console.log("this.product: ", this.product);

    this.order = navParams.get("order");
    console.log("this.order: ", this.order);

    this.currency = this.services.defaultCurrency();
    // Get mock user information
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad ChatdetailPage");
  }

  ionViewWillEnter() {
    let loading = this.loadingCtrl.create();
    loading.present();
    let vuser = JSON.parse(localStorage.getItem("app.user")) || {};
    this.chatService
      .getUserInfo()
      .then(res => {
        this.currentUser = res;
        console.log("this.currentUser: ", this.currentUser);
      })
      .then(() => {
        if (vuser) {
          console.log("vuser: ", vuser);
          this.chatService.connect().then(
            () => {
              this.chatService.ChannelHandler.onMessageReceived = () => {
                this.getMsg();
              };
              this.chatService.ChannelHandler.onMessageUpdated = () => {
                this.getMsg();
              };
              this.chatService.ChannelHandler.onReadReceiptUpdated = () => {
                this.events.publish("onMessageUpdated");
              };

              this.chatService.ChannelHandler.onTypingStatusUpdated = groupChannel => {
                this.scrollToBottom();
                if (!groupChannel.isTyping()) {
                  groupChannel.endTyping();
                  this.usersTyping = [];
                } else {
                  this.usersTyping = groupChannel.getTypingMembers();
                }
              };

              this.chatService.sendBird.addChannelHandler(
                "UNIQUE_HANDLER_ID3",
                this.chatService.ChannelHandler
              );

              this.createChatChannel(loading).then((channel: any) => {
                const invited = _.find(channel.members, (o: any) => {
                  return o.userId == this.toUser.id;
                });
                if (invited) {
                  this.toUser.connectionStatus = _.upperFirst(
                    invited.connectionStatus
                  );
                }

                this.getMsg();
              });
            },
            error => {
              console.log("chatService.connect Error: ", error);
            }
          );
        }
      });
  }

  ionViewDidEnter() {
    // Subscribe to received  new message events
  }

  ionViewWillLeave() {
    this.chatService.sendBird.removeChannelHandler("UNIQUE_HANDLER_ID3");
  }

  onFocus() {
    this.showEmojiPicker = false;
    this.content.resize();
    this.scrollToBottom();
  }

  onTyping(event) {
    if (this.channel) {
      this.channel.startTyping();
    }
  }

  switchEmojiPicker() {
    this.showEmojiPicker = !this.showEmojiPicker;
    if (!this.showEmojiPicker) {
      this.messageInput.setFocus();
    }
    this.content.resize();
    this.scrollToBottom();
  }

  /**
   * @name createChatChannel
   * @returns {void}
   */
  createChatChannel(loading?: any) {
    let params = new this.chatService.sendBird.GroupChannelParams();

    params.isPublic = false;
    params.isEphemeral = false;
    params.isDistinct = true;
    params.addUserIds([this.currentUser.id, this.toUser.id]);
    params.name = this.currentUser.id + "-" + this.toUser.id;

    return new Promise((resolve, reject) => {
      this.chatService.sendBird.GroupChannel.createChannel(
        params,
        (createdChannel, error) => {
          if (error) {
            console.log("createChannel Error: ", error);
            reject(error);
          }
          this.channel = createdChannel;
          if (loading) {
            loading.dismiss();
          }
          resolve(this.channel);
        }
      );
    });
  }

  /**
   * @name getMsg
   * @returns {Promise<ChatMessage[]>}
   */
  private getMsg() {
    let vm = this;
    let messageListQuery = this.channel.createPreviousMessageListQuery();
    messageListQuery.load(30, true, (messageList, error) => {
      if (error) {
        console.error("messageListQuery: ", error);
        return;
      }
      _.orderBy(messageList, ["messageId"], ["asc"]);

      vm.msgList = [];
      _.each(messageList, (message: any) => {
        const newMsg: any = {
          messageId: message.messageId,
          userId: message._sender.userId,
          userName: message._sender.nickname,
          userAvatar: "./assets/imgs/user.png",
          toUserId: this.toUser.id,
          createdAt: message.createdAt,
          updatedAt: message.updatedAt,
          message: message.message,
          channelUrl: message.channelUrl,
          status: "success",
          customType: message.customType,
          channelType: message.channelType
        };
        if (message.data) {
          newMsg.data = JSON.parse(message.data);
        }
        vm.msgList.unshift(newMsg);
      });
      console.log("msgList: ", this.msgList);
      this.scrollToBottom();
      this.channel.markAsRead();
    });
  }

  /**
   * @name sendMsg
   */
  private sendMsg() {
    if (!this.editorMsg.trim()) return;

    if (!this.showEmojiPicker) {
      this.messageInput.setFocus();
    }

    if (this.channel) {
      let msgData: any = null;
      let customType: any = this.chatService.MessageType.MESSAGE_INFO;

      if (this.isLink) {
        msgData = JSON.stringify(this.product);
        customType = this.chatService.MessageType.PRODUCT_INFO;
      } else if (this.isOrder) {
        msgData = JSON.stringify(this.order);
        customType = this.chatService.MessageType.ORDER_INFO;
      }

      this.channel.sendUserMessage(
        this.editorMsg,
        msgData,
        customType,
        (message, error) => {
          if (error) {
            console.error("sendUserMessage error: ", error);
            return;
          }
          this.channel.endTyping();
          this.getMsg();

          this.product = {};
          this.isLink = false;
          this.editorMsg = "";
        }
      );
    }
  }

  getMsgIndexById(id: string) {
    return this.msgList.findIndex(e => e.messageId === id);
  }

  scrollToBottom() {
    setTimeout(() => {
      if (this.content.scrollToBottom) {
        this.content.scrollToBottom();
      }
    }, 400);
  }

  viewDetails(order) {
    let action;

    if (order.isPrepared === 1 && order.isShipped === 0) {
      action = "unpaid";
    } else if (order.isPrepared === 1 && order.isShipped === 1) {
      action = "shipping";
    }

    console.log("order: ", order);
    console.log("action: ", action);

    // this.navCtrl.push(OrderdetailsPage, {
    //   orderId: order.orderId,
    //   status: action,
    //   // usertype: "seller"
    // });
  }

  more(myEvent) {
    let popover = this.popoverCtrl.create(ChatDetailPopoverPage);
    popover.present({
      ev: myEvent
    });
  }

  buyProduct() {
    if (_.size(this.product.variants) > 0) {
      const modal = this.modalCtrl.create(
        ModalComponentFormPage,
        {
          action: "product_variants_modal",
          variants: this.product.variants,
          image: this.product.image,
          details: this.product,
          modalInset: true
        },
        { cssClass: "inset-modal" }
      );

      modal.onDidDismiss(resp => {
        if (resp) {
          this.shoppingCart.addItem(this.product, 1);
          setTimeout(() => {
            this.navCtrl.push(ShoppingcartPage);
          }, 300);
        }
      });
      modal.present();
    } else {
      let loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      loading.present().then(() => {
        this.shoppingCart.addItem(this.product, 1);
        setTimeout(() => {
          this.navCtrl.push(ShoppingcartPage);
        }, 300);
      });
      setTimeout(() => {
        loading.dismiss();
      }, 600);
    }
  }
}
