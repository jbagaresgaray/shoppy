import { Component, ViewChild } from '@angular/core';
import { NavController, LoadingController, AlertController, App, ModalController, Events, Searchbar, Platform } from 'ionic-angular';
import * as _ from 'lodash';
import * as async from 'async';
import { Storage } from '@ionic/storage';

import { DetailsPage } from '../../pages/details/details';
import { ShoppingcartPage } from '../../pages/shoppingcart/shoppingcart';
import { ChatPage } from '../../pages/chat/chat';
import { SearchPage } from '../../pages/search/search';
import { LoginPage } from '../../pages/login/login';
import { SearchresultPage } from '../../pages/searchresult/searchresult';
import { ShopprofilePage } from '../../pages/shopprofile/shopprofile';
import { ContentFormPage } from '../../pages/content-form/content-form';


import { CategoriesServiceProvider } from '../../providers/service/categories';
import { BrandsServiceProvider } from '../../providers/service/brands';
import { ServiceProvider } from '../../providers/service/service';
import { ProductsServiceProvider } from '../../providers/service/products';
import { UsersServiceProvider } from '../../providers/service/users';
import { ShoppingCartProvider } from '../../providers/service/shopping-cart';
import { ChatService } from '../../providers/service/chat';
import { SellerServiceProvider } from '../../providers/service/seller';


declare let safari: any;

@Component({
	selector: 'page-home',
	templateUrl: 'home.html'
})
export class HomePage {

	tickerArr: any[] = [];
	tickerArrCopy: any[] = [];
	items: any[] = [];
	favorites: any[] = [];
	categoriesArr: any[] = [];
	brandsArr: any[] = [];
	bannersArr: any[] = [];
	productsArr: any[] = [];
	userLikesArr: any[] = [];
	dealsArr: any[] = [];
	officialShopsArr: any[] = [];

	fakeArr: any[] = [];

	actions: string = 'top';
	search: any;
	global: any = {};
	user: any = {};
	currency: any = {};

	showList: boolean = false;
	showCategory: boolean = false;
	showBrand: boolean = false;
	showProducts: boolean = false;
	showBanners: boolean = false;
	showOfficialShops: boolean = false;
	showLoading: boolean = true;
	isSafari: boolean = false;

	cartCount: number;
	unreadCount: number = 0;

	@ViewChild('mySearchbar') searchbar: Searchbar;

	constructor(public app: App, public navCtrl: NavController, public loadingCtrl: LoadingController, public storage: Storage, public alertCtrl: AlertController, public modalCtrl: ModalController,
		public categories: CategoriesServiceProvider, public brands: BrandsServiceProvider, public services: ServiceProvider, public products: ProductsServiceProvider, public users: UsersServiceProvider,
		public shoppingCart: ShoppingCartProvider, public events: Events, public chatService: ChatService, public platform: Platform, public sellers: SellerServiceProvider) {

		this.user = JSON.parse(localStorage.getItem('app.user')) || {};
		this.shoppingCart.getCart().then((data: any) => {
			this.cartCount = (!_.isEmpty(data)) ? data.length : 0;
		});
		this.isSafari = /constructor/i.test(window['HTMLElement']) || (function(p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || safari.pushNotification);

		storage.get('search').then((val) => {
			if (!_.isEmpty(val)) {
				this.items = val;
			}
		});

		for (var i = 0; i < 20; ++i) {
			this.fakeArr.push(i);
		}

		this.currency = this.services.defaultCurrency();

		// ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
		// ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
		// ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //

		events.unsubscribe('update:cart');
		events.unsubscribe('onMessageReceived');
		events.unsubscribe('onMessageUpdated');
		events.unsubscribe('getTotalUnreadMessageCount');

		events.subscribe('update:cart', () => {
			this.cartCount = this.shoppingCart.cartCount();
		});

		events.subscribe('onMessageReceived', () => {
			this.unreadCount = this.chatService.unreadCount;
		});

		events.subscribe('onMessageUpdated', () => {
			this.unreadCount = this.chatService.unreadCount;
		});

		events.subscribe('getTotalUnreadMessageCount', () => {
			this.unreadCount = this.chatService.unreadCount;
		});


		/*if (this.user) {
			this.socket.connect();
			this.socket.emit('create-room', this.user.uuid);
		}*/
	}

	private initData(ev?: any) {

		if (!_.isEmpty(this.user)) {
			this.users.getCurrentUserLike().then((data: any) => {
				if (data && data.success) {
					this.userLikesArr = data.result;
					_.each(this.productsArr, (row: any) => {
						let res = _.find(this.userLikesArr, { 'productId': row.productId });
						if (res) {
							row.isLiked = true;
						} else {
							row.isLiked = false;
						}
					});
				}
			}, (error) => {
				console.log('getCurrentUserLike: ', error);
			});

			async.parallel([
				(callback) => {
					this.users.getCurrentUserFollowers().then((data: any) => {
						if (data && data.success) {
							this.user.followers = data.result;
						}
						callback();
					}, (error) => {
						console.log('error: ', error);
						this.user.followers = [];
						callback();
					});
				},
				(callback) => {
					this.users.getCurrentUserFollowing().then((data: any) => {
						if (data && data.success) {
							this.user.following = data.result;
						}
						callback();
					}, (error) => {
						console.log('error: ', error);
						this.user.following = [];
						callback();
					});
				}
			], () => {
				localStorage.setItem('app.user', JSON.stringify(this.user));
			});
		}

		async.parallel([
			(callback) => {
				this.categories.getAllCategories().then((data: any) => {
					if (data && data.success) {
						this.categoriesArr = [];
						this.categoriesArr = data.result;
					}
					this.showCategory = true;
					callback();
				}, (error) => {
					this.showCategory = true;
					console.log('category error: ', error);
					callback();
				});
			},
			(callback) => {
				this.sellers.getAllOfficialShop({
					isApproved: true
				}).then((data: any) => {
					if (data && data.success) {
						this.officialShopsArr = [];
						this.officialShopsArr = data.result;
					}
					this.showOfficialShops = true;
					callback();
				}, (error) => {
					this.showOfficialShops = true;
					console.log('officialshops error: ', error);
					callback();
				})
			},
			(callback) => {
				this.services.getHomeBanners().then((data: any) => {
					if (data && data.success) {
						this.bannersArr = [];
						this.bannersArr = data.result;
					}
					this.showBanners = true;
					callback();
				}, (error) => {
					this.showBanners = true;
					console.log('banners error: ', error);
					callback();
				});
			},
			(callback) => {
				this.products.getAllProducts().then((data: any) => {
					if (data && data.success) {
						this.productsArr = [];
						_.each(data.result, (row: any) => {
							if (this.isSafari && this.platform.is('ios')) {
								row.date_created = new Date(row.date_created.replace(/-/g, "/"));
							}
						});
						this.productsArr = data.result;
					}
					this.showProducts = true;
					callback();
				}, (error) => {
					this.showProducts = true;
					console.log('products error: ', error);
					callback();
				});
			}
		], () => {
			this.showLoading = false;

			if (ev) {
				ev.complete();
			}
			this.search = '';
			// this.searchbar.clearInput(null);
		});
	}

	ionViewDidLoad() {
		this.showList = false;
		this.showCategory = false;
		this.showBrand = false;
		this.showProducts = false;
		this.showBanners = false;
		this.showOfficialShops = false;
		this.showLoading = true;

		this.isSafari = /constructor/i.test(window['HTMLElement']) || (function(p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || safari.pushNotification);

		this.initData();
	}

	ionViewWillLeave() {
		console.log('ionViewWillLeave');
		this.showList = false;
	}

	ionViewWillEnter() {
		console.log('ionViewWillEnter');
		this.showList = false;

		this.user = JSON.parse(localStorage.getItem('app.user')) || {};
		this.cartCount = this.shoppingCart.cartCount();
		this.unreadCount = this.chatService.unreadCount;
	}

	doRefresh(refresher) {
		this.showCategory = false;
		this.showBrand = false;
		this.showProducts = false;
		this.showBanners = false;
		this.showOfficialShops = false;
		this.user = JSON.parse(localStorage.getItem('app.user')) || {};

		this.initData(refresher);
	}

	checkBlur() {
		// this.showList = false;
	}

	checkFocus() {
		this.showList = true;
	}

	onCancel() {
		this.showList = false;
	}

	clearSearch() {
		this.search = '';
		this.searchbar.clearInput(null);
		this.storage.remove('search');
		this.storage.get('search').then((val) => {
			if (!_.isEmpty(val)) {
				this.items = val;
			} else {
				this.items = [];
			}
		});
	}

	gotoSearch(item) {
		console.log('gotoSearch')
		this.showList = false;
		this.app.getRootNav().push(SearchPage, {
			search: item
		}).then(() => {
			this.search = '';
			this.searchbar.clearInput(null);
		});
	}

	submitSearch(ev: any) {
		let val = this.search;
		console.log('val: ', val)
		if (val && val.trim() != '') {
			let res = _.find(this.items, (row: any) => {
				return (row == val);
			});
			if (_.isEmpty(res)) {
				this.items.push(val);
				this.storage.set('search', this.items);
			}
			this.showList = false;
			this.app.getRootNav().push(SearchPage, {
				search: val
			}).then(() => {
				this.search = '';
				this.searchbar.clearInput(null);
			});
		} else {
			this.tickerArr = this.tickerArrCopy;
		}
	}



	getCurrentUser() {
		this.user = JSON.parse(localStorage.getItem('app.user')) || {};
		if (!_.isEmpty(this.user)) {
			this.users.getCurrentUser().then((data: any) => {
				if (data && data.success) {
					localStorage.setItem('app.user', JSON.stringify(data.result));
					this.chatService.connect();
					this.events.publish('reloadAppNotifications');
				}
			}, (error) => {
				console.log('error: ', error);
			}).then(() => {
				if (!_.isEmpty(this.user)) {
					this.user = JSON.parse(localStorage.getItem('app.user')) || {};
					async.parallel([
						(callback) => {
							this.users.getCurrentUserFollowers().then((data: any) => {
								if (data && data.success) {
									console.log('getCurrentUserFollowers: ', data);
									this.user.followers = data.result;
								}
								callback();
							}, (error) => {
								console.log('error: ', error);
								this.user.followers = [];
								callback();
							});
						},
						(callback) => {
							this.users.getCurrentUserFollowing().then((data: any) => {
								if (data && data.success) {
									console.log('getCurrentUserFollowing: ', data);
									this.user.following = data.result;
								}
								callback();
							}, (error) => {
								console.log('error: ', error);
								this.user.following = [];
								callback();
							});
						}
					], () => {
						localStorage.setItem('app.user', JSON.stringify(this.user));
					});
				}
			});
		}
	}


	gotoDetails(item) {
		/* this.navCtrl.push(DetailsPage, {
			crypto: item
		}); */
		this.app.getRootNav().push(DetailsPage, {
			product: item
		});
	}

	addToFav(item) {

	}

	segmentChanged(ev) {

	}

	viewCart() {
		// TODO: Check if Authenticated
		if (_.isEmpty(this.user)) {
			let modal = this.modalCtrl.create(LoginPage);
			modal.onDidDismiss((res) => {
				if (res == 'login') {
					this.getCurrentUser();
				}
			});
			modal.present();
			return;
		}

		this.app.getRootNav().push(ShoppingcartPage);
	}

	viewChat() {
		if (_.isEmpty(this.user)) {
			let modal = this.modalCtrl.create(LoginPage);
			modal.onDidDismiss((res) => {
				if (res == 'login') {
					this.getCurrentUser();
				}
			});
			modal.present();
			return;
		}

		let modal = this.modalCtrl.create(ChatPage);
		modal.present();
	}

	viewCategories() {
		this.app.getRootNav().push(ContentFormPage,{
			action: 'category'
		});
	}

	viewBrands() {
		this.app.getRootNav().push(ContentFormPage,{
			action: 'brands'
		});
	}

	viewResults(item, type) {
		this.app.getRootNav().push(SearchresultPage, {
			action: type,
			item: item
		});
	}

	viewShop() {
		this.app.getRootNav().push(ShopprofilePage);
	}

	showMoreProducts() {
		this.app.getRootNav().push(ContentFormPage, {
			action: 'products',
			params: null,
			title: 'Popular Products'
		});
	}
}
