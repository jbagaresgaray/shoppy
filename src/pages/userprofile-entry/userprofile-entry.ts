import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';

import { UserprofilePage } from '../../pages/userprofile/userprofile';

import { UsersServiceProvider } from '../../providers/service/users';

@IonicPage()
@Component({
	selector: 'page-userprofile-entry',
	templateUrl: 'userprofile-entry.html',
})
export class UserprofileEntryPage {
	user: any = {};
	type: any;
	next: any;
	title: string;

	item: any = {};
	errArr: any[] = [];
	constructor(public navCtrl: NavController, public navParams: NavParams, public users: UsersServiceProvider, public loadingCtrl: LoadingController, public alertCtrl: AlertController) {
		this.type = navParams.get('type');
		this.next = navParams.get('next');
		this.user = navParams.get('user');

		if (this.type == 'validate') {
			this.title = 'Verify Password'
		} else if (this.type == 'password') {
			this.title = 'Change Password';
		} else if (this.type == 'email') {
			this.title = 'Edit Email';
		} else if (this.type == 'phone') {
			this.title = 'New Phone No.';
			this.item = this.user;
		} else if (this.type == 'fax') {
			this.title = 'New Fax No.';
			this.item = this.user;
		}
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad UserprofileEntryPage');
	}

	verifyPassword() {
		let loading = this.loadingCtrl.create();
		loading.present();
		this.users.validatePassword({
			password: this.item.password
		}).then((data: any) => {
			if (data && data.success) {
				loading.dismiss();

				this.navCtrl.push(UserprofileEntryPage, {
					type: this.next,
					user: this.user
				});
			} else {
				loading.dismiss();
				let alert = this.alertCtrl.create({
					title: 'Error!',
					subTitle: data.msg,
					buttons: ['OK']
				});
				alert.present();
			}
		}, (error) => {
			loading.dismiss();
		});
	}

	savePassword() {
		let loading = this.loadingCtrl.create();
		loading.present();
		this.users.updateUserPassword({
			password: this.item.password,
			confirmpassword: this.item.confirmpassword
		}).then((data: any) => {
			if (data && data.success) {
				loading.dismiss();

				let alert = this.alertCtrl.create({
					title: 'Success!',
					subTitle: data.msg,
					buttons: ['OK']
				});
				alert.onDidDismiss(() => {
					this.navCtrl.popTo(this.navCtrl.getByIndex(this.navCtrl.length() - 3));
				});
				alert.present();
			} else {
				loading.dismiss();
				let alert = this.alertCtrl.create({
					title: 'Error!',
					subTitle: data.msg,
					buttons: ['OK']
				});
				alert.present();
			}
		}, (error) => {
			loading.dismiss();
			let err = error.data;
			this.errArr = err.result;
			setTimeout(() => {
				this.errArr = [];
			}, 3000);
		});
	}

	saveEmail() {
		let loading = this.loadingCtrl.create();
		loading.present();
		this.users.updateUserEmail({
			email: this.item.email,
		}).then((data: any) => {
			if (data && data.success) {
				loading.dismiss();

				let alert = this.alertCtrl.create({
					title: 'Success!',
					subTitle: data.msg,
					buttons: ['OK']
				});
				alert.onDidDismiss(() => {
					this.navCtrl.popTo(this.navCtrl.getByIndex(this.navCtrl.length() - 3));
				});
				alert.present();
			} else {
				loading.dismiss();
				let alert = this.alertCtrl.create({
					title: 'Error!',
					subTitle: data.msg,
					buttons: ['OK']
				});
				alert.present();
			}
		}, (error) => {
			loading.dismiss();
			let err = error.data;
			this.errArr = err.result;
			setTimeout(() => {
				this.errArr = [];
			}, 3000);
		});
	}

	savePhone() {
		let loading = this.loadingCtrl.create();
		loading.present();
		this.users.updateUserProfile(this.item).then((data: any) => {
			if (data && data.success) {
				loading.dismiss();

				let alert = this.alertCtrl.create({
					title: 'Success!',
					subTitle: data.msg,
					buttons: ['OK']
				});
				alert.onDidDismiss(() => {
					this.navCtrl.popTo(this.navCtrl.getByIndex(this.navCtrl.length() - 3));
				});
				alert.present();
			} else {
				loading.dismiss();
				let alert = this.alertCtrl.create({
					title: 'Error!',
					subTitle: data.msg,
					buttons: ['OK']
				});
				alert.present();
			}
		}, (error) => {
			loading.dismiss();
			let err = error.data;
			this.errArr = err.result;
			setTimeout(() => {
				this.errArr = [];
			}, 3000);
		});
	}

	saveFax() {
		let loading = this.loadingCtrl.create();
		loading.present();
		this.users.updateUserProfile(this.item).then((data: any) => {
			if (data && data.success) {
				loading.dismiss();

				let alert = this.alertCtrl.create({
					title: 'Success!',
					subTitle: data.msg,
					buttons: ['OK']
				});
				alert.onDidDismiss(() => {
					this.navCtrl.popTo(this.navCtrl.getByIndex(this.navCtrl.length() - 3));
				});
				alert.present();
			} else {
				loading.dismiss();
				let alert = this.alertCtrl.create({
					title: 'Error!',
					subTitle: data.msg,
					buttons: ['OK']
				});
				alert.present();
			}
		}, (error) => {
			loading.dismiss();
			let err = error.data;
			this.errArr = err.result;
			setTimeout(() => {
				this.errArr = [];
			}, 3000);
		});
	}
}
