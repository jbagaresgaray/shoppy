import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserprofileEntryPage } from './userprofile-entry';

@NgModule({
  declarations: [
    UserprofileEntryPage,
  ],
  imports: [
    IonicPageModule.forChild(UserprofileEntryPage),
  ],
})
export class UserprofileEntryPageModule {}
