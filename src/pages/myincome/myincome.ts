import { Component } from "@angular/core";
import {
	IonicPage,
	NavController,
	NavParams,
	LoadingController,
	ActionSheetController
} from "ionic-angular";
import * as _ from "lodash";

import { OrderdetailsPage } from "../../pages/orderdetails/orderdetails";
import { ChatdetailPage } from "../chatdetail/chatdetail";

import { SellerServiceProvider } from "../../providers/service/seller";

@IonicPage()
@Component({
	selector: "page-myincome",
	templateUrl: "myincome.html"
})
export class MyincomePage {
	orders: string = "ongoing";
	myDate1: Date = new Date();
	myDate2: Date = new Date();

	mapTabEnabled: boolean = false;
	mapTabEnabled2: boolean = false;

	income: any = {};

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public loadingCtrl: LoadingController,
		public actionSheetCtrl: ActionSheetController,
		public sellers: SellerServiceProvider
	) {}

	ionViewDidLoad() {
		console.log("ionViewDidLoad MyincomePage");
		this.getCurrentUser();
	}

	getCurrentUser(ev?: any) {
		let loading = this.loadingCtrl.create();
		loading.present();
		this.mapTabEnabled = false;
		this.mapTabEnabled2 = false;
		this.sellers.getSellerIncome().then(
			(data: any) => {
				if (data && data.success) {
					console.log("data: ", data);
					this.income = data.result;
					if (!_.isEmpty(this.income.onGoingOrders.orders)) {
						this.mapTabEnabled = true;
					}

					if (!_.isEmpty(this.income.completedOrders.orders)) {
						this.mapTabEnabled2 = true;
					}
				}
				loading.dismiss();
				if (ev) {
					ev.complete();
				}
			},
			error => {
				loading.dismiss();
				if (ev) {
					ev.complete();
				}
				this.income = {
					onGoingOrders: {
						totalAmt: 0,
						orders: []
					},
					completedOrders: {
						totalAmt: 0,
						orders: []
					}
				};
			}
		);
	}

	doRefresh(ev) {
		this.getCurrentUser(ev);
	}

	viewOrderDetails(order) {
		let action;

		if (order.isPrepared === 1 && order.isShipped === 0) {
			action = "unpaid";
		} else if (order.isPrepared === 1 && order.isShipped === 1) {
			action = "shipping";
		}

		this.navCtrl.push(OrderdetailsPage, {
			orderId: order.orderId,
			status: action,
			usertype: "seller"
		});
	}

	contactSeller(order: any, event) {
		event.preventDefault();
		event.stopPropagation();

		console.log("contactSeller: ", order);
		this.navCtrl.push(ChatdetailPage, {
			user: {
				user_id: order.user_info.uuid,
				name: order.user_info.username,
				avatar: order.user_info.img_path
			},
			isOrder: true,
			order: order
		});
	}

	withdraw() {
		const actionSheet = this.actionSheetCtrl.create({
			title: "Confirm withdrawal of Income?",
			buttons: [
				{
					text: "Confirm",
					handler: () => {
						console.log("Archive clicked");
					}
				},
				{
					text: "Cancel",
					role: "cancel",
					handler: () => {
						console.log("Cancel clicked");
					}
				}
			]
		});
		actionSheet.present();
	}
}
