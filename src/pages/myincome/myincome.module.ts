import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NgPipesModule } from 'ngx-pipes';


import { MyincomePage } from './myincome';

@NgModule({
  declarations: [
    MyincomePage,
  ],
  imports: [
  	NgPipesModule,
    IonicPageModule.forChild(MyincomePage),
  ],
})
export class MyincomePageModule {}
