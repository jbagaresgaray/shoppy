import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController
} from "ionic-angular";
import * as _ from "lodash";

import { DetailsPage } from "../../pages/details/details";
import { ProductsServiceProvider } from "../../providers/service/products";

@IonicPage()
@Component({
  selector: "page-productratings",
  templateUrl: "productratings.html"
})
export class ProductratingsPage {
  product: any = {};
  productId: any;
  ratingsArr: any[] = [];
  ratingsArrCopy: any[] = [];

  actions: string = "all";

  rate5 = 5;
  rate4 = 4;
  rate3 = 3;
  rate2 = 2;
  rate1 = 1;

  allCount: number = 0;
  withCommentCount: number = 0;
  withPhotoCount: number = 0;
  fiveCount: number = 0;
  fourCount: number = 0;
  threeCount: number = 0;
  twoCount: number = 0;
  oneCount: number = 0;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public products: ProductsServiceProvider
  ) {
    this.productId = navParams.get("productId");
    this.product = navParams.get("product");
    console.log("this.productId: ", this.productId);
    console.log("this.product: ", this.product);
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad ProductratingsPage");
    this.allCount = 0;
    this.withCommentCount = 0;
    this.withPhotoCount = 0;
    this.fiveCount = 0;
    this.fourCount = 0;
    this.threeCount = 0;
    this.twoCount = 0;
    this.oneCount = 0;

    this.initData();
  }

  initData(ev?: any) {
    let loading = this.loadingCtrl.create();
    if (!_.isEmpty(ev)) {
      loading.present();
    }
    this.products
      .getAllProductRatings(this.productId)
      .then((data: any) => {
        if (data && data.success) {
          this.ratingsArr = data.result.result;
          this.ratingsArrCopy = _.clone(data.result.result);

          console.log("this.ratingsArr: ", this.ratingsArr);
          console.log("this.ratingsArrCopy: ", this.ratingsArrCopy);

          this.allCount = _.size(this.ratingsArrCopy);
          this.fiveCount = _.size(_.filter(this.ratingsArrCopy, { rating: 5 }));
          this.fourCount = _.size(_.filter(this.ratingsArrCopy, { rating: 4 }));
          this.threeCount = _.size(
            _.filter(this.ratingsArrCopy, { rating: 3 })
          );
          this.twoCount = _.size(_.filter(this.ratingsArrCopy, { rating: 2 }));
          this.oneCount = _.size(_.filter(this.ratingsArrCopy, { rating: 1 }));
        }
        loading.dismiss();
        if (ev) {
          ev.complete();
        }
      })
      .catch((error: any) => {
        console.log("error: ", error);
        loading.dismiss();
        if (ev) {
          ev.complete();
        }
      });
  }

  filterRatings(action) {
    this.actions = action;

    if (action == "all") {
      this.ratingsArr = _.clone(this.ratingsArrCopy);
    } else if (action == "comments") {
      this.ratingsArr = [];
    } else if (action == "photos") {
      this.ratingsArr = [];
    } else if (action == "5") {
      this.ratingsArr = _.filter(this.ratingsArrCopy, { rating: 5 });
    } else if (action == "4") {
      this.ratingsArr = _.filter(this.ratingsArrCopy, { rating: 4 });
    } else if (action == "3") {
      this.ratingsArr = _.filter(this.ratingsArrCopy, { rating: 3 });
    } else if (action == "2") {
      this.ratingsArr = _.filter(this.ratingsArrCopy, { rating: 2 });
    } else if (action == "1") {
      this.ratingsArr = _.filter(this.ratingsArrCopy, { rating: 1 });
    }
  }

  viewProduct() {
    this.navCtrl.push(DetailsPage);
  }

  doRefresh(ev) {
    this.initData(ev);
  }
}
