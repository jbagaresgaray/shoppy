import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Ionic2RatingModule } from 'ionic2-rating';

import { ProductratingsPage } from './productratings';

@NgModule({
  declarations: [
    ProductratingsPage,
  ],
  imports: [
    IonicPageModule.forChild(ProductratingsPage),
    Ionic2RatingModule
  ],
})
export class ProductratingsPageModule {}
