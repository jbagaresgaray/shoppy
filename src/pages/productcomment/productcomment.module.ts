import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import { ProductcommentPage } from './productcomment';

import { PipesModule } from '../../pipes/pipes.module';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    ProductcommentPage,
  ],
  imports: [
  	PipesModule,
  	ComponentsModule,
    IonicPageModule.forChild(ProductcommentPage),
  ],
})
export class ProductcommentPageModule {}
