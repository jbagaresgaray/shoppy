import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams,TextInput,Content } from 'ionic-angular';

@IonicPage()
@Component({
	selector: 'page-productcomment',
	templateUrl: 'productcomment.html',
})
export class ProductcommentPage {
	commentsArr: any[] = [];
	@ViewChild('chat_input') messageInput: TextInput;
	@ViewChild(Content) content: Content;
	showEmojiPicker = false;
	editorMsg = '';

    product: any = {};

	constructor(public navCtrl: NavController, public navParams: NavParams) {
        this.product = navParams.get('product');
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad ProductcommentPage');
        this.product = this.navParams.get('product');
        console.log('product: ', this.product);
	}

	onFocus() {
        this.showEmojiPicker = false;
        this.content.resize();
        this.scrollToBottom();
    }

    switchEmojiPicker() {
        this.showEmojiPicker = !this.showEmojiPicker;
        if (!this.showEmojiPicker) {
            this.messageInput.setFocus();
        }
        this.content.resize();
        this.scrollToBottom();
    }

    scrollToBottom() {
        setTimeout(() => {
            if (this.content.scrollToBottom) {
                this.content.scrollToBottom();
            }
        }, 400)
    }

    sendMsg() {
        if (!this.editorMsg.trim()) return;

        // Mock message
        const id = Date.now().toString();
        let newMsg: any = {
            messageId: Date.now().toString(),
            time: Date.now(),
            message: this.editorMsg,
            status: 'pending'
        };

        this.pushNewMsg(newMsg);
        this.editorMsg = '';

        if (!this.showEmojiPicker) {
            this.messageInput.setFocus();
        }
    }

    pushNewMsg(msg: any) {
        this.commentsArr.push(msg);
        this.scrollToBottom();
    }
}
