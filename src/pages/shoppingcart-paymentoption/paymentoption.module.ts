import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaymentoptionPage } from './paymentoption';

@NgModule({
  declarations: [
    PaymentoptionPage,
  ],
  imports: [
    IonicPageModule.forChild(PaymentoptionPage),
  ],
})
export class PaymentoptionPageModule {}
