import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import * as _ from 'lodash';

import { PaymentmethodPage } from '../../pages/shoppingcart-paymentmethod/paymentmethod';
import { ConfirmationPage } from '../../pages/shoppingcart-confirmation/confirmation';

import { ShoppingCartProvider } from '../../providers/service/shopping-cart';
import { ServiceProvider } from '../../providers/service/service';

@IonicPage()
@Component({
	selector: 'page-paymentoption',
	templateUrl: 'paymentoption.html',
})
export class PaymentoptionPage {
	order: any = {};
	currency: any = {};
	paymentOption: string = "cod";

	cartArr: any[] = [];

	total: number = 0;
	shippingTotal: number = 0;
	walletBalance: number = 0;

	enableWallet: boolean = true;

	constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController,
		public shoppingCart: ShoppingCartProvider, public services: ServiceProvider) {
		
		this.order = navParams.get('order');
		this.currency = this.services.defaultCurrency();
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad PaymentoptionPage');
		this.initData();

		if (this.walletBalance < 1) {
			this.enableWallet = false;
		}
	}

	initData() {
		this.order = this.navParams.get('order');
		console.log('this.order: ', this.order);
		if(this.order){
			this.total = this.order.orderAmt;
			this.shippingTotal = this.order.orderShippingFee;
		}
	}


	viewPaymentMethod() {
		this.navCtrl.push(PaymentmethodPage);
	}

	selectPayment(payment) {
		if (payment == 'wallet') {
			if (this.walletBalance < 1) {
				let alert = this.alertCtrl.create({
					title: 'Insufficient Balance',
					subTitle: 'Sorry, you do not have enough balance in your Wallet for this checkout. Please choose another payment method.',
					buttons: ['OK']
				});
				alert.present();
				return;
			}
		}

		this.paymentOption = payment;
	}

	proceedPaymentOption() {
		if (this.paymentOption == 'wallet') {
			if (this.walletBalance < 1) {
				let alert = this.alertCtrl.create({
					title: 'Insufficient Balance',
					subTitle: 'Sorry, you do not have enough balance in your Wallet for this checkout. Please choose another payment method.',
					buttons: ['OK']
				});
				alert.present();
				return;
			}
		}
		this.order.modePayment = this.paymentOption;		
		localStorage.setItem('app.orders', JSON.stringify(this.order));
		this.navCtrl.push(ConfirmationPage, {
			order: this.order
		});
	}
}
