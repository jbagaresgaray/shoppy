import { Component, NgZone } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  ModalController,
  AlertController,
  Events
} from "ionic-angular";
import * as _ from "lodash";

import { ChatPage } from "../../pages/chat/chat";
import { DetailsPage } from "../../pages/details/details";
import { ProductEntryPage } from "../../pages/product-entry/product-entry";

import { UsersServiceProvider } from "../../providers/service/users";
import { SellerServiceProvider } from "../../providers/service/seller";
import { ProductsServiceProvider } from "../../providers/service/products";
import { ChatService } from "../../providers/service/chat";

@IonicPage()
@Component({
  selector: "page-myproducts",
  templateUrl: "myproducts.html"
})
export class MyproductsPage {
  productsArr: any[] = [];
  productsArrCopy: any[] = [];
  fakeArr: any[] = [];

  profile: any = {};
  details: any = {};

  showContent: boolean = false;
  isLow: boolean = true;
  unreadCount: number = 0;

  mode: string = "popular";

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public events: Events,
    public zone: NgZone,
    public chatService: ChatService,
    public users: UsersServiceProvider,
    public products: ProductsServiceProvider,
    public sellers: SellerServiceProvider
  ) {
    this.profile = navParams.get("profile");
    console.log("this.profile: ", this.profile);
    this.details = navParams.get("shop");
    console.log("this.details: ", this.details);

    this.productsArr = [];
    this.productsArrCopy = [];
    this.showContent = false;

    for (var i = 0; i < 12; ++i) {
      this.fakeArr.push(i);
    }

    // ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
    // ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
    // ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
    events.unsubscribe("onMessageReceived");
    events.unsubscribe("onMessageUpdated");
    events.unsubscribe("getTotalUnreadMessageCount");

    events.subscribe("onMessageReceived", () => {
      this.unreadCount = this.chatService.unreadCount;
    });

    events.subscribe("onMessageUpdated", () => {
      this.unreadCount = this.chatService.unreadCount;
    });

    events.subscribe("getTotalUnreadMessageCount", () => {
      this.unreadCount = this.chatService.unreadCount;
    });
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad MyproductsPage");
    this.loadProducts();
  }

  ionViewWillEnter() {
    this.unreadCount = this.chatService.unreadCount;
  }

  loadProducts(ev?: any) {
    let loading = this.loadingCtrl.create();
    if (_.isEmpty(ev)) {
      loading.present();
    }

    this.showContent = false;
    this.sellers.getSellerProducts().then(
      (data: any) => {
        if (data && data.success) {
          console.log("data: ", data.result);
          _.each(data.result, (row: any) => {
            row.date_created_timestamp = new Date(row.date_created).getTime();
          });

          this.productsArr = data.result;
          this.productsArrCopy = data.result;
        }
        this.showContent = true;
        loading.dismiss();
        if (ev) {
          ev.complete();
        }
      },
      error => {
        console.log("error: ", error);
        this.showContent = true;
        loading.dismiss();
        if (ev) {
          ev.complete();
        }
      }
    );
  }

  doRefresh(ev) {
    this.loadProducts(ev);
  }

  createProduct() {
    let modal = this.modalCtrl.create(ProductEntryPage, {
      action: "create"
    });
    modal.onDidDismiss(resp => {
      if (resp == "save") {
        this.loadProducts();
      }
    });
    modal.present();
  }

  viewChat() {
    let modal = this.modalCtrl.create(ChatPage);
    modal.present();
  }

  viewDetails(item) {
    this.navCtrl.push(DetailsPage, {
      product: item
    });
  }

  editProduct(item, event) {
    event.preventDefault();
    event.stopPropagation();

    /*this.navCtrl.push(ProductEntryPage, {
			action: 'update',
			product: item
		});*/

    let modal = this.modalCtrl.create(ProductEntryPage, {
      action: "update",
      product: item
    });
    modal.onDidDismiss(resp => {
      if (resp == "save") {
        this.loadProducts();
      }
    });
    modal.present();
  }

  deleteProduct(product) {
    let vm = this;

    function deleteProduct() {
      let loading = vm.loadingCtrl.create({
        content: "Deleting..."
      });
      loading.present();
      vm.products.deleteProduct(product.productId).then(
        (data: any) => {
          if (data && data.success) {
            let alert = vm.alertCtrl.create({
              title: "Delete Success",
              message: data.msg,
              buttons: ["OK"]
            });
            alert.onDidDismiss(() => {
              vm.loadProducts();
            });
            alert.present();
          }
          loading.dismiss();
        },
        error => {
          console.log("deleteProduct Error: ", error);
          loading.dismiss();
        }
      );
    }

    let confirm = this.alertCtrl.create({
      title: "Remove Product?",
      message: "Are you sure to remove this product?",
      buttons: [
        {
          text: "No",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Yes",
          handler: () => {
            console.log("Agree clicked");
            deleteProduct();
          }
        }
      ]
    });
    confirm.present();
  }

  segmentChanged(ev: any) {
    console.log("this.mode: ", this.mode);
    let loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    loading.present();

    if (this.mode == "popular") {
      this.productsArr = _.orderBy(this.productsArr, "rating", "desc");
    } else if (this.mode == "latest") {
      this.productsArr = _.orderBy(
        this.productsArr,
        "date_created_timestamp",
        "asc"
      );
    } else if (this.mode == "top") {
      this.productsArr = _.orderBy(this.productsArr, "orderscount", "desc");
    } else if (this.mode == "price") {
      if (this.isLow) {
        this.productsArr = _.orderBy(this.productsArr, "productPrice", "asc");
      } else {
        this.productsArr = _.orderBy(this.productsArr, "productPrice", "desc");
      }
    }
    loading.dismiss();
  }

  sortPrice() {
    this.isLow = !this.isLow;
    if (this.isLow) {
      this.zone.run(() => {
        this.productsArr = _.orderBy(this.productsArr, "productPrice", "asc");
      });
    } else {
      this.zone.run(() => {
        this.productsArr = _.orderBy(this.productsArr, "productPrice", "desc");
      });
    }
    console.log("sortPrice: ", this.productsArr);
  }
}
