import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ModalController,
  Tabs,
  App,
  AlertController,
  LoadingController,
  Events
} from "ionic-angular";
import * as _ from "lodash";
import * as async from "async";
// import { Socket } from 'ng-socket-io';
import * as SendBird from "SendBird";

import { environment } from "@app/env";

import { ShoppingcartPage } from "../shoppingcart/shoppingcart";
import { ChatPage } from "../chat/chat";
import { MyratingPage } from "../myrating/myrating";
import { AccountsettingsPage } from "../accountsettings/accountsettings";
import { MywalletPage } from "../mywallet/mywallet";
import { MypurchasesPage } from "../mypurchases/mypurchases";
import { MyshippingPage } from "../myshipping/myshipping";
import { ProductEntryPage } from "../product-entry/product-entry";
import { ShopprofilePage } from "../shopprofile/shopprofile";
import { UserprofilePage } from "../userprofile/userprofile";
import { MyincomePage } from "../myincome/myincome";
import { ManageShopPage } from "../manage-shop/manage-shop";
import { MyfollowerFollowingPage } from "../myfollower-following/myfollower-following";
import { HelpCentrePage } from "../help-centre/help-centre";
import { MyvouchersPage } from "../myvouchers/myvouchers";

import { LoginPage } from "../../pages/login/login";
import { SignupPage } from "../../pages/signup/signup";
import { AboutPage } from "../../pages/about/about";

import { UsersServiceProvider } from "../../providers/service/users";
import { ShoppingCartProvider } from "../../providers/service/shopping-cart";
import { OrdersServiceProvider } from "../../providers/service/orders";
import { ChatService } from "../../providers/service/chat";

@IonicPage()
@Component({
  selector: "page-myprofile",
  templateUrl: "myprofile.html"
})
export class MyprofilePage {
  imageUrl: string = "assets/imgs/banner_def.jpg";
  user: any = {};
  mode: string = "buyer";
  showProfile: boolean = false;
  isSeller: boolean = false;
  isApplyAsSeller: boolean = false;
  cartCount: number = 0;
  unreadCount: number = 0;

  buyer_orderPayCount: number = 0;
  seller_orderPayCount: number = 0;

  buyer_shippingCount: number = 0;
  seller_shippingCount: number = 0;

  receivableCount: number = 0;
  acceptOrderCount: number = 0;
  followerCount: number = 0;
  followingCount: number = 0;

  constructor(
    public app: App,
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public users: UsersServiceProvider,
    public events: Events,
    public shoppingCart: ShoppingCartProvider,
    public orders: OrdersServiceProvider,
    public chatService: ChatService
  ) {
    this.getCurrentUser();

    this.shoppingCart.getCart().then((data: any) => {
      this.cartCount = !_.isEmpty(data) ? data.length : 0;
    });

    // ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
    // ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
    // ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
    events.unsubscribe("update:cart");
    events.unsubscribe("approve-seller");
    events.unsubscribe("onMessageReceived");
    events.unsubscribe("onMessageUpdated");
    events.unsubscribe("getTotalUnreadMessageCount");

    events.subscribe("update:cart", () => {
      this.cartCount = this.shoppingCart.cartCount();
    });

    events.subscribe("onMessageReceived", () => {
      this.unreadCount = this.chatService.unreadCount;
    });

    events.subscribe("onMessageUpdated", () => {
      this.unreadCount = this.chatService.unreadCount;
    });

    events.subscribe("getTotalUnreadMessageCount", () => {
      this.unreadCount = this.chatService.unreadCount;
    });

    events.subscribe("approve-seller", () => {
      console.log("approve-seller");
      this.isSeller = false;
    });
  }

  ionViewDidEnter() {
    console.log("ionViewWillEnter MyprofilePage");
    this.getCurrentUser();
    this.cartCount = this.shoppingCart.cartCount();
    this.unreadCount = this.chatService.unreadCount;
  }

  getCurrentUser(ev?: any) {
    this.user = JSON.parse(localStorage.getItem("app.user")) || {};
    if (!_.isEmpty(this.user)) {
      this.showProfile = true;

      if (this.user.isSeller) {
        this.isSeller = true;
      } else {
        this.isSeller = false;
      }

      if (!_.isEmpty(this.user.sellerApprovalCode)) {
        this.isApplyAsSeller = true;
      } else {
        this.isApplyAsSeller = false;
      }

      this.imageUrl = this.user.banner_path;
      this.users
        .getCurrentUser()
        .then(
          (data: any) => {
            if (data && data.success) {
              localStorage.setItem("app.user", JSON.stringify(data.result));

              this.chatService.connect();
              this.events.publish("reloadAppNotifications");
            }
          },
          error => {
            console.log("error: ", error);
          }
        )
        .then(() => {
          this.user = JSON.parse(localStorage.getItem("app.user")) || {};
          this.imageUrl = this.user.banner_path;

          async.parallel(
            [
              callback => {
                this.users.getCurrentUserFollowers().then(
                  (data: any) => {
                    if (data && data.success) {
                      this.followerCount = _.size(data.result);
                      this.user.followers = data.result;
                    }
                    callback();
                  },
                  error => {
                    console.log("error: ", error);
                    this.user.followers = [];
                    callback();
                  }
                );
              },
              callback => {
                this.users.getCurrentUserFollowing().then(
                  (data: any) => {
                    if (data && data.success) {
                      this.followingCount = _.size(data.result);
                      this.user.following = data.result;
                    }
                    callback();
                  },
                  error => {
                    console.log("error: ", error);
                    this.user.following = [];
                    callback();
                  }
                );
              }
            ],
            () => {
              localStorage.setItem("app.user", JSON.stringify(this.user));
              if (ev) {
                ev.complete();
              }
            }
          );
        });

      this.getMyPurchasesSales();
    } else {
      this.showProfile = false;
    }
  }

  doRefresh(ev) {
    if (!_.isEmpty(this.user)) {
      this.getCurrentUser(ev);
    } else {
      ev.complete();
    }
  }

  segmentChanged(segmentButton) {
    console.log("segmentChanged:");
    this.buyer_orderPayCount = 0;
    this.seller_orderPayCount = 0;
    this.acceptOrderCount = 0;
    this.receivableCount = 0;
    this.buyer_shippingCount = 0;
    this.seller_shippingCount = 0;
    this.acceptOrderCount = 0;

    this.getMyPurchasesSales();
  }

  getMyPurchasesSales() {
    if (this.mode == "buyer") {
      this.orders.getOrderList("pay", "buyer").then(
        (data: any) => {
          if (data && data.success) {
            this.buyer_orderPayCount = _.size(data.result);
          }
        },
        error => {
          this.buyer_orderPayCount = 0;
        }
      );
      this.orders.getOrderList("ship", "buyer").then(
        (data: any) => {
          if (data && data.success) {
            this.buyer_shippingCount = _.size(data.result);
          }
        },
        error => {
          this.buyer_shippingCount = 0;
        }
      );
      this.orders.getOrderList("receive", "buyer").then(
        (data: any) => {
          if (data && data.success) {
            this.receivableCount = _.size(data.result);
          }
        },
        error => {
          this.receivableCount = 0;
        }
      );
    } else if (this.mode == "seller") {
      if (!_.isNull(this.user.sellerApprovalDate)) {
        this.orders.getOrderList("unpaid", "seller").then(
          (data: any) => {
            if (data && data.success) {
              this.seller_orderPayCount = _.size(data.result);
            }
          },
          error => {
            this.seller_orderPayCount = 0;
          }
        );
        this.orders.getOrderList("ship", "seller").then(
          (data: any) => {
            if (data && data.success) {
              this.acceptOrderCount = _.size(data.result);
            }
          },
          error => {
            this.acceptOrderCount = 0;
          }
        );
        this.orders.getOrderList("shipping", "seller").then(
          (data: any) => {
            if (data && data.success) {
              this.seller_shippingCount = _.size(data.result);
            }
          },
          error => {
            this.seller_shippingCount = 0;
          }
        );
      }
    }
  }

  openLogin() {
    // TODO: Check for authentication
    let modal = this.modalCtrl.create(LoginPage);
    modal.onDidDismiss(res => {
      if (res == "login") {
        this.getCurrentUser();
      }
    });
    modal.present();
  }

  openSignUp() {
    // TODO: Check for authentication
    console.log("openSignUp");
    let modal = this.modalCtrl.create(SignupPage);
    modal.present();
  }

  viewProfile() {
    if (_.isEmpty(this.user)) {
      this.openLogin();
      return;
    }

    this.app.getRootNav().push(UserprofilePage);
  }

  viewCart() {
    // TODO: Check if Authenticated
    // this.navCtrl.push(ShoppingcartPage);

    if (_.isEmpty(this.user)) {
      this.openLogin();
      return;
    }

    this.app.getRootNav().push(ShoppingcartPage);
  }

  viewChat() {
    if (_.isEmpty(this.user)) {
      this.openLogin();
      return;
    }
    let modal = this.modalCtrl.create(ChatPage);
    modal.present();
  }

  viewAccountSettings() {
    if (_.isEmpty(this.user)) {
      this.openLogin();
      return;
    }

    // this.navCtrl.push(AccountsettingsPage);
    this.app.getRootNav().push(AccountsettingsPage);
  }

  viewMyRating(mode) {
    // this.navCtrl.push(MyratingPage);
    this.app.getRootNav().push(MyratingPage, {
      mode: mode
    });
  }

  viewMyPurchases() {
    if (_.isEmpty(this.user)) {
      this.openLogin();
      return;
    }
    this.app.getRootNav().push(MypurchasesPage, {
      action: "buyer"
    });
  }

  viewSalesPurchaseList(mode) {
    if (_.isEmpty(this.user)) {
      this.openLogin();
      return;
    }
    this.app.getRootNav().push(MypurchasesPage, {
      action: this.mode,
      mode: mode
    });
  }

  viewMySales() {
    if (_.isEmpty(this.user)) {
      this.openLogin();
      return;
    }
    this.app.getRootNav().push(MypurchasesPage, {
      action: "seller"
    });
  }

  viewMyWallet() {
    // this.navCtrl.push(MywalletPage);
    this.app.getRootNav().push(MywalletPage);
  }

  viewMyVouchers(){
    this.app.getRootNav().push(MyvouchersPage);
  }

  viewMyIncome() {
    this.app.getRootNav().push(MyincomePage);
  }

  viewShopProfile() {
    this.app.getRootNav().push(ShopprofilePage, {
      profile: this.user
    });
  }

  viewManageShop() {
    this.app.getRootNav().push(ManageShopPage, {
      profile: this.user
    });
  }

  viewMyShipping() {
    this.app.getRootNav().push(MyshippingPage);
  }

  viewMyFollower() {
    this.app.getRootNav().push(MyfollowerFollowingPage, {
      action: "follower"
    });
  }

  viewMyFollowing() {
    this.app.getRootNav().push(MyfollowerFollowingPage, {
      action: "following"
    });
  }

  viewAbout() {
    this.navCtrl.push(AboutPage);
  }

  openHelp() {
    this.navCtrl.push(HelpCentrePage);
  }

  addProduct() {
    let modal = this.modalCtrl.create(ProductEntryPage, {
      action: "create"
    });
    modal.onDidDismiss(resp => {
      if (resp == "save") {
        let t: Tabs = this.navCtrl.parent;
        t.select(4);
      }
    });
    modal.present();
  }

  applyAsSeller() {
    let ctrl = this;

    function applySeller() {
      let loader = ctrl.loadingCtrl.create();
      loader.present();
      ctrl.users.applyAsSeller().then(
        (data: any) => {
          if (data && data.success) {
            loader.dismiss();
            let alert = ctrl.alertCtrl.create({
              title: "Success!",
              subTitle: data.msg,
              buttons: ["OK"]
            });
            alert.present();
          } else if (data && !data.success) {
            loader.dismiss();
            let alert = ctrl.alertCtrl.create({
              title: "Warning!",
              subTitle: data.msg,
              buttons: ["OK"]
            });
            alert.present();
          }
        },
        (error: any) => {
          loader.dismiss();
          console.log("error: ", error);
        }
      );
    }

    let confirm = this.alertCtrl.create({
      title: "Apply as a Seller?",
      message:
        "Applying as a seller will you give you an opportunity to sell your preloved items and other stuffs you no longer used. Otherwise you can sell brand new items.",
      buttons: [
        {
          text: "No",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Proceed",
          handler: () => {
            applySeller();
          }
        }
      ]
    });
    confirm.present();
  }
}
