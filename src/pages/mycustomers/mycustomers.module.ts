import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MycustomersPage } from './mycustomers';

@NgModule({
  declarations: [
    MycustomersPage,
  ],
  imports: [
    IonicPageModule.forChild(MycustomersPage),
  ],
})
export class MycustomersPageModule {}
