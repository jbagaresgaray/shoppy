import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';

import { SellerServiceProvider } from '../../providers/service/seller';

@IonicPage()
@Component({
	selector: 'page-mycustomers',
	templateUrl: 'mycustomers.html',
})
export class MycustomersPage {

	customersArr: any[] = [];
	customersArrCopy: any[] = [];
	fakeArr: any[] = [];

	showContent: boolean = false;
	constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public sellers: SellerServiceProvider) {
		for (var i = 0; i < 12; ++i) {
			this.fakeArr.push(i);
		}
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad MycustomersPage');
		this.loadCustomers();
	}

	loadCustomers(ev?: any) {
		this.showContent = false;
		this.sellers.getSellerCustomers().then((data: any) => {
			if (data && data.success) {
				this.customersArr = data.result;
				this.customersArrCopy = data.result;

				this.showContent = true;
			}
			if (ev) {
				ev.complete();
			}
		}, (error) => {
			if (ev) {
				ev.complete();
			}
		});
	}

	doRefresh(ev) {
		this.loadCustomers(ev);
	}


	getItems(ev) {

	}

}
