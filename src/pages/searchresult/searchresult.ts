import { Component, ViewChild, NgZone } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  App,
  MenuController,
  LoadingController,
  Content,
  Searchbar,
  Events
} from "ionic-angular";
import * as $ from "jquery";
import * as _ from "lodash";
import "slick-carousel/slick/slick";

import { DetailsPage } from "../../pages/details/details";
import { ShopprofilePage } from "../../pages/shopprofile/shopprofile";

import { CategoriesServiceProvider } from "../../providers/service/categories";
import { BrandsServiceProvider } from "../../providers/service/brands";
import { SellerServiceProvider } from "../../providers/service/seller";
import { ServiceProvider } from "../../providers/service/service";

@IonicPage()
@Component({
  selector: "page-searchresult",
  templateUrl: "searchresult.html"
})
export class SearchresultPage {
  action: string;
  type: string;
  item: any = {};
  shop: any = {};
  sub_item: any = {};
  filter_datas: any = {};
  currency: any = {};

  userSellerArr: any[] = [];
  userSellerArrCopy: any[] = [];
  productsArr: any[] = [];
  productsArrCopy: any[] = [];
  bannersArr: any[] = [];
  categoriesArr: any[] = [];

  myInput: string;
  search: string;

  showSellerResult: boolean = false;
  showProductResult: boolean = false;
  showCategory: boolean = false;
  showBanners: boolean = false;
  isLow: boolean = true;

  mode: string = "popular";

  @ViewChild(Content) content: Content;
  @ViewChild("mySearchbar") searchbar: Searchbar;

  constructor(
    public app: App,
    public navCtrl: NavController,
    public navParams: NavParams,
    public menuCtrl: MenuController,
    public loadingCtrl: LoadingController,
    public events: Events,
    public zone: NgZone,
    public categories: CategoriesServiceProvider,
    public sellers: SellerServiceProvider,
    public brands: BrandsServiceProvider,
    public services: ServiceProvider
  ) {
    // this.userSellerArr = navParams.get('results');
    this.action = navParams.get("action");
    console.log("this.action: ", this.action);
    this.currency = this.services.defaultCurrency();
  }

  ionViewDidLoad() {
    const inputs: any = document
      .getElementById("mySearchbar")
      .getElementsByTagName("INPUT");
    inputs[0].disabled = true;
  }

  ionViewWillLeave() {
    this.menuCtrl.enable(false, "filtermenu");
  }

  ionViewWillEnter() {
    this.showBanners = false;
    if (this.action == "shops") {
      this.search = this.navParams.get("search");
      if (this.search) {
        this.myInput = this.search;
        this.filterUserSeller(this.myInput);
      }
    } else if (this.action == "category") {
      this.item = this.navParams.get("item");
      this.myInput = this.item.name;

      this.getAllCategoryProducts(this.item.categoryId);
      this.getAddBanners();
      this.getSubCategories(this.item.categoryId);
    } else if (this.action == "brands") {
      this.item = this.navParams.get("item");
      this.myInput = this.item.name;

      this.getAllBrandProducts(this.item.brandId);
    } else if (this.action == "subcategory") {
      this.item = this.navParams.get("item");
      this.sub_item = this.navParams.get("sub_item");
      this.myInput = this.sub_item.name;

      this.getAllSubCategoryProducts(
        this.item.categoryId,
        this.sub_item.subcategoryId
      );
    } else if (this.action == "shop_category") {
      this.item = this.navParams.get("item");
      this.shop = this.navParams.get("shop");
      this.myInput = this.item.name;

      // this.getAllCategoryProducts(this.item.categoryId);
      this.getAddBanners();
    }
  }

  doRefresh(ev) {
    if (this.action == "shops") {
      this.filterUserSeller(this.myInput, ev);
    } else if (this.action == "category") {
      this.item = this.navParams.get("item");
      this.myInput = this.item.name;

      this.getAllCategoryProducts(this.item.categoryId, ev);
      this.getAddBanners();
      this.getSubCategories(this.item.categoryId);
    } else if (this.action == "brands") {
      this.item = this.navParams.get("item");
      this.myInput = this.item.name;

      this.getAllBrandProducts(this.item.brandId, ev);
    } else if (this.action == "subcategory") {
      this.item = this.navParams.get("item");
      this.sub_item = this.navParams.get("sub_item");
      this.myInput = this.sub_item.name;

      this.getAllSubCategoryProducts(
        this.item.categoryId,
        this.sub_item.subcategoryId,
        ev
      );
    }
  }

  getAddBanners() {
    this.services.getHomeBanners().then(
      (data: any) => {
        if (data && data.success) {
          this.bannersArr = [];
          this.bannersArr = data.result;
        }
        this.showBanners = true;
      },
      error => {
        this.showBanners = true;
        console.log("banners error: ", error);
      }
    );
  }

  filterUserSeller(search, ev?: any) {
    this.showSellerResult = false;
    this.sellers
      .getAllSellers({
        search: search
      })
      .then(
        (data: any) => {
          if (data && data.success) {
            this.userSellerArr = data.result;
            this.userSellerArrCopy = _.clone(data.result);
          }
          this.showSellerResult = true;
          if (ev) {
            ev.complete();
          }
        },
        error => {
          this.showSellerResult = true;
          if (ev) {
            ev.complete();
          }
        }
      );
  }

  getAllBrandProducts(brandId, ev?: any) {
    this.showProductResult = false;
    this.brands.getAllBrandProducts(brandId).then(
      (data: any) => {
        if (data && data.success) {
          _.each(data.result, (row: any) => {
            row.date_created_timestamp = new Date(row.date_created).getTime();
          });

          this.productsArr = data.result;
          this.productsArrCopy = _.clone(data.result);
        }
        this.showProductResult = true;
        if (ev) {
          ev.complete();
        }
      },
      (error: any) => {
        console.log("error: ", error);
        if (error) {
          this.showProductResult = true;
        }
        if (ev) {
          ev.complete();
        }
      }
    );
  }

  getAllCategoryProducts(categoryId, ev?: any) {
    this.showProductResult = false;
    this.categories.getAllCategoryProducts(categoryId).then(
      (data: any) => {
        if (data && data.success) {
          _.each(data.result, (row: any) => {
            row.date_created_timestamp = new Date(row.date_created).getTime();
          });

          this.productsArr = data.result;
          this.productsArrCopy = _.clone(data.result);
        }
        this.showProductResult = true;
      },
      (error: any) => {
        console.log("error: ", error);
        if (error) {
          this.showProductResult = true;
        }
        if (ev) {
          ev.complete();
        }
      }
    );
  }

  getAllSubCategoryProducts(categoryId, subId, ev?: any) {
    this.showProductResult = false;
    this.categories.getAllSubCategoryProducts(categoryId, subId).then(
      (data: any) => {
        if (data && data.success) {
          _.each(data.result, (row: any) => {
            row.date_created_timestamp = new Date(row.date_created).getTime();
          });

          this.productsArr = data.result;
          this.productsArrCopy = _.clone(data.result);
        }
        this.showProductResult = true;
        if (ev) {
          ev.complete();
        }
      },
      (error: any) => {
        console.log("error: ", error);
        if (error) {
          this.showProductResult = true;
        }
        if (ev) {
          ev.complete();
        }
      }
    );
  }

  getSubCategories(categoryId, ev?: any) {
    this.categories.getAllSubCategories(categoryId).then(
      (data: any) => {
        if (data && data.success) {
          this.categoriesArr = data.result;
        }
        this.showCategory = true;
        if (ev) {
          ev.complete();
        }
      },
      (error: any) => {
        console.log("error: ", error);
        if (error) {
          this.showCategory = true;
        }
        if (ev) {
          ev.complete();
        }
      }
    );
  }

  checkFocus() {
    this.content.resize();
  }

  submitSearch(ev: any) {
    let val = this.myInput;
    if (val && val.trim() != "") {
      this.filterUserSeller(val);
    }
  }

  viewShop(item) {
    this.navCtrl.push(ShopprofilePage, {
      profile: item
    });
  }

  gotoDetails(item) {
    this.navCtrl.push(DetailsPage, {
      product: item
    });
  }

  viewSubCategory(item) {
    this.navCtrl.push(SearchresultPage, {
      action: "subcategory",
      item: this.item,
      sub_item: item
    });
  }

  viewFilters() {
    console.log("viewFilters");
    this.menuCtrl.enable(true, "filtermenu");
    this.events.publish("filter_datas", this.filter_datas);
    this.menuCtrl.toggle("right");
  }

  segmentChanged(ev: any) {
    console.log("this.mode: ", this.mode);
    let loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    loading.present();

    if (this.mode == "popular") {
      this.productsArr = _.orderBy(this.productsArr, "rating", "desc");
    } else if (this.mode == "latest") {
      this.productsArr = _.orderBy(
        this.productsArr,
        "date_created_timestamp",
        "asc"
      );
    } else if (this.mode == "top") {
      this.productsArr = _.orderBy(this.productsArr, "orderscount", "desc");
    } else if (this.mode == "price") {
      if (this.isLow) {
        this.productsArr = _.orderBy(this.productsArr, "productPrice", "asc");
      } else {
        this.productsArr = _.orderBy(this.productsArr, "productPrice", "desc");
      }
    }
    loading.dismiss();
  }

  sortPrice() {
    this.isLow = !this.isLow;
    if (this.isLow) {
      this.zone.run(() => {
        this.productsArr = _.orderBy(this.productsArr, "productPrice", "asc");
      });
    } else {
      this.zone.run(() => {
        this.productsArr = _.orderBy(this.productsArr, "productPrice", "desc");
      });
    }
    console.log("sortPrice: ", this.productsArr);
  }

  resetFilter() {
    let loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    loading.present();
    this.productsArr = this.productsArrCopy;
    setTimeout(() => {
      loading.dismiss();
    }, 1000);
  }

  globalFilter(filters: any) {
    console.log("search_filter: ", filters);
    let tempArr: any[] = [];
    let loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    loading.present();

    if (filters.categories) {
      _.each(filters.categories, (row: any) => {
        let categories = _.filter(this.productsArrCopy, (item: any) => {
          return item.category.categoryId == row.categoryId;
        });
        _.each(categories, (item: any) => {
          tempArr.push(item);
        });
      });
    } else if (filters.shipping_options) {
      _.each(filters.shipping_options, (row: any) => {
        let shippingFee = _.filter(this.productsArrCopy, (item: any) => {
          return _.find(item.shippingFee, {
            shippingCourierId: row.shippingCourierId
          });
        });
        console.log("shippingFee1: ", shippingFee);
        _.each(shippingFee, (item: any) => {
          tempArr.push(item);
        });
      });
    }

    if (filters.productWholeSale) {
      let productWholeSale = _.filter(this.productsArrCopy, (item: any) => {
        return _.size(item.wholesale) > 0;
      });
      _.each(productWholeSale, (item: any) => {
        tempArr.push(item);
      });
    } else if (filters.productDiscounted) {
    } else if (filters.productLowPrice) {
    } else if (filters.productCOD) {
    } else if (filters.productFreeShipping) {
      let productFreeShipping = _.filter(this.productsArrCopy, (item: any) => {
        return _.find(item.shippingFee, { isCover: 1 });
      });
      console.log("shippingFee1: ", productFreeShipping);
      _.each(productFreeShipping, (item: any) => {
        tempArr.push(item);
      });
    }

    if (filters.productNewCondition) {
      let productNewCondition = _.filter(this.productsArrCopy, {
        productCondition: "new"
      });
      _.each(productNewCondition, (item: any) => {
        tempArr.push(item);
      });
    } else if (filters.productUsedCondition) {
      let productUsedCondition = _.filter(this.productsArrCopy, {
        productCondition: "used"
      });
      _.each(productUsedCondition, (item: any) => {
        tempArr.push(item);
      });
    }

    if (filters.minValue && _.isEmpty(filters.maxValue)) {
      let minValue = _.filter(this.productsArrCopy, (item: any) => {
        return item.productPrice >= filters.minValue;
      });
      _.each(minValue, (item: any) => {
        tempArr.push(item);
      });
    } else if (filters.maxValue && _.isEmpty(filters.minValue)) {
      let minValue = _.filter(this.productsArrCopy, (item: any) => {
        return item.productPrice <= filters.maxValue;
      });
      _.each(minValue, (item: any) => {
        tempArr.push(item);
      });
    } else if (filters.minValue && filters.maxValue) {
      let values = _.filter(this.productsArrCopy, (item: any) => {
        return (
          item.productPrice >= filters.minValue &&
          item.productPrice <= filters.maxValue
        );
      });
      _.each(values, (item: any) => {
        tempArr.push(item);
      });
    }

    console.log("tempArr: ", tempArr);

    this.productsArr = _.uniqBy(tempArr, "productId");
    loading.dismiss();
  }
}
