import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { Ionic2RatingModule } from "ionic2-rating";

import { SearchresultPage } from "./searchresult";

import { PipesModule } from "../../pipes/pipes.module";
import { ComponentsModule } from "../../components/components.module";

@NgModule({
	declarations: [SearchresultPage],
	imports: [
		Ionic2RatingModule,
		PipesModule,
		ComponentsModule,
		IonicPageModule.forChild(SearchresultPage)
	]
})
export class SearchresultPageModule {}
