import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import * as _ from 'lodash';

import { OrderdetailsPage } from '../../pages/orderdetails/orderdetails';

import { ShoppingCartProvider } from '../../providers/service/shopping-cart';
import { OrdersServiceProvider } from '../../providers/service/orders';

@IonicPage()
@Component({
	selector: 'page-makepayment',
	templateUrl: 'makepayment.html',
})
export class MakepaymentPage {

	cartArr: any[] = [];

	order: any = {};

	total: number = 0;
	shippingTotal: number = 0;

	constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public alertCtrl: AlertController,
		public shoppingCart: ShoppingCartProvider, public orderService: OrdersServiceProvider) {
		this.order = navParams.get('order');
	}

	ionViewWillEnter() {
		this.initData();
	}

	initData() {

		this.order = this.navParams.get('order');
		console.log('this.order: ', this.order);
		this.cartArr = this.order.cartOrder;
		this.shippingTotal = this.order.orderShippingFee;
		this.total = this.order.orderAmt;
	}

	groupCart(carts) {
		let sortedContacts = _.orderBy(carts, ['storeId'], ['asc']);
		let currentLetter = false;
		let currentContacts = [];
		this.cartArr = [];
		sortedContacts.forEach((value: any, index) => {
			if (value.storeId != currentLetter) {
				currentLetter = value.storeId;
				let newGroup = {
					store: value.store,
					storeId: value.storeId,
					details: []
				};
				currentContacts = newGroup.details;
				this.cartArr.push(newGroup);
			}
			currentContacts.push(value);
		});
		_.each(this.cartArr, (row: any) => {
			row.subTotal = _.sumBy(row.details, (o: any) => { return o.quantity * o.details.productPrice; });
		});

		this.total = _.sumBy(this.cartArr, 'subTotal');
	}

	next() {
		if (_.isEmpty(this.order.orderEmail)) {
			this.alertCtrl.create({
				title: 'WARNING',
				message: 'Please fill in and confirm your email address.',
				buttons: ['OK']
			}).present();
			return;
		}

		this.order.orderDateTime = new Date();
		let loading = this.loadingCtrl.create({
			dismissOnPageChange: true,
			content: 'Saving...'
		});
		loading.present();
		this.orderService.createOrder(this.order).then((data: any) => {
			console.log('data: ', data);
			if (data && data.success) {
				this.shoppingCart.emptyCart();
				this.navCtrl.push(OrderdetailsPage, {
					batchId: data.result,
					status: 'topay',
					usertype: 'buyer'
				});
			}
			loading.dismiss();
		}, (error) => {
			loading.dismiss();
		});
	}
}
