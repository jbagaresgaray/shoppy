import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShippingoptionPage } from './shippingoption';

@NgModule({
  declarations: [
    ShippingoptionPage,
  ],
  imports: [
    IonicPageModule.forChild(ShippingoptionPage),
  ],
})
export class ShippingoptionPageModule {}
