import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import * as _ from 'lodash';
import * as async from 'async';

import { ShoppingcartSelectshippingoptionPage } from '../../pages/shoppingcart-selectshippingoption/shoppingcart-selectshippingoption';
import { PaymentoptionPage } from '../../pages/shoppingcart-paymentoption/paymentoption';

import { ShoppingCartProvider } from '../../providers/service/shopping-cart';
import { ServiceProvider } from "../../providers/service/service";
import { UserShippingServiceProvider } from "../../providers/service/user-shipping";
import { ProductsServiceProvider } from "../../providers/service/products";
import { SellerServiceProvider } from "../../providers/service/seller";
@IonicPage()
@Component({
	selector: 'page-shippingoption',
	templateUrl: 'shippingoption.html',
})
export class ShippingoptionPage {
	user: any = {};
	order: any = {};
	currency: any = {};

	cartArr: any[] = [];
	shippingArr: any[] = [];
	tmpShippingArr:any [] = [];
	total: number = 0;
	shippingTotal: number = 0;
	cartCount: number = 0;

	disableNext: boolean = false;

	constructor(public navCtrl: NavController, public navParams: NavParams, public shoppingCart: ShoppingCartProvider, public loadingCtrl: LoadingController,
		public sellershipping: UserShippingServiceProvider, public services: ServiceProvider, public products: ProductsServiceProvider, public sellers: SellerServiceProvider) {

		this.order = navParams.get('order');
		this.order.products = [];
		console.log('this.order: ', this.order);

		this.currency = this.services.defaultCurrency();
	}

	ionViewDidLoad() {
		this.initData();
	}

	ionViewWillEnter() {
		// this.initData();
	}


	initData() {
		let vm = this;
		this.shoppingCart.getCart().then((data: any) => {
			this.groupCart(data);
		}).then(() => {
			let loading = vm.loadingCtrl.create();
			loading.present();
			async.eachSeries(vm.cartArr, (cart, callback) => {
				async.parallel([
					(callback2) => {
						async.eachSeries(cart.details, (item, callback1) => {
							vm.products.getProductShipping(item.uuid).then((data: any) => {
								if (data && data.success) {
									item.shipping = data.result;
									if (_.isEmpty(item.shipping)) {
										this.disableNext = true;
									}
								}
								vm.order.products.push(item);
								callback1();
							}, (error) => {
								callback1();
							});
						}, () => {
							callback2();
						});
					},
					(callback2) => {
						this.sellers.getAllUserSellerShipping(cart.storeId).then((data: any) => {
							if (data && data.success) {
								this.shippingArr = data.result;
							}
							callback2();
						}, (error) => {
							callback2();
						});
					}
				], () => {
					callback();
				})
			}, () => {
				loading.dismiss();
				_.each(this.cartArr, (row: any) => {
					_.each(row.details, (row2: any) => {
						_.each(row2.shipping, (o: any) => { 
							this.tmpShippingArr.push(o);
						});
					});

					this.tmpShippingArr = _.uniqBy(this.tmpShippingArr,'shippingCourierId');
					let selected_shipping: any = _.maxBy(this.tmpShippingArr, (o: any) => { return o.fee; });
					if (_.find(this.shippingArr, { 'shippingId': selected_shipping.shippingCourierId })) {
						row.shippingFee = selected_shipping.fee;
						row.selected_shipping = selected_shipping;
					}else{
						row.shippingFee = 0;
						row.selected_shipping = null;
					}
					row.subTotal = _.sumBy(row.details, (o: any) => { return o.quantity * o.details.productPrice; });
				});
				this.shippingTotal = _.sumBy(this.cartArr, 'shippingFee');
				this.total = _.sumBy(this.cartArr, 'subTotal') + this.shippingTotal;
			});
		});
	}

	groupCart(carts) {
		let sortedContacts = _.orderBy(carts, ['storeId'], ['asc']);
		let currentLetter = false;
		let currentContacts = [];
		this.cartArr = [];
		sortedContacts.forEach((value: any, index) => {
			if (value.storeId != currentLetter) {
				currentLetter = value.storeId;
				let newGroup = {
					store: value.store,
					storeId: value.storeId,
					details: []
				};
				currentContacts = newGroup.details;
				this.cartArr.push(newGroup);
			}
			currentContacts.push(value);
		});
		_.each(this.cartArr, (row: any) => {
			row.shippingFee = 0;
			row.subTotal = _.sumBy(row.details, (o: any) => { return o.quantity * o.details.productPrice; });
		});

		this.total = _.sumBy(this.cartArr, 'subTotal');
		this.shippingTotal = _.sumBy(this.cartArr, 'shippingFee');
		this.cartCount = this.shoppingCart.cartCount();
	}

	changeShipping(order) {
		this.navCtrl.push(ShoppingcartSelectshippingoptionPage, {
			order: order,
			tmpShippingArr: this.tmpShippingArr,
			callback: this.updateProductShipping,
		});
	}

	updateProductShipping = (data) => {
		let ctrl = this;
		return new Promise((resolve, reject) => {
			_.each(ctrl.cartArr, (row: any) => {
				if (row.storeId == data.storeId) {
					row.selected_shipping = data;
				}
				row.shippingFee = row.selected_shipping.fee;
				row.subTotal = _.sumBy(row.details, (o: any) => { return o.quantity * o.details.productPrice; });
			});
			this.total = _.sumBy(this.cartArr, 'subTotal');
			this.shippingTotal = _.sumBy(this.cartArr, 'shippingFee');
			resolve();
		});
	}

	proceedPaymentOption() {
		this.order.cartOrder = this.cartArr;
		this.order.orderAmt = this.total;
		this.order.orderShippingFee = this.shippingTotal;
		
		localStorage.setItem('app.orders', JSON.stringify(this.order));
		this.navCtrl.push(PaymentoptionPage, {
			order: this.order
		});
	}
}
