import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";

import { PipesModule } from "../../pipes/pipes.module";
import { ComponentsModule } from "../../components/components.module";

import { MyvouchersPage } from "./myvouchers";

@NgModule({
	declarations: [MyvouchersPage],
	imports: [
		PipesModule,
		ComponentsModule,
		IonicPageModule.forChild(MyvouchersPage)
	]
})
export class MyvouchersPageModule {}
