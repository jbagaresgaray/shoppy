import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides } from 'ionic-angular';

import { Storage } from '@ionic/storage';

import { TabsPage } from '../../pages/tabs/tabs';

@IonicPage()
@Component({
	selector: 'page-onboarding',
	templateUrl: 'onboarding.html',
})
export class OnboardingPage {
	@ViewChild(Slides) slides: Slides;
	skipMsg: string = "Skip";
	state: string = 'x';

	constructor(public navCtrl: NavController, public navParams: NavParams,public storage: Storage) {
	}

	skip() {
		this.storage.set('introShown', true);
		this.navCtrl.push(TabsPage);
	}

	slideChanged() {
		if (this.slides.isEnd()){
			this.skipMsg = "Start Shopping";
			this.storage.set('introShown', true);
		}
	}

	animationDone() {
		this.state = 'x';
	}

}
