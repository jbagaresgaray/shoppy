import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShopprofileSettingsPage } from './shopprofile-settings';

@NgModule({
  declarations: [
    ShopprofileSettingsPage,
  ],
  imports: [
    IonicPageModule.forChild(ShopprofileSettingsPage),
  ],
})
export class ShopprofileSettingsPageModule {}
