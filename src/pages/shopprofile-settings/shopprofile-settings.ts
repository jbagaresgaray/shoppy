import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import * as _ from 'lodash';

import { SellerServiceProvider } from '../../providers/service/seller';
import { UsersServiceProvider } from '../../providers/service/users';

@IonicPage()
@Component({
	selector: 'page-shopprofile-settings',
	templateUrl: 'shopprofile-settings.html',
})
export class ShopprofileSettingsPage {
	user: any = {};
	action: string;
	title: string;

	isEmailNotif: boolean = true;
	isPushNotif: boolean = true;
	constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public alertCtrl: AlertController, public sellers: SellerServiceProvider, public users: UsersServiceProvider) {
		this.user = JSON.parse(localStorage.getItem('app.user')) || {};
		this.action = navParams.get('action');
		console.log('action: ',this.action);
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad ShopprofileSettingsPage');
		console.log('action: ',this.action);
		if (this.action == 'shop') {
			this.title = 'Shop Settings';

			this.getCurrentUser();
		}else if(this.action =='notification'){
			this.title = 'Notification Settings';
		}
	}


	getCurrentUser() {
		let loading = this.loadingCtrl.create();
		loading.present();
		this.user = JSON.parse(localStorage.getItem('app.user')) || {};
		if (!_.isEmpty(this.user)) {
			this.users.getCurrentUser().then((data: any) => {
				if (data && data.success) {
					this.user = data.result;
					loading.dismiss();
				}
			}, (error) => {
				loading.dismiss();
			});
		}
	}

	toggleEmailNotif(){
		this.isEmailNotif = !this.isEmailNotif;
	}

	togglePushNotif(){
		this.isPushNotif = !this.isPushNotif;
	}

	private saveSellerSettings() {
		let loading = this.loadingCtrl.create();
		loading.present();
		this.sellers.updateStoreInformation(this.user).then((data: any) => {
			if (data && data.success) {
				let alert = this.alertCtrl.create({
					title: 'Success!',
					subTitle: data.msg,
					buttons: ['OK']
				});
				alert.onDidDismiss(() => {
					this.navCtrl.pop();
				});
				alert.present();
			}
			loading.dismiss();
		}, (error) => {
			loading.dismiss();
		});
	}

	updateItem() {
		console.log('updateItem: ', this.user);
		setTimeout(() => {
			this.saveSellerSettings();
		}, 600);
	}




}
