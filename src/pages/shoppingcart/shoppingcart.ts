import { Component } from "@angular/core";
import {
	IonicPage,
	NavController,
	NavParams,
	ViewController,
	ModalController,
	Events
} from "ionic-angular";
import * as _ from "lodash";

import { ChatPage } from "../../pages/chat/chat";
import { LoginPage } from "../../pages/login/login";
import { DeliveryaddressPage } from "../../pages/shoppingcart-deliveryaddress/deliveryaddress";
import { ShoppingcartCheckoutPage } from "../../pages/shoppingcart-checkout/shoppingcart-checkout";

import { ShoppingCartProvider } from "../../providers/service/shopping-cart";
import { ChatService } from "../../providers/service/chat";
import { ServiceProvider } from "../../providers/service/service";

@IonicPage()
@Component({
	selector: "page-shoppingcart",
	templateUrl: "shoppingcart.html"
})
export class ShoppingcartPage {
	user: any = {};
	currency: any = {};

	cartArr: any[] = [];
	total: number = 0;
	cartCount: number = 0;
	unreadCount: number = 0;
	showCart: boolean = false;

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public viewCtrl: ViewController,
		public modalCtrl: ModalController,
		public events: Events,
		public shoppingCart: ShoppingCartProvider,
		public chatService: ChatService,
		public services: ServiceProvider
	) {
		this.cartCount = this.shoppingCart.cartCount();
		this.user = JSON.parse(localStorage.getItem("app.user")) || {};
		this.currency = this.services.defaultCurrency();

		// ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
		// ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
		// ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
		events.unsubscribe("onMessageReceived");
		events.unsubscribe("onMessageUpdated");
		events.unsubscribe("getTotalUnreadMessageCount");

		events.subscribe("onMessageReceived", () => {
			this.unreadCount = this.chatService.unreadCount;
		});

		events.subscribe("onMessageUpdated", () => {
			this.unreadCount = this.chatService.unreadCount;
		});

		events.subscribe("getTotalUnreadMessageCount", () => {
			this.unreadCount = this.chatService.unreadCount;
		});
	}

	ionViewDidLoad() {
		console.log("ionViewDidLoad ShoppingcartPage");
		this.initData();
		this.unreadCount = this.chatService.unreadCount;
	}

	ionViewWillLeave() {
		console.log("ionViewWillLeave ShoppingcartPage");
		localStorage.removeItem("app.orders");
	}

	initData() {
		this.shoppingCart.getCart().then((data: any) => {
			this.groupCart(data);
		});
	}

	groupCart(carts) {
		let sortedContacts = _.orderBy(carts, ["storeId"], ["asc"]);
		let currentLetter = false;
		let currentContacts = [];
		this.cartArr = [];
		console.log("sortedContacts: ", sortedContacts);
		sortedContacts.forEach((value: any, index) => {
			if (value.storeId != currentLetter) {
				currentLetter = value.storeId;
				let newGroup = {
					store: value.store,
					storeId: value.storeId,
					details: []
				};
				currentContacts = newGroup.details;
				this.cartArr.push(newGroup);
			}
			currentContacts.push(value);
		});
		console.log("this.cartArr: ", this.cartArr);
		_.each(this.cartArr, (row: any) => {
			row.subTotal = _.sumBy(row.details, (o: any) => {
				return o.quantity * o.details.productPrice;
			});
		});

		this.total = _.sumBy(this.cartArr, "subTotal");
		this.cartCount = this.shoppingCart.cartCount();

		this.events.publish("update:cart");

		if (!_.isEmpty(this.cartArr)) {
			this.showCart = true;
		} else {
			this.showCart = false;
		}
	}

	addToCart(item) {
		if (item) {
			this.shoppingCart.addItem(item, 1);
			setTimeout(() => {
				this.initData();
				this.events.publish("update:cart");
			}, 300);
		}
	}

	removeToCart(item) {
		if (item) {
			this.shoppingCart.addItem(item, -1);
			setTimeout(() => {
				this.initData();
				this.events.publish("update:cart");
			}, 300);
		}
	}

	deleteProduct(item) {
		console.log("deleteProduct: ", item);
		if (item) {
			this.shoppingCart.removeItem(item);
			setTimeout(() => {
				this.initData();
				this.events.publish("update:cart");
			}, 300);
		}
	}

	checkout() {
		console.log("this.user: ", this.user);
		if (_.isEmpty(this.user)) {
			this.openLogin();
			return;
		}
		// this.navCtrl.push(DeliveryaddressPage);
		this.navCtrl.push(ShoppingcartCheckoutPage);
	}

	viewChat() {
		let modal = this.modalCtrl.create(ChatPage);
		modal.present();
	}

	back() {
		this.viewCtrl.dismiss();
	}

	openLogin() {
		// TODO: Check for authentication
		let modal = this.modalCtrl.create(LoginPage);
		modal.onDidDismiss(res => {
			if (res == "login") {
			}
		});
		modal.present();
	}
}
