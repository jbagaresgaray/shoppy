import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ActionSheetController,
  LoadingController,
  ModalController,
  Events,
  App
} from "ionic-angular";
import * as _ from "lodash";
import * as async from "async";

import { DetailsPage } from "../../pages/details/details";
import { ChatPage } from "../../pages/chat/chat";
import { ShoppingcartPage } from "../../pages/shoppingcart/shoppingcart";
import { LoginPage } from "../../pages/login/login";
import { SignupPage } from '../../pages/signup/signup';

import { UsersServiceProvider } from "../../providers/service/users";
import { ProductsServiceProvider } from "../../providers/service/products";
import { ChatService } from "../../providers/service/chat";
import { ServiceProvider } from "../../providers/service/service";
import { ShoppingCartProvider } from "../../providers/service/shopping-cart";

@IonicPage()
@Component({
  selector: "page-mylikes",
  templateUrl: "mylikes.html"
})
export class MylikesPage {
  userLikesArr: any[] = [];
  fakeArr: any[] = [];
  unreadCount: number = 0;
  cartCount: number;

  mapTabEnabled: boolean = false;
  showContent: boolean = false;

  user: any = {};
  currency: any = {};

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public actionSheetCtrl: ActionSheetController,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public events: Events,
    public app: App,
    public chatService: ChatService,
    private services: ServiceProvider,
    public users: UsersServiceProvider,
    public products: ProductsServiceProvider,
    private shoppingCart: ShoppingCartProvider
  ) {
    this.currency = this.services.defaultCurrency();

    for (var i = 0; i < 20; ++i) {
      this.fakeArr.push(i);
    }

    // ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
    // ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
    // ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
    events.unsubscribe("update:cart");
    events.unsubscribe("onMessageReceived");
    events.unsubscribe("onMessageUpdated");
    events.unsubscribe("getTotalUnreadMessageCount");

    events.subscribe("update:cart", () => {
      this.cartCount = this.shoppingCart.cartCount();
    });

    events.subscribe("onMessageReceived", () => {
      this.unreadCount = this.chatService.unreadCount;
    });

    events.subscribe("onMessageUpdated", () => {
      this.unreadCount = this.chatService.unreadCount;
    });

    events.subscribe("getTotalUnreadMessageCount", () => {
      this.unreadCount = this.chatService.unreadCount;
    });
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad MylikesPage");
    this.unreadCount = this.chatService.unreadCount;
    this.user = JSON.parse(localStorage.getItem("app.user")) || {};
    if (!_.isEmpty(this.user)) {
      this.mapTabEnabled = true;
    } else {
      this.mapTabEnabled = false;
    }
  }

  ionViewWillEnter() {
    this.user = JSON.parse(localStorage.getItem("app.user")) || {};
    this.cartCount = this.shoppingCart.cartCount();
    this.unreadCount = this.chatService.unreadCount;
  }

  ionViewDidEnter() {
    this.user = JSON.parse(localStorage.getItem("app.user")) || {};
    if (!_.isEmpty(this.user)) {
      this.mapTabEnabled = true;
      this.initializeData();
    } else {
      this.mapTabEnabled = false;
      this.showContent = true;
    }
  }

  initializeData(ev?: any) {
    this.showContent = false;
    this.users.getCurrentUserLike().then(
      (data: any) => {
        if (data && data.success) {
          this.userLikesArr = data.result;

          _.each(this.userLikesArr, (row: any) => {
            row.isLiked = true;
          });
        }
        console.log("this.userLikesArr: ", this.userLikesArr);
        if (ev) {
          ev.complete();
        }
        this.showContent = true;
      },
      error => {
        console.log("getCurrentUserLike: ", error);
        this.showContent = true;
      }
    );
  }

  gotoDetails(item) {
    this.navCtrl.push(DetailsPage, {
      product: item
    });
  }

  viewChat() {
    let modal = this.modalCtrl.create(ChatPage);
    modal.present();
  }

  openLogin() {
    // TODO: Check for authentication
    let modal = this.modalCtrl.create(LoginPage);
    modal.onDidDismiss(res => {
      if (res == "login") {
        this.getCurrentUser();
      }
    });
    modal.present();
  }

  openSignUp() {
    // TODO: Check for authentication
    console.log("openSignUp");
    let modal = this.modalCtrl.create(SignupPage);
    modal.present();
  }

  viewCart() {
    // TODO: Check if Authenticated
    if (_.isEmpty(this.user)) {
      let modal = this.modalCtrl.create(LoginPage);
      modal.onDidDismiss(res => {
        if (res == "login") {
          this.getCurrentUser();
        }
      });
      modal.present();
      return;
    }

    this.app.getRootNav().push(ShoppingcartPage);
  }

  doRefresh(ev) {
    if (!_.isEmpty(this.user)) {
      this.initializeData();
    } else {
      ev.complete();
    }
  }

  getCurrentUser() {
    this.user = JSON.parse(localStorage.getItem("app.user")) || {};
    if (!_.isEmpty(this.user)) {
      this.mapTabEnabled = true;

      this.users
        .getCurrentUser()
        .then(
          (data: any) => {
            if (data && data.success) {
              localStorage.setItem("app.user", JSON.stringify(data.result));
              this.chatService.connect();

              this.events.publish("reloadAppNotifications");
            }
          },
          error => {
            console.log("error: ", error);
          }
        )
        .then(() => {
          if (!_.isEmpty(this.user)) {
            this.user = JSON.parse(localStorage.getItem("app.user")) || {};
            async.parallel(
              [
                callback => {
                  this.users.getCurrentUserFollowers().then(
                    (data: any) => {
                      if (data && data.success) {
                        console.log("getCurrentUserFollowers: ", data);
                        this.user.followers = data.result;
                      }
                      callback();
                    },
                    error => {
                      console.log("error: ", error);
                      this.user.followers = [];
                      callback();
                    }
                  );
                },
                callback => {
                  this.users.getCurrentUserFollowing().then(
                    (data: any) => {
                      if (data && data.success) {
                        console.log("getCurrentUserFollowing: ", data);
                        this.user.following = data.result;
                      }
                      callback();
                    },
                    error => {
                      console.log("error: ", error);
                      this.user.following = [];
                      callback();
                    }
                  );
                }
              ],
              () => {
                localStorage.setItem("app.user", JSON.stringify(this.user));
              }
            );
          }
        });
    }
  }

  presentActionSheet(item) {
    let actionSheet = this.actionSheetCtrl.create({
      title: "Unlike product?",
      buttons: [
        {
          text: "Unlike",
          role: "destructive",
          handler: () => {
            console.log("Unlike clicked");
            this.products.unlikeProduct(item.uuid).then(
              (data: any) => {
                if (data && data.success) {
                  this.initializeData();
                }
              },
              error => {
                console.log("unlikeProduct: ", error);
              }
            );
          }
        },
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        }
      ]
    });
    actionSheet.present();
  }
}
