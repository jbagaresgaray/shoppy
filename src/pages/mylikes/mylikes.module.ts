import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MylikesPage } from './mylikes';

import { ComponentsModule } from '../../components/components.module';

import { Ionic2RatingModule } from 'ionic2-rating';

@NgModule({
	declarations: [
		MylikesPage,
	],
	imports: [
		Ionic2RatingModule,
		ComponentsModule,
		IonicPageModule.forChild(MylikesPage),
	],
})
export class MylikesPageModule { }
