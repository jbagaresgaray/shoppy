import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import * as _ from 'lodash';
import * as async from 'async';

import { MakepaymentPage } from '../../pages/shoppingcart-makepayment/makepayment';
import { DeliveryaddressPage } from '../../pages/shoppingcart-deliveryaddress/deliveryaddress';
import { PaymentoptionPage } from '../../pages/shoppingcart-paymentoption/paymentoption';
import { ShoppingcartSelectshippingoptionPage } from '../../pages/shoppingcart-selectshippingoption/shoppingcart-selectshippingoption';

import { ShoppingCartProvider } from '../../providers/service/shopping-cart';
import { UserAddressessServiceProvider } from '../../providers/service/user-address';
import { ServiceProvider } from '../../providers/service/service';
import { SellerServiceProvider } from '../../providers/service/seller';

@IonicPage()
@Component({
	selector: 'page-confirmation',
	templateUrl: 'confirmation.html',
})
export class ConfirmationPage {
	order: any = {};
	payMethod: any = {};
	orderAddress: any = {};
	currency: any = {};

	cartArr: any[] = [];
	paymentOptions: any[] = [];
	userAddressArr: any[] = [];
	tmpShippingArr: any[] = [];

	total: number = 0;
	shippingTotal: number = 0;
	constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController,
		public shoppingCart: ShoppingCartProvider, public addressess: UserAddressessServiceProvider, public services: ServiceProvider, public sellers: SellerServiceProvider) {

		this.order = navParams.get('order');
		// Will going to add more
		this.paymentOptions = [{
			id: 'wallet',
			name: 'Wallet'
		}, {
			id: 'cod',
			name: 'Cash On Delivery'
		}];

		this.payMethod = _.find(this.paymentOptions, { id: this.order.modePayment });
		this.currency = this.services.defaultCurrency();
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad ConfirmationPage');
		this.initData();
	}

	initData() {
		this.order = this.navParams.get('order');
		console.log('this.order: ', this.order);

		this.cartArr = this.order.cartOrder;
		this.orderAddress = {
			addressId: this.order.addressId,
			orderShipAddress: this.order.orderShipAddress,
			orderShipCity: this.order.orderShipCity,
			orderShipState: this.order.orderShipState,
			orderShipZipCode: this.order.orderShipZipCode,
			orderShipCountry: this.order.orderShipCountry,
			orderShipSuburb: this.order.orderShipSuburb,
			orderPhone: this.order.orderPhone,
			orderShipName: this.order.orderShipName
		};
		_.each(this.cartArr, (row: any) => {
			row.subTotal = _.sumBy(row.details, (o: any) => { return o.quantity * o.details.productPrice; });
		});
		console.log('this.cartArr: ', this.cartArr);
		this.shippingTotal = this.order.orderShippingFee;
		this.total = this.order.orderAmt;
	}

	placeOrder() {
		this.order.orderAmt = this.total;
		this.order.orderShippingFee = this.shippingTotal;
		localStorage.setItem('app.orders', JSON.stringify(this.order));
		this.navCtrl.push(MakepaymentPage, {
			order: this.order
		})
	}
}
