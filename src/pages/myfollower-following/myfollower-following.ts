import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import * as _ from 'lodash';
import * as async from 'async';

import { ShopprofilePage } from '../../pages/shopprofile/shopprofile';

import { UsersServiceProvider } from '../../providers/service/users';
import { SellerServiceProvider } from '../../providers/service/seller';

@IonicPage()
@Component({
	selector: 'page-myfollower-following',
	templateUrl: 'myfollower-following.html',
})
export class MyfollowerFollowingPage {
	user: any = {};
	params: any = {};
	action: string;
	mode: string;
	title: string;

	usersArr: any[] = [];

	constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public alertCtrl: AlertController,
		public users: UsersServiceProvider, public sellers: SellerServiceProvider) {

		this.action = navParams.get('action');
		this.mode = navParams.get('mode');
		this.params = navParams.get('params');
		this.user = JSON.parse(localStorage.getItem('app.user')) || {};

		console.log('this.action: ', this.action);
		console.log('this.user: ', this.user);

		if (this.action == 'follower') {
			this.title = 'Followers';
		} else if (this.action == 'following') {
			this.title = 'Following';
		}
	}

	initData(ev?: any) {
		this.user = JSON.parse(localStorage.getItem('app.user')) || {};
		async.parallel([
			(callback) => {
				this.users.getCurrentUserFollowers().then((data: any) => {
					if (data && data.success) {
						this.user.followers = data.result;
					}
					callback();
				}, (error) => {
					console.log('error: ', error);
					this.user.followers = [];
					callback();
				});
			},
			(callback) => {
				this.users.getCurrentUserFollowing().then((data: any) => {
					if (data && data.success) {
						this.user.following = data.result;
					}
					callback();
				}, (error) => {
					console.log('error: ', error);
					this.user.following = [];
					callback();
				});
			}
		], () => {
			localStorage.setItem('app.user', JSON.stringify(this.user));

			if (this.action == 'follower') {
				this.title = 'Followers';
				if (this.mode == 'shop') {
					this.getSellerFollowers(this.params.uuid, ev);
				} else {
					this.getFollowers(ev);
				}
			} else if (this.action == 'following') {
				this.title = 'Following';
				if (this.mode == 'shop') {
					this.getSellerFollowing(this.params.uuid, ev);
				} else {
					this.getFollowing(ev);
				}
			}
		});
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad MyfollowerFollowingPage');
		this.initData();
	}

	ionViewDidEnter(){
	}

	doRefresh(ev) {
		this.initData(ev);
	}


	getSellerFollowers(userId, ev?: any) {
		let loading = this.loadingCtrl.create({
			dismissOnPageChange: true
		});
		loading.present();
		this.sellers.getSellerFollowers(userId).then((data: any) => {
			this.usersArr = [];
			if (data && data.success) {
				this.usersArr = data.result;
				console.log('getSellerFollowers: ', this.usersArr);
				_.each(this.usersArr, (row: any) => {
					if (row.uuid == this.user.uuid) {
						row.isFollowing = false;
					} else {
						row.isFollowing = true;
					}
				});
			}
			loading.dismiss();
			if (ev) {
				ev.complete();
			}
		}, (error) => {
			console.log('error: ', error);
			loading.dismiss();
			if (ev) {
				ev.complete();
			}
		});
	}

	getSellerFollowing(userId, ev?: any) {
		let loading = this.loadingCtrl.create({
			dismissOnPageChange: true
		});
		loading.present();
		this.sellers.getSellerFollowing(userId).then((data: any) => {
			this.usersArr = [];
			if (data && data.success) {
				this.usersArr = data.result;
				console.log('getSellerFollowing: ', this.usersArr);
				_.each(this.usersArr, (row: any) => {
					if (_.find(this.user.following, { 'uuid': row.uuid })) {
						row.isFollowing = true;
					} else {
						row.isFollowing = false;
					}
				});
			}
			loading.dismiss();
			if (ev) {
				ev.complete();
			}
		}, (error) => {
			console.log('error: ', error);
			loading.dismiss();
			if (ev) {
				ev.complete();
			}
		});
	}

	getFollowers(ev?: any) {
		this.user = JSON.parse(localStorage.getItem('app.user')) || {};
		let loading = this.loadingCtrl.create({
			dismissOnPageChange: true
		});
		loading.present();
		if (!_.isEmpty(this.user)) {
			console.log('this.user: ', this.user);
			this.users.getCurrentUserFollowers().then((data: any) => {
				this.usersArr = [];
				if (data && data.success) {
					this.usersArr = data.result;
					console.log('getCurrentUserFollowers: ', this.usersArr);
					_.each(this.usersArr, (row: any) => {
						if (_.find(this.user.following, { 'uuid': row.uuid })) {
							row.isFollowing = true;
						} else {
							row.isFollowing = false;
						}
					});
				}
				loading.dismiss();
				if (ev) {
					ev.complete();
				}
			}, (error) => {
				console.log('error: ', error);
				loading.dismiss();
				if (ev) {
					ev.complete();
				}
			});
		}
	}

	getFollowing(ev?: any) {
		this.user = JSON.parse(localStorage.getItem('app.user')) || {};
		let loading = this.loadingCtrl.create({
			dismissOnPageChange: true
		});
		loading.present();
		if (!_.isEmpty(this.user)) {
			console.log('this.user: ', this.user);
			this.users.getCurrentUserFollowing().then((data: any) => {
				this.usersArr = [];
				if (data && data.success) {
					this.usersArr = data.result;
					console.log('getCurrentUserFollowing: ', this.usersArr);
					_.each(this.usersArr, (row: any) => {
						row.isFollowing = true;
					});
				}
				loading.dismiss();
				if (ev) {
					ev.complete();
				}
			}, (error) => {
				console.log('error: ', error);
				loading.dismiss();
				if (ev) {
					ev.complete();
				}
			});
		}
	}

	follow(user: any, event) {
		event.preventDefault();
		event.stopPropagation();

		let loading = this.loadingCtrl.create({
			dismissOnPageChange: true
		});
		loading.present();
		this.users.followSeller({
			userId: user.uuid
		}).then((data: any) => {
			if (data && data.success) {
				this.initData();
			} else if (data && !data.success) {
				let alert = this.alertCtrl.create({
					title: 'Warning',
					message: data.message,
					buttons: ['OK']
				});
				alert.present();
			}
			loading.dismiss();
		}, (error) => {
			loading.dismiss();
			console.log('Error: ', error);
		});
	}

	unfollow(user: any, event) {
		event.preventDefault();
		event.stopPropagation();

		let loading = this.loadingCtrl.create({
			dismissOnPageChange: true
		});
		loading.present();
		this.users.unfollowSeller({
			userId: user.uuid
		}).then((data: any) => {
			if (data && data.success) {
				this.initData();
			}
			loading.dismiss();
		}, (error) => {
			loading.dismiss();
			console.log('Error: ', error);
		});
	}

	viewDetail(user: any) {
		if (user.uuid == this.user.uuid) {
			return;
		}

		this.navCtrl.push(ShopprofilePage, {
			profile: user
		});
	}
}
