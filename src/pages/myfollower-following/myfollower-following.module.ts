import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyfollowerFollowingPage } from './myfollower-following';

@NgModule({
  declarations: [
    MyfollowerFollowingPage,
  ],
  imports: [
    IonicPageModule.forChild(MyfollowerFollowingPage),
  ],
})
export class MyfollowerFollowingPageModule {}
