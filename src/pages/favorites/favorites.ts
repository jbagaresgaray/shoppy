import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController, Events, Platform, ToastController,ModalController,Tabs } from 'ionic-angular';
import * as _ from 'lodash';

import { ProductEntryPage } from '../../pages/product-entry/product-entry';

import { UsersServiceProvider } from '../../providers/service/users';

import { Camera, CameraOptions } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';




declare let cordova: any;

@Component({
  selector: 'page-favorites',
  templateUrl: 'favorites.html',
})
export class FavoritesPage {

  user: any = {};
  isSeller: boolean = false;
  isApplyAsSeller: boolean = false;

  files: FileList;
  filestring: string;
  filename: string;
  picImage: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public alertCtrl: AlertController, public users: UsersServiceProvider, public events: Events,
    private camera: Camera, public platform: Platform, public toastCtrl: ToastController, private filePath: FilePath, public file: File, public modalCtrl: ModalController) {

    this.getCurrentUser();

    this.events.subscribe('approve-seller', () => {
      console.log('approve-seller');
      this.getCurrentUser();
    });
  }

  ionViewDidLoad() {

  }

  ionViewWillEnter() {
    this.getCurrentUser();
  }

  getCurrentUser() {
    this.user = JSON.parse(localStorage.getItem('app.user')) || {};
    if (!_.isEmpty(this.user)) {
      if (this.user.isSeller) {
        this.isSeller = true;
      } else {
        this.isSeller = false;
      }

      if (!_.isEmpty(this.user.sellerApprovalCode)) {
        this.isApplyAsSeller = true;
      } else {
        this.isApplyAsSeller = false;
      }
    } else {
      this.isSeller = false;
    }
  }


  applyAsSeller() {
    let ctrl = this;

    function applySeller() {
      let loader = ctrl.loadingCtrl.create();
      loader.present();
      ctrl.users.applyAsSeller().then((data: any) => {
        if (data && data.success) {
          loader.dismiss();
          let alert = ctrl.alertCtrl.create({
            title: 'Success!',
            subTitle: data.msg,
            buttons: ['OK']
          });
          alert.present();
        } else if (data && !data.success) {
          loader.dismiss();
          let alert = ctrl.alertCtrl.create({
            title: 'Warning!',
            subTitle: data.msg,
            buttons: ['OK']
          });
          alert.present();
        }
      }, (error: any) => {
        loader.dismiss();
        console.log('error: ', error);
      });
    }

    let confirm = this.alertCtrl.create({
      title: 'Apply as a Seller?',
      message: 'Applying as a seller will you give you an opportunity to sell your preloved items and other stuffs you no longer used. Otherwise you can sell brand new items.',
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Proceed',
          handler: () => {
            applySeller();
          }
        }
      ]
    });
    confirm.present();
  }

  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

  public isBase64(str) {
    try {
      return window.btoa(atob(str)) == str;
    } catch (err) {
      return false;
    }
  }


  public pathForImage(img) {
    if (img === null) {
      return '';
    } else {
      if (this.isBase64(img)) {
        return 'data:image/png;base64,' + img;
      } else {
        if (this.platform.is('ios')) {
          let path = cordova.file.dataDirectory + img;
          path = path.replace(/^file:\/\//, '');
          return path;
        } else {
          return cordova.file.dataDirectory + img;
        }
      }
    }
  }

  public pathForImage2(img) {
    if (img === null) {
      return '';
    } else {
      return cordova.file.dataDirectory + img;
    }
  }

  private copyFileToLocalDir(namePath, currentName, newFileName) {
    let path = cordova.file.dataDirectory;
    this.file.copyFile(namePath, currentName, path, newFileName).then(success => {
      this.picImage = newFileName;

      console.log('this.pathForImage: ', this.pathForImage(this.picImage));
      console.log('this.pathForImage2: ', this.pathForImage2(this.picImage));
    }, error => {
      console.log('error: ', error);
      this.presentToast('Error while storing file.');
    });
  }

  private createFileName() {
    var d = new Date(),
      n = d.getTime(),
      newFileName = n + ".jpg";
    return newFileName;
  }

  private showCameraAlert(sourceType) {
    let options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: sourceType,
      encodingType: 0,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };

    if (this.platform.is('cordova')) {
      this.camera.getPicture(options).then((imageData) => {
        this.filename = this.createFileName();
        // this.picImage = imageData;
        console.log('imageData: ', imageData);
        if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
          console.log('android: ');
          this.filePath.resolveNativePath(imageData).then(filePath => {
            console.log('filePath: ', filePath);
            let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
            console.log('correctPath: ', correctPath);
            let currentName = imageData.substring(imageData.lastIndexOf('/') + 1, imageData.lastIndexOf('?'));
            console.log('currentName: ', currentName);
            this.copyFileToLocalDir(correctPath, currentName, this.filename);
          });
        } else {
          var currentName = imageData.substr(imageData.lastIndexOf('/') + 1);
          var correctPath = imageData.substr(0, imageData.lastIndexOf('/') + 1);
          this.copyFileToLocalDir(correctPath, currentName, this.filename);
        }
      }, (err) => {
        console.log('Error: ', err);
      });
    } else {
      console.log('No cordova found');
    }
  }

  openCamera() {
    this.showCameraAlert(this.camera.PictureSourceType.CAMERA);
  }

  openAlbum() {
    this.showCameraAlert(this.camera.PictureSourceType.PHOTOLIBRARY);
  }

  addProduct(){
    let modal = this.modalCtrl.create(ProductEntryPage);
    modal.onDidDismiss((resp)=>{
      if(resp == 'save'){
        let t: Tabs = this.navCtrl.parent;
        t.select(4);
      }
    });
    modal.present();
  }

}
