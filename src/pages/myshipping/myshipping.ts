import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import * as async from 'async';
import * as _ from 'lodash';

import { MyaddressPage } from '../../pages/myaddress/myaddress';

import { ServiceProvider } from '../../providers/service/service';
import { UserAddressessServiceProvider } from '../../providers/service/user-address';
import { UserShippingServiceProvider } from '../../providers/service/user-shipping';

@IonicPage()
@Component({
	selector: 'page-myshipping',
	templateUrl: 'myshipping.html',
})
export class MyshippingPage {
	shippingArr: any[] = [];
	address: any = {};

	selectedShipping: any[] = [];

	constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public services: ServiceProvider, public addressess: UserAddressessServiceProvider, public sellershipping: UserShippingServiceProvider) {
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad MyshippingPage');
		let loading = this.loadingCtrl.create();
		loading.present();
		async.waterfall([
			(callback) => {
				this.services.getAllShippingCouriers().then((data: any) => {
					if (data && data.success) {
						this.shippingArr = data.result;
						_.each(this.shippingArr, (row: any) => {
							row.selected = false;
						});
					}
					callback();
				});
			},
			(callback) => {
				this.sellershipping.getAllUserSellerShipping().then((data: any) => {
					if (data && data.success) {
						let shippingArr:any = data.result;
						_.each(this.shippingArr, (row: any) => {
							let res = _.find(shippingArr,{'shippingId': row.shippingCourierId});
							if(res){
								row.selected = true;
							}
						});
					}
					callback();
				});
			},
			(callback) => {
				this.addressess.getAllUserAddress().then((data: any) => {
					if (data && !_.isEmpty(data.result)) {
						let addressess = data.result;
						this.address = _.find(addressess, { 'isPickUpAddress': true });
						loading.dismiss();
					}
					callback();
				}, (error) => {
					loading.dismiss();
				});
			}
		]);
	}

	viewMyAddressess() {
		this.navCtrl.push(MyaddressPage);
	}

	saveShipping() {
		let loading = this.loadingCtrl.create();
		loading.present();

		let vm = this;
		async.eachSeries(this.selectedShipping, (row: any, callback) => {
			if (row.selected) {
				vm.sellershipping.createUserSellerShipping({
					shippingId: row.shippingCourierId
				}).then((data:any)=>{
					if(data && data.success){
						callback();
					}else{
						loading.dismiss();	
						return false;
					}
				},(error)=>{
					loading.dismiss();
					return false;
				});
			}else{
				vm.sellershipping.deleteUserSellerShipping(row.shippingCourierId).then((data:any)=>{
					if(data && data.success){
						callback();
					}else{
						loading.dismiss();	
						return false;
					}
				},(error)=>{
					loading.dismiss();
					return false;
				});
			}
		},()=>{
			loading.dismiss();
		});
	}

	selected(item) {
		console.log('item: ', item);
		let result = _.find(this.selectedShipping, { 'shippingCourierId': item.shippingCourierId });
		if (result) {
			_.each(this.selectedShipping, (row: any) => {
				if (row.shippingCourierId == item.shippingCourierId) {
					row.selected = item.selected;
				}
			});
		} else {
			this.selectedShipping.push(item);
		}
		console.log('this.selectedShipping: ', this.selectedShipping);
	}

}
