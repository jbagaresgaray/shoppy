import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyshippingPage } from './myshipping';

@NgModule({
  declarations: [
    MyshippingPage,
  ],
  imports: [
    IonicPageModule.forChild(MyshippingPage),
  ],
})
export class MyshippingPageModule {}
