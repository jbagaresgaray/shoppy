import { Component, ViewChild, NgZone } from "@angular/core";
import {
	IonicPage,
	NavController,
	NavParams,
	PopoverController,
	ViewController,
	App,
	MenuController,
	Content,
	LoadingController,
	Platform,
	Events,
	Searchbar
} from "ionic-angular";
import { Storage } from "@ionic/storage";
import * as _ from "lodash";

import { DetailsPage } from "../../pages/details/details";
import { SearchresultPage } from "../../pages/searchresult/searchresult";

import { ProductsServiceProvider } from "../../providers/service/products";
import { UsersServiceProvider } from "../../providers/service/users";
import { SellerServiceProvider } from "../../providers/service/seller";
import { ServiceProvider } from "../../providers/service/service";

@Component({
	template: `
		<ion-list>
			<button ion-item (click)="close()">Best Match</button>
			<button ion-item (click)="close()">Price (Low to High)</button>
			<button ion-item (click)="close()">Price (High to Low)</button>
			<button ion-item (click)="close()">Number of Orders</button>
			<button ion-item (click)="close()">Top Sales</button>
			<button ion-item (click)="close()">Date Added</button>
		</ion-list>
	`
})
export class SearchPopoverPage {
	constructor(public viewCtrl: ViewController) {}

	close() {
		this.viewCtrl.dismiss();
	}
}
@IonicPage()
@Component({
	selector: "page-search",
	templateUrl: "search.html"
})
export class SearchPage {
	items: any[] = [];
	productsArr: any[] = [];
	productsArrCopy: any[] = [];
	userLikesArr: any[] = [];
	userSellerArr: any[] = [];

	user: any = {};
	filter_datas: any = {};
	currency: any = {};

	showList: boolean = false;
	showProductResult: boolean = false;
	showSellerResult: boolean = false;
	isLow: boolean = true;

	myInput: string;
	type: string;
	search: string;
	mode: string = "popular";

	@ViewChild(Content) content: Content;
	@ViewChild("mySearchbar") searchbar: Searchbar;

	constructor(
		public app: App,
		public navCtrl: NavController,
		public navParams: NavParams,
		public popoverCtrl: PopoverController,
		public menuCtrl: MenuController,
		public loadingCtrl: LoadingController,
		public storage: Storage,
		public products: ProductsServiceProvider,
		public users: UsersServiceProvider,
		public sellers: SellerServiceProvider,
		public zone: NgZone,
		public platform: Platform,
		public events: Events,
		public services: ServiceProvider
	) {
		this.search = navParams.get("search");
		if (this.search) {
			this.myInput = this.search;
		}
		this.user = JSON.parse(localStorage.getItem("app.user")) || {};
		this.showList = false;
		this.showProductResult = false;
		this.showSellerResult = false;

		this.currency = this.services.defaultCurrency();

		storage.get("search").then(val => {
			if (!_.isEmpty(val)) {
				this.items = val;
			}
		});

		this.filter_datas.categories = [];
		this.filter_datas.shipping_options = [];

		// ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
		// ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
		// ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //

		events.unsubscribe("search_filter");
		events.unsubscribe("reset_filter");

		events.subscribe("search_filter", filters => {
			setTimeout(() => {
				this.globalFilter(filters);
			}, 300);
		});

		events.subscribe("reset_filter", filters => {
			setTimeout(() => {
				this.resetFilter();
			}, 300);
		});
	}

	presentPopover(myEvent) {
		let popover = this.popoverCtrl.create(SearchPopoverPage);
		popover.present({
			ev: myEvent
		});
	}

	filterProducts(search, ev?: any) {
		this.showProductResult = false;
		this.products
			.getAllProducts({
				search: search
			})
			.then(
				(data: any) => {
					if (data && data.success) {
						this.productsArr = [];
						this.productsArrCopy = [];

						_.each(data.result, (row: any) => {
							if (this.platform.is("ios")) {
								row.date_created = new Date(
									row.date_created.replace(/-/g, "/")
								);
							}
							row.date_created_timestamp = Math.floor(
								new Date(row.date_created).getTime() / 1000
							);
						});

						this.productsArr = data.result;
						this.productsArrCopy = _.clone(data.result);

						console.log("this.productsArr: ", this.productsArr);

						let categories: any[] = [];
						let shipping_options: any[] = [];
						categories = _.map(this.productsArrCopy, (row: any) => {
							return row.category;
						});
						this.filter_datas.categories = categories;
						_.each(this.productsArrCopy, (row: any) => {
							_.each(row.shippingFee, (item: any) => {
								shipping_options.push(item);
							});
						});
						this.filter_datas.shipping_options = shipping_options;

						if (!_.isEmpty(this.user)) {
							this.users.getCurrentUserLike().then(
								(data: any) => {
									if (data && data.success) {
										this.userLikesArr = data.result;
										_.each(this.productsArr, (row: any) => {
											let res = _.find(
												this.userLikesArr,
												{ productId: row.productId }
											);
											if (res) {
												row.isLiked = true;
											} else {
												row.isLiked = false;
											}
										});
									}
								},
								error => {
									console.log("getCurrentUserLike: ", error);
								}
							);
						}
					}
					this.showProductResult = true;
					if (ev) {
						ev.complete();
					}
				},
				error => {
					this.showProductResult = true;
					if (ev) {
						ev.complete();
					}
					console.log("products error: ", error);
				}
			)
			.catch(error => {
				this.showProductResult = true;
				if (ev) {
					ev.complete();
				}
				console.log("products error: ", error);
			});
	}

	filterUserSeller(search, ev?: any) {
		this.showSellerResult = false;
		this.sellers
			.getAllSellers({
				search: search
			})
			.then((data: any) => {
				if (data && data.success) {
					console.log("sellers: ", data);
					this.userSellerArr = data.result;
				}
				this.showSellerResult = true;
				if (ev) {
					ev.complete();
				}
			})
			.catch(error => {
				this.showSellerResult = true;
				if (ev) {
					ev.complete();
				}
			});
	}

	ionViewDidLoad() {
		console.log("ionViewDidLoad SearchPage");
		this.user = JSON.parse(localStorage.getItem("app.user")) || {};
		this.filterProducts(this.search);
		this.filterUserSeller(this.search);
	}

	ionViewWillLeave() {
		this.menuCtrl.enable(false, "filtermenu");
	}

	doRefresh(ev) {
		this.filterProducts(this.myInput, ev);
		this.filterUserSeller(this.myInput, ev);
	}

	onInput(ev) {}

	onCancel(ev) {
		this.showList = false;
		this.content.resize();
	}

	checkBlur() {
		// this.showList = false;
	}

	checkFocus() {
		this.showList = true;
		this.content.resize();
	}

	viewFilters() {
		console.log("viewFilters");
		this.menuCtrl.enable(true, "filtermenu");
		this.events.publish("filter_datas", this.filter_datas);
		this.menuCtrl.toggle("right");
	}

	gotoDetails(item) {
		this.app.getRootNav().push(DetailsPage, {
			product: item
		});
	}

	clearSearch() {
		this.myInput = "";
		this.searchbar.clearInput(null);
		this.storage.remove("search");
		this.storage.get("search").then(val => {
			if (!_.isEmpty(val)) {
				this.items = val;
			} else {
				this.items = [];
			}
		});
	}

	submitSearch(ev: any) {
		let val = this.myInput;
		console.log("val: ", val);
		if (val && val.trim() != "") {
			this.showList = false;

			this.filterProducts(val);
			this.filterUserSeller(val);

			let res = _.find(this.items, (row: any) => {
				return row == val;
			});
			if (_.isEmpty(res)) {
				this.items.push(val);
				this.storage.set("search", this.items);
			}
		}
	}

	segmentChanged(ev: any) {
		console.log("this.mode: ", this.mode);
		let loading = this.loadingCtrl.create({
			dismissOnPageChange: true
		});
		loading.present();

		if (this.mode == "popular") {
			this.productsArr = _.orderBy(this.productsArr, "rating", "desc");
		} else if (this.mode == "latest") {
			this.productsArr = _.orderBy(
				this.productsArr,
				"date_created_timestamp",
				"asc"
			);
		} else if (this.mode == "top") {
			this.productsArr = _.orderBy(
				this.productsArr,
				"orderscount",
				"desc"
			);
		} else if (this.mode == "price") {
			if (this.isLow) {
				this.productsArr = _.orderBy(
					this.productsArr,
					"productPrice",
					"asc"
				);
			} else {
				this.productsArr = _.orderBy(
					this.productsArr,
					"productPrice",
					"desc"
				);
			}
		}
		loading.dismiss();
	}

	sortPrice() {
		this.isLow = !this.isLow;
		if (this.isLow) {
			this.zone.run(() => {
				this.productsArr = _.orderBy(
					this.productsArr,
					"productPrice",
					"asc"
				);
			});
		} else {
			this.zone.run(() => {
				this.productsArr = _.orderBy(
					this.productsArr,
					"productPrice",
					"desc"
				);
			});
		}
		console.log("sortPrice: ", this.productsArr);
	}

	resetFilter() {
		let loading = this.loadingCtrl.create({
			dismissOnPageChange: true
		});
		loading.present();
		this.productsArr = this.productsArrCopy;
		setTimeout(() => {
			loading.dismiss();
		}, 1000);
	}

	globalFilter(filters: any) {
		console.log("search_filter: ", filters);
		let tempArr: any[] = [];
		let loading = this.loadingCtrl.create({
			dismissOnPageChange: true
		});
		loading.present();

		if (filters.categories) {
			_.each(filters.categories, (row: any) => {
				let categories = _.filter(this.productsArrCopy, (item: any) => {
					return item.category.categoryId == row.categoryId;
				});
				_.each(categories, (item: any) => {
					tempArr.push(item);
				});
			});
		} else if (filters.shipping_options) {
			_.each(filters.shipping_options, (row: any) => {
				let shippingFee = _.filter(
					this.productsArrCopy,
					(item: any) => {
						return _.find(item.shippingFee, {
							shippingCourierId: row.shippingCourierId
						});
					}
				);
				console.log("shippingFee1: ", shippingFee);
				_.each(shippingFee, (item: any) => {
					tempArr.push(item);
				});
			});
		}

		if (filters.productWholeSale) {
			let productWholeSale = _.filter(
				this.productsArrCopy,
				(item: any) => {
					return _.size(item.wholesale) > 0;
				}
			);
			_.each(productWholeSale, (item: any) => {
				tempArr.push(item);
			});
		} else if (filters.productDiscounted) {
		} else if (filters.productLowPrice) {
		} else if (filters.productCOD) {
		} else if (filters.productFreeShipping) {
			let productFreeShipping = _.filter(
				this.productsArrCopy,
				(item: any) => {
					return _.find(item.shippingFee, { isCover: 1 });
				}
			);
			console.log("shippingFee1: ", productFreeShipping);
			_.each(productFreeShipping, (item: any) => {
				tempArr.push(item);
			});
		}

		if (filters.productNewCondition) {
			let productNewCondition = _.filter(this.productsArrCopy, {
				productCondition: "new"
			});
			_.each(productNewCondition, (item: any) => {
				tempArr.push(item);
			});
		} else if (filters.productUsedCondition) {
			let productUsedCondition = _.filter(this.productsArrCopy, {
				productCondition: "used"
			});
			_.each(productUsedCondition, (item: any) => {
				tempArr.push(item);
			});
		}

		if (filters.minValue && _.isEmpty(filters.maxValue)) {
			let minValue = _.filter(this.productsArrCopy, (item: any) => {
				return item.productPrice >= filters.minValue;
			});
			_.each(minValue, (item: any) => {
				tempArr.push(item);
			});
		} else if (filters.maxValue && _.isEmpty(filters.minValue)) {
			let minValue = _.filter(this.productsArrCopy, (item: any) => {
				return item.productPrice <= filters.maxValue;
			});
			_.each(minValue, (item: any) => {
				tempArr.push(item);
			});
		} else if (filters.minValue && filters.maxValue) {
			let values = _.filter(this.productsArrCopy, (item: any) => {
				return (
					item.productPrice >= filters.minValue &&
					item.productPrice <= filters.maxValue
				);
			});
			_.each(values, (item: any) => {
				tempArr.push(item);
			});
		}

		console.log("tempArr: ", tempArr);

		this.productsArr = _.uniqBy(tempArr, "productId");
		loading.dismiss();
	}

	menuOpened() {}

	showMoreShops() {
		this.navCtrl.push(SearchresultPage, {
			search: this.myInput,
			action: "shops"
		});
	}
}
