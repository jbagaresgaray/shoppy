import { NgModule } from "@angular/core";
import { IonicPageModule } from "ionic-angular";
import { Ionic2RatingModule } from "ionic2-rating";

import { PipesModule } from "../../pipes/pipes.module";
import { ComponentsModule } from "../../components/components.module";

import { SearchPage, SearchPopoverPage } from "./search";

@NgModule({
  declarations: [SearchPage, SearchPopoverPage],
  imports: [
    Ionic2RatingModule,
    PipesModule,
    ComponentsModule,
    IonicPageModule.forChild(SearchPage)
  ],
  entryComponents: [SearchPopoverPage]
})
export class SearchPageModule {}
