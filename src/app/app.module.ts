import { NgModule, ErrorHandler } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { IonicApp, IonicModule, IonicErrorHandler } from "ionic-angular";
import { RestangularModule } from "ngx-restangular/dist/esm/src";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { ChartsModule } from "ng2-charts/ng2-charts";
import { IonicStorageModule } from "@ionic/storage";
import { HttpClientModule } from "@angular/common/http";
import { JwtModule } from "@auth0/angular-jwt";
// import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';
import { NgPipesModule } from "ngx-pipes";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { SocialLoginModule, AuthServiceConfig } from "angular4-social-login";
import {
  GoogleLoginProvider,
  FacebookLoginProvider
} from "angular4-social-login";
import { Ionic2RatingModule } from "ionic2-rating";

import { environment } from "@app/env";

import { MyApp } from "./app.component";
import { AboutPage } from "../pages/about/about";
import { FavoritesPage } from "../pages/favorites/favorites";
import { HomePage } from "../pages/home/home";
import { SettingsPage } from "../pages/settings/settings";

import { TabsPageModule } from "../pages/tabs/tabs.module";
import { ChatPageModule } from "../pages/chat/chat.module";
import { ChatdetailPageModule } from "../pages/chatdetail/chatdetail.module";
import { DetailsPageModule } from "../pages/details/details.module";
import { LoginPageModule } from "../pages/login/login.module";
import { MyaccountPageModule } from "../pages/myaccount/myaccount.module";
import { MyprofilePageModule } from "../pages/myprofile/myprofile.module";
import { MyratingPageModule } from "../pages/myrating/myrating.module";
import { MyaddressPageModule } from "../pages/myaddress/myaddress.module";
import { MybankcardsPageModule } from "../pages/mybankcards/mybankcards.module";
import { MywalletPageModule } from "../pages/mywallet/mywallet.module";
import { MypurchasesPageModule } from "../pages/mypurchases/mypurchases.module";
import { MylikesPageModule } from "../pages/mylikes/mylikes.module";
import { MyshippingPageModule } from "../pages/myshipping/myshipping.module";
import { MyincomePageModule } from "../pages/myincome/myincome.module";
import { MycustomersPageModule } from "../pages/mycustomers/mycustomers.module";
import { MyproductsPageModule } from "../pages/myproducts/myproducts.module";
import { MyfollowerFollowingPageModule } from "../pages/myfollower-following/myfollower-following.module";
import { OrderdetailsPageModule } from "../pages/orderdetails/orderdetails.module";
import { ProductcommentPageModule } from "../pages/productcomment/productcomment.module";
import { ProductratingsPageModule } from "../pages/productratings/productratings.module";
import { ProductratingsEntryPageModule } from "../pages/productratings-entry/productratings-entry.module";
import { ProductEntryPageModule } from "../pages/product-entry/product-entry.module";
import { ProductWholesaleEntryPageModule } from "../pages/product-wholesale-entry/product-wholesale-entry.module";
import { ProductVariationEntryPageModule } from "../pages/product-variation-entry/product-variation-entry.module";
import { ProductPackagingEntryPageModule } from "../pages/product-packaging-entry/product-packaging-entry.module";
import { ProductShippingEntryPageModule } from "../pages/product-shipping-entry/product-shipping-entry.module";
import { SearchPageModule } from "../pages/search/search.module";
import { SearchresultPageModule } from "../pages/searchresult/searchresult.module";
import { ShoppingcartPageModule } from "../pages/shoppingcart/shoppingcart.module";
import { PaymentmethodPageModule } from "../pages/shoppingcart-paymentmethod/paymentmethod.module";
import { PaymentoptionPageModule } from "../pages/shoppingcart-paymentoption/paymentoption.module";
import { ShippingoptionPageModule } from "../pages/shoppingcart-shippingoption/shippingoption.module";
import { ShoppingcartSelectshippingoptionPageModule } from "../pages/shoppingcart-selectshippingoption/shoppingcart-selectshippingoption.module";
import { DeliveryaddressPageModule } from "../pages/shoppingcart-deliveryaddress/deliveryaddress.module";
import { ConfirmationPageModule } from "../pages/shoppingcart-confirmation/confirmation.module";
import { MakepaymentPageModule } from "../pages/shoppingcart-makepayment/makepayment.module";
import { ShopprofilePageModule } from "../pages/shopprofile/shopprofile.module";
import { ShopprofileEntryPageModule } from "../pages/shopprofile-entry/shopprofile-entry.module";
import { ShopprofileSettingsPageModule } from "../pages/shopprofile-settings/shopprofile-settings.module";
import { SignupPageModule } from "../pages/signup/signup.module";
import { ForgotPageModule } from "../pages/forgot/forgot.module";
import { UserprofilePageModule } from "../pages/userprofile/userprofile.module";
import { UserprofileEntryPageModule } from "../pages/userprofile-entry/userprofile-entry.module";
import { NotificationPageModule } from "../pages/notification/notification.module";
import { AccountsettingsPageModule } from "../pages/accountsettings/accountsettings.module";
import { FeedPageModule } from "../pages/feed/feed.module";
import { CountryCurrencyPageModule } from "../pages/country-currency/country-currency.module";
import { ManageShopPageModule } from "../pages/manage-shop/manage-shop.module";
import { OnboardingPageModule } from "../pages/onboarding/onboarding.module";
import { ContentFormPageModule } from "../pages/content-form/content-form.module";
import { AllFormEntryPageModule } from "../pages/all-form-entry/all-form-entry.module";
import { HelpCentrePageModule } from "../pages/help-centre/help-centre.module";
import { ShoppingcartCheckoutPageModule } from "../pages/shoppingcart-checkout/shoppingcart-checkout.module";
import { MyvouchersPageModule } from "../pages/myvouchers/myvouchers.module";
import { ModalComponentFormPageModule } from "../pages/modal-component-form/modal-component-form.module";

/*import { EmojiPickerComponent } from '../components/emoji-picker/emoji-picker';
import { MultiImageUploadComponent } from '../components/multi-image-upload/multi-image-upload';*/

import { PipesModule } from "../pipes/pipes.module";
import { ComponentsModule } from "../components/components.module";

import { ServiceProvider } from "../providers/service/service";
import { AuthProvider } from "../providers/service/authentication";
import { EmojiProvider } from "../providers/service/emojis";
import { ChatService } from "../providers/service/chat";
import { CategoriesServiceProvider } from "../providers/service/categories";
import { BrandsServiceProvider } from "../providers/service/brands";
import { UsersServiceProvider } from "../providers/service/users";
import { ProductsServiceProvider } from "../providers/service/products";
import { ShoppingCartProvider } from "../providers/service/shopping-cart";
import { UserAddressessServiceProvider } from "../providers/service/user-address";
import { UserBanksServiceProvider } from "../providers/service/user-bank";
import { BanksServiceProvider } from "../providers/service/banks";
import { UserShippingServiceProvider } from "../providers/service/user-shipping";
import { OrdersServiceProvider } from "../providers/service/orders";
import { SellerServiceProvider } from "../providers/service/seller";
import { NotificationsServiceProvider } from "../providers/service/notifications";
import { DiscountPromotionsService } from "../providers/service/discount-promotions";
import { SellerVouchersService } from "../providers/service/seller-vouchers";

import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";
import { InAppBrowser } from "@ionic-native/in-app-browser";
import { HTTP } from "@ionic-native/http";
import { Camera } from "@ionic-native/camera";
import { FilePath } from "@ionic-native/file-path";
import { File } from "@ionic-native/file";
import { FileTransfer } from "@ionic-native/file-transfer";
import { OneSignal } from "@ionic-native/onesignal";
import { GooglePlus } from "@ionic-native/google-plus";
import { Facebook } from "@ionic-native/facebook";
import { Crop } from "@ionic-native/crop";
import { LocalNotifications } from "@ionic-native/local-notifications";
import { SocialSharing } from "@ionic-native/social-sharing";
import { Device } from "@ionic-native/device";
import { AppVersion } from "@ionic-native/app-version";

console.log("environment: ", environment);

export function RestangularConfigFactory(RestangularProvider) {
  // RestangularProvider.setBaseUrl((environment.production == true) ? environment.api_prod_url : environment.api_url);
  RestangularProvider.setBaseUrl(environment.api_url);
  RestangularProvider.setPlainByDefault(true);
  RestangularProvider.setFullResponse(true);
}

export function tokenGetter() {
  return localStorage.getItem("app.token");
}

// const config: SocketIoConfig = { url: environment.socket_url, options: {} };

let socialConfig = new AuthServiceConfig([
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider(environment.GoogleClientID)
  },
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider(environment.FacebookAppID)
  }
]);

export function provideConfig() {
  return socialConfig;
}

@NgModule({
  declarations: [MyApp, AboutPage, FavoritesPage, HomePage, SettingsPage],
  imports: [
    BrowserModule,
    CommonModule,
    NgPipesModule,
    IonicModule.forRoot(MyApp, {
      backButtonText: "",
      iconMode: "ios",
      mode: "ios"
    }),
    PipesModule,
    ComponentsModule,
    RestangularModule.forRoot(RestangularConfigFactory),
    FormsModule,
    IonicStorageModule.forRoot(),
    ChartsModule,
    HttpClientModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter
      }
    }),
    // SocketIoModule.forRoot(config),
    BrowserAnimationsModule,
    SocialLoginModule.initialize(socialConfig),
    Ionic2RatingModule,
    TabsPageModule,
    ChatPageModule,
    ChatdetailPageModule,
    DetailsPageModule,
    LoginPageModule,
    MypurchasesPageModule,
    ConfirmationPageModule,
    DeliveryaddressPageModule,
    LoginPageModule,
    MakepaymentPageModule,
    MyaccountPageModule,
    MyprofilePageModule,
    MyratingPageModule,
    MylikesPageModule,
    MyshippingPageModule,
    MyincomePageModule,
    MycustomersPageModule,
    MyproductsPageModule,
    MyvouchersPageModule,
    MyfollowerFollowingPageModule,
    OrderdetailsPageModule,
    PaymentmethodPageModule,
    PaymentoptionPageModule,
    ProductcommentPageModule,
    ProductratingsPageModule,
    ProductratingsEntryPageModule,
    SearchPageModule,
    SearchresultPageModule,
    ShippingoptionPageModule,
    ShoppingcartPageModule,
    ShopprofilePageModule,
    ShopprofileEntryPageModule,
    ShopprofileSettingsPageModule,
    SignupPageModule,
    UserprofilePageModule,
    UserprofileEntryPageModule,
    NotificationPageModule,
    AccountsettingsPageModule,
    ForgotPageModule,
    MyaddressPageModule,
    MybankcardsPageModule,
    MywalletPageModule,
    FeedPageModule,
    ProductEntryPageModule,
    ProductWholesaleEntryPageModule,
    ProductVariationEntryPageModule,
    ProductPackagingEntryPageModule,
    ProductShippingEntryPageModule,
    CountryCurrencyPageModule,
    ManageShopPageModule,
    ShoppingcartSelectshippingoptionPageModule,
    OnboardingPageModule,
    ContentFormPageModule,
    AllFormEntryPageModule,
    HelpCentrePageModule,
    ShoppingcartCheckoutPageModule,
    ModalComponentFormPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [MyApp, AboutPage, HomePage, FavoritesPage, SettingsPage],
  providers: [
    StatusBar,
    SplashScreen,
    HTTP,
    Camera,
    FilePath,
    File,
    FileTransfer,
    OneSignal,
    GooglePlus,
    Facebook,
    Crop,
    InAppBrowser,
    LocalNotifications,
    SocialSharing,
    Device,
    AppVersion,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    /*{
            provide: AuthServiceConfig,
            useFactory: provideConfig
        },*/
    ServiceProvider,
    AuthProvider,
    EmojiProvider,
    ChatService,
    CategoriesServiceProvider,
    BrandsServiceProvider,
    UsersServiceProvider,
    ProductsServiceProvider,
    ShoppingCartProvider,
    UserAddressessServiceProvider,
    BanksServiceProvider,
    UserBanksServiceProvider,
    UserShippingServiceProvider,
    OrdersServiceProvider,
    SellerServiceProvider,
    NotificationsServiceProvider,
    DiscountPromotionsService,
    SellerVouchersService
  ]
})
export class AppModule {}
