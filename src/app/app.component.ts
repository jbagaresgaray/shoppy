import { Component } from "@angular/core";
import {
  Platform,
  MenuController,
  Events,
  AlertController,
  App
} from "ionic-angular";
import { JwtHelperService } from "@auth0/angular-jwt";
// import { Socket } from 'ng-socket-io';
import * as _ from "lodash";

import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";
import { Storage } from "@ionic/storage";
import { OneSignal } from "@ionic-native/onesignal";
import { LocalNotifications } from "@ionic-native/local-notifications";

// import { environment } from '../environments/environment';
import { environment } from "@app/env";

import { TabsPage } from "../pages/tabs/tabs";
import { OnboardingPage } from "../pages/onboarding/onboarding";

import { DetailsPage } from "../pages/details/details";
import { ShopprofilePage } from "../pages/shopprofile/shopprofile";
import { ContentFormPage } from "../pages/content-form/content-form";
import { OrderdetailsPage } from "../pages/orderdetails/orderdetails";

import { UsersServiceProvider } from "../providers/service/users";
import { ChatService } from "../providers/service/chat";

declare let Notification: any;

@Component({
  templateUrl: "app.html"
})
export class MyApp {
  rootPage: any = TabsPage;
  user: any = {};
  filter: any = {};

  app_categories: any[] = [];
  app_shippingoptions: any[] = [];

  // Services
  isWholeSale: boolean = false;
  isDiscounted: boolean = false;
  isLowPrice: boolean = false;
  isCOD: boolean = false;
  isFreeShipping: boolean = false;
  // Condition
  isNew: boolean = false;
  isUsed: boolean = false;

  constructor(
    platform: Platform,
    public menuCtrl: MenuController,
    public events: Events,
    public alertCtrl: AlertController,
    public app: App,
    public users: UsersServiceProvider,
    public chatService: ChatService,
    public storage: Storage,
    private oneSignal: OneSignal,
    private localNotifications: LocalNotifications,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen
  ) {
    // ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
    // ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //
    // ======================== EVENTS / SUBSCRIPTION / BROADCAST / NOTIFICATIONS ============================ //

    events.unsubscribe("filter_datas");
    events.subscribe("filter_datas", datas => {
      console.log("filter_datas: ", datas);
      this.filterDatas(datas);
    });

    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();

      if (platform.is("cordova")) {
        if (window["cordova"] && window["cordova"].plugins.Keyboard) {
          window["cordova"].plugins.Keyboard.disableScroll(true);
        }

        this.oneSignal.startInit(
          environment.OneSignal,
          environment.GoogleProjectID
        );
        // this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);
        this.oneSignal.inFocusDisplaying(
          this.oneSignal.OSInFocusDisplayOption.Notification
        );

        this.oneSignal.handleNotificationReceived().subscribe(() => {
          console.log("do something when notification is received: ");
          this.events.publish("reloadAppNotifications");
        });

        this.oneSignal
          .handleNotificationOpened()
          .subscribe((notification: any) => {
            console.log(
              "do something when a notification is opened: ",
              notification
            );
            this.viewNotification(notification);
          });

        this.oneSignal.getIds().then((data: any) => {
          console.log("getIds: ", data);
        });

        this.oneSignal.endInit();
      }

      this.storage.get("introShown").then(result => {
        if (result) {
          this.rootPage = TabsPage;
        } else {
          this.rootPage = OnboardingPage;
        }
      });

      /*this.localNotifications.on('click', (notification: any, state) => {
        let resp = JSON.parse(notification.data);
        console.log('localNotifications resp: ', resp);
      });*/
    });

    this.initData();
  }

  private initData() {
    let vm = this;

    const helper = new JwtHelperService();
    let myRawToken = localStorage.getItem("app.token");
    const isExpired = helper.isTokenExpired(myRawToken);
    if (isExpired) {
      localStorage.removeItem("app.user");
      localStorage.removeItem("app.token");
    } else {
      this.user = JSON.parse(localStorage.getItem("app.user")) || {};
      if (!_.isEmpty(this.user)) {
        this.users
          .getCurrentUser()
          .then((data: any) => {
            if (data && data.success) {
              localStorage.removeItem("app.user");
              localStorage.setItem("app.user", JSON.stringify(data.result));
            }
          })
          .then(() => {
            this.user = JSON.parse(localStorage.getItem("app.user")) || {};
            this.chatService.connect().then(() => {
              this.chatService.ChannelHandler.onMessageReceived = (
                channel,
                message
              ) => {
                if (window["cordova"]) {
                  vm.localNotifyMe(message._sender, message);
                } else {
                  vm.notifyMe(message._sender.nickname, message.message);
                }

                this.chatService.getTotalUnreadMessageCount();
                vm.events.publish("onMessageReceived");
              };

              this.chatService.ChannelHandler.onMessageUpdated = (
                channel,
                message
              ) => {
                if (window["cordova"]) {
                  vm.localNotifyMe(message._sender, message);
                } else {
                  vm.notifyMe(message._sender.nickname, message.message);
                }

                this.chatService.getTotalUnreadMessageCount();
                vm.events.publish("onMessageUpdated");
              };

              this.chatService.ChannelHandler.onReadReceiptUpdated = (
                channel,
                message
              ) => {
                console.log("onReadReceiptUpdated");
                this.chatService.getTotalUnreadMessageCount();
              };

              this.chatService.sendBird.addChannelHandler(
                "GLOBAL_UNIQUE_HANDLER_ID",
                this.chatService.ChannelHandler
              );
              this.chatService.getTotalUnreadMessageCount();
            });
          });
      }
    }
  }

  notifyMe(user, message) {
    console.log("notifyMe: ", user);
    console.log("notifyMe: ", message);

    if (!("Notification" in window)) {
      let alert = this.alertCtrl.create({
        message: "This browser does not support desktop notification",
        buttons: ["OK"]
      });
      alert.present();
    } else if (Notification.permission === "granted") {
      var options = {
        body: message,
        dir: "ltr"
      };
      new Notification(user + " Posted a message", options);
    } else if (Notification.permission !== "denied") {
      Notification.requestPermission(function(permission) {
        if (!("permission" in Notification)) {
          Notification.permission = permission;
        }
        if (permission === "granted") {
          var options = {
            body: message,
            dir: "ltr"
          };
          new Notification(user + " Posted a message", options);
        }
      });
    }
  }

  localNotifyMe(user: any, message: any) {
    console.log("localNotifyMe");
    this.localNotifications.schedule({
      id: message.messageId,
      text: user.nickname + ":" + message.message,
      data: {
        user: user,
        message: message,
        action: "message"
      }
    });
  }

  closeMenu() {
    this.menuCtrl.close();
  }

  applyFilter() {
    if (this.filter.minValue && this.filter.maxValue) {
      if (parseInt(this.filter.minValue) > parseInt(this.filter.maxValue)) {
        let alert = this.alertCtrl.create({
          title: "Warning",
          message:
            "Filter minimum value must not be greater than filter maximum value!",
          buttons: ["OK"]
        });
        alert.present();
        return;
      }
    }

    // Services
    this.filter.productWholeSale = this.isWholeSale;
    this.filter.productDiscounted = this.isDiscounted;
    this.filter.productLowPrice = this.isLowPrice;
    this.filter.productCOD = this.isCOD;
    this.filter.productFreeShipping = this.isFreeShipping;

    // Condition
    this.filter.productNewCondition = this.isNew;
    this.filter.productUsedCondition = this.isUsed;

    this.events.publish("search_filter", this.filter);
    this.menuCtrl.close();
  }

  resetFilter() {
    this.filter.minValue = null;
    this.filter.maxValue = null;

    this.events.publish("reset_filter");
    this.menuCtrl.close();
  }

  selectCategories(item: any) {
    _.each(this.app_categories, (row: any) => {
      if (row.categoryId == item.categoryId) {
        row.selected = !item.selected;
      }
    });
    this.filter.categories = _.filter(this.app_categories, { selected: true });
  }

  selectShipping(item: any) {
    _.each(this.app_shippingoptions, (row: any) => {
      if (row.shippingCourierId == item.shippingCourierId) {
        row.selected = !item.selected;
      }
    });
    this.filter.shipping_options = _.filter(this.app_shippingoptions, {
      selected: true
    });
  }

  filterDatas(data: any) {
    this.app_categories = [];
    this.app_shippingoptions = [];

    this.app_categories = _.uniqBy(data.categories, "categoryId");
    this.app_shippingoptions = _.uniqBy(
      data.shipping_options,
      "shippingCourierId"
    );
  }

  viewNotification(notif: any) {
    console.log("viewNotification: ", notif);
    if (notif.notification) {
      let notification: any = notif.notification;
      let addData: any = notification.payload.additionalData;
      console.log("addData: ", addData);

      if (addData.action == "like") {
        this.app.getRootNav().push(DetailsPage, {
          product: {
            userId: addData.storeUserId,
            productId: addData.productId
          }
        });
      } else if (addData.action == "follow") {
        this.app.getRootNav().push(ShopprofilePage, {
          profile: {
            uuid: addData.followerUUID,
            userId: addData.followerId
          }
        });
      } else if (addData.action == "newsletter") {
        this.app.getRootNav().push(ContentFormPage, {
          action: "newsletter",
          params: {
            link: JSON.stringify(addData)
          }
        });
      } else if (addData.action == "order") {
        this.app.getRootNav().push(OrderdetailsPage, {
          // batchId: addData.batchNum,
          orderId: addData.orderId,
          status: "topay",
          usertype: "buyer"
        });
      } else if (addData.action == "order_ship") {
        this.app.getRootNav().push(OrderdetailsPage, {
          // batchId: addData.batchNum,
          orderId: addData.orderId,
          status: "ship",
          usertype: "buyer"
        });
      }
    }
  }
}
