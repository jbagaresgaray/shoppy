import { Injectable } from '@angular/core';
import * as _ from 'lodash';

import { Storage } from '@ionic/storage';

@Injectable()
export class ShoppingCartProvider {
    public grossTotal: number = 0;
    public deliveryTotal: number = 0;
    public itemsTotal: number = 0;

    cartsArr: any[] = [];
    constructor(public storage: Storage) {
        this.retrieve();
    }

    public emptyCart() {
        this.storage.remove('app.cart');
    }

    public getCart(): any {
        return this.storage.get('app.cart').then((val) => {
            this.cartsArr = val;
            return val;
        });
    }

    public cartCount(): number {
        return (!_.isEmpty(this.cartsArr)) ? this.cartsArr.length : 0;
    }

    public addItem(item: any, quantity: number) {
        let cartItem: any = {
            productId: item.productId,
            variantId: item.variantId,
            variant: item.variant,
            uuid: item.uuid,
            storeId: item.userId,
            quantity: item.quantity || 0,
            details: item,
            store: item.shopInfo
        }
        console.log('cartItem: ', cartItem);
        this.retrieve().then((data: any) => {
            let cartsArr: any = [];
            if (!_.isEmpty(data)) {
                cartsArr = data;
                let cart = _.find(data, (p: any) => p.uuid === item.uuid);
                if (cart === undefined) {
                    cartItem.quantity += quantity;
                    cartsArr.push(cartItem);
                } else {
                    _.each(cartsArr, (p: any) => {
                        if (p.uuid === item.uuid) {
                            p.quantity = parseInt(p.quantity) + quantity;
                        }
                    });
                }
            } else {
                cartItem.quantity += quantity;
                cartsArr.push(cartItem);
            }
            this.save(cartsArr);
        });
    }

    public removeItem(item) {
        this.retrieve().then((data: any) => {
            let cartsArr: any = [];
            if (!_.isEmpty(data)) {
                cartsArr = data;
                let newCart = _.reject(cartsArr, (p: any) => p.uuid === item.uuid);
                this.save(newCart);
            }
        });
    }

    private save(cart: any): void {
        this.storage.set('app.cart', cart);
    }

    private retrieve() {
        return this.storage.get('app.cart').then((val) => {
            this.cartsArr = val;
            return val;
        });
    }
}
