import { Injectable } from "@angular/core";
import * as SendBird from "SendBird";
import * as _ from "lodash";

import { environment } from "@app/env";

export class ChatMessage {
  messageId: string;
  userId: string;
  userName: string;
  userAvatar: string;
  toUserId: string;
  createdAt: number | string;
  updatedAt: number | string;
  message: string;
  channelUrl: string;
  status: string;
}

export class ProductInfo {
  id: string;
  name?: string;
  price?: number;
  image?: string;
  uuid?: string;
}

export class UserInfo {
  id: string;
  name?: string;
  avatar?: string;
  connectionStatus?: string;
}

@Injectable()
export class ChatService {
  public unreadCount: number = 0;
  public sendBird: any;
  public ChannelHandler: any;
  public MessageType: any = {
    MESSAGE_INFO: "MESSAGE_INFO",
    PRODUCT_INFO: "PRODUCT_INFO",
    ORDER_INFO: "ORDER_INFO",
    MAKE_OFFER_INFO: "MAKE_OFFER_INFO"
  };

  constructor() {
    this.sendBird = new SendBird({
      appId: environment.SendBird
    });
  }

  connect(): Promise<any> {
    return new Promise((resolve, reject) => {
      let _user: any = JSON.parse(localStorage.getItem("app.user")) || {};
      this.sendBird.connect(
        _user.uuid,
        _user.sendBirdToken,
        (user, error) => {
          if (error) {
            console.log("error: ", error);
            reject(error);
          }

          if (user) {
            this.ChannelHandler = new this.sendBird.ChannelHandler();
            resolve(user);
          }
        }
      );
    });
  }

  getUserInfo(): Promise<UserInfo> {
    let user = JSON.parse(localStorage.getItem("app.user")) || {};
    const userInfo: UserInfo = {
      id: user.uuid,
      name: user.username,
      avatar: user.img_path || "./assets/imgs/user.png"
    };
    return new Promise(resolve => resolve(userInfo));
  }

  getTotalUnreadMessageCount(): number {
    return this.sendBird.GroupChannel.getTotalUnreadMessageCount(
      (count: number, error: any) => {
        if (!error) {
          return count;
        } else {
          return 0;
        }
      }
    );
  }

  getMessageList(channel: any, toUser: any) {
    let messageListQuery = channel.createPreviousMessageListQuery();
    messageListQuery.load(30, true, (messageList, error) => {
      if (error) {
        console.error("messageListQuery: ", error);
        return;
      }
      _.orderBy(messageList, ["messageId"], ["asc"]);
      // vm.msgList = [];
      _.each(messageList, (message: any) => {
        let newMsg: ChatMessage = {
          messageId: message.messageId,
          userId: message._sender.userId,
          userName: message._sender.nickname,
          userAvatar: "./assets/imgs/user.png",
          toUserId: toUser.id,
          createdAt: message.createdAt,
          updatedAt: message.updatedAt,
          message: message.message,
          channelUrl: message.channelUrl,
          status: "success"
        };
        // vm.msgList.unshift(newMsg);
      });
      console.log("markAsRead");
      channel.markAsRead();
    });
  }
}
