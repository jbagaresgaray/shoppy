import { Restangular } from 'ngx-restangular/dist/esm/src';
import { Injectable } from '@angular/core';

@Injectable()
export class CategoriesServiceProvider {

    constructor(public restangular: Restangular) { }

    getAllCategories() {
        return new Promise((resolve, reject) => {
            let callbackResponse = (resp: any) => {
                resolve(resp.data);
            };

            let errorResponse = (error) => {
                console.log('error: ', error);
                reject(error);
            };
            this.restangular.all('category').customGET().subscribe(callbackResponse, errorResponse);
        });
    }

    getAllCategoryProducts(categoryId) {
        return new Promise((resolve, reject) => {
            let callbackResponse = (resp: any) => {
                resolve(resp.data);
            };

            let errorResponse = (error) => {
                console.log('error: ', error);
                reject(error);
            };
            this.restangular.all('category/' + categoryId + '/products').customGET().subscribe(callbackResponse, errorResponse);
        });
    }

    getAllSubCategories(categoryId) {
        return new Promise((resolve, reject) => {
            let callbackResponse = (resp: any) => {
                resolve(resp.data);
            };

            let errorResponse = (error) => {
                console.log('error: ', error);
                reject(error);
            };
            this.restangular.all('category/' + categoryId + '/subcategory').customGET().subscribe(callbackResponse, errorResponse);
        });
    }

    getAllSubCategoryProducts(categoryId, subId) {
        return new Promise((resolve, reject) => {
            let callbackResponse = (resp: any) => {
                resolve(resp.data);
            };

            let errorResponse = (error) => {
                console.log('error: ', error);
                reject(error);
            };
            this.restangular.all('category/' + categoryId + '/subcategory/' + subId + '/products').customGET().subscribe(callbackResponse, errorResponse);
        });
    }
}
