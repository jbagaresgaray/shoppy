import { Restangular } from 'ngx-restangular/dist/esm/src';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ServiceProvider {

  constructor(public restangular: Restangular, public http: HttpClient) { }

  defaultCurrency(){
    return {
        code: "QAR",
        decimal_digits: 2,
        id: "QAR",
        name: "Qatari Rial",
        name_plural: "Qatari rials",
        rounding: 0,
        symbol: "QR",
        symbol_native: "ر.ق.‏"
    };
  }

  getAllCountries() {
    return new Promise((resolve, reject) => {
      let callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      let errorResponse = (error) => {
        console.log('error: ', error);
        reject(error);
      };
      this.restangular.all('country').customGET().subscribe(callbackResponse, errorResponse);
    });
  }

  getRestCountries() {
    return new Promise((resolve, reject) => {
      let callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      let errorResponse = (error) => {
        console.log('error: ', error);
        reject(error);
      };
      this.restangular.withConfig((RestangularConfigurer) => {
        RestangularConfigurer.setBaseUrl('https://restcountries.eu/rest/v2/');
      }).all('all').customGET().subscribe(callbackResponse, errorResponse);
    });
  }

  getCurrencies() {
    return new Promise((resolve, reject) => {
      let callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      let errorResponse = (error) => {
        console.log('error: ', error);
        reject(error);
      };
      this.restangular.all('currency').customGET().subscribe(callbackResponse, errorResponse);
    });
  }

  getAllTimezones() {
    return new Promise((resolve, reject) => {
      let callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      let errorResponse = (error) => {
        console.log('error: ', error);
        reject(error);
      };
      this.restangular.all('timezone').customGET().subscribe(callbackResponse, errorResponse);
    });
  }

  getHomeBanners() {
    return new Promise((resolve, reject) => {
      let callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      let errorResponse = (error) => {
        console.log('error: ', error);
        reject(error);
      };
      this.restangular.all('mobile/banner').customGET().subscribe(callbackResponse, errorResponse);
    });
  }

  getAllShippingCouriers() {
    return new Promise((resolve, reject) => {
      let callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      let errorResponse = (error) => {
        console.log('error: ', error);
        reject(error);
      };
      this.restangular.all('shipping').customGET().subscribe(callbackResponse, errorResponse);
    });
  }

  getPrivacyPolicy() {
    return new Promise((resolve, reject) => {
      let callbackResponse = (resp: any) => {
        resolve(resp);
      };

      let errorResponse = (error) => {
        console.log('error: ', error);
        reject(error.error);
      };
      this.http.get('./assets/data/privacy.html',{ responseType: 'text' }).subscribe(callbackResponse, errorResponse);
    });
  }

}
