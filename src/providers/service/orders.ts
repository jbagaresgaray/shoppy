import { Restangular } from 'ngx-restangular/dist/esm/src';
import { Injectable } from '@angular/core';

@Injectable()
export class OrdersServiceProvider {

    constructor(public restangular: Restangular) { }

    createOrder(data) {
        return new Promise((resolve, reject) => {
            let callbackResponse = (resp: any) => {
                resolve(resp.data);
            };

            let errorResponse = (error) => {
                console.log('error: ', error);
                reject(error);
            };
            this.restangular.withConfig((RestangularConfigurer) => {
                RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
            }).all('orders').customPOST(data).subscribe(callbackResponse, errorResponse);
        });
    }


    getOrderDetails(orderId, type) {
        return new Promise((resolve, reject) => {
            let callbackResponse = (resp: any) => {
                resolve(resp.data);
            };

            let errorResponse = (error) => {
                console.log('error: ', error);
                reject(error);
            };
            this.restangular.withConfig((RestangularConfigurer) => {
                RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
            }).all('orders').customGET(orderId, {
                type: type
            }).subscribe(callbackResponse, errorResponse);
        });
    }

    getOrderDetailsByBatchNum(batchId) {
        return new Promise((resolve, reject) => {
            let callbackResponse = (resp: any) => {
                resolve(resp.data);
            };

            let errorResponse = (error) => {
                console.log('error: ', error);
                reject(error);
            };
            this.restangular.withConfig((RestangularConfigurer) => {
                RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
            }).all('orders/' + batchId + '/batch').customGET().subscribe(callbackResponse, errorResponse);
        });
    }

    getOrderList(action:any,type?:any) {
        return new Promise((resolve, reject) => {
            let callbackResponse = (resp: any) => {
                resolve(resp.data);
            };

            let errorResponse = (error) => {
                console.log('error: ', error);
                reject(error);
            };
            this.restangular.withConfig((RestangularConfigurer) => {
                RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
            }).all('orders').customGET("", {
                action: action,
                type: type
            }).subscribe(callbackResponse, errorResponse);
        });
    }

    acceptOrder(orderId,data){
        return new Promise((resolve, reject) => {
            let callbackResponse = (resp: any) => {
                resolve(resp.data);
            };

            let errorResponse = (error) => {
                console.log('error: ', error);
                reject(error);
            };
            this.restangular.withConfig((RestangularConfigurer) => {
                RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
            }).all('orders/' + orderId + '/accept').customPOST(data).subscribe(callbackResponse, errorResponse);
        });
    }

    payOrder(orderId, data) {
        return new Promise((resolve, reject) => {
            let callbackResponse = (resp: any) => {
                resolve(resp.data);
            };

            let errorResponse = (error) => {
                console.log('error: ', error);
                reject(error);
            };
            this.restangular.withConfig((RestangularConfigurer) => {
                RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
            }).all('orders/' + orderId + '/pay').customPOST(data).subscribe(callbackResponse, errorResponse);
        });
    }

    shipOrder(orderId, data) {
        return new Promise((resolve, reject) => {
            let callbackResponse = (resp: any) => {
                resolve(resp.data);
            };

            let errorResponse = (error) => {
                console.log('error: ', error);
                reject(error);
            };
            this.restangular.withConfig((RestangularConfigurer) => {
                RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
            }).all('orders/' + orderId + '/ship').customPOST(data).subscribe(callbackResponse, errorResponse);
        });
    }

    receivedOrder(orderId, data) {
        return new Promise((resolve, reject) => {
            let callbackResponse = (resp: any) => {
                resolve(resp.data);
            };

            let errorResponse = (error) => {
                console.log('error: ', error);
                reject(error);
            };
            this.restangular.withConfig((RestangularConfigurer) => {
                RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
            }).all('orders/' + orderId + '/received').customPOST(data).subscribe(callbackResponse, errorResponse);
        });
    }

    cancelOrderRequest(orderId,data){
        return new Promise((resolve, reject) => {
            let callbackResponse = (resp: any) => {
                resolve(resp.data);
            };

            let errorResponse = (error) => {
                console.log('error: ', error);
                reject(error);
            };
            this.restangular.withConfig((RestangularConfigurer) => {
                RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
            }).all('orders/' + orderId + '/cancel').customPOST(data).subscribe(callbackResponse, errorResponse);
        });
    }

    cancelOrder(orderId, data) {
        return new Promise((resolve, reject) => {
            let callbackResponse = (resp: any) => {
                resolve(resp.data);
            };

            let errorResponse = (error) => {
                console.log('error: ', error);
                reject(error);
            };
            this.restangular.withConfig((RestangularConfigurer) => {
                RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
            }).all('orders/' + orderId + '/cancel').customPUT(data).subscribe(callbackResponse, errorResponse);
        });
    }

    withdrawCancelOrder(orderId,data){
        return new Promise((resolve, reject) => {
            let callbackResponse = (resp: any) => {
                resolve(resp.data);
            };

            let errorResponse = (error) => {
                console.log('error: ', error);
                reject(error);
            };
            this.restangular.withConfig((RestangularConfigurer) => {
                RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
            }).all('orders/' + orderId + '/cancel/withdraw').customPOST(data).subscribe(callbackResponse, errorResponse);
        });
    }


    rejectCancelOrder(orderId,data){
        return new Promise((resolve, reject) => {
            let callbackResponse = (resp: any) => {
                resolve(resp.data);
            };

            let errorResponse = (error) => {
                console.log('error: ', error);
                reject(error);
            };
            this.restangular.withConfig((RestangularConfigurer) => {
                RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
            }).all('orders/' + orderId + '/cancel/reject').customPOST(data).subscribe(callbackResponse, errorResponse);
        });
    }

    returnOrderRequest(orderId,data){
        return new Promise((resolve, reject) => {
            let callbackResponse = (resp: any) => {
                resolve(resp.data);
            };

            let errorResponse = (error) => {
                console.log('error: ', error);
                reject(error);
            };
            this.restangular.withConfig((RestangularConfigurer) => {
                RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
            }).all('orders/' + orderId + '/return').customPOST(data).subscribe(callbackResponse, errorResponse);
        });
    }

    acceptReturnOrderRequest(orderId,data){
        return new Promise((resolve, reject) => {
            let callbackResponse = (resp: any) => {
                resolve(resp.data);
            };

            let errorResponse = (error) => {
                console.log('error: ', error);
                reject(error);
            };
            this.restangular.withConfig((RestangularConfigurer) => {
                RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
            }).all('orders/' + orderId + '/return_accept').customPOST(data).subscribe(callbackResponse, errorResponse);
        });
    }

    cancelReturnOrderRequest(orderId,data){
        return new Promise((resolve, reject) => {
            let callbackResponse = (resp: any) => {
                resolve(resp.data);
            };

            let errorResponse = (error) => {
                console.log('error: ', error);
                reject(error);
            };
            this.restangular.withConfig((RestangularConfigurer) => {
                RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
            }).all('orders/' + orderId + '/return_cancel').customPOST(data).subscribe(callbackResponse, errorResponse);
        });
    }

    returnOrderRequestShipment(orderId,data){
        return new Promise((resolve, reject) => {
            let callbackResponse = (resp: any) => {
                resolve(resp.data);
            };

            let errorResponse = (error) => {
                console.log('error: ', error);
                reject(error);
            };
            this.restangular.withConfig((RestangularConfigurer) => {
                RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
            }).all('orders/' + orderId + '/return_shipment').customPOST(data).subscribe(callbackResponse, errorResponse);
        });
    }

    returnOrder(orderId, data) {
        return new Promise((resolve, reject) => {
            let callbackResponse = (resp: any) => {
                resolve(resp.data);
            };

            let errorResponse = (error) => {
                console.log('error: ', error);
                reject(error);
            };
            this.restangular.withConfig((RestangularConfigurer) => {
                RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
            }).all('orders/' + orderId + '/return_approved').customPOST(data).subscribe(callbackResponse, errorResponse);
        });
    }

    arrangeShipment(orderId,data){
        return new Promise((resolve, reject) => {
            let callbackResponse = (resp: any) => {
                resolve(resp.data);
            };

            let errorResponse = (error) => {
                console.log('error: ', error);
                reject(error);
            };
            this.restangular.withConfig((RestangularConfigurer) => {
                RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
            }).all('orders/' + orderId + '/arrangepickup').customPOST(data).subscribe(callbackResponse, errorResponse);
        });   
    }

    isOrderArrangeShipment(orderId,slug){
        return new Promise((resolve, reject) => {
            let callbackResponse = (resp: any) => {
                resolve(resp.data);
            };

            let errorResponse = (error) => {
                console.log('error: ', error);
                reject(error);
            };
            this.restangular.withConfig((RestangularConfigurer) => {
                RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
            }).all('orders/' + orderId + '/arrangepickup/' + slug).customGET().subscribe(callbackResponse, errorResponse);
        });   
    }


    getOrderTrackings(orderId,slug){
        return new Promise((resolve, reject) => {
            let callbackResponse = (resp: any) => {
                resolve(resp.data);
            };

            let errorResponse = (error) => {
                console.log('error: ', error);
                reject(error);
            };
            this.restangular.withConfig((RestangularConfigurer) => {
                RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
            }).all('orders/' + orderId + '/trackings/' + slug).customGET().subscribe(callbackResponse, errorResponse);
        });   
    }

    getOrderTrackingLastCheckPoint(orderId,slug){
        return new Promise((resolve, reject) => {
            let callbackResponse = (resp: any) => {
                resolve(resp.data);
            };

            let errorResponse = (error) => {
                console.log('error: ', error);
                reject(error);
            };
            this.restangular.withConfig((RestangularConfigurer) => {
                RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
            }).all('orders/' + orderId + '/last_checkpoint/' + slug).customGET().subscribe(callbackResponse, errorResponse);
        });   
    }
}
