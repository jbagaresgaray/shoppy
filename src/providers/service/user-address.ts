import { Restangular } from 'ngx-restangular/dist/esm/src';
import { Injectable } from '@angular/core';

@Injectable()
export class UserAddressessServiceProvider {

    constructor(public restangular: Restangular) { }

    getAllUserAddress() {
        return new Promise((resolve, reject) => {
            let callbackResponse = (resp: any) => {
                resolve(resp.data);
            };

            let errorResponse = (error) => {
                console.log('error: ', error);
                reject(error);
            };
            this.restangular.withConfig((RestangularConfigurer) => {
                RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
            }).all('users/addresses').customGET().subscribe(callbackResponse, errorResponse);
        });
    }

    createUserAddress(data) {
        return new Promise((resolve, reject) => {
            let callbackResponse = (resp: any) => {
                resolve(resp.data);
            };

            let errorResponse = (error) => {
                console.log('error: ', error);
                reject(error);
            };
            this.restangular.withConfig((RestangularConfigurer) => {
                RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
            }).all('users/addresses').customPOST(data).subscribe(callbackResponse, errorResponse);
        });
    }

    getUserAddress(Id) {
        return new Promise((resolve, reject) => {
            let callbackResponse = (resp: any) => {
                resolve(resp.data);
            };

            let errorResponse = (error) => {
                console.log('error: ', error);
                reject(error);
            };
            this.restangular.withConfig((RestangularConfigurer) => {
                RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
            }).all('users/addresses').customGET(Id).subscribe(callbackResponse, errorResponse);
        });
    }

    updateUserAddress(Id, data) {
        return new Promise((resolve, reject) => {
            let callbackResponse = (resp: any) => {
                resolve(resp.data);
            };

            let errorResponse = (error) => {
                console.log('error: ', error);
                reject(error);
            };
            this.restangular.withConfig((RestangularConfigurer) => {
                RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
            }).all('users/addresses/' + Id).customPUT(data).subscribe(callbackResponse, errorResponse);
        });
    }

    deleteUserAddress(Id){
        return new Promise((resolve, reject) => {
            let callbackResponse = (resp: any) => {
                resolve(resp.data);
            };

            let errorResponse = (error) => {
                console.log('error: ', error);
                reject(error);
            };
            this.restangular.withConfig((RestangularConfigurer) => {
                RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
            }).all('users/addresses/' + Id).customDELETE().subscribe(callbackResponse, errorResponse);
        });
    }
}
