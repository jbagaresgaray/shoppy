import { Restangular } from 'ngx-restangular/dist/esm/src';
import { Injectable } from '@angular/core';

@Injectable()
export class UserBanksServiceProvider {

    constructor(public restangular: Restangular) { }

    getAllUserBanks() {
        return new Promise((resolve, reject) => {
            let callbackResponse = (resp: any) => {
                resolve(resp.data);
            };

            let errorResponse = (error) => {
                console.log('error: ', error);
                reject(error);
            };
            this.restangular.withConfig((RestangularConfigurer) => {
                RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
            }).all('users/bank').customGET().subscribe(callbackResponse, errorResponse);
        });
    }

    saveUserBank(data) {
        return new Promise((resolve, reject) => {
            let callbackResponse = (resp: any) => {
                resolve(resp.data);
            };

            let errorResponse = (error) => {
                console.log('error: ', error);
                reject(error);
            };
            this.restangular.withConfig((RestangularConfigurer) => {
                RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
            }).all('users/bank').customPOST(data).subscribe(callbackResponse, errorResponse);
        });
    }

    getUserBank(Id) {
        return new Promise((resolve, reject) => {
            let callbackResponse = (resp: any) => {
                resolve(resp.data);
            };

            let errorResponse = (error) => {
                console.log('error: ', error);
                reject(error);
            };
            this.restangular.withConfig((RestangularConfigurer) => {
                RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
            }).all('users/bank').customGET(Id).subscribe(callbackResponse, errorResponse);
        });
    }

    deleteUserBank(Id){
        return new Promise((resolve, reject) => {
            let callbackResponse = (resp: any) => {
                resolve(resp.data);
            };

            let errorResponse = (error) => {
                console.log('error: ', error);
                reject(error);
            };
            this.restangular.withConfig((RestangularConfigurer) => {
                RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
            }).all('users/bank/' + Id).customDELETE().subscribe(callbackResponse, errorResponse);
        });
    }
}
