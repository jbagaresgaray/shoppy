import { Restangular } from 'ngx-restangular/dist/esm/src';
import { Injectable } from '@angular/core';

@Injectable()
export class BanksServiceProvider {

    constructor(public restangular: Restangular) {}

    getAllBanks() {
        return new Promise((resolve, reject) => {
            let callbackResponse = (resp: any) => {
                resolve(resp.data);
            };

            let errorResponse = (error) => {
                console.log('error: ', error);
                reject(error);
            };
            this.restangular.all('banks').customGET().subscribe(callbackResponse, errorResponse);
        });
    }
}
