import { Restangular } from "ngx-restangular/dist/esm/src";
import { Injectable } from "@angular/core";

@Injectable()
export class ProductsServiceProvider {
  constructor(public restangular: Restangular) {}

  getAllProducts(params?: any) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .all("products")
        .customGET("", params)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  getProductInfo(productId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .all("products")
        .customGET(productId)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  getProductOnSameShop(productId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .all("products/" + productId + "/more")
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  saveProduct(data) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("products")
        .customPOST(data)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  saveProductImage(productId, data) {
    var formData = new FormData();
    formData.append("image", data.image);
    return this.restangular
      .withConfig(config => {
        config.setDefaultHeaders({
          Authorization: "Bearer " + localStorage.getItem("app.token")
        });
      })
      .all("products/" + productId + "/image")
      .customPUT(formData, "", undefined, {
        "Content-Type": undefined
      })
      .then(
        function(res) {
          return res;
        },
        function(err) {
          return err.data;
        }
      );
  }

  saveProductVariants(productId, data) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("products/" + productId + "/variation")
        .customPOST(data)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  deleteAllProductVariations(productId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("products/" + productId + "/variation")
        .customDELETE()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  deleteProductVariations(productId, variantId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("products/" + productId + "/variation/" + variantId)
        .customDELETE()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  saveProductWholesale(productId, data) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("products/" + productId + "/wholesale")
        .customPOST(data)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  deleteAllProductWholeSale(productId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("products/" + productId + "/wholesale")
        .customDELETE()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  deleteProductWholeSale(productId, wholesaleId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("products/" + productId + "/wholesale/" + wholesaleId)
        .customDELETE()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  saveProductShipping(productId, data) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("products/" + productId + "/shipping")
        .customPOST(data)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  getProductShipping(productId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("products/" + productId + "/shipping")
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  deleteProductShipping(productId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("products/" + productId + "/shipping")
        .customDELETE()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  mailCreatedProduct(productId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("products/" + productId + "/mail")
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  likeProduct(productId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("products/" + productId + "/like")
        .customPOST()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  unlikeProduct(productId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("products/" + productId + "/like")
        .customDELETE()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  deleteProductImage(productId, imageId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("products/" + productId + "/image/" + imageId)
        .customDELETE()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  updateProduct(productId, data) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("products/" + productId)
        .customPUT(data)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  deleteProduct(productId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("products")
        .customDELETE(productId)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  getAllProductRatings(productId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("products/" + productId + "/rating")
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  rateProduct(productId, data) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("products/" + productId + "/rating")
        .customPOST(data)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  updateProductRating(productId, data) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("products/" + productId + "/rating")
        .customPUT(data)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  getAllProductVouchers(productId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("products/" + productId + "/vouchers")
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  getProductDiscount(productId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(config => {
          config.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("products/" + productId + "/discounts")
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }
}
