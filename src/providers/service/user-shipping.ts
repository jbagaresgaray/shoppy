import { Restangular } from 'ngx-restangular/dist/esm/src';
import { Injectable } from '@angular/core';

@Injectable()
export class UserShippingServiceProvider {

    constructor(public restangular: Restangular) { }

    getAllUserSellerShipping() {
        return new Promise((resolve, reject) => {
            let callbackResponse = (resp: any) => {
                resolve(resp.data);
            };

            let errorResponse = (error) => {
                console.log('error: ', error);
                reject(error);
            };
            this.restangular.withConfig((RestangularConfigurer) => {
                RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
            }).all('users/shipping').customGET().subscribe(callbackResponse, errorResponse);
        });
    }

    createUserSellerShipping(data) {
        return new Promise((resolve, reject) => {
            let callbackResponse = (resp: any) => {
                resolve(resp.data);
            };

            let errorResponse = (error) => {
                console.log('error: ', error);
                reject(error);
            };
            this.restangular.withConfig((RestangularConfigurer) => {
                RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
            }).all('users/shipping').customPOST(data).subscribe(callbackResponse, errorResponse);
        });
    }

    deleteUserSellerShipping(Id){
        return new Promise((resolve, reject) => {
            let callbackResponse = (resp: any) => {
                resolve(resp.data);
            };

            let errorResponse = (error) => {
                console.log('error: ', error);
                reject(error);
            };
            this.restangular.withConfig((RestangularConfigurer) => {
                RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
            }).all('users/shipping/' + Id).customDELETE().subscribe(callbackResponse, errorResponse);
        });
    }
}
