import { Restangular } from 'ngx-restangular/dist/esm/src';
import { Injectable } from '@angular/core';

@Injectable()
export class BrandsServiceProvider {

    constructor(public restangular: Restangular) { }

    getAllBrands() {
        return new Promise((resolve, reject) => {
            let callbackResponse = (resp: any) => {
                resolve(resp.data);
            };

            let errorResponse = (error) => {
                console.log('error: ', error);
                reject(error);
            };
            this.restangular.all('brand').customGET().subscribe(callbackResponse, errorResponse);
        });
    }

    getAllBrandProducts(brandId) {
        return new Promise((resolve, reject) => {
            let callbackResponse = (resp: any) => {
                resolve(resp.data);
            };

            let errorResponse = (error) => {
                console.log('error: ', error);
                reject(error);
            };
            this.restangular.all('brand/' + brandId + '/products').customGET().subscribe(callbackResponse, errorResponse);
        });
    }

    createBrand(data) {
        return new Promise((resolve, reject) => {
            let callbackResponse = (resp: any) => {
                resolve(resp.data);
            };

            let errorResponse = (error) => {
                console.log('error: ', error);
                reject(error);
            };
            this.restangular.withConfig((config) => {
                config.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
            }).all('brand').customPOST(data).subscribe(callbackResponse, errorResponse);
        });
    }
}
