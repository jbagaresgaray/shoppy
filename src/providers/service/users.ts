import { Restangular } from 'ngx-restangular/dist/esm/src';
import { Injectable } from '@angular/core';

@Injectable()
export class UsersServiceProvider {

  constructor(public restangular: Restangular) { }

  getShopProfile(Id) {
    return new Promise((resolve, reject) => {
      let callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      let errorResponse = (error) => {
        console.log('error: ', error);
        reject(error);
      };
      this.restangular.all('shop').customGET(Id).subscribe(callbackResponse, errorResponse);
    });
  }

  getCurrentUser() {
    return new Promise((resolve, reject) => {
      let callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      let errorResponse = (error) => {
        console.log('error: ', error);
        reject(error);
      };
      this.restangular.withConfig((RestangularConfigurer) => {
        RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
      }).all('users').customGET().subscribe(callbackResponse, errorResponse);
    });
  }

  signUpUser(data) {
    return new Promise((resolve, reject) => {
      let callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      let errorResponse = (error) => {
        console.log('error: ', error);
        reject(error);
      };
      this.restangular.all('register').customPOST(data).subscribe(callbackResponse, errorResponse);
    });
  }

  socialSignUp(data) {
    return new Promise((resolve, reject) => {
      let callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      let errorResponse = (error) => {
        console.log('error: ', error);
        reject(error);
      };
      this.restangular.all('social/signup').customPOST(data).subscribe(callbackResponse, errorResponse);
    });
  }

  applyAsSeller() {
    return new Promise((resolve, reject) => {
      let callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      let errorResponse = (error) => {
        console.log('error: ', error);
        reject(error);
      };
      this.restangular.withConfig((RestangularConfigurer) => {
        RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
      }).all('users/apply').customPOST().subscribe(callbackResponse, errorResponse);
    });
  }

  getCurrentUserLike() {
    return new Promise((resolve, reject) => {
      let callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      let errorResponse = (error) => {
        console.log('error: ', error);
        reject(error);
      };
      this.restangular.withConfig((RestangularConfigurer) => {
        RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
      }).all('users/likes').customGET().subscribe(callbackResponse, errorResponse);
    });
  }

  updateUserProfile(data) {
    return new Promise((resolve, reject) => {
      let callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      let errorResponse = (error) => {
        console.log('error: ', error);
        reject(error);
      };
      this.restangular.withConfig((RestangularConfigurer) => {
        RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
      }).all('users/profile').customPUT(data).subscribe(callbackResponse, errorResponse);
    });
  }

  updateUserEmail(data) {
    return new Promise((resolve, reject) => {
      let callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      let errorResponse = (error) => {
        console.log('error: ', error);
        reject(error);
      };
      this.restangular.withConfig((RestangularConfigurer) => {
        RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
      }).all('users/email').customPUT(data).subscribe(callbackResponse, errorResponse);
    });
  }

  updateUserPassword(data) {
    return new Promise((resolve, reject) => {
      let callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      let errorResponse = (error) => {
        console.log('error: ', error);
        reject(error);
      };
      this.restangular.withConfig((RestangularConfigurer) => {
        RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
      }).all('users/password').customPUT(data).subscribe(callbackResponse, errorResponse);
    });
  }

  updateUserCurrency(data) {
    return new Promise((resolve, reject) => {
      let callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      let errorResponse = (error) => {
        console.log('error: ', error);
        reject(error);
      };
      this.restangular.withConfig((RestangularConfigurer) => {
        RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
      }).all('users/currency').customPUT(data).subscribe(callbackResponse, errorResponse);
    });
  }

  updateUserCountry(data) {
    return new Promise((resolve, reject) => {
      let callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      let errorResponse = (error) => {
        console.log('error: ', error);
        reject(error);
      };
      this.restangular.withConfig((RestangularConfigurer) => {
        RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
      }).all('users/country').customPUT(data).subscribe(callbackResponse, errorResponse);
    });
  }

  validatePassword(data) {
    return new Promise((resolve, reject) => {
      let callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      let errorResponse = (error) => {
        console.log('error: ', error);
        reject(error);
      };
      this.restangular.withConfig((RestangularConfigurer) => {
        RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
      }).all('users/password').customPOST(data).subscribe(callbackResponse, errorResponse);
    });
  }

  linkFacebookAccount(data) {
    return new Promise((resolve, reject) => {
      let callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      let errorResponse = (error) => {
        console.log('error: ', error);
        reject(error);
      };
      this.restangular.withConfig((RestangularConfigurer) => {
        RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
      }).all('users/social/fb').customPOST(data).subscribe(callbackResponse, errorResponse);
    });
  }

  unlinkFacebookAccount() {
    return new Promise((resolve, reject) => {
      let callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      let errorResponse = (error) => {
        console.log('error: ', error);
        reject(error);
      };
      this.restangular.withConfig((RestangularConfigurer) => {
        RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
      }).all('users/social/fb').customDELETE().subscribe(callbackResponse, errorResponse);
    });
  }

  linkGoogleAccount(data) {
    return new Promise((resolve, reject) => {
      let callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      let errorResponse = (error) => {
        console.log('error: ', error);
        reject(error);
      };
      this.restangular.withConfig((RestangularConfigurer) => {
        RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
      }).all('users/social/google').customPOST(data).subscribe(callbackResponse, errorResponse);
    });
  }

  unlinkGoogleAccount() {
    return new Promise((resolve, reject) => {
      let callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      let errorResponse = (error) => {
        console.log('error: ', error);
        reject(error);
      };
      this.restangular.withConfig((RestangularConfigurer) => {
        RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
      }).all('users/social/google').customDELETE().subscribe(callbackResponse, errorResponse);
    });
  }


  getCurrentUserFollowing() {
    return new Promise((resolve, reject) => {
      let callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      let errorResponse = (error) => {
        console.log('error: ', error);
        reject(error);
      };
      this.restangular.withConfig((RestangularConfigurer) => {
        RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
      }).all('users/following').customGET().subscribe(callbackResponse, errorResponse);
    });
  }

  getCurrentUserFollowers() {
    return new Promise((resolve, reject) => {
      let callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      let errorResponse = (error) => {
        console.log('error: ', error);
        reject(error);
      };
      this.restangular.withConfig((RestangularConfigurer) => {
        RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
      }).all('users/followers').customGET().subscribe(callbackResponse, errorResponse);
    });
  }

  followSeller(data) {
    return new Promise((resolve, reject) => {
      let callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      let errorResponse = (error) => {
        console.log('error: ', error);
        reject(error);
      };
      this.restangular.withConfig((RestangularConfigurer) => {
        RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
      }).all('users/follow').customPOST(data).subscribe(callbackResponse, errorResponse);
    });
  }

  unfollowSeller(data) {
    return new Promise((resolve, reject) => {
      let callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      let errorResponse = (error) => {
        console.log('error: ', error);
        reject(error);
      };
      this.restangular.withConfig((RestangularConfigurer) => {
        RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
      }).all('users/follow').customOperation('remove', '', {}, { 'Content-Type': 'application/json' }, data).subscribe(callbackResponse, errorResponse);
    });
  }

}
