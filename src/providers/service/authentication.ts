import { Restangular } from 'ngx-restangular/dist/esm/src';
import { Injectable } from '@angular/core';

@Injectable()
export class AuthProvider {

  constructor(public restangular: Restangular) {
  }

  authenticate(data: any) {
    return new Promise((resolve, reject) => {
      let callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      let errorResponse = (error) => {
        console.log('error: ', error);
        reject(error);
      };

      var authdata = window.btoa(data.username + ':' + data.password);
      this.restangular.withConfig((RestangularConfigurer) => {
        RestangularConfigurer.setDefaultHeaders({ 'Authorization': 'Basic ' + authdata });
      }).all('login').customPOST(data).subscribe(callbackResponse, errorResponse);
    });
  }

  forgotPassword(data: any) {
    return new Promise((resolve, reject) => {
      let callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      let errorResponse = (error) => {
        console.log('error: ', error);
        reject(error);
      };
      this.restangular.all('forgot').customPOST(data).subscribe(callbackResponse, errorResponse);
    });
  }

  socialAuth(data: any) {
    return new Promise((resolve, reject) => {
      let callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      let errorResponse = (error) => {
        console.log('error: ', error);
        reject(error);
      };
      this.restangular.all('social').customPOST(data).subscribe(callbackResponse, errorResponse);
    });
  }

  logoutApp(data: any) {
    return new Promise((resolve, reject) => {
      let callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      let errorResponse = (error) => {
        console.log('error: ', error);
        reject(error);
      };
      this.restangular.withConfig((config) => {
        config.setDefaultHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('app.token') });
      }).all('logout').customPOST(data).subscribe(callbackResponse, errorResponse);
    });
  }

}
