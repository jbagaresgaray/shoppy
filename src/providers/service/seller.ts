import { Restangular } from "ngx-restangular/dist/esm/src";
import { Injectable } from "@angular/core";

@Injectable()
export class SellerServiceProvider {
  constructor(public restangular: Restangular) {}

  getAllSellers(params?: any) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .all("shop")
        .customGET("", params)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  updateStoreInformation(data) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("shop")
        .customPOST(data)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  getSellerCustomers() {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("shop/customer")
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  getSellerIncome() {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("shop/income")
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  getSellerInfo(userId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("shop/" + userId + "/info")
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  getSellerFollowers(userId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("shop/" + userId + "/followers")
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  getSellerFollowing(userId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("shop/" + userId + "/following")
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  getSellerProducts() {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("shop/products")
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  getShopProducts(userId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("shop/" + userId + "/products")
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  applyForOfficialShop(userId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("shop/" + userId + "/apply")
        .customPOST()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  getAllOfficialShop(params?: any) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("officialshop")
        .customGET("", params)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  getOfficialShopApplicationStatus(userId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("shop/" + userId + "/apply")
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  getAllUserSellerShipping(userId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("shop/" + userId + "/shipping")
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  getShopRatings(shopId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("shop/" + shopId + "/rating")
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  getShopRatingSummary(shopId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("shop/" + shopId + "/rating_summary")
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  getSellerActiveTopPickCollection(sellerId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("shop/" + sellerId + "/toppicks/active")
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  createSellerTopPickCollection(sellerId, data) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("shop/" + sellerId + "/toppicks")
        .customPOST(data)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  getSellerAllTopPickCollection(sellerId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("shop/" + sellerId + "/toppicks")
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  getSellerTopPickCollection(sellerId, collectionId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("shop/" + sellerId + "/toppicks/" + collectionId)
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  getSellerTopPickCollectionProducts(sellerId, collectionId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("shop/" + sellerId + "/toppicks/" + collectionId + "/products")
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  deleteSellerTopPickCollection(sellerId, collectionId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("shop/" + sellerId + "/toppicks/" + collectionId)
        .customDELETE()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  updateSellerTopPickCollection(sellerId, collectionId, data) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("shop/" + sellerId + "/toppicks/" + collectionId)
        .customPUT(data)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  activateTopPickCollection(sellerId, collectionId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("shop/" + sellerId + "/toppicks/" + collectionId + "/activate")
        .customPOST()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  deActivateTopPickCollection(sellerId, collectionId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("shop/" + sellerId + "/toppicks/" + collectionId + "/deactivate")
        .customPOST()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  createCollectionTopPickProduct(sellerId, collectionId, data) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("shop/" + sellerId + "/toppicks/" + collectionId + "/products")
        .customPOST(data)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  getCollectionTopPickProductDetail(sellerId, collectionId, productId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all(
          "shop/" +
            sellerId +
            "/toppicks/" +
            collectionId +
            "/products/" +
            productId
        )
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  deleteCollectionTopPickProductDetail(sellerId, collectionId, productId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all(
          "shop/" +
            sellerId +
            "/toppicks/" +
            collectionId +
            "/products/" +
            productId
        )
        .customDELETE()
        .subscribe(callbackResponse, errorResponse);
    });
  }
}
