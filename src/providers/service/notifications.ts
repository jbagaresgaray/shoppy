import { Restangular } from "ngx-restangular/dist/esm/src";
import { Injectable } from "@angular/core";

@Injectable()
export class NotificationsServiceProvider {
    constructor(public restangular: Restangular) {}

    getUpdatesNotifications() {
        return new Promise((resolve, reject) => {
            const callbackResponse = (resp: any) => {
                resolve(resp.data);
            };

            const errorResponse = error => {
                console.log("error: ", error);
                reject(error);
            };
            this.restangular
                .withConfig(RestangularConfigurer => {
                    RestangularConfigurer.setDefaultHeaders({
                        Authorization:
                            "Bearer " + localStorage.getItem("app.token")
                    });
                })
                .all("users/notification")
                .customGET()
                .subscribe(callbackResponse, errorResponse);
        });
    }

    getNewsletter(cmsId) {
        return new Promise((resolve, reject) => {
            const callbackResponse = (resp: any) => {
                resolve(resp.data);
            };

            const errorResponse = error => {
                console.log("error: ", error);
                reject(error);
            };
            this.restangular
                .withConfig(RestangularConfigurer => {
                    RestangularConfigurer.setDefaultHeaders({
                        Authorization:
                            "Bearer " + localStorage.getItem("app.token")
                    });
                })
                .all("newsletter")
                .customGET(cmsId)
                .subscribe(callbackResponse, errorResponse);
        });
    }

    updateNotification(notifId) {
        return new Promise((resolve, reject) => {
            const callbackResponse = (resp: any) => {
                resolve(resp.data);
            };

            const errorResponse = error => {
                console.log("error: ", error);
                reject(error);
            };
            this.restangular
                .withConfig(RestangularConfigurer => {
                    RestangularConfigurer.setDefaultHeaders({
                        Authorization:
                            "Bearer " + localStorage.getItem("app.token")
                    });
                })
                .all("users/notification/" + notifId)
                .customPUT()
                .subscribe(callbackResponse, errorResponse);
        });
    }

    deleteNotification(notifId) {
        return new Promise((resolve, reject) => {
            const callbackResponse = (resp: any) => {
                resolve(resp.data);
            };

            const errorResponse = error => {
                console.log("error: ", error);
                reject(error);
            };
            this.restangular
                .withConfig(RestangularConfigurer => {
                    RestangularConfigurer.setDefaultHeaders({
                        Authorization:
                            "Bearer " + localStorage.getItem("app.token")
                    });
                })
                .all("users/notification/" + notifId)
                .customDELETE()
                .subscribe(callbackResponse, errorResponse);
        });
    }
}
