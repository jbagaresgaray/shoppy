import { Restangular } from "ngx-restangular";
import { Injectable } from "@angular/core";

@Injectable()
export class SellerVouchersService {
  constructor(public restangular: Restangular) {}

  createSellerVoucher(uuid, data: any) {
    return new Promise((resolve, reject) => {
      let callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      let errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("shop/" + uuid + "/vouchers")
        .customPOST(data)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  getSellerVouchers(uuid) {
    return new Promise((resolve, reject) => {
      let callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      let errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("shop/" + uuid + "/vouchers")
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  getSellerVoucherDetail(uuid, voucherId) {
    return new Promise((resolve, reject) => {
      let callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      let errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("shop/" + uuid + "/vouchers/" + voucherId)
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  deleteSellerVoucherDetail(uuid, voucherId) {
    return new Promise((resolve, reject) => {
      let callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      let errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("shop/" + uuid + "/vouchers/" + voucherId)
        .customDELETE()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  updateSellerVoucherDetail(uuid, voucherId, data) {
    return new Promise((resolve, reject) => {
      let callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      let errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("shop/" + uuid + "/vouchers/" + voucherId)
        .customPUT(data)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  createSellerProductVoucher(uuid, voucherId, data) {
    return new Promise((resolve, reject) => {
      let callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      let errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("shop/" + uuid + "/vouchers/" + voucherId + "/products")
        .customPOST(data)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  deleteSellerProductVoucher(uuid, voucherId, productId) {
    return new Promise((resolve, reject) => {
      let callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      let errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all(
          "shop/" + uuid + "/vouchers/" + voucherId + "/products/" + productId
        )
        .customDELETE()
        .subscribe(callbackResponse, errorResponse);
    });
  }
}
