import { Restangular } from "ngx-restangular";
import { Injectable } from "@angular/core";

@Injectable()
export class DiscountPromotionsService {
  constructor(public restangular: Restangular) {}

  createDiscountPromotion(uuid, data: any) {
    return new Promise((resolve, reject) => {
      let callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      let errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("shop/" + uuid + "/discountpromotions")
        .customPOST(data)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  getAllDiscountPromotion(uuid) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("shop/" + uuid + "/discountpromotions")
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  deleteDiscountPromotion(uuid, discountId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("shop/" + uuid + "/discountpromotions/" + discountId)
        .customDELETE()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  createDiscountPromotionProducts(uuid, discountId, data: any) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("shop/" + uuid + "/discountpromotions/" + discountId + "/products")
        .customPOST(data)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  getDiscountPromotionProducts(uuid, discountId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all("shop/" + uuid + "/discountpromotions/" + discountId + "/products")
        .customGET()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  updateDiscountPromotionProduct(uuid, discountId, productId, data) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all(
          "shop/" +
            uuid +
            "/discountpromotions/" +
            discountId +
            "/products/" +
            productId
        )
        .customPUT(data)
        .subscribe(callbackResponse, errorResponse);
    });
  }

  deleteDiscountPromotionProduct(uuid, discountId, productId) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all(
          "shop/" +
            uuid +
            "/discountpromotions/" +
            discountId +
            "/products/" +
            productId
        )
        .customDELETE()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  deleteDiscountPromotionProductVariant(
    uuid,
    discountId,
    productId,
    variantId
  ) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all(
          "shop/" +
            uuid +
            "/discountpromotions/" +
            discountId +
            "/products/" +
            productId +
            "/variant/" +
            variantId
        )
        .customDELETE()
        .subscribe(callbackResponse, errorResponse);
    });
  }

  updateDiscountPromotionProductVariant(
    uuid,
    discountId,
    productId,
    variantId,
    data
  ) {
    return new Promise((resolve, reject) => {
      const callbackResponse = (resp: any) => {
        resolve(resp.data);
      };

      const errorResponse = error => {
        console.log("error: ", error);
        reject(error);
      };
      this.restangular
        .withConfig(RestangularConfigurer => {
          RestangularConfigurer.setDefaultHeaders({
            Authorization: "Bearer " + localStorage.getItem("app.token")
          });
        })
        .all(
          "shop/" +
            uuid +
            "/discountpromotions/" +
            discountId +
            "/products/" +
            productId +
            "/variant/" +
            variantId
        )
        .customPUT(data)
        .subscribe(callbackResponse, errorResponse);
    });
  }
}
