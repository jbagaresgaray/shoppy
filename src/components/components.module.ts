import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { BrowserModule } from "@angular/platform-browser";
import { IonicModule } from "ionic-angular";
import { Ionic2RatingModule } from "ionic2-rating";

import { PipesModule } from "../pipes/pipes.module";
import { NgPipesModule } from "ngx-pipes";

import { EmojiPickerComponent } from "./emoji-picker/emoji-picker";
import { MultiImageUploadComponent } from "./multi-image-upload/multi-image-upload";
import { ParallaxHeaderDirective } from "./parallax-header/parallax-header";
import { SkeletonItemComponent } from "./skeleton-item/skeleton-item";

import { ChatDetailMessageComponent } from "./chat/chat-detail-message/chat-detail-message";
import { ChatProductMessageComponent } from "./chat/chat-product-message/chat-product-message";
import { ChatFileMessageComponent } from "./chat/chat-file-message/chat-file-message";
import { ChatOrderMessageComponent } from "./chat/chat-order-message/chat-order-message";

import { FaIconComponent } from "./fa-icon/fa-icon";

import { ContentProductsComponent } from "./contents/content-products/content-products";
import { ContentNotificationComponent } from "./contents/content-notification/content-notification";
import { ContentSellerModeComponent } from "./contents/content-seller-mode/content-seller-mode";
import { ContentOrderTrackingComponent } from "./contents/content-order-tracking/content-order-tracking";
import { OfficialShopComponent } from "./contents/official-shop/official-shop";
import { CategoriesPage } from "../components/contents/content-categories/categories";
import { BrandsPage } from "../components/contents/content-brands/brands";
import { ContentDiscountPromotionsComponent } from "../components/contents/content-discount-promotions/content-discount-promotions";
import { ContentSellerToppicksComponent } from "../components/contents/content-seller-toppicks/content-seller-toppicks";
import { ContentSellerVouchersComponent } from "../components/contents/content-seller-vouchers/content-seller-vouchers";
import { ContentSellerVouchersDetailComponent } from "../components/contents/content-seller-vouchers-detail/content-seller-vouchers-detail";

import { ArrangeShipmentComponent } from "./form-entry/arrange-shipment/arrange-shipment";
import { PickupShipmentComponent } from "./form-entry/pickup-shipment/pickup-shipment";
import { CancelOrderEntryComponent } from "./form-entry/cancel-order-entry/cancel-order-entry";
import { ViewCanceOrderComponent } from "./form-entry/view-cance-order/view-cance-order";
import { ReturnOrderEntryComponent } from "./form-entry/return-order-entry/return-order-entry";
import { ReturnRefundDetailsComponent } from "./form-entry/return-refund-details/return-refund-details";
import { ReturnOrderShipmentComponent } from "./form-entry/return-order-shipment/return-order-shipment";
import { ReturnShipmentViewComponent } from "./form-entry/return-shipment-view/return-shipment-view";
import { ReturnShipmentShippingComponent } from "./form-entry/return-shipment-shipping/return-shipment-shipping";
import { ChatSettingsComponent } from "./form-entry/chat-settings/chat-settings";
import { NotificationSettingsComponent } from "./form-entry/notification-settings/notification-settings";
import { ShopSettingsComponent } from "./form-entry/shop-settings/shop-settings";
import { MyaddressEntryPage } from "./form-entry/myaddress-entry/myaddress-entry";
import { MybankcardsEntryPage } from "./form-entry/mybankcards-entry/mybankcards-entry";
import { DiscountPromotionsComponent } from "./form-entry/discount-promotions/discount-promotions";
import { SellerToppicksComponent } from "./form-entry/seller-toppicks/seller-toppicks";
import { SellerVouchersComponent } from "./form-entry/seller-vouchers/seller-vouchers";
import { ContentDiscountPromotionsProductsComponent } from "../components/form-entry/content-discount-promotions-products/content-discount-promotions-products";

import { ProductOfficialShopBadgeComponent } from "./products/product-official-shop-badge/product-official-shop-badge";
import { ProductFreeShippingComponent } from "./products/product-free-shipping/product-free-shipping";
import { ProductSoldOutComponent } from "./products/product-sold-out/product-sold-out";
import { ProductDiscountPromotionComponent } from "./products/product-discount-promotion/product-discount-promotion";
import { ProductPreferredComponent } from "./products/product-preferred/product-preferred";
import { ProductLowestGuaranteeComponent } from "./products/product-lowest-guarantee/product-lowest-guarantee";
import { ProductReviewPreferredComponent } from "./products/product-review-preferred/product-review-preferred";
import { ProductsPickerModalComponent } from "./products/products-picker-modal/products-picker-modal";
import { ProductVariantsModalPage } from "./products/product-variants-modal/variants.modal";
import { VouchersPage } from "./products/product-vouchers-modal/vouchers";
import { ProductShippingModalComponent } from "./products/product-shipping-modal/product-shipping-modal";
import { ProductWholesaleModalComponent } from "./products/product-wholesale-modal/product-wholesale-modal";
import { MakeOfferModalComponent } from "./products/make-offer-modal/make-offer-modal";

import { VoucherTicketComponent } from "./shops/voucher-ticket/voucher-ticket";
import { MyVouchersTicketComponent } from "./shops/my-vouchers-ticket/my-vouchers-ticket";

const componentArr: any[] = [
  ProductShippingModalComponent,
  ProductWholesaleModalComponent,
  EmojiPickerComponent,
  MultiImageUploadComponent,
  ParallaxHeaderDirective,
  SkeletonItemComponent,
  ChatDetailMessageComponent,
  ChatProductMessageComponent,
  ChatFileMessageComponent,
  ContentProductsComponent,
  ContentNotificationComponent,
  ContentSellerModeComponent,
  FaIconComponent,
  ContentOrderTrackingComponent,
  ArrangeShipmentComponent,
  PickupShipmentComponent,
  CancelOrderEntryComponent,
  ViewCanceOrderComponent,
  ReturnOrderEntryComponent,
  ReturnRefundDetailsComponent,
  ReturnOrderShipmentComponent,
  ReturnShipmentViewComponent,
  ReturnShipmentShippingComponent,
  OfficialShopComponent,
  ChatOrderMessageComponent,
  ChatSettingsComponent,
  NotificationSettingsComponent,
  ShopSettingsComponent,
  MyaddressEntryPage,
  MybankcardsEntryPage,
  CategoriesPage,
  BrandsPage,
  ContentSellerToppicksComponent,
  ContentDiscountPromotionsComponent,
  ContentSellerVouchersComponent,
  DiscountPromotionsComponent,
  SellerToppicksComponent,
  SellerVouchersComponent,
  ProductOfficialShopBadgeComponent,
  ProductFreeShippingComponent,
  ProductSoldOutComponent,
  ProductDiscountPromotionComponent,
  ProductPreferredComponent,
  ProductLowestGuaranteeComponent,
  ProductReviewPreferredComponent,
  VoucherTicketComponent,
  MyVouchersTicketComponent,
  ContentDiscountPromotionsProductsComponent,
  MakeOfferModalComponent,
  ProductsPickerModalComponent,
  ProductVariantsModalPage,
  VouchersPage,
  ContentSellerVouchersDetailComponent
];

const entryComponentsArr: any[] = [
  MakeOfferModalComponent,
  ProductsPickerModalComponent,
  ProductVariantsModalPage,
  VouchersPage
];

@NgModule({
  declarations: componentArr,
  imports: [
    CommonModule,
    BrowserModule,
    IonicModule,
    PipesModule,
    Ionic2RatingModule,
    NgPipesModule
  ],
  exports: componentArr,
  entryComponents: entryComponentsArr
})
export class ComponentsModule {}
