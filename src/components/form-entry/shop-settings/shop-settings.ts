import {
	Component,
	Input,
	Output,
	EventEmitter,
	ViewEncapsulation
} from "@angular/core";

/**
 * Generated class for the ShopSettingsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
	selector: "shop-settings",
	encapsulation: ViewEncapsulation.None,
	templateUrl: "shop-settings.html"
})
export class ShopSettingsComponent {
	@Input() user: any = {};

	@Output() _updateItem = new EventEmitter<any>();

	constructor() {}

	updateItem() {
		this._updateItem.emit();
	}
}
