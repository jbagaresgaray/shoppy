import { Component, Input, ViewEncapsulation } from "@angular/core";

/**
 * Generated class for the ReturnRefundDetailsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
	selector: "return-refund-details",
	encapsulation: ViewEncapsulation.None,
	templateUrl: "return-refund-details.html"
})
export class ReturnRefundDetailsComponent {
	@Input() order: any = {};
	@Input() usertype: string;

	constructor() {}
}
