import { Component, Input, ViewEncapsulation } from '@angular/core';

/**
 * Generated class for the ViewCanceOrderComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'view-cancel-order',
  encapsulation: ViewEncapsulation.None,
  templateUrl: 'view-cance-order.html'
})
export class ViewCanceOrderComponent {

  @Input() order:any = {};
  @Input() usertype: string;

  constructor() {
  }

}
