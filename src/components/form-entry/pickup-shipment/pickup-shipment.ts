import {
	Component,
	Input,
	ViewEncapsulation,
	OnInit,
	EventEmitter
} from "@angular/core";

import {
	NavController,
	NavParams,
	LoadingController,
	AlertController,
	ToastController,
	ActionSheetController,
	Platform,
	normalizeURL
} from "ionic-angular";

import { OrdersServiceProvider } from "../../../providers/service/orders";

@Component({
	selector: "pickup-shipment",
	encapsulation: ViewEncapsulation.None,
	templateUrl: "pickup-shipment.html"
})
export class PickupShipmentComponent implements OnInit {
	@Input() order: any = {};
	@Input() create = new EventEmitter<any>();

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public alertCtrl: AlertController,
		public loadingCtrl: LoadingController,
		public orders: OrdersServiceProvider
	) {}

	ngOnInit(){
		this.create.subscribe(()=>{
			this.saveShippingEntry();
		});
	}

	private saveShippingEntry() {
		const loading = this.loadingCtrl.create({
			dismissOnPageChange: true
		});
		loading.present();
		let deliveryDeadline = new Date();
		deliveryDeadline.setDate(deliveryDeadline.getDate() + 5);

		this.orders
			.shipOrder(this.order.orderId, {
				shippedDateTime: new Date(),
				orderTrackingNumber: this.order.orderTrackingNumber,
				deliveryDeadline: deliveryDeadline
			})
			.then(
				(data: any) => {
					if (data && data.success) {
						this.navCtrl.popTo(
							this.navCtrl.getByIndex(this.navCtrl.length() - 3)
						);
					} else {
						let alert = this.alertCtrl.create({
							title: "Warning!",
							subTitle: data.msg,
							buttons: ["OK"]
						});
						alert.onDidDismiss(() => {
							this.navCtrl.popTo(
								this.navCtrl.getByIndex(
									this.navCtrl.length() - 3
								)
							);
						});
						alert.present();
					}
					loading.dismiss();
				},
				error => {
					loading.dismiss();
				}
			);
	}
}
