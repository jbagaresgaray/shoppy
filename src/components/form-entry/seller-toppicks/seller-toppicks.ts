import {
  Component,
  OnInit,
  ViewEncapsulation,
  Input,
  EventEmitter
} from "@angular/core";
import * as _ from "lodash";
import {
  LoadingController,
  AlertController,
  NavController,
  NavParams,
  ModalController
} from "ionic-angular";

import { ModalComponentFormPage } from "./../../../pages/modal-component-form/modal-component-form";
import { SellerServiceProvider } from "../../../providers/service/seller";

@Component({
  selector: "seller-toppicks",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "seller-toppicks.html"
})
export class SellerToppicksComponent implements OnInit {
  productArr: any[] = [];
  collection: any = {};
  _collection: any = {};
  user: any = {};

  callback: any;
  collectionId: any;
  mode: string;

  hasProducts: boolean;
  showAddProduct: boolean;

  @Input() create = new EventEmitter<any>();
  @Input() delete = new EventEmitter<any>();

  constructor(
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public sellerService: SellerServiceProvider
  ) {}

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem("app.user")) || {};
    this.mode = this.navParams.get("mode");
    this.callback = this.navParams.get("callback");

    console.log("mode: ", this.mode);
    this.refreshData();

    this.create.subscribe(() => {
      this.saveEntry();
    });

    this.delete.subscribe(() => {
      this.deleteCollection();
    });
  }

  private refreshData() {
    if (this.mode == "update") {
      this._collection = this.navParams.get("collection");
      this.getCollectionDetails(this._collection.collectionId).then(() => {
        this.getSellerTopPickCollectionProducts(
          this.user.uuid,
          this._collection.collectionId
        );
      });
    }
  }

  addProduct() {
    const modal = this.modalCtrl.create(ModalComponentFormPage, {
      action: "product_picker_modal",
      modalInset: false
    });
    modal.onDidDismiss((resp: any) => {
      if (resp && resp.action == "save") {
        _.each(resp.products, (row: any) => {
          if (!_.isEmpty(this.productArr)) {
            const result = _.find(this.productArr, {
              productId: row.productId
            });
            if (result) {
              this.alertCtrl
                .create({
                  title: "WARNING",
                  subTitle: "One of the product already existed!",
                  buttons: ["OK"]
                })
                .present();
              return false;
            } else {
              this.productArr.push(row);
            }
          } else {
            this.productArr.push(row);
          }
        });
        console.log("this.productArr: ", this.productArr);
      }
    });
    modal.present();
  }

  deleteCollection() {
    const _deleteCollection = () => {
      const loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      loading.present();
      this.sellerService
        .deleteSellerTopPickCollection(
          this.user.uuid,
          this.collection.collectionId
        )
        .then(
          (data: any) => {
            loading.dismiss();
            if (data && data.success) {
              const alert = this.alertCtrl.create({
                title: "SUCCESS",
                subTitle: data.msg,
                buttons: ["OK"]
              });
              alert.onDidDismiss(() => {
                this.callback("save").then(() => {
                  this.navCtrl.pop();
                });
              });
              alert.present();
            }
          },
          error => {
            console.log("error: ", error);
            loading.dismiss();
          }
        );
    };

    const confirm = this.alertCtrl.create({
      title: "Delete this Collection?",
      message: "Are you sure you want to delete this Collection?",
      buttons: [
        {
          text: "Cancel",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Confirm",
          handler: () => {
            _deleteCollection();
          }
        }
      ]
    });
    confirm.present();
  }

  removeProductOnCollection(item: any) {
    if (item.stp_product_uuid) {
      const _removeProductOnCollection = () => {
        const loading = this.loadingCtrl.create({
          dismissOnPageChange: true
        });
        loading.present();
        this.sellerService
          .deleteCollectionTopPickProductDetail(
            this.user.uuid,
            this._collection.collectionId,
            item.productId
          )
          .then(
            (data: any) => {
              if (data && data.success) {
                this.refreshData();
                const alert = this.alertCtrl.create({
                  title: "SUCCESS",
                  subTitle: data.msg,
                  buttons: ["OK"]
                });
                alert.present();
              }
              loading.dismiss();
            },
            error => {
              console.log("error: ", error);
              loading.dismiss();
            }
          );
      };

      const alert = this.alertCtrl.create({
        title: "Remove this product on collection?",
        message: "Are you sure you want to remove this on Collection?",
        buttons: [
          {
            text: "Cancel",
            handler: () => {
              console.log("Disagree clicked");
            }
          },
          {
            text: "Confirm",
            handler: () => {
              _removeProductOnCollection();
            }
          }
        ]
      });
      alert.present();
    } else {
      _.remove(this.productArr, { productId: item.productId });
    }
  }

  private async getCollectionDetails(collectionId) {
    await this.sellerService
      .getSellerTopPickCollection(this.user.uuid, collectionId)
      .then(
        (data: any) => {
          if (data && data.success) {
            const collection = data.result;
            console.log("collection: ", collection);
            this.collection = collection;
          }
        },
        error => {
          console.log("error: ", error);
        }
      );
  }

  private getSellerTopPickCollectionProducts(sellerId, collectionId) {
    this.sellerService
      .getSellerTopPickCollectionProducts(sellerId, collectionId)
      .then(
        (data: any) => {
          if (data && data.success) {
            const products = data.result;
            console.log("products: ", products);
            this.productArr = products;
          }
        },
        error => {
          console.log("error: ", error);
        }
      );
  }

  private submitCollectionProducts(): Promise<Array<any>> {
    let selectedProducts: any[] = [];
    _.each(this.productArr, (row: any) => {
      if (_.isEmpty(row.stp_product_uuid)) {
        selectedProducts.push({
          collectionId: this.collection.collectionId || this.collectionId,
          productId: row.productId
        });
      }
    });
    console.log("selectedProducts: ", selectedProducts);

    return new Promise((resolve, reject) => {
      Promise.all(
        selectedProducts.map((product: any) => {
          return this.sellerService.createCollectionTopPickProduct(
            this.user.uuid,
            this.collection.collectionId || this.collectionId,
            product
          );
        })
      )
        .then((data: any) => {
          console.log("createCollectionTopPickProduct: ", data);
          const alert = this.alertCtrl.create({
            title: "SUCCESS",
            subTitle: "Record successfully saved!",
            buttons: ["OK"]
          });
          alert.onDidDismiss(() => {
            this.callback("save").then(() => {
              this.navCtrl.pop();
            });
          });
          alert.present();
          resolve();
        })
        .catch(error => {
          console.log("error: ", error);
        });
    });
  }

  private saveCollectionName() {
    const _saveCollectionName = () => {
      const loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      loading.present();
      this.sellerService
        .createSellerTopPickCollection(this.user.uuid, this.collection)
        .then(
          (data: any) => {
            if (data && data.success) {
              this.collectionId = data.result[0];
              this.showAddProduct = true;
              this.mode == "update";

              if (_.isEmpty(this.productArr)) {
                const alert = this.alertCtrl.create({
                  title: "SUCCESS",
                  subTitle: data.msg,
                  buttons: ["OK"]
                });
                alert.onDidDismiss(() => {
                  this.callback("save").then(() => {
                    this.navCtrl.pop();
                  });
                });
                alert.present();
              } else {
                this.submitCollectionProducts();
              }
            }
            loading.dismiss();
          },
          error => {
            console.log("error: ", error);
            loading.dismiss();
          }
        );
    };

    const confirm = this.alertCtrl.create({
      title: "Save this Collection?",
      message: "Are you sure you want to save this Collection?",
      buttons: [
        {
          text: "Cancel",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Confirm",
          handler: () => {
            _saveCollectionName();
          }
        }
      ]
    });
    confirm.present();
  }

  private updateCollectionName() {
    const _updateCollectionName = () => {
      const loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      loading.present();
      this.sellerService
        .updateSellerTopPickCollection(
          this.user.uuid,
          this.collection.collectionId,
          this.collection
        )
        .then(
          (data: any) => {
            if (data && data.success) {
              if (_.isEmpty(this.productArr)) {
                const alert = this.alertCtrl.create({
                  title: "SUCCESS",
                  subTitle: data.msg,
                  buttons: ["OK"]
                });
                alert.onDidDismiss(() => {
                  this.callback("save").then(() => {
                    this.navCtrl.pop();
                  });
                });
                alert.present();
              } else {
                this.submitCollectionProducts();
              }
            }
            loading.dismiss();
          },
          error => {
            console.log("error: ", error);
            loading.dismiss();
          }
        );
    };

    const confirm = this.alertCtrl.create({
      title: "Update this Collection?",
      message: "Are you sure you want to update this Collection?",
      buttons: [
        {
          text: "Cancel",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Confirm",
          handler: () => {
            _updateCollectionName();
          }
        }
      ]
    });
    confirm.present();
  }

  saveEntry() {
    if (this.mode == "create") {
      if (!this.hasProducts) {
        this.saveCollectionName();
      }
    } else if (this.mode == "update") {
      this.updateCollectionName();
    }
  }
}
