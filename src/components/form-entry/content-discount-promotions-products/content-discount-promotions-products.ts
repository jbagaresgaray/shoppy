import {
  Component,
  OnInit,
  ViewEncapsulation,
  Input,
  EventEmitter
} from "@angular/core";
import {
  NavController,
  NavParams,
  LoadingController,
  ModalController,
  AlertController
} from "ionic-angular";
import * as _ from "lodash";
import * as moment from "moment";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";

import { ModalComponentFormPage } from "../../../pages/modal-component-form/modal-component-form";
import { DiscountPromotionsService } from "../../../providers/service/discount-promotions";
import { AllFormEntryPage } from "./../../../pages/all-form-entry/all-form-entry";

@Component({
  selector: "content-discount-promotions-products",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "content-discount-promotions-products.html"
})
export class ContentDiscountPromotionsProductsComponent implements OnInit {
  @Input() refresh: EventEmitter<any>;
  @Input() create: EventEmitter<any>;
  @Input() save: EventEmitter<any>;
  @Input() delete: EventEmitter<any>;
  @Input() update: EventEmitter<any>;

  discountForm: FormGroup;

  discount: any = {};
  settings: any = {};
  user: any = {};
  productsArr: any[] = [];
  callback: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public discountService: DiscountPromotionsService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem("app.user")) || {};
    this.discount = this.navParams.get("params");
    this.callback = this.navParams.get("callback");

    this.initDiscountProducts();

    this.refresh.subscribe(data => {
      this.initDiscountProducts(data);
    });

    this.create.subscribe(data => {
      this.updateDiscountPromotion();
    });

    this.save.subscribe(() => {
      this.saveDiscountedProducts();
    });

    this.delete.subscribe(() => {
      this.deleteDiscountPromotion();
    });

    this.update.subscribe((data: any) => {
      console.log("update: ", data);
      if (data) {
        this.settings = data;
        this.updateProducts();
      }
    });

    this.discountForm = this.formBuilder.group({
      purchase_limit: ["", Validators.required],
      discount_price: ["", Validators.required],
      discount: ["", Validators.required]
    });
  }

  private initDiscountProducts(ev?: any) {
    this.discountService
      .getDiscountPromotionProducts(this.user.uuid, this.discount.dPromoteId)
      .then(
        (data: any) => {
          if (data && data.success) {
            const products = data.result;
            console.log("getDiscountPromotionProducts: ", products);
            if (!_.isEmpty(products)) {
              this.productsArr = products;
            } else {
              this.productsArr = [];
            }
          }
          if (ev) {
            ev.complete();
          }
        },
        error => {
          console.log("Error: ", error);
          if (ev) {
            ev.complete();
          }
        }
      );
  }

  calculateDiscountedPrice(price, discount) {
    return price - (discount / 100) * price;
  }

  calculateDiscount(price, discount_price) {
    return ((discount_price / price) * 100).toFixed(2);
  }

  calculateItemDiscount(item) {
    if (item.variants && _.isEmpty(item.variants)) {
      item.discount_price =
        item.productPrice - (item.discount / 100) * item.productPrice;
    } else {
      console.log("else");
      item.discount_price = item.price - (item.discount / 100) * item.price;
    }
    console.log("return price: ", item.discount_price);
    return item;
  }

  calculateItemPrice(item) {
    if (item.variants && _.isEmpty(item.variants)) {
      item.discount = ((item.discount_price / item.productPrice) * 100).toFixed(
        2
      );
    } else {
      console.log("else");
      item.discount = ((item.discount_price / item.price) * 100).toFixed(2);
    }
    console.log("return discount: ", item.discount);
    return item;
  }

  deleteDiscountPromotion() {
    const _deleteDiscountPromotion = () => {
      const loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      loading.present();
      this.discountService
        .deleteDiscountPromotion(this.user.uuid, this.discount.dPromoteId)
        .then(
          (data: any) => {
            if (data && data.success) {
              loading.dismiss();
              const alert = this.alertCtrl.create({
                title: "SUCCESS",
                subTitle: data.msg,
                buttons: ["OK"]
              });
              alert.onDidDismiss(() => {
                this.callback("save").then(() => {
                  this.navCtrl.pop();
                });
              });
              alert.present();
            } else {
              loading.dismiss();
            }
          },
          error => {
            console.log("error: ", error);
            loading.dismiss();
          }
        );
    };

    const confirm = this.alertCtrl.create({
      title: "Delete Discount Promotion?",
      message: "Confirm to delete this discount promotion?",
      buttons: [
        {
          text: "Cancel",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Confirm",
          handler: () => {
            _deleteDiscountPromotion();
          }
        }
      ]
    });
    confirm.present();
  }

  removeItem(item: any) {
    if (item.discount_uuid) {
      const _deleteDiscountPromotion = () => {
        const loading = this.loadingCtrl.create({
          dismissOnPageChange: true
        });
        loading.present();
        this.discountService
          .deleteDiscountPromotionProduct(
            this.user.uuid,
            this.discount.dPromoteId,
            item.productId
          )
          .then(
            (data: any) => {
              if (data && data.success) {
                loading.dismiss();
                this.initDiscountProducts(null);

                const alert = this.alertCtrl.create({
                  title: "SUCCESS",
                  subTitle: data.msg,
                  buttons: ["OK"]
                });
                alert.present();
              } else {
                loading.dismiss();
              }
            },
            error => {
              console.log("error: ", error);
              loading.dismiss();
            }
          );
      };

      const confirm = this.alertCtrl.create({
        title: "Delete Product?",
        message: "Confirm to delete this discounted product?",
        buttons: [
          {
            text: "Cancel",
            handler: () => {
              console.log("Disagree clicked");
            }
          },
          {
            text: "Confirm",
            handler: () => {
              _deleteDiscountPromotion();
            }
          }
        ]
      });
      confirm.present();
    } else {
      _.remove(this.productsArr, { productId: item.productId });
    }
  }

  updateDiscountPromotion() {
    this.navCtrl.push(AllFormEntryPage, {
      action: "discount_promotions",
      mode: "update",
      params: this.discount
    });
  }

  selectProduct() {
    const modal = this.modalCtrl.create(ModalComponentFormPage, {
      action: "product_picker_modal",
      details: this.discount,
      modalInset: false
    });
    modal.onDidDismiss((resp: any) => {
      console.log("resp: ", resp);
      if (resp && resp.action == "save") {
        this.productsArr = resp.products;

        _.each(this.productsArr, (row: any) => {
          row.purchase_limit = 0;
          row.discount_price = 0;
          row.discount = 0;

          const minPrice: any = _.minBy(row.variants, (row: any) => {
            return row.price;
          });
          const maxPrice: any = _.maxBy(row.variants, (row: any) => {
            return row.price;
          });

          if (!_.isEmpty(row.variants)) {
            _.each(row.variants, (col: any) => {
              col.selected = true;
              col.purchase_limit = 0;
              col.discount_price = 0;
              col.discount = 0;
            });

            if (minPrice.price == maxPrice.price) {
              row.priceTitle = minPrice.price;
            } else {
              row.priceTitle = minPrice.price + "-" + maxPrice.price;
            }
          } else {
            row.priceTitle = _.clone(row.productPrice);
          }
        });
      }
    });
    modal.present();
  }

  updateProducts() {
    _.each(this.productsArr, (row: any) => {
      row.purchase_limit = this.settings.purchase_limit;
      if (_.isEmpty(row.variants)) {
        row.discount = this.settings.discount;
        row.discount_price = this.calculateDiscountedPrice(
          row.productPrice,
          this.settings.discount
        );
      } else {
        _.each(row.variants, (col: any) => {
          col.discount = this.settings.discount;
          col.discount_price = this.calculateDiscountedPrice(
            col.price,
            this.settings.discount
          );
        });
      }
    });
  }

  saveDiscountedProducts() {
    const _saveDiscountedProducts = () => {
      const loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      loading.present();
      this.submitDiscountProducts().then(
        () => {
          loading.dismiss();

          const alert = this.alertCtrl.create({
            title: "SUCCESS",
            subTitle: "Product discounts successfully created",
            buttons: ["OK"]
          });
          alert.present();
          setTimeout(() => {
            this.initDiscountProducts(null);
          }, 300);
        },
        () => {
          loading.dismiss();
        }
      );
    };

    const confirm = this.alertCtrl.create({
      title: "Save Discounted Products?",
      message: "Are you sure to save discounted products?",
      buttons: [
        {
          text: "Cancel",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Confirm",
          handler: () => {
            _saveDiscountedProducts();
          }
        }
      ]
    });
    confirm.present();
  }

  private submitDiscountProducts(): Promise<Array<any>> {
    if (_.isEmpty(this.productsArr)) {
      this.alertCtrl
        .create({
          title: "WARNING",
          subTitle: "Unable to save. Please select products!",
          buttons: ["OK"]
        })
        .present();
    }

    let selectedProducts: any[] = [];
    let toDeleteProducts: any[] = [];
    _.each(this.productsArr, (row: any) => {
      if (_.isEmpty(row.variants)) {
        if (row.selected) {
          selectedProducts.push({
            discount_uuid: row.discount_uuid,
            dPromoteId: this.discount.dPromoteId,
            productId: row.productId,
            productVariationId: null,
            discount_price: row.discount_price,
            discount: row.discount,
            purchase_limit: row.purchase_limit
          });
        }
      } else {
        _.each(row.variants, (col: any) => {
          if (col.selected) {
            selectedProducts.push({
              discount_uuid: row.discount_uuid,
              dp_uuid: col.dp_uuid,
              dPromoteId: this.discount.dPromoteId,
              productId: row.productId,
              productVariationId: col.productVariationId,
              discount_price: col.discount_price,
              discount: col.discount,
              purchase_limit: row.purchase_limit
            });
          } else {
            toDeleteProducts.push({
              discount_uuid: row.discount_uuid,
              dp_uuid: col.dp_uuid,
              productId: row.productId,
              productVariationId: col.productVariationId
            });
          }
        });
      }
    });
    console.log("selectedProducts: ", selectedProducts);
    console.log("toDeleteProducts: ", toDeleteProducts);

    return new Promise((resolve, reject) => {
      Promise.all([
        selectedProducts.map((product: any) => {
          if (product.dp_uuid) {
            return this.discountService.updateDiscountPromotionProductVariant(
              this.user.uuid,
              this.discount.dPromoteId,
              product.productId,
              product.productVariationId,
              product
            );
          } else {
            return this.discountService.createDiscountPromotionProducts(
              this.user.uuid,
              this.discount.dPromoteId,
              product
            );
          }
        }),
        toDeleteProducts.map((product: any) => {
          return this.discountService.deleteDiscountPromotionProductVariant(
            this.user.uuid,
            this.discount.dPromoteId,
            product.productId,
            product.productVariationId
          );
        })
      ])
        .then((data: any) => {
          console.log("createDiscountPromotionProducts: ", data);
          resolve();
        })
        .catch(error => {
          console.log("error: ", error);
        });
    });
  }
}
