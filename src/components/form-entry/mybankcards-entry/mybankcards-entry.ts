import {
	Component,
	OnInit,
	Input,
	EventEmitter,
	ViewEncapsulation
} from "@angular/core";
import {
	NavController,
	NavParams,
	LoadingController,
	AlertController
} from "ionic-angular";
import * as async from "async";

import { BanksServiceProvider } from "../../../providers/service/banks";
import { UserBanksServiceProvider } from "../../../providers/service/user-bank";

@Component({
	selector: "page-mybankcards-entry",
	encapsulation: ViewEncapsulation.None,
	templateUrl: "mybankcards-entry.html"
})
export class MybankcardsEntryPage implements OnInit {
	banksArr: any[] = [];
	errArr: any[] = [];

	bank: any = {};
	bankId: any;

	@Input() create = new EventEmitter<any>();
	@Input() delete = new EventEmitter<any>();
	@Input() mode: string;

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public alertCtrl: AlertController,
		public loadingCtrl: LoadingController,
		public banksService: BanksServiceProvider,
		public userbank: UserBanksServiceProvider
	) {
		this.bankId = this.navParams.get("bankId");
	}

	ngOnInit() {
		console.log("ionViewDidLoad MybankcardsEntryPage");
		this.initData();

		this.create.subscribe(data => {
			if (data == "view") {
				this.setDefault();
			} else if (data == "entry") {
				this.saveBank();
			}
		});

		this.delete.subscribe(() => {
			this.deleteAccount();
		});
	}

	private initData() {
		let loading = this.loadingCtrl.create();
		loading.present();

		async.waterfall([
			callback => {
				this.banksService.getAllBanks().then(
					(data: any) => {
						if (data && data.success) {
							this.banksArr = data.result;
						}
						callback();
					},
					error => {
						loading.dismiss();
					}
				);
			},
			callback => {
				if (this.bankId) {
					this.userbank.getUserBank(this.bankId).then(
						(data: any) => {
							if (data && data.success) {
								this.bank = data.result;
							}
							loading.dismiss();
							callback();
						},
						error => {
							loading.dismiss();
						}
					);
				} else {
					loading.dismiss();
				}
			}
		]);
	}

	saveBank() {
		console.log("bank: ", this.bank);
		const loading = this.loadingCtrl.create({
			dismissOnPageChange: true
		});
		loading.present();
		this.userbank.saveUserBank(this.bank).then(
			(data: any) => {
				let alert = this.alertCtrl.create({
					title: "Success!",
					subTitle: data.msg,
					buttons: ["OK"]
				});
				alert.onDidDismiss(() => {
					this.navCtrl.pop();
				});

				if (data && data.success) {
					loading.dismiss().then(() => {
						alert.present();
					});
				} else {
					loading.dismiss();

					let alert = this.alertCtrl.create({
						title: "Error",
						subTitle: data.msg,
						buttons: ["OK"]
					});
					alert.present();
					return;
				}
			},
			error => {
				loading.dismiss();
				console.log("error: ", error);
				let err = error.data;
				this.errArr = err.result;
				setTimeout(() => {
					this.errArr = [];
				}, 3000);
			}
		);
	}

	setDefault() {}

	deleteAccount() {
		const _deleteBank = () => {
			const loading = this.loadingCtrl.create({
				dismissOnPageChange: true
			});
			loading.present();
			this.userbank.deleteUserBank(this.bank._id).then(
				(data: any) => {
					if (data && data.success) {
						loading.dismiss().then(() => {
							this.navCtrl.pop();
						});
					}
				},
				error => {
					console.log("error: ", error);
					loading.dismiss();
				}
			);
		};

		const confirm = this.alertCtrl.create({
			title: "Delete Bank Account?",
			message: "Are you sure to delete?",
			buttons: [
				{
					text: "Cancel",
					handler: () => {
						console.log("Disagree clicked");
					}
				},
				{
					text: "Confirm",
					handler: () => {
						_deleteBank();
					}
				}
			]
		});
		confirm.present();
	}
}
