import {
  Component,
  Input,
  ViewEncapsulation,
  OnInit,
  EventEmitter
} from "@angular/core";
import * as _ from "lodash";
import * as moment from "moment";
import {
  LoadingController,
  AlertController,
  NavController,
  NavParams
} from "ionic-angular";

import { DiscountPromotionsService } from "../../../providers/service/discount-promotions";

@Component({
  selector: "discount-promotions",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "discount-promotions.html"
})
export class DiscountPromotionsComponent implements OnInit {
  discount: any = {};
  errArr: any[] = [];
  mode: string;

  @Input() create = new EventEmitter<any>();
  @Input() params: any = {};

  constructor(
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public navParams: NavParams,
    public discountsPromotions: DiscountPromotionsService
  ) {}

  ngOnInit() {
    this.params = this.navParams.get("params");
    this.mode = this.navParams.get("mode");
    console.log("params: ", this.params);
    console.log("mode: ", this.mode);

    this.discount.event_from = new Date();
    this.discount.event_to = new Date();

    this.create.subscribe(data => {
      this.saveEntry();
    });
  }

  saveEntry() {
    const createDiscountPromotion = () => {
      const loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      loading.present();
      this.discountsPromotions
        .createDiscountPromotion(this.params.uuid, this.discount)
        .then(
          (data: any) => {
            if (data && data.success) {
              const alert = this.alertCtrl.create({
                title: "SUCCESS",
                subTitle: data.msg,
                buttons: ["OK"]
              });
              alert.onDidDismiss(() => {
                this.navCtrl.pop();
              });
              alert.present();
            }
            loading.dismiss();
          },
          error => {
            loading.dismiss();
            console.log("error: ", error);
            const err = error.data;
            console.log("err: ", err);
            if (
              !err.success &&
              _.isArray(err.result) &&
              !_.isEmpty(err.result)
            ) {
              this.errArr = err.result;
              setTimeout(() => {
                this.errArr = [];
              }, 3000);
            } else {
              this.alertCtrl
                .create({
                  title: "WARNING",
                  subTitle: err.response.msg,
                  buttons: ["OK"]
                })
                .present();
            }
          }
        );
    };

    const dateFrom = moment(this.discount.event_from);
    const dateTo = moment(this.discount.event_to);

    if(dateTo.isBefore(dateFrom)){
      this.alertCtrl.create({
        message: "Event end date must be later than the Event start date!",
        buttons: ["OK"]
      }).present();
      return;
    }

    const alert = this.alertCtrl.create({
      title: "Save Entry?",
      subTitle:
        "Once you've set your Promotional Period, you will not be able to extend the Promotional Period. You may only shorten the period of time. Please check before you continue.",
      buttons: [
        {
          text: "Discard",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Confirm",
          handler: () => {
            createDiscountPromotion();
          }
        }
      ]
    });
    alert.present();
  }
}
