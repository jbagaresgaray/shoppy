import { Component } from '@angular/core';

/**
 * Generated class for the ChatSettingsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'chat-settings',
  templateUrl: 'chat-settings.html'
})
export class ChatSettingsComponent {

  text: string;

  constructor() {
    console.log('Hello ChatSettingsComponent Component');
    this.text = 'Hello World';
  }

}
