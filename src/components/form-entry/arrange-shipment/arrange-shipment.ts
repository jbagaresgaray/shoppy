import {
	Component,
	Input,
	Output,
	EventEmitter,
	ViewEncapsulation,
	OnInit
} from "@angular/core";

import {
	NavController,
	NavParams,
	LoadingController,
	AlertController,
	ToastController,
	ActionSheetController,
	Platform
} from "ionic-angular";
import * as _ from "lodash";
import * as async from "async";

import { AllFormEntryPage } from "../../../pages/all-form-entry/all-form-entry";

import { OrdersServiceProvider } from "../../../providers/service/orders";
import { UserAddressessServiceProvider } from "../../../providers/service/user-address";

@Component({
	selector: "arrange-shipment",
	encapsulation: ViewEncapsulation.None,
	templateUrl: "arrange-shipment.html"
})
export class ArrangeShipmentComponent implements OnInit {
	pickup: any = {};
	@Input() order: any = {};
	@Input() userAddressArr: any[] = [];

	@Input() create = new EventEmitter<any>();

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public alertCtrl: AlertController,
		public loadingCtrl: LoadingController,
		public addressess: UserAddressessServiceProvider,
		public orders: OrdersServiceProvider
	) {}

	ngOnInit() {
		this.pickup.checkpoint = {};
		this.pickup.pickUpDate = new Date(
			this.order.shippingDeadline
		).toISOString();
		this.pickup.shipment_pickup_date = new Date(
			this.order.shippingDeadline
		).toISOString();
		this.pickup.shipment_package_count = _.size(this.order.details);
		this.pickup.slug = this.order.shippingSlug;
		this.pickup.shippingCourierId = this.order.shippingCourierId;
		this.pickup.checkpoint.checkpoint_time = new Date();

		this.initUserAddress();

		this.create.subscribe(data => {
			this.arrangeShipment();
		});
	}

	initUserAddress() {
		let loading = this.loadingCtrl.create();
		loading.present();

		this.addressess
			.getAllUserAddress()
			.then(
				(data: any) => {
					if (data && data.success) {
						this.userAddressArr = data.result;
						_.each(this.userAddressArr, (row: any) => {
							row.selected = row.isPickUpAddress;

							if (row.isPickUpAddress) {
								this.pickup.pickUpUserAddressId = row._id;
								this.pickup.pickUpLocation = row.detailAddress;
								this.pickup.pickUpCity = row.city;
								this.pickup.pickUpState = row.state;
								this.pickup.pickUpZip = row.zipcode;
								this.pickup.pickUpCountry = row.country;
								this.pickup.customer_name = row.name;

								this.pickup.checkpoint.location =
									row.detailAddress;
								this.pickup.checkpoint.city = row.city;
								this.pickup.checkpoint.state = row.state;
								this.pickup.checkpoint.country_name =
									row.country;
								this.pickup.checkpoint.country_iso3 =
									row.country;
								this.pickup.checkpoint.zip = row.zipcode;
							}
						});
					}
					console.log("this.userAddressArr: ", this.userAddressArr);
					loading.dismiss();
				},
				error => {
					loading.dismiss();
				}
			)
			.then(() => {
				/*if (!_.isEmpty(this.orderAddress)) {
				_.each(this.userAddressArr, (row: any) => {
					row.selected = (row._id == this.orderAddress.addressId) ? true : false;
				});
			}*/
			});
	}

	selectAddressItem(item) {
		_.each(this.userAddressArr, (row: any) => {
			if (item._id == row._id) {
				row.selected = true;

				this.pickup.pickUpUserAddressId = row._id;
				this.pickup.pickUpLocation = row.detailAddress;
				this.pickup.pickUpCity = row.city;
				this.pickup.pickUpState = row.state;
				this.pickup.pickUpZip = row.zipcode;
				this.pickup.pickUpCountry = row.country;
				this.pickup.customer_name = row.name;

				this.pickup.checkpoint.location = row.detailAddress;
				this.pickup.checkpoint.city = row.city;
				this.pickup.checkpoint.state = row.state;
				this.pickup.checkpoint.country_name = row.country;
				this.pickup.checkpoint.country_iso3 = row.country;
				this.pickup.checkpoint.zip = row.zipcode;
			} else {
				row.selected = false;
			}
		});
	}

	addNewAddress() {
		this.navCtrl.push(AllFormEntryPage, {
			action: "my_address_entry"
		});
	}

	private arrangeShipment() {
		let loading = this.loadingCtrl.create({
			dismissOnPageChange: true,
			content: "Arranging..."
		});
		loading.present();
		console.log("this.pickup: ", this.pickup);

		this.orders.arrangeShipment(this.order.orderId, this.pickup).then(
			(data: any) => {
				if (data && data.success) {
					console.log("data: ", data);
					let alert = this.alertCtrl.create({
						title: "SUCCESS",
						message: data.msg,
						buttons: ["OK"]
					});
					alert.onDidDismiss(() => {
						this.navCtrl.popTo(
							this.navCtrl.getByIndex(this.navCtrl.length() - 3)
						);
					});
					alert.present();
				} else if (data && !data.success) {
					let alert = this.alertCtrl.create({
						title: "WARNING",
						message: data.msg,
						buttons: ["OK"]
					});
					alert.onDidDismiss(() => {
						this.navCtrl.popTo(
							this.navCtrl.getByIndex(this.navCtrl.length() - 3)
						);
					});
					alert.present();
				}
				loading.dismiss();
			},
			(error: any) => {
				loading.dismiss();
			}
		);
	}
}
