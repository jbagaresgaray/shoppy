import {
	Component,
	Input,
	Output,
	EventEmitter,
	ViewEncapsulation,
	OnInit
} from "@angular/core";

import {
	NavController,
	NavParams,
	LoadingController,
	AlertController,
	ToastController,
	ActionSheetController,
	Platform,
	normalizeURL
} from "ionic-angular";
import * as _ from "lodash";

/**
 * Generated class for the ReturnShipmentShippingComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
	selector: "return-shipment-shipping",
	encapsulation: ViewEncapsulation.None,
	templateUrl: "return-shipment-shipping.html"
})
export class ReturnShipmentShippingComponent implements OnInit {
	@Input() shippingArr: any[] = [];
	@Input() create = new EventEmitter<any>();

	callback: any;

	constructor(public navCtrl: NavController, public navParams: NavParams) {}

	ngOnInit() {
		this.shippingArr = this.navParams.get("tmpShippingArr");

		this.create.subscribe((data: any) => {});
	}

	selectShippingItem(item) {
		_.each(this.shippingArr, (row: any) => {
			if (item.shippingId == row.shippingId) {
				row.selected = true;
				setTimeout(() => {
					this.callback(row).then(() => {
						this.navCtrl.pop();
					});
				}, 800);
			} else {
				row.selected = false;
			}
		});
	}
}
