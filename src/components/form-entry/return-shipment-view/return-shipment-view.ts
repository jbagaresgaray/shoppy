import {
	Component,
	Input,
	ViewEncapsulation
} from "@angular/core";

/**
 * Generated class for the ReturnShipmentViewComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
	selector: "return-shipment-view",
	encapsulation: ViewEncapsulation.None,
	templateUrl: "return-shipment-view.html"
})
export class ReturnShipmentViewComponent {
	
	@Input() return: any = {};
	
	constructor() {
	}
}
