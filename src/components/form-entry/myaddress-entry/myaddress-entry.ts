import {
	Component,
	ViewEncapsulation,
	OnInit,
	Input,
	EventEmitter
} from "@angular/core";
import {
	IonicPage,
	NavController,
	NavParams,
	AlertController,
	LoadingController
} from "ionic-angular";

import { ServiceProvider } from "../../../providers/service/service";
import { UserAddressessServiceProvider } from "../../../providers/service/user-address";

@Component({
	selector: "page-myaddress-entry",
	encapsulation: ViewEncapsulation.None,
	templateUrl: "myaddress-entry.html"
})
export class MyaddressEntryPage implements OnInit {
	address: any = {};
	countries: any[] = [];
	errArr: any[] = [];
	showDelete: boolean = false;

	@Input() create = new EventEmitter<any>();

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public alertCtrl: AlertController,
		public loadingCtrl: LoadingController,
		public services: ServiceProvider,
		public addressess: UserAddressessServiceProvider
	) {}

	ngOnInit() {
		console.log("ionViewDidLoad MyaddressEntryPage");
		if (this.navParams.get("addressId")) {
			this.initAddress();
		} else {
			this.showDelete = false;
		}

		this.initCountries();

		this.create.subscribe(() => {
			this.saveAddress();
		});
	}

	private initCountries() {
		this.services.getRestCountries().then((data: any) => {
			if (data) {
				this.countries = data;
			}
		});
	}

	private initAddress() {
		this.showDelete = true;
		const loading = this.loadingCtrl.create({
			dismissOnPageChange: true
		});
		loading.present();
		this.addressess.getUserAddress(this.navParams.get("addressId")).then(
			(data: any) => {
				if (data && data.success) {
					loading.dismiss();
					this.address = data.result;
					this.address.isDefault = this.address.isDefaultAddress;
					this.address.isPickup = this.address.isPickUpAddress;
				}
			},
			error => {
				console.log("error: ", error);
				loading.dismiss();
			}
		);
	}

	saveAddress() {
		console.log("address: ", this.address);
		let loading = this.loadingCtrl.create();
		loading.present();

		if (this.navParams.get("addressId")) {
			this.addressess
				.updateUserAddress(
					this.navParams.get("addressId"),
					this.address
				)
				.then(
					(data: any) => {
						if (data && data.success) {
							loading.dismiss().then(() => {
								this.navCtrl.pop();
							});
						}
					},
					error => {
						loading.dismiss();
						console.log("error: ", error);
						let err = error.data;
						this.errArr = err.result;
						setTimeout(() => {
							this.errArr = [];
						}, 3000);
					}
				);
		} else {
			this.addressess.createUserAddress(this.address).then(
				(data: any) => {
					console.log("data: ", data);
					let alert = this.alertCtrl.create({
						title: "Success!",
						subTitle: data.msg,
						buttons: ["OK"]
					});
					alert.onDidDismiss(() => {
						this.navCtrl.pop();
					});

					if (data && data.success) {
						loading.dismiss().then(() => {
							alert.present();
						});
					} else {
						loading.dismiss();
					}
				},
				error => {
					loading.dismiss();
					console.log("error: ", error);
					let err = error.data;
					this.errArr = err.result;
					setTimeout(() => {
						this.errArr = [];
					}, 3000);
				}
			);
		}
	}

	deleteAddress() {
		console.log("address: ", this.address);
		let vm = this;
		let confirm = this.alertCtrl.create({
			title: "Delete Address?",
			message: "Are you sure to delete?",
			buttons: [
				{
					text: "No",
					handler: () => {
						console.log("Disagree clicked");
					}
				},
				{
					text: "Yes",
					handler: () => {
						let loading = this.loadingCtrl.create();
						loading.present();
						vm.addressess.deleteUserAddress(this.address._id).then(
							(data: any) => {
								if (data && data.success) {
									loading.dismiss().then(() => {
										vm.navCtrl.pop();
									});
								}
							},
							error => {
								console.log("error: ", error);
								loading.dismiss();
							}
						);
					}
				}
			]
		});
		confirm.present();
	}
}
