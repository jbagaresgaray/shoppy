import {
  Component,
  OnInit,
  ViewEncapsulation,
  Input,
  EventEmitter
} from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from "@angular/forms";
import {
  NavController,
  NavParams,
  LoadingController,
  ModalController,
  AlertController
} from "ionic-angular";
import * as _ from "lodash";
import * as async from "async";

import { ModalComponentFormPage } from "../../../pages/modal-component-form/modal-component-form";

import { SellerServiceProvider } from "../../../providers/service/seller";
import { SellerVouchersService } from "../../../providers/service/seller-vouchers";

@Component({
  selector: "seller-vouchers",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "seller-vouchers.html"
})
export class SellerVouchersComponent implements OnInit {
  @Input() create = new EventEmitter<any>();

  voucher: any = {};
  user: any = {};
  productsArr: any[] = [];
  callback: any;

  isSaving = false;
  submitted = false;

  mode: string;

  showFixed: boolean;
  showPercentage: boolean;
  showNoLimit: boolean;
  isApplytoWholeShop: boolean;

  voucherForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public navParams: NavParams,
    public navCtrl: NavController,
    public sellerService: SellerServiceProvider,
    public sellerVouchers: SellerVouchersService
  ) {}

  ngOnInit() {
    this.create.subscribe(() => {
      this.saveEntry();
    });

    this.user = JSON.parse(localStorage.getItem("app.user")) || {};
    this.callback = this.navParams.get("callback");
    this.mode = this.navParams.get("mode");
    this.voucher = this.navParams.get("voucher");

    this.voucherForm = this.formBuilder.group({
      voucherName: ["", Validators.required],
      voucherCode: ["", Validators.required],
      voucherType: ["fixed", Validators.required],
      voucherAmt: [""],
      minimumOrderAmt: [""],
      voucherFrom: ["", Validators.required],
      voucherTo: ["", Validators.required],
      voucherQuantity: ["", Validators.required],
      maxDiscountPrice: [""],
      // isNoLimit: [false],
      isApplytoWholeShop: [false],
      isDisplayToApp: [false],
      sellerId: [""]
    });
    this.showFixed = true;
    this.showPercentage = false;
    this.showNoLimit = false;
    this.isApplytoWholeShop = false;

    if (this.voucher) {
      const voucherFrom = new Date(this.voucher.voucherFrom);
      const voucherTo = new Date(this.voucher.voucherTo);

      this.showFixed = this.voucher.voucherType == "fixed" ? true : false;
      this.showPercentage =
        this.voucher.voucherType == "percentage" ? true : false;
      this.showNoLimit = this.voucher.isNoLimit == 1 ? true : false;
      this.isApplytoWholeShop =
        this.voucher.isApplytoWholeShop == 1 ? true : false;

      this.voucher.isApplytoWholeShop =
        this.voucher.isApplytoWholeShop == 1 ? true : false;
      this.voucher.isDisplayToApp =
        this.voucher.isDisplayToApp == 1 ? true : false;
      this.voucher.isNoLimit = this.voucher.isNoLimit == 1 ? true : false;
      this.voucher.voucherFrom = new Date(
        voucherFrom.getTime() - voucherFrom.getTimezoneOffset() * 60000
      ).toJSON();
      this.voucher.voucherTo = new Date(
        voucherTo.getTime() - voucherTo.getTimezoneOffset() * 60000
      ).toJSON();

      _.each(this.voucher.products, (row: any) => {
        this.productsArr.push({
          productId: row.productId
        });
      });

      this.voucherForm.patchValue(this.voucher);
    }

    this.voucherForm.get("voucherType").valueChanges.subscribe(val => {
      if (val === "fixed") {
        this.showFixed = true;
        this.showPercentage = false;
      } else if (val === "percentage") {
        this.showFixed = false;
        this.showPercentage = true;
      }
    });
    /* this.voucherForm.get("isNoLimit").valueChanges.subscribe(val => {
      this.showNoLimit = val;
    }); */
    this.voucherForm.get("isApplytoWholeShop").valueChanges.subscribe(val => {
      this.isApplytoWholeShop = val;
    });
  }

  get form() {
    return this.voucherForm.controls;
  }

  private validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  addProducts() {
    const modal = this.modalCtrl.create(ModalComponentFormPage, {
      action: "product_picker_modal",
      details: this.voucher,
      products: this.productsArr,
      mode: "update",
      modalInset: false
    });
    modal.onDidDismiss((resp: any) => {
      console.log("resp: ", resp);
      this.productsArr = [];
      if (resp && resp.action == "save") {
        _.each(resp.products, (row: any) => {
          this.productsArr.push({
            productId: row.productId
          });
        });

        console.log("this.productsArr: ", this.productsArr);
      }
    });
    modal.present();
  }

  saveEntry() {
    console.log("saveEntry: ", this.voucherForm.value);
    let title = "";
    let message = "";

    this.submitted = true;
    this.isSaving = true;

    this.voucherForm.patchValue({
      sellerId: this.user.sellerId
    });

    if (this.voucherForm.invalid) {
      this.isSaving = false;
      this.validateAllFormFields(this.voucherForm);
      return;
    }

    const _createSellerVouchers = () => {
      const loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      loading.present();
      this.sellerVouchers
        .createSellerVoucher(this.user.userId, this.voucherForm.value)
        .then(
          (data: any) => {
            if (data && data.success) {
              console.log("data: ", data.result);
              const voucherId = data.result[0];

              loading.dismiss();
              const alert = this.alertCtrl.create({
                title: "SUCCESS",
                subTitle: data.msg,
                buttons: ["OK"]
              });
              alert.onDidDismiss(() => {
                this.callback("save").then(() => {
                  this.navCtrl.pop();
                });
              });

              if (!_.isEmpty(this.productsArr)) {
                async.eachSeries(
                  this.productsArr,
                  (product, callback) => {
                    this.sellerVouchers
                      .createSellerProductVoucher(this.user.userId, voucherId, {
                        productId: product.productId
                      })
                      .then(
                        (data: any) => {
                          if (data && data.success) {
                            callback();
                          }
                        },
                        error => {
                          console.log(
                            "createSellerProductVoucher ERROR: ",
                            error
                          );
                        }
                      );
                  },
                  () => {
                    alert.present();
                  }
                );
              } else {
                alert.present();
              }
            } else {
              loading.dismiss();
            }
          },
          error => {
            console.log("error: ", error);
            loading.dismiss();
          }
        );
    };

    const _updateSellerVouchers = () => {
      const loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      loading.present();
      this.sellerVouchers
        .updateSellerVoucherDetail(
          this.user.userId,
          this.voucher.voucherId,
          this.voucherForm.value
        )
        .then(
          (data: any) => {
            if (data && data.success) {
              console.log("data: ", data.result);
              loading.dismiss();
              const alert = this.alertCtrl.create({
                title: "SUCCESS",
                subTitle: data.msg,
                buttons: ["OK"]
              });
              alert.onDidDismiss(() => {
                this.callback("save").then(() => {
                  this.navCtrl.pop();
                });
              });

              if (!_.isEmpty(this.productsArr)) {
                const toRemoveProduct = [];
                _.each(this.voucher.products, (row: any) => {
                  const result = _.find(this.productsArr, {
                    productId: row.productId
                  });
                  if (!result) {
                    toRemoveProduct.push({
                      productId: row.productId
                    });
                  } else {
                    _.remove(this.productsArr, (n: any) => {
                      return n.productId == row.productId;
                    });
                  }
                });
                console.log("toRemoveProduct: ", toRemoveProduct);
                console.log("this.productsArr: ", this.productsArr);
                async.parallel(
                  [
                    callback1 => {
                      async.eachSeries(
                        this.productsArr,
                        (product, callback) => {
                          this.sellerVouchers
                            .createSellerProductVoucher(
                              this.user.userId,
                              this.voucher.voucherId,
                              {
                                productId: product.productId
                              }
                            )
                            .then((data: any) => {
                              if (data && data.success) {
                                callback();
                              }
                            });
                        },
                        () => {
                          callback1();
                        }
                      );
                    },
                    callback1 => {
                      async.eachSeries(
                        toRemoveProduct,
                        (product, callback) => {
                          this.sellerVouchers
                            .deleteSellerProductVoucher(
                              this.user.userId,
                              this.voucher.voucherId,
                              product.productId
                            )
                            .then((data: any) => {
                              if (data && data.success) {
                                callback();
                              }
                            });
                        },
                        () => {
                          callback1();
                        }
                      );
                    }
                  ],
                  () => {
                    alert.present();
                  }
                );
              } else {
                alert.present();
              }
            } else {
              loading.dismiss();
            }
          },
          error => {
            console.log("error: ", error);
            loading.dismiss();
          }
        );
    };

    if (this.mode == "create") {
      title = "Create Voucher?";
      message = "Save and create voucher?";
    } else if (this.mode == "update") {
      title = "Update Voucher?";
      message = "Confirm to update voucher?";
    }

    const confirm = this.alertCtrl.create({
      title: title,
      message: message,
      buttons: [
        {
          text: "Cancel",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Confirm",
          handler: () => {
            if (this.mode == "create") {
              _createSellerVouchers();
            } else if (this.mode == "update") {
              _updateSellerVouchers();
            }
          }
        }
      ]
    });
    confirm.present();
  }
}
