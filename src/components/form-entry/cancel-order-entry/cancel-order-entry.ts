import {
	Component,
	Input,
	Output,
	EventEmitter,
	ViewEncapsulation,
	OnInit
} from "@angular/core";
import {
	NavController,
	NavParams,
	LoadingController,
	AlertController,
	ToastController,
	ActionSheetController,
	Platform
} from "ionic-angular";
import * as _ from "lodash";

import { OrdersServiceProvider } from "../../../providers/service/orders";

@Component({
	selector: "cancel-order-entry",
	encapsulation: ViewEncapsulation.None,
	templateUrl: "cancel-order-entry.html"
})
export class CancelOrderEntryComponent implements OnInit {
	@Input() reasonArr: any[] = [];
	@Input() order: any = {};
	@Input() create = new EventEmitter<any>();

	reason: any = {};
	callback: any;

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public alertCtrl: AlertController,
		public loadingCtrl: LoadingController,
		public orders: OrdersServiceProvider
	) {
		this.callback = navParams.get("callback");
	}

	ngOnInit() {
		this.create.subscribe(data => {
			this.cancelOrder();
		});
	}

	selectCancelReason(item: any) {
		_.each(this.reasonArr, (row: any) => {
			if (row.id == item.id) {
				this.reason = row.reason;
				row.selected = true;
			} else {
				row.selected = false;
			}
		});
	}

	private cancelOrder() {
		let vm = this;

		const _cancelOrder = () => {
			let loading = vm.loadingCtrl.create({
				dismissOnPageChange: true
			});
			loading.present();

			let cancelledRequestDeadline = new Date();
			cancelledRequestDeadline.setDate(
				cancelledRequestDeadline.getDate() + 2
			);

			let cancelObj: any = {};
			cancelObj.cancelledBy = "buyer";
			cancelObj.cancelledReason = vm.reason;

			if (
				vm.order.isPrepared == 0 &&
				_.isEmpty(vm.order.preparedDateTime)
			) {
				cancelObj.cancelledDateTime = new Date();
			} else {
				cancelObj.cancelledRequested = new Date();
				cancelObj.cancelledRequestDeadline = new Date(
					cancelledRequestDeadline
				);
			}

			vm.orders.cancelOrderRequest(vm.order.orderId, cancelObj).then(
				(data: any) => {
					if (data && data.success) {
						vm.callback("save").then(() => {
							vm.navCtrl.popTo(
								vm.navCtrl.getByIndex(vm.navCtrl.length() - 4)
							);
						});
					}
					loading.dismiss();
				},
				error => {
					console.log("error: ", error);
					loading.dismiss();
				}
			);
		};

		if (_.isEmpty(this.reason)) {
			this.alertCtrl
				.create({
					title: "WARNING",
					message:
						"Please select atleast one reason for cancellation.",
					buttons: ["OK"]
				})
				.present();
			return;
		}

		let confirmMessage: string = "";
		if (
			this.order.isCOD == 1 &&
			this.order.isPaid == 0 &&
			_.isEmpty(this.order.checkpoint)
		) {
			confirmMessage =
				"Your order will be cancelled immediately. Confirm to cancel this order?";
		} else if (
			this.order.isCOD == 1 &&
			this.order.isPaid == 0 &&
			!_.isEmpty(this.order.checkpoint)
		) {
			confirmMessage =
				"Your payment for this order will be fully refunded to your Wallet immediately. Confirm to cancel this order?";
		} else if (
			this.order.isCOD == 0 &&
			this.order.isPaid == 1 &&
			_.isEmpty(this.order.checkpoint)
		) {
			confirmMessage =
				"Your cancellation will be subject to the seller's approval, as the order is currently being processed. Confirm your request to cancel this order?";
		} else if (
			this.order.isCOD == 0 &&
			this.order.isPaid == 1 &&
			!_.isEmpty(this.order.checkpoint)
		) {
			confirmMessage =
				"Your cancellation will be subject to the seller's approval, as the order is currently being processed. Confirm your request to cancel this order?";
		}

		const confirm = this.alertCtrl.create({
			message: confirmMessage,
			buttons: [
				{
					text: "Not Now",
					handler: () => {
						console.log("Disagree clicked");
					}
				},
				{
					text: "Confirm",
					handler: () => {
						_cancelOrder();
					}
				}
			]
		});
		confirm.present();
	}
}
