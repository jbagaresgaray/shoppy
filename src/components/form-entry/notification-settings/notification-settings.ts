import { Component,Input, Output,EventEmitter, ViewEncapsulation } from '@angular/core';

/**
 * Generated class for the NotificationSettingsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'notification-settings',
  encapsulation: ViewEncapsulation.None,
  templateUrl: 'notification-settings.html'
})
export class NotificationSettingsComponent {

  @Input() isPushNotif: boolean;
  @Input() isEmailNotif: boolean;
  @Output() _togglePushNotif = new EventEmitter<any>();

  constructor() {
    
  }

  togglePushNotif(){
  	this._togglePushNotif.emit();
  }
}
