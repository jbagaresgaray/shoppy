import {
	Component,
	Input,
	Output,
	ViewEncapsulation,
	EventEmitter,
	OnInit,
	NgZone
} from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import {
	NavController,
	NavParams,
	LoadingController,
	AlertController,
	ToastController,
	ActionSheetController,
	Platform,
	normalizeURL
} from "ionic-angular";
import * as _ from "lodash";
import * as async from "async";

import { environment } from "@app/env";

import { Camera, CameraOptions } from "@ionic-native/camera";
import { File, IFile, FileEntry } from "@ionic-native/file";
import { FileTransfer, FileTransferObject } from "@ionic-native/file-transfer";

import { AllFormEntryPage } from "../../../pages/all-form-entry/all-form-entry";

import { OrdersServiceProvider } from "../../../providers/service/orders";
import { SellerServiceProvider } from "../../../providers/service/seller";

@Component({
	selector: "return-order-shipment",
	encapsulation: ViewEncapsulation.None,
	templateUrl: "return-order-shipment.html"
})
export class ReturnOrderShipmentComponent implements OnInit {
	@Input() order: any = {};
	@Input() create = new EventEmitter<any>();

	return: any = {};
	callback: any;

	shippingArr: any[] = [];

	public serverUrl = environment.api_url + "orders";
	public isUploading = false;
	public uploadingProgress = {};
	public uploadingHandler = {};
	public images: any = [];
	public filesToUpload: Array<File> = [];
	protected imagesValue: Array<any>;

	constructor(
		private zone: NgZone,
		private sanitization: DomSanitizer,
		public navCtrl: NavController,
		public navParams: NavParams,
		public loadingCtrl: LoadingController,
		public alertCtrl: AlertController,
		public actionSheetCtrl: ActionSheetController,
		public platform: Platform,
		public toastCtrl: ToastController,
		private camera: Camera,
		private file: File,
		private transfer: FileTransfer,
		public orders: OrdersServiceProvider,
		public sellers: SellerServiceProvider
	) {}

	ngOnInit() {
		this.create.subscribe(()=>{
			this.saveOrderReturnShipping();
		});

		if (this.order && this.order.order_return_info) {
			this.sellers
				.getAllUserSellerShipping(this.order.order_return_info.sellerId)
				.then(
					(data: any) => {
						if (data && data.success) {
							_.each(data.result, (row: any) => {
								row.selected = false;
							});
							this.shippingArr = data.result;
						}
					},
					error => {
						console.log("getAllUserSellerShipping: ", error);
					}
				);
		}
	}

	// ======================================================================
	// ======================================================================
	// ======================================================================

	protected removeImage(image) {
		if (this.isUploading) {
			return;
		}

		this.confirm("Are you sure to remove it?").then(value => {
			if (value) {
				this.removeFromArray(this.imagesValue, image);
				_.remove(this.images, {
					uuid: image.uuid
				});

				console.log("this.images: ", this.images);
				console.log("this.imagesValue: ", this.imagesValue);
			}
		});
	}

	protected showAddImage() {
		let uuid = parseInt(new Date().getTime().toString());

		if (!window["cordova"]) {
			let input = document.createElement("input");
			input.type = "file";
			input.accept = "image/x-png,image/gif,image/jpeg";
			input.click();
			input.onchange = () => {
				let blob = window.URL.createObjectURL(input.files[0]);
				let blob2: any = input.files[0];
				this.images.push({
					url: blob,
					uuid: uuid
				});
				this.filesToUpload.push(blob2);
				console.log("this.filesToUpload: ", this.filesToUpload);
				this.trustImages();
			};
		} else {
			new Promise((resolve, reject) => {
				let actionSheet = this.actionSheetCtrl.create({
					title: "Add a photo",
					buttons: [
						{
							text: "From photo library",
							handler: () => {
								resolve(
									this.camera.PictureSourceType.PHOTOLIBRARY
								);
							}
						},
						{
							text: "From camera",
							handler: () => {
								resolve(this.camera.PictureSourceType.CAMERA);
							}
						},
						{
							text: "Cancel",
							role: "cancel",
							handler: () => {
								reject();
							}
						}
					]
				});
				actionSheet.present();
			})
				.then(sourceType => {
					if (!window["cordova"]) return;
					let options: CameraOptions = {
						quality: 100,
						sourceType: sourceType as number,
						saveToPhotoAlbum: false,
						correctOrientation: true
					};
					this.camera.getPicture(options).then(imagePath => {
						console.log("CameraOptions imagePath: ", imagePath);
						this.file.resolveLocalFilesystemUrl(imagePath).then(
							(entry: FileEntry) => {
								entry.file(
									(meta: IFile) => {
										console.log("file: ", meta);
										if (this.platform.is("ios")) {
											this.images.push({
												url: normalizeURL(imagePath),
												file: meta,
												size: meta.size,
												name: meta.name,
												uuid: uuid
											});
										} else {
											this.images.push({
												url: imagePath,
												file: meta,
												size: meta.size,
												name: meta.name,
												uuid: uuid
											});
										}
										this.filesToUpload.push(imagePath);
										this.trustImages();
									},
									error => {
										console.error(
											"error getting fileentry file!",
											error
										);
									}
								);
							},
							error => {
								console.error("error getting file! ", error);
							}
						);
					});
				})
				.catch(() => {});
		}
	}

	private removeFromArray<T>(array: Array<T>, item: T) {
		let index: number = array.indexOf(item);
		if (index !== -1) {
			array.splice(index, 1);
		}
	}

	private confirm(text, title = "", yes = "Yes", no = "No") {
		return new Promise(resolve => {
			this.alertCtrl
				.create({
					title: title,
					message: text,
					buttons: [
						{
							text: no,
							role: "cancel",
							handler: () => {
								resolve(false);
							}
						},
						{
							text: yes,
							handler: () => {
								resolve(true);
							}
						}
					]
				})
				.present();
		});
	}

	private trustImages() {
		this.zone.run(() => {
			this.imagesValue = this.images.map(val => {
				return {
					url: val.url,
					uuid: val.uuid,
					sanitized: this.sanitization.bypassSecurityTrustStyle(
						"url(" + val.url + ")"
					)
				};
			});
		});
		console.log("this.imagesValue: ", this.imagesValue);
	}

	private showToast(text: string) {
		this.toastCtrl
			.create({
				message: text,
				duration: 5000,
				position: "bottom"
			})
			.present();
	}

	private uploadImage(targetPath, uploadUrl) {
		return new Promise((resolve, reject) => {
			this.uploadingProgress[targetPath] = 0;
			console.log("targetPath: ", targetPath);
			if (this.platform.is("cordova")) {
				let options: any = {
					fileKey: "image",
					chunkedMode: false,
					httpMethod: "POST",
					headers: {
						Authorization:
							"Bearer " + localStorage.getItem("app.token")
					}
				};

				const fileTransfer: FileTransferObject = this.transfer.create();
				this.uploadingHandler[targetPath] = fileTransfer;
				fileTransfer
					.upload(targetPath, uploadUrl, options, false)
					.then(data => {
						resolve(JSON.parse(data.response));
					})
					.catch(error => {
						console.log("error: ", error);
						if (error) {
							let err = JSON.parse(error.body);
							console.log("err: ", err);
						}
						askRetry();
					});

				fileTransfer.onProgress(event2 => {
					this.uploadingProgress[targetPath] =
						(event2.loaded * 100) / event2.total;
				});
			} else {
				let formData: FormData = new FormData(),
					xhr2: XMLHttpRequest = new XMLHttpRequest();

				formData.append("image", targetPath);
				this.uploadingHandler[targetPath] = xhr2;

				xhr2.onreadystatechange = () => {
					if (xhr2.readyState === 4) {
						if (xhr2.status === 200)
							resolve(JSON.parse(xhr2.response));
						else askRetry();
					}
				};

				xhr2.upload.onprogress = event => {
					this.uploadingProgress[targetPath] =
						(event.loaded * 100) / event.total;
				};
				xhr2.open("POST", uploadUrl, true);
				xhr2.setRequestHeader(
					"Authorization",
					"Bearer " + localStorage.getItem("app.token")
				);
				xhr2.send(formData);
			}

			let askRetry = () => {
				// might have been aborted
				if (!this.isUploading) return reject(null);
				this.confirm("Do you wish to retry?", "Upload failed").then(
					res => {
						if (!res) {
							this.isUploading = false;
							for (let key in this.uploadingHandler) {
								this.uploadingHandler[key].abort();
							}
							return reject(null);
						} else {
							if (!this.isUploading) return reject(null);
							this.uploadImage(targetPath, uploadUrl).then(
								resolve,
								reject
							);
						}
					}
				);
			};
		});
	}

	private uploadImages(uploadUrl): Promise<Array<any>> {
		return new Promise((resolve, reject) => {
			this.isUploading = true;
			Promise.all(
				this.filesToUpload.map((image: any) => {
					return this.uploadImage(image, uploadUrl);
				})
			)
				.then(() => {
					resolve();
					this.isUploading = false;
				})
				.catch((reason: any) => {
					console.log("reasons: ", reason);
					this.isUploading = false;
					this.uploadingProgress = {};
					this.uploadingHandler = {};
					// reject(reason);
				});
		});
	}

	// ======================================================================
	// ======================================================================
	// ======================================================================

	setReturnShippingCarrier() {
		this.navCtrl.push(AllFormEntryPage, {
			action: "return_shipment_shipping",
			orders: this.order,
			return: this.return,
			tmpShippingArr: this.shippingArr,
			callback: this.updateOrderShipmentShipping
		});
	}

	private updateOrderShipmentShipping(resp) {
		let ctrl = this;
		return new Promise((resolve, reject) => {
			console.log("updateOrderShipmentShipping: ", resp);
			if (resp) {
				this.return.returnCarrierId = resp.shippingId;
				this.return.returnShippingCarrier = resp;
			}
			console.log("this.return: ", this.return);
			resolve();
		});
	}

	private saveOrderReturnShipping() {
		let vm = this;

		function saveOrderReturnShipping() {
			let loading = vm.loadingCtrl.create({
				dismissOnPageChange: true
			});
			loading.present();

			let uploadUrl =
				vm.serverUrl +
				"/" +
				vm.order.orderId +
				"/return/shipment_files";

			console.log("vm.return: ", vm.return);
			async.waterfall(
				[
					callback => {
						vm.orders
							.returnOrderRequestShipment(
								vm.order.orderId,
								vm.return
							)
							.then(
								(data: any) => {
									if (data && data.success) {
										callback();
									} else {
										loading.dismiss();
									}
								},
								(error: any) => {
									console.log("error: ", error);
									loading.dismiss();
								}
							);
					},
					callback => {
						vm.uploadImages(uploadUrl).then(
							() => {
								callback();
							},
							(error: any) => {
								loading.dismiss();
							}
						);
					}
				],
				() => {
					loading.dismiss();
					vm.callback("save").then(() => {
						vm.navCtrl.popTo(
							vm.navCtrl.getByIndex(vm.navCtrl.length() - 3)
						);
					});
				}
			);
		}

		if (_.isNull(this.return.returnCarrierId)) {
			this.alertCtrl
				.create({
					title: "WARNING",
					message: "Please provide Return shipping carrier.",
					buttons: ["OK"]
				})
				.present();
			return;
		}

		if (_.isNull(this.return.returnTrackingNumber)) {
			this.alertCtrl
				.create({
					title: "WARNING",
					message: "Please provide Return shipping tracking number.",
					buttons: ["OK"]
				})
				.present();
			return;
		}

		if (_.isEmpty(this.imagesValue)) {
			this.alertCtrl
				.create({
					title: "WARNING",
					message:
						"Please provide photos of the product/item to be returned for evaluation purposes.",
					buttons: ["OK"]
				})
				.present();
			return;
		}

		const confirm = this.alertCtrl.create({
			message:
				"Confirm your request to ship out this order return requested product?",
			buttons: [
				{
					text: "Not Now",
					handler: () => {
						console.log("Disagree clicked");
					}
				},
				{
					text: "Confirm",
					handler: () => {
						saveOrderReturnShipping();
					}
				}
			]
		});
		confirm.present();
	}
}
