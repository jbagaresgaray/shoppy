import { Component, Input, OnInit, ViewEncapsulation } from "@angular/core";
/**
 * Generated class for the ContentProductsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
	selector: "content-products",
	encapsulation: ViewEncapsulation.None,
	templateUrl: "content-products.html"
})
export class ContentProductsComponent implements OnInit {
	@Input() item: any = {};
	@Input() currency: any = {};
	@Input() showBadge: boolean = true;

	constructor() {}

	ngOnInit() {}
}
