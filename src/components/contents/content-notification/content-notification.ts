import { Component, Input, Output, EventEmitter, OnInit } from "@angular/core";
import { Platform, NavController } from "ionic-angular";
import * as _ from "lodash";
import { ContentFormPage } from "../../../pages/content-form/content-form";

import { NotificationsServiceProvider } from "../../../providers/service/notifications";

declare let safari: any;

@Component({
  selector: "content-notification",
  templateUrl: "content-notification.html"
})
export class ContentNotificationComponent {
  @Input() params: any[] = [];
  @Input() fakeArr: any[] = [];
  @Input() action: string;

  @Input() refresh: EventEmitter<any>;

  showContent: boolean;
  isSafari: boolean;

  constructor(
    public platform: Platform,
    public navCtrl: NavController,
    public notifications: NotificationsServiceProvider
  ) {
    this.isSafari =
      /constructor/i.test(window["HTMLElement"]) ||
      (function(p) {
        return p.toString() === "[object SafariRemoteNotification]";
      })(!window["safari"] || safari.pushNotification);
  }

  private initNotifications(ev?: any) {
    this.showContent = false;
    this.params = [];
    this.notifications.getUpdatesNotifications().then(
      (data: any) => {
        if (data && data.success) {
          let notification = data.result;
          if (this.action == "notification") {
            let promotions = _.filter(notification, {
              notifTypeId: 1
            });
            console.log("promotions: ", promotions);

            this.params = promotions;
          } else if (this.action == "sellers") {
            const sellerupdates = _.filter(notification, {
              notifTypeId: 2
            });
            const sellerupdates2 = _.filter(notification, {
              notifTypeId: 6
            });

            let newsellerupdates: any[] = [];
            _.each(sellerupdates, (row: any) => {
              newsellerupdates.push(row);
            });
            _.each(sellerupdates2, (row: any) => {
              newsellerupdates.push(row);
            });

            newsellerupdates = _.orderBy(
              newsellerupdates,
              ["datetime"],
              ["desc"]
            );
            _.each(newsellerupdates, (row: any) => {
              if (this.platform.is("ios")) {
                if (this.isSafari) {
                  row.datetime = new Date(row.datetime.replace(/-/g, "/"));
                } else {
                  row.datetime = new Date(row.datetime.replace(/-/g, "/"));
                }
              }
            });
            this.params = newsellerupdates;
          }
          // let appupdates = _.filter(notification, { 'notifTypeId': 4, 'isRead': 0 });
        }

        this.showContent = true;
        if (ev) {
          ev.complete();
        }
      },
      error => {
        console.log("notification error: ", error);
        this.showContent = false;
        if (ev) {
          ev.complete();
        }
      }
    );
  }

  ngOnInit() {
    this.refresh.subscribe(data => {
      this.initNotifications(data);
    });

    this.initNotifications();
  }

  viewDetails(item) {
    this.navCtrl.push(ContentFormPage, {
      action: "newsletter",
      params: item,
      title: item.title
    });
  }
}
