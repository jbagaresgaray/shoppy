import {
  Component,
  ViewEncapsulation,
  Input,
  EventEmitter,
  OnInit
} from "@angular/core";
import {
  NavController,
  NavParams,
  LoadingController,
  ModalController,
  AlertController
} from "ionic-angular";
import * as _ from "lodash";

import { AllFormEntryPage } from "../../../pages/all-form-entry/all-form-entry";
import { SellerServiceProvider } from "../../../providers/service/seller";

@Component({
  selector: "content-seller-toppicks",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "content-seller-toppicks.html"
})
export class ContentSellerToppicksComponent implements OnInit {
  images: any[] = [];
  collectionsArr: any[] = [];
  user: any = {};

  showLoading: boolean;

  @Input() refresh: EventEmitter<any>;
  @Input() create: EventEmitter<any>;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public sellerService: SellerServiceProvider
  ) {}

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem("app.user")) || {};

    for (let i = 0; i < 8; ++i) {
      this.images.push(i);
    }

    this.initData();

    this.refresh.subscribe(data => {
      this.initData(data);
    });

    this.create.subscribe(data => {
      this.createTopPick();
    });
  }

  private initData(ev?: any) {
    this.showLoading = true;
    this.sellerService.getSellerAllTopPickCollection(this.user.userId).then(
      (data: any) => {
        if (data && data.success) {
          const collectionsArr = data.result.result;
          this.collectionsArr = collectionsArr;
          _.each(this.collectionsArr, (row: any) => {
            row.isActive = row.isActive == 1 ? true : false;
          });
          console.log("collections: ", collectionsArr);
        }
        this.showLoading = false;
        if (ev) {
          ev.complete();
        }
      },
      error => {
        this.showLoading = false;
        console.log("error: ", error);
        if (ev) {
          ev.complete();
        }
      }
    );
  }

  private getRefresh = resp => {
    return new Promise((resolve, reject) => {
      if (resp == "save") {
        this.initData(null);
        resolve();
      }
    });
  };

  createTopPick() {
    this.navCtrl.push(AllFormEntryPage, {
      action: "seller_toppicks",
      mode: "create",
      callback: this.getRefresh
    });
  }

  updateTopPick(item: any, ev: any) {
    ev.preventDefault();
    ev.stopPropagation();

    this.navCtrl.push(AllFormEntryPage, {
      action: "seller_toppicks",
      mode: "update",
      collection: item,
      callback: this.getRefresh
    });
  }

  activateCollection(item: any, ev: any) {
    let title: string;
    let subTitle: string;
    let isActivate: boolean;

    const result = _.find(this.collectionsArr, { isActive: true });
    console.log("result: ", result);

    if (result) {
      if (item.isActive === false) {
        title = "Deactivate Collection?";
        subTitle =
          "Are you sure you want to close this Collection? All your information will be lost.";
        isActivate = false;
      } else {
        title = "Activate Collection?";
        subTitle = "You can only activate one Collection at a time?";
        isActivate = true;
      }
    } else {
      if (item.isActive === false) {
        title = "Deactivate Collection?";
        subTitle =
          "Are you sure you want to close this Collection? All your information will be lost.";
        isActivate = false;
      } else {
        title = "Activate Collection?";
        subTitle = "Activate this collection?";
        isActivate = true;
      }
    }

    const _activateCollection = () => {
      const loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      loading.present();
      this.sellerService
        .activateTopPickCollection(this.user.userId, item.collectionId)
        .then(
          (data: any) => {
            if (data && data.success) {
              this.initData(null);
            }
            loading.dismiss();
          },
          error => {
            console.log("error: ", error);
            loading.dismiss();
            item.isActive = false;
          }
        );
    };

    const _deActivateCollection = () => {
      const loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      loading.present();
      this.sellerService
        .deActivateTopPickCollection(this.user.userId, item.collectionId)
        .then(
          (data: any) => {
            if (data && data.success) {
              this.initData(null);
            }
            loading.dismiss();
          },
          error => {
            console.log("error: ", error);
            loading.dismiss();
            item.isActive = false;
          }
        );
    };

    const confirm = this.alertCtrl.create({
      title: title,
      message: subTitle,
      buttons: [
        {
          text: "Cancel",
          handler: () => {
            console.log("Disagree clicked");
            if (item.isActive == false) {
              item.isActive = true;
            } else {
              item.isActive = false;
            }
          }
        },
        {
          text: "Confirm",
          handler: () => {
            if (isActivate) {
              _activateCollection();
            } else {
              _deActivateCollection();
            }
          }
        }
      ]
    });

    confirm.present();
  }
}
