import {
  Component,
  ViewChild,
  Input,
  OnInit,
  EventEmitter,
  ViewEncapsulation
} from "@angular/core";
import {
  NavController,
  NavParams,
  LoadingController,
  AlertController,
  Content
} from "ionic-angular";
import * as _ from "lodash";

import { SearchresultPage } from "../../../pages/searchresult/searchresult";

import { BrandsServiceProvider } from "../../../providers/service/brands";

@Component({
  selector: "page-brands",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "brands.html"
})
export class BrandsPage {
  brand: any = {};
  @Input() selected: any = {};
  brandsArr: any[] = [];
  brandsArrCopy: any[] = [];
  fakeArr: any[] = [];

  brandName: string;

  @Input() isProduct: boolean = false;
  @Input() callback: any;

  isSaving: boolean = false;
  isLoading: boolean = true;

  @Input() clearItems: EventEmitter<boolean>;
  @Input() _getItems: EventEmitter<any>;
  @Input() refresh: EventEmitter<any>;

  @ViewChild(Content) content: Content;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public brands: BrandsServiceProvider
  ) {
    if (this.navParams.get("isProduct")) {
      this.isProduct = this.navParams.get("isProduct");
      this.selected = this.navParams.get("selected");
      this.callback = this.navParams.get("callback");

      console.log("this.selected: ", this.selected);
    }

    this.brandName = "";
    this.isSaving = false;

    for (let index = 0; index < 20; index++) {
      this.fakeArr.push(index);
    }
  }

  initData(ev?: any) {
    this.isLoading = true;
    this.brands.getAllBrands().then(
      (data: any) => {
        if (data && data.success) {
          this.brandsArr = data.result;
          _.each(this.brandsArr, (row: any) => {
            if (this.selected) {
              if (row.brandId == this.selected.brandId) {
                row.opened = true;
              } else {
                row.opened = false;
              }
            } else {
              row.opened = false;
            }
          });
          this.brandsArrCopy = _.clone(this.brandsArr);
          console.log("brandsArr: ", this.brandsArr);
          // this.content.resize();
        }
        this.isLoading = false;
        if (ev) {
          ev.complete();
        }
      },
      error => {
        this.isLoading = false;
        if (ev) {
          ev.complete();
        }
      }
    );
  }

  ngOnInit() {
    console.log("ionViewDidLoad BrandsPage");
    this.initData();

    this.refresh.subscribe(data => {
      this.initData(data);
    });

    this.clearItems.subscribe(data => {
      this.onClear();
    });

    this._getItems.subscribe(data => {
      console.log("brandGetItems: ", data);
      this.getItems(data);
    });
  }

  doRefresh(ev) {
    console.log("ionViewDidLoad CategoriesPage");
    this.initData(ev);
  }

  viewBrands(item) {
    console.log("viewBrands");
    if (!this.isProduct) {
      this.navCtrl.push(SearchresultPage, {
        action: "brands",
        item: item
      });
    } else {
      if (this.isSaving) {
        return;
      }

      this.brand = item;
      _.each(this.brandsArr, (row: any) => {
        if (row.brandId == this.brand.brandId) {
          row.opened = true;
        } else {
          row.opened = false;
        }
      });
      this.callback(this.brand).then(() => {
        this.navCtrl.pop();
      });
    }
  }

  addBrand() {
    console.log("brandName: ", this.brandName);
    this.isSaving = true;
    this.brands
      .createBrand({
        name: this.brandName
      })
      .then(
        (data: any) => {
          console.log("createBrand: ", data);
          this.isSaving = false;

          if (data && data.success) {
            this.callback({
              brandId: data.result,
              name: this.brandName,
              opened: true
            }).then(() => {
              this.navCtrl.pop();
            });
          } else if (data && !data.success) {
            this.alertCtrl
              .create({
                title: "WARNING",
                message: data.msg,
                buttons: ["OK"]
              })
              .present();
            return;
          }
        },
        (error: any) => {
          console.log("error: ", error);
          this.isSaving = false;
          if (error) {
            let err = error.data;
            let errMsg = "<ul>";
            _.each(err.result, (row: any) => {
              errMsg = errMsg + "<li>" + row.msg + "</li>";
            });
            errMsg = errMsg + "</ul>";
            console.log("errMsg: ", errMsg);

            this.alertCtrl
              .create({
                title: "WARNING",
                message: errMsg,
                buttons: ["OK"]
              })
              .present();
          }
        }
      );
  }

  getItems(ev) {
    const val = ev.target.value;

    if (val && val.trim() != "") {
      this.brandsArr = this.brandsArrCopy.filter(item => {
        return item.name.toLowerCase().indexOf(val.toLowerCase()) > -1;
      });
    } else {
      this.brandsArr = this.brandsArrCopy;
    }
  }

  onClear() {
    this.brandsArr = [];
    this.brandsArr = this.brandsArrCopy;
  }
}
