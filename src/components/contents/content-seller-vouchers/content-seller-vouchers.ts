import {
  Component,
  ViewEncapsulation,
  Input,
  EventEmitter,
  OnInit
} from "@angular/core";
import {
  NavController,
  NavParams,
  LoadingController,
  ModalController,
  AlertController
} from "ionic-angular";
import * as _ from "lodash";
import * as moment from "moment";

import { AllFormEntryPage } from "../../../pages/all-form-entry/all-form-entry";
import { ContentFormPage } from "../../../pages/content-form/content-form";

import { SellerVouchersService } from "../../../providers/service/seller-vouchers";
import { ServiceProvider } from "../../../providers/service/service";

@Component({
  selector: "content-seller-vouchers",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "content-seller-vouchers.html"
})
export class ContentSellerVouchersComponent implements OnInit {
  @Input() refresh: EventEmitter<any>;
  @Input() create: EventEmitter<any>;

  user: any = {};
  currency: any = {};
  vouchersArr: any[] = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public sellerVouchers: SellerVouchersService,
    public serviceProvider: ServiceProvider
  ) {}

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem("app.user")) || {};
    this.currency = this.serviceProvider.defaultCurrency();
    this.refresh.subscribe(data => {
      this.initData(data);
    });

    this.create.subscribe(data => {
      this.createVouchers();
    });

    this.initData(null);
  }

  private initData(ev?: any) {
    const loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    loading.dismiss();
    const dateNow = moment(new Date());
    this.sellerVouchers.getSellerVouchers(this.user.uuid).then(
      (data: any) => {
        if (data && data.success) {
          this.vouchersArr = data.result;
          _.each(this.vouchersArr, (row: any) => {
            row.voucherFrom = moment(row.voucherFrom).format("YYYY-MM-DD");
            row.voucherTo = moment(row.voucherTo).format("YYYY-MM-DD");

            const ongoing = dateNow.isBetween(row.voucherFrom, row.voucherTo);
            const upcoming = dateNow.isBefore(row.voucherFrom);
            const expired = dateNow.isAfter(row.voucherTo);
            if (ongoing) {
              row.status = "ongoing";
            }
            if (expired) {
              row.status = "expired";
            }
            if (upcoming) {
              row.status = "upcoming";
            }
          });
        }
        loading.dismiss();
        console.log("this.vouchersArr: ", this.vouchersArr);
      },
      error => {
        loading.dismiss();
        console.log("error: ", error);
      }
    );
  }

  private getRefresh = resp => {
    return new Promise((resolve, reject) => {
      if (resp == "save") {
        this.initData(null);
        resolve();
      }
    });
  };

  createVouchers() {
    this.navCtrl.push(AllFormEntryPage, {
      action: "seller_vouchers",
      mode: "create",
      callback: this.getRefresh
    });
  }

  viewDetails(item: any) {
    this.navCtrl.push(ContentFormPage, {
      action: "seller_vouchers_details",
      params: item,
      callback: this.getRefresh
    });
  }
}
