import {
	Component,
	Input,
	Output,
	EventEmitter,
	OnInit,
	ViewEncapsulation
} from "@angular/core";
import {
	Platform,
	NavController,
	LoadingController,
	AlertController
} from "ionic-angular";
import * as async from "async";
import * as _ from "lodash";

import { DetailsPage } from "../../../pages/details/details";
import { ShopprofilePage } from "../../../pages/shopprofile/shopprofile";
import { NotificationsServiceProvider } from "../../../providers/service/notifications";

declare let safari: any;

@Component({
	selector: "content-seller-mode",
	encapsulation: ViewEncapsulation.None,
	templateUrl: "content-seller-mode.html"
})
export class ContentSellerModeComponent {
	@Input() params: any[] = [];
	@Input() fakeArr: any[] = [];
	@Input() refresh: EventEmitter<any>;

	isSafari: boolean;
	showContent: boolean;

	constructor(
		public platform: Platform,
		public navCtrl: NavController,
		public loadingCtrl: LoadingController,
		public alertCtrl: AlertController,
		public notifications: NotificationsServiceProvider
	) {
		this.isSafari =
			/constructor/i.test(window["HTMLElement"]) ||
			(function(p) {
				return p.toString() === "[object SafariRemoteNotification]";
			})(!window["safari"] || safari.pushNotification);
	}

	private initNotifications(ev?: any) {
		this.showContent = false;
		this.params = [];
		this.notifications.getUpdatesNotifications().then(
			(data: any) => {
				if (data && data.success) {
					let notification = data.result;
					const sellerupdates = _.filter(notification, {
						notifTypeId: 2
					});
					const sellerupdates2 = _.filter(notification, {
						notifTypeId: 6
					});

					let newsellerupdates: any[] = [];
					_.each(sellerupdates, (row: any) => {
						newsellerupdates.push(row);
					});
					_.each(sellerupdates2, (row: any) => {
						newsellerupdates.push(row);
					});

					newsellerupdates = _.orderBy(
						newsellerupdates,
						["datetime"],
						["desc"]
					);
					_.each(newsellerupdates, (row: any) => {
						if (this.platform.is("ios")) {
							if (this.isSafari) {
								row.datetime = new Date(
									row.datetime.replace(/-/g, "/")
								);
							} else {
								row.datetime = new Date(
									row.datetime.replace(/-/g, "/")
								);
							}
						}
					});
					this.params = newsellerupdates;
				}

				this.showContent = true;
				if (ev) {
					ev.complete();
				}
			},
			error => {
				console.log("notification error: ", error);
				this.showContent = false;
				if (ev) {
					ev.complete();
				}
			}
		);
	}

	ngOnInit() {
		this.refresh.subscribe(data => {
			this.initNotifications(data);
		});

		this.initNotifications();
	}

	viewDetails(item) {
		const info: any = JSON.parse(item.link) || {};
		console.log("sellers info: ", info);
		// Na View nah
		item.isRead = true;
		const loading = this.loadingCtrl.create({
			dismissOnPageChange: true
		});
		loading.present();
		async.waterfall([
			callback => {
				if (item.isRead !== 1) {
					this.notifications
						.updateNotification(item.notificationId)
						.then((data: any) => {
							if (data && data.success) {
								callback();
							}
						});
				} else {
					callback();
				}
			},
			callback => {
				loading.dismiss();
				if (info.action == "like") {
					this.navCtrl.push(DetailsPage, {
						product: {
							productId: info.productId,
							userId: info.storeUserId
						}
					});
				} else {
					this.navCtrl.push(ShopprofilePage, {
						profile: item
					});
				}
			}
		]);
	}

	deleteUserNotification(notifId) {
		const deleteNotification = notifId => {
			let loading = this.loadingCtrl.create({
				dismissOnPageChange: true,
				content: "Deleting..."
			});
			loading.present();
			this.notifications.deleteNotification(notifId).then(
				(data: any) => {
					if (data && data.success) {
						setTimeout(() => {
							this.initNotifications(null);
						}, 300);
					}
					loading.dismiss();
				},
				error => {
					console.log("deleteNotification Error: ", error);
					loading.dismiss();
				}
			);
		};

		const confirm = this.alertCtrl.create({
			title: "Delete Notification?",
			message: "Are you sure to delete this notification?",
			buttons: [
				{
					text: "Cancel",
					handler: () => {
						console.log("Disagree clicked");
					}
				},
				{
					text: "Confirm",
					handler: () => {
						console.log("Agree clicked");
						deleteNotification(notifId);
					}
				}
			]
		});
		confirm.present();
	}
}
