import {
  Component,
  OnInit,
  ViewEncapsulation,
  Input,
  EventEmitter
} from "@angular/core";
import {
  NavController,
  NavParams,
  LoadingController,
  AlertController
} from "ionic-angular";
import * as _ from "lodash";

import { SellerVouchersService } from "../../../providers/service/seller-vouchers";
import { ServiceProvider } from "../../../providers/service/service";
import { AllFormEntryPage } from "../../../pages/all-form-entry/all-form-entry";

@Component({
  selector: "content-seller-vouchers-detail",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "content-seller-vouchers-detail.html"
})
export class ContentSellerVouchersDetailComponent implements OnInit {
  voucher: any = {};
  _voucher: any = {};
  currency: any = {};
  user: any = {};

  callback: any;

  @Input() mode: string;
  @Input() refresh: EventEmitter<any>;
  @Input() create: EventEmitter<any>;
  @Input() delete: EventEmitter<any>;

  constructor(
    public navParams: NavParams,
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public sellerVouchers: SellerVouchersService,
    public serviceProvider: ServiceProvider
  ) {
    this.user = JSON.parse(localStorage.getItem("app.user")) || {};
    this.currency = this.serviceProvider.defaultCurrency();
    this._voucher = this.navParams.get("params");
    this.callback = this.navParams.get("callback");
    console.log("this.mode: ", this.mode);
  }

  ngOnInit() {
    this.create.subscribe(() => {});

    this.refresh.subscribe(ev => {
      this.initData(ev);
    });

    this.delete.subscribe(() => {
      this.deleteVoucher();
    });

    this.initData();
  }

  private initData(ev?: any) {
    this.sellerVouchers
      .getSellerVoucherDetail(this.user.userId, this._voucher.voucherId)
      .then(
        (data: any) => {
          if (data && data.success) {
            this.voucher = data.result;
          }
          console.log("this.voucher: ", this.voucher);
          if (ev) {
            ev.complete();
          }
        },
        error => {
          console.log("error: ", error);
          if (ev) {
            ev.complete();
          }
        }
      );
  }

  private deleteVoucher() {
    const _deleteSellerVoucher = () => {
      const loading = this.loadingCtrl.create({
        dismissOnPageChange: true
      });
      loading.present();
      this.sellerVouchers
        .deleteSellerVoucherDetail(this.user.userId, this._voucher.voucherId)
        .then(
          (data: any) => {
            if (data && data.success) {
              loading.dismiss();
              const alert = this.alertCtrl.create({
                title: "SUCCESS",
                subTitle: data.msg,
                buttons: ["OK"]
              });
              alert.onDidDismiss(() => {
                this.callback("save").then(() => {
                  this.navCtrl.pop();
                });
              });
              alert.present();
            } else {
              loading.dismiss();
            }
          },
          error => {
            console.log("error: ", error);
          }
        );
    };

    const confirm = this.alertCtrl.create({
      title: "Delete this voucher?",
      message: "Confirm to remove this voucher?",
      buttons: [
        {
          text: "Cancel",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Confirm",
          handler: () => {
            _deleteSellerVoucher();
          }
        }
      ]
    });
    confirm.present();
  }

  private getRefresh = resp => {
    return new Promise((resolve, reject) => {
      if (resp == "save") {
        this.initData(null);
        resolve();
      }
    });
  };

  updateVoucher() {
    this.navCtrl.push(AllFormEntryPage, {
      action: "seller_vouchers",
      mode: "update",
      voucher: this.voucher,
      callback: this.getRefresh
    });
  }
}
