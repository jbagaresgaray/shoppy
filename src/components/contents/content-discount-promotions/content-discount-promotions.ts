import {
  Component,
  ViewEncapsulation,
  Input,
  EventEmitter,
  OnInit
} from "@angular/core";
import {
  NavController,
  NavParams,
  LoadingController,
  ModalController,
  AlertController,
  Loading
} from "ionic-angular";
import * as moment from "moment";
import * as _ from "lodash";

import { AllFormEntryPage } from "../../../pages/all-form-entry/all-form-entry";
import { DiscountPromotionsService } from "../../../providers/service/discount-promotions";

@Component({
  selector: "content-discount-promotions",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "content-discount-promotions.html"
})
export class ContentDiscountPromotionsComponent implements OnInit {
  @Input() discountMode: string;
  @Input() refresh: EventEmitter<any>;
  @Input() create: EventEmitter<any>;

  params: any = {};
  user: any = {};
  discountsArr: any[] = [];
  fakeArr: any[] = [];
  showContent: boolean;

  upcomingArr: any[] = [];
  ongoingArr: any[] = [];
  expiredArr: any[] = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public discountPromotions: DiscountPromotionsService
  ) {}

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem("app.user")) || {};
    for (let index = 0; index < 10; index++) {
      this.fakeArr.push(index);
    }

    this.params = this.navParams.get("params");
    this.refresh.subscribe(data => {
      this.initData(data);
    });

    this.create.subscribe(data => {
      this.createDiscountPromotions();
    });

    this.initData();
  }

  private initData(ev?: any) {
    this.showContent = false;
    const dateNow = moment(new Date());
    this.discountPromotions.getAllDiscountPromotion(this.params.uuid).then(
      (data: any) => {
        if (data && data.success) {
          if (ev) {
            this.discountsArr = [];
            this.ongoingArr = [];
            this.expiredArr = [];
            this.upcomingArr = [];
          }

          this.discountsArr = data.result;
          _.each(this.discountsArr, (row: any) => {
            row.event_from = moment(row.event_from).format("YYYY-MM-DD");
            row.event_to = moment(row.event_to).format("YYYY-MM-DD");

            const ongoing = dateNow.isBetween(row.event_from, row.event_to);
            const upcoming = dateNow.isBefore(row.event_from);
            const expired = dateNow.isAfter(row.event_to);
            if (ongoing) {
              row.isExpired = false;
              row.isOngoing = true;
              row.isUpcoming = false;
              this.ongoingArr.push(row);
            }
            if (expired) {
              row.isExpired = true;
              row.isOngoing = false;
              row.isUpcoming = false;
              this.expiredArr.push(row);
            }
            if (upcoming) {
              row.isExpired = false;
              row.isOngoing = false;
              row.isUpcoming = true;
              this.upcomingArr.push(row);
            }
          });
        }
        this.showContent = true;
        if (ev) {
          ev.complete();
        }
      },
      error => {
        console.log("getAllDiscountPromotion Error: ", error);
        this.showContent = true;
        if (ev) {
          ev.complete();
        }
      }
    );
  }

  private getRefresh = resp => {
    return new Promise((resolve, reject) => {
      if (resp == "save") {
        this.discountsArr = [];
        this.ongoingArr = [];
        this.expiredArr = [];
        this.upcomingArr = [];

        this.initData(null);
        resolve();
      }
    });
  };

  createDiscountPromotions() {
    this.navCtrl.push(AllFormEntryPage, {
      action: "discount_promotions",
      params: this.params,
      callback: this.getRefresh
    });
  }

  viewDiscount(item: any) {
    this.navCtrl.push(AllFormEntryPage, {
      action: "discount_promotions_products",
      params: item,
      callback: this.getRefresh
    });
  }
}
