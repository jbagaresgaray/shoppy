import {
	Component,
	Input,
	OnInit,
	ViewEncapsulation,
	EventEmitter
} from "@angular/core";
import { LoadingController } from "ionic-angular";
import * as _ from "lodash";
import { OrdersServiceProvider } from "../../../providers/service/orders";

@Component({
	selector: "content-order-tracking",
	encapsulation: ViewEncapsulation.None,
	templateUrl: "content-order-tracking.html"
})
export class ContentOrderTrackingComponent {
	@Input() params: any = {};
	@Input() refresh: EventEmitter<any>;

	trackingArr: any[] = [];

	constructor(
		public loadingCtrl: LoadingController,
		public orderServices: OrdersServiceProvider
	) {}

	private initLogisticTracking(order: any, ev?: any) {
		const loading = this.loadingCtrl.create();
		loading.present();

		const orderCreated: any = {
			slug: order.courier_slug,
			city: order.orderShipCity,
			created_at: order.orderDateTime,
			location: order.orderShipAddress,
			country_name: null,
			message: "Order Created",
			country_iso3: order.orderShipCountry,
			tag: "InfoReceived",
			subtag: "InfoReceived_001",
			subtag_message: "Info Received",
			checkpoint_time: order.orderDateTime,
			coordinates: [],
			state: order.orderShipState,
			zip: order.orderShipZipCode
		};

		const orderPrepared: any = {
			slug: order.courier_slug,
			city: order.orderShipCity,
			created_at: order.orderDateTime,
			location: order.orderShipAddress,
			country_name: null,
			message: "Order details sent to seller",
			country_iso3: order.orderShipCountry,
			tag: "InfoReceived",
			subtag: "InfoReceived_002",
			subtag_message: "Info Received",
			checkpoint_time: order.orderDateTime,
			coordinates: [],
			state: order.orderShipState,
			zip: order.orderShipZipCode
		};

		this.orderServices.getOrderTrackings(order.orderId, order.courier_slug).then(
			(data: any) => {
				if (data && data.success) {
					this.trackingArr = [];

					if (
						order.isPrepared == 0 &&
						order.isCancelled == 1 &&
						!_.isEmpty(order.cancelledDateTime)
					) {
						this.trackingArr.push(orderCreated);
					} else if (
						order.isPrepared == 1 &&
						order.isCancelled == 1 &&
						!_.isEmpty(order.cancelledDateTime)
					) {
						this.trackingArr.push(orderPrepared, orderCreated);
					} else if (
						order.isPrepared == 0 &&
						order.isCancelled == 0 &&
						order.isShipped == 0
					) {
						this.trackingArr.push(orderCreated);
					} else {
						this.trackingArr.push(orderPrepared, orderCreated);
					}

					let trackings = data.result.checkpoints;
					_.each(trackings, (row: any) => {
						this.trackingArr.unshift(row);
					});
				}
				loading.dismiss();
				if (ev) {
					ev.complete();
				}
			},
			(error: any) => {
				console.log("getOrderTrackings err: ", error);
				loading.dismiss();
				if (ev) {
					ev.complete();
				}
			}
		);
	}

	ngOnInit() {
		console.log("params: ", this.params);
		this.refresh.subscribe(data => {
			this.initLogisticTracking(this.params);
		});
		this.initLogisticTracking(this.params);
	}
}
