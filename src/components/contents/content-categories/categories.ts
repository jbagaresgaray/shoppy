import {
  Component,
  OnInit,
  ViewEncapsulation,
  Input,
  EventEmitter
} from "@angular/core";
import { NavController, NavParams, LoadingController } from "ionic-angular";
import * as _ from "lodash";

import { SearchresultPage } from "../../../pages/searchresult/searchresult";

import { CategoriesServiceProvider } from "../../../providers/service/categories";

@Component({
  selector: "page-categories",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "categories.html"
})
export class CategoriesPage {
  category: any = {};
  subcategory: any = {};

  categoriesArr: any[] = [];
  fakeArr: any[] = [];

  isProduct: boolean = false;
  showLoading: boolean = true;

  callback: any;

  @Input() refresh: EventEmitter<any>;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public categories: CategoriesServiceProvider
  ) {
    if (this.navParams.get("isProduct")) {
      this.isProduct = this.navParams.get("isProduct");
      this.callback = this.navParams.get("callback");
    }

    for (let index = 0; index < 20; index++) {
      this.fakeArr.push(index);
    }
    console.log("isProduct: ", this.isProduct);
  }

  initData(ev?: any) {
    this.showLoading = true;
    this.categories.getAllCategories().then(
      (data: any) => {
        console.log("categories: ", data);
        if (data && data.success) {
          this.categoriesArr = data.result;
          _.each(this.categoriesArr, (row: any) => {
            row.opened = false;
          });
        }
        this.showLoading = false;
        if (ev) {
          ev.complete();
        }
      },
      error => {
        this.showLoading = false;
        if (ev) {
          ev.complete();
        }
      }
    );
  }

  ngOnInit() {
    console.log("ionViewDidLoad CategoriesPage");
    this.initData();

    this.refresh.subscribe(data => {
      this.initData(data);
    });
  }

  doRefresh(ev) {
    console.log("ionViewDidLoad CategoriesPage");
    this.initData(ev);
  }

  viewCategory(item, type, $event) {
    if (!this.isProduct && type === "category") {
      console.log("1");
      this.navCtrl.push(SearchresultPage, {
        action: type,
        item: item
      });
    } else if (!this.isProduct && type === "subcategory") {
      console.log("2");
      this.navCtrl.push(SearchresultPage, {
        action: type,
        item: this.category,
        sub_item: item
      });
    } else if (this.isProduct && type === "category") {
      console.log("3");
      this.category = item;
      if (item.subcategory && item.subcategory.length < 1) {
        this.subcategory = {};
        this.callback({
          category: this.category,
          subcategory: this.subcategory
        }).then(() => {
          this.navCtrl.pop();
        });
      }

      _.each(this.categoriesArr, (row: any) => {
        if (item.categoryId == row.categoryId) {
          row.opened = !row.opened;
        }
      });
      $event.stopPropagation();
    } else if (this.isProduct && type === "subcategory") {
      console.log("4");
      this.subcategory = item;
      this.callback({
        category: this.category,
        subcategory: this.subcategory
      }).then(() => {
        this.navCtrl.pop();
      });
    }
  }

  viewSubcategory(item, $event) {
    console.log("viewSubcategory");
    _.each(this.categoriesArr, (row: any) => {
      if (item.categoryId == row.categoryId) {
        row.opened = !row.opened;
        this.category = item;
      }
    });
    $event.stopPropagation();
  }
}
