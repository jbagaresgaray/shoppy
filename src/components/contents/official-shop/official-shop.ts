import { Component, Input, ViewEncapsulation, OnInit, EventEmitter } from '@angular/core';
import * as _ from "lodash";
import {
	LoadingController
} from "ionic-angular";

import { SellerServiceProvider } from "../../../providers/service/seller";

@Component({
	selector: 'official-shop',
	encapsulation: ViewEncapsulation.None,
	templateUrl: 'official-shop.html'
})
export class OfficialShopComponent implements OnInit {
	@Input() params: any = {};
	@Input() officialShop: any = {};
	@Input() progressCount: number;

	@Input() refresh: EventEmitter<any>;

	constructor(public loadingCtrl: LoadingController, public sellers: SellerServiceProvider) {
	}

	ngOnInit() {
		this.refresh.subscribe(data => {
			this.getOfficialShopApplicationStatus(this.params.shopId);
		});

		this.getOfficialShopApplicationStatus(this.params.shopId);
	}


	private getOfficialShopApplicationStatus(shopId, ev?: any) {
		let loading = this.loadingCtrl.create();
		loading.present();
		this.sellers.getOfficialShopApplicationStatus(shopId).then(
			(data: any) => {
				if (data && data.success) {
					console.log("data: ", data);
					this.officialShop = data.result;

					if (!_.isEmpty(this.officialShop.activity)) {
						if (
							_.find(this.officialShop.activity, {
								eval_categoryID: "APPLICANTS"
							})
						) {
							this.progressCount += 25;
						}

						if (
							_.find(this.officialShop.activity, {
								eval_categoryID: "IN-REVIEW"
							})
						) {
							this.progressCount += 25;
						}

						if (
							_.find(this.officialShop.activity, {
								eval_categoryID: "FOR-APPROVAL"
							})
						) {
							this.progressCount += 25;
						}

						if (
							_.find(this.officialShop.activity, {
								eval_categoryID: "COMPLETED"
							})
						) {
							this.progressCount += 25;
						}
					} else {
						this.progressCount = 0;
					}
				}
				loading.dismiss();
				if (ev) {
					ev.complete();
				}
			},
			(error: any) => {
				console.log("Error: ", error);
				loading.dismiss();
				if (ev) {
					ev.complete();
				}
			}
		);
	}
}
