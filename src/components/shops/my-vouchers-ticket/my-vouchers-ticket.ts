import { Component, ViewEncapsulation, Input } from "@angular/core";

/**
 * Generated class for the MyVouchersTicketComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
	selector: "my-vouchers-ticket",
	encapsulation: ViewEncapsulation.None,
	templateUrl: "my-vouchers-ticket.html"
})
export class MyVouchersTicketComponent {
	@Input() isInValid: boolean = false;
	@Input() isUsed: boolean = false;

	constructor() {}
}
