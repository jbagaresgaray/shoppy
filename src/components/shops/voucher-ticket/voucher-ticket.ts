import { Component, ViewEncapsulation, OnInit, Input } from "@angular/core";
import * as _ from "lodash";

/**
 * Generated class for the VoucherTicketComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: "voucher-ticket",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "voucher-ticket.html"
})
export class VoucherTicketComponent implements OnInit {
  @Input() voucher: any = {};
  @Input() currency: any = {};
  user: any = {};

  constructor() {}

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem("app.user")) || {};
    this.voucher.isClaimed = false;
    if (!_.isEmpty(this.user)) {
      if (this.voucher.sellerId === this.user.userId) {
        this.voucher.enableClaim = false;
      } else {
        this.voucher.enableClaim = true;
      }
    }
  }
}
