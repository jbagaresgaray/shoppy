import { Component, ViewEncapsulation } from "@angular/core";

/**
 * Generated class for the ProductDiscountPromotionComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
	selector: "product-discount-promotion",
	encapsulation: ViewEncapsulation.None,
	templateUrl: "product-discount-promotion.html"
})
export class ProductDiscountPromotionComponent {
	constructor() {}
}
