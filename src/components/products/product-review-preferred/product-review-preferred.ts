import { Component, ViewEncapsulation } from "@angular/core";

/**
 * Generated class for the ProductReviewPreferredComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
	selector: "product-review-preferred",
	encapsulation: ViewEncapsulation.None,
	templateUrl: "product-review-preferred.html"
})
export class ProductReviewPreferredComponent {
	constructor() {}
}
