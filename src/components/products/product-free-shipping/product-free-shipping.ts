import { Component } from '@angular/core';

/**
 * Generated class for the ProductFreeShippingComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'product-free-shipping',
  templateUrl: 'product-free-shipping.html'
})
export class ProductFreeShippingComponent {

  text: string;

  constructor() {
    console.log('Hello ProductFreeShippingComponent Component');
    this.text = 'Hello World';
  }

}
