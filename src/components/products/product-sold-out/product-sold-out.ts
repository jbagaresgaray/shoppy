import { Component, ViewEncapsulation } from "@angular/core";

/**
 * Generated class for the ProductSoldOutComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
	selector: "product-sold-out",
	encapsulation: ViewEncapsulation.None,
	templateUrl: "product-sold-out.html"
})
export class ProductSoldOutComponent {
	constructor() {}
}
