import { Component, Input, ViewEncapsulation, OnInit } from "@angular/core";

@Component({
  selector: "product-wholesale-modal",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "product-wholesale-modal.html"
})
export class ProductWholesaleModalComponent implements OnInit {
  @Input() wholesaleArr: any[] = [];
  @Input() currency: any = {};

  constructor() {}

  ngOnInit() {
    console.log("wholesaleArr: ", this.wholesaleArr);
  }
}
