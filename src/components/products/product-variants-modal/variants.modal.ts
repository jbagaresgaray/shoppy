import { Component, NgZone } from "@angular/core";
import {
  NavParams,
  ViewController,
  LoadingController,
  Events,
  ModalController
} from "ionic-angular";
import * as _ from "lodash";

import { LoginPage } from "../../../pages/login/login";

import { ShoppingCartProvider } from "../../../providers/service/shopping-cart";

@Component({
  selector: "page-variants-modal",
  templateUrl: "variants.modal.html"
})
export class ProductVariantsModalPage {
  variantsArr: string;
  priceTitle: string;
  stockCount: number;
  image: any;
  action: string;
  details: any = {};
  cartItem: any = {};
  user: any = {};
  hasSelected: boolean = false;

  enableBuying: boolean = true;

  constructor(
    public viewCtrl: ViewController,
    public params: NavParams,
    public loadingCtrl: LoadingController,
    public events: Events,
    public zone: NgZone,
    public modalCtrl: ModalController,
    public shoppingCart: ShoppingCartProvider
  ) {
    this.variantsArr = params.get("variants");
    _.each(this.variantsArr, (row: any) => {
      row.selected = false;
      if (row.stock < 1) {
        row.isDisable = true;
      } else {
        row.isDisable = false;
      }
    });
    this.image = params.get("image");
    this.details = params.get("details");
    this.user = JSON.parse(localStorage.getItem("app.user")) || {};
    console.log("this.user: ", this.user);
    console.log("this.details: ", this.details);

    if (!_.isEmpty(this.user)) {
      if (this.details.userId == this.user.userId) {
        this.enableBuying = false;
      }
    }

    this.cartItem.quantity = 1;

    let minPrice: any = _.minBy(this.variantsArr, (row: any) => {
      return row.price;
    });
    let maxPrice: any = _.maxBy(this.variantsArr, (row: any) => {
      return row.price;
    });
    let stockCount: any = _.sumBy(this.variantsArr, (row: any) => {
      return row.stock;
    });
    this.stockCount = stockCount;

    if (minPrice.price == maxPrice.price) {
      this.priceTitle = minPrice.price;
    } else {
      this.priceTitle = minPrice.price + "-" + maxPrice.price;
    }
  }

  ionViewWillEnter() {
    console.log("ionViewWillEnter ProductVariantsModalPage");
    if (!_.isEmpty(this.user)) {
      if (this.details.userId == this.user.userId) {
        this.enableBuying = false;
      }
    }
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  selectItem(item: any) {
    _.each(this.variantsArr, (row: any) => {
      if (row.productVariationId == item.productVariationId) {
        this.zone.run(() => {
          row.selected = true;
          this.hasSelected = true;
        });
        this.details.variantId = row.productVariationId;
        this.details.variant = row;
        this.details.productPrice = row.price;
        this.priceTitle = row.price;
        this.stockCount = row.stock;
        console.log("row: ", row);
      } else {
        row.selected = false;
      }
    });
  }

  lessQuantity() {
    if (this.details.quantity < 2) {
      return;
    }
    this.details.quantity = parseInt(this.details.quantity) - 1;
  }

  addQuantity() {
    this.details.quantity = parseInt(this.details.quantity) + 1;
  }

  addToCart() {
    if (_.isEmpty(this.user)) {
      let modal = this.modalCtrl.create(LoginPage);

      this.viewCtrl.dismiss().then(() => {
        modal.present();
      });
      return;
    }

    this.details.quantity = parseInt(this.details.quantity);
    console.log("this.details: ", this.details);
    let loading = this.loadingCtrl.create();
    loading.onDidDismiss(() => {
      this.events.publish("update:cart");
      this.viewCtrl.dismiss("save");
    });
    loading.present().then(() => {
      this.shoppingCart.addItem(this.details, 0);
    });
    setTimeout(() => {
      loading.dismiss();
    }, 600);
  }
}
