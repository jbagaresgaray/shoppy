import { Component, ViewEncapsulation } from "@angular/core";

/**
 * Generated class for the ProductPreferredComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
	selector: "product-preferred",
	encapsulation: ViewEncapsulation.None,
	templateUrl: "product-preferred.html"
})
export class ProductPreferredComponent {
	constructor() {}
}
