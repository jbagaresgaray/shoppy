import { Component, Input, ViewEncapsulation, OnInit } from "@angular/core";

@Component({
  selector: "product-shipping-modal",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "product-shipping-modal.html"
})
export class ProductShippingModalComponent implements OnInit {
  @Input() shippingFeeArr: any[] = [];
  @Input() currency: any = {};

  constructor() {
  }

  ngOnInit(){
    console.log("shippingFeeArr: ", this.shippingFeeArr);
  }
}
