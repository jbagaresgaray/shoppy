import { Component, ViewEncapsulation } from "@angular/core";

/**
 * Generated class for the ProductOfficialShopBadgeComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
	selector: "product-official-shop-badge",
	encapsulation: ViewEncapsulation.None,
	templateUrl: "product-official-shop-badge.html"
})
export class ProductOfficialShopBadgeComponent {
	constructor() {}
}
