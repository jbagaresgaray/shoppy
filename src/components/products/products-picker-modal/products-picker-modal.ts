import {
  Component,
  OnInit,
  ViewEncapsulation,
  Input,
  EventEmitter,
  NgZone
} from "@angular/core";
import {
  NavController,
  NavParams,
  LoadingController,
  ModalController,
  AlertController,
  ViewController
} from "ionic-angular";
import * as _ from "lodash";

import { SellerServiceProvider } from "../../../providers/service/seller";

@Component({
  selector: "products-picker-modal",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "products-picker-modal.html"
})
export class ProductsPickerModalComponent implements OnInit {
  showContent: boolean;
  productsArr: any[] = [];
  productsArrCopy: any[] = [];
  selectedProducts: any[] = [];
  fakeArr: any[] = [];

  mode: string;

  @Input() discount: any = {};
  @Input() isLow: boolean;
  @Input() searchMode: string;

  @Input() create: EventEmitter<any>;
  @Input() _sortPrice: EventEmitter<any>;
  @Input() _segmentChanged: EventEmitter<any>;
  @Input() _getItems: EventEmitter<any>;
  @Input() _onClear: EventEmitter<any>;

  constructor(
    public zone: NgZone,
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public viewCtrl: ViewController,
    public sellers: SellerServiceProvider
  ) {
    for (let index = 0; index < 20; index++) {
      this.fakeArr.push(index);
    }
  }

  ngOnInit() {
    console.log("discount: ", this.discount);
    this.mode = this.navParams.get("mode");
    if (this.navParams.get("products")) {
      this.selectedProducts = this.navParams.get("products");
      console.log("this.selectedProducts: ", this.selectedProducts);
    }

    this.initData();

    this.create.subscribe(() => {
      this.selectProducts();
    });

    this._sortPrice.subscribe(ev => {
      this.sortPrice();
    });

    this._segmentChanged.subscribe(ev => {
      this.segmentChanged(ev);
    });

    this._getItems.subscribe(ev => {
      this.getItems(ev);
    });

    this._onClear.subscribe(() => {
      this.onClear();
    });
  }

  errorHandler(event) {
    event.target.src = "../../../assets/imgs/products/no-preview-150.png";
  }

  private initData(ev?: any) {
    const loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    if (_.isEmpty(ev)) {
      loading.present();
    }

    this.showContent = false;
    this.sellers.getSellerProducts().then(
      (data: any) => {
        if (data && data.success) {
          _.each(data.result, (row: any) => {
            row.date_created_timestamp = new Date(row.date_created).getTime();
            row.selected = false;
          });

          this.productsArr = data.result;
          this.productsArrCopy = data.result;

          if (!_.isEmpty(this.selectedProducts)) {
            _.each(this.productsArr, (row: any) => {
              const result = _.find(this.selectedProducts, {
                productId: row.productId
              });
              if (result) {
                row.selected = !row.selected;
              }
            });
          }
        }
        this.showContent = true;
        loading.dismiss();
        if (ev) {
          ev.complete();
        }
      },
      error => {
        console.log("error: ", error);
        this.showContent = true;
        loading.dismiss();
        if (ev) {
          ev.complete();
        }
      }
    );
  }

  checkChanged(item: any) {
    _.each(this.productsArr, (row: any) => {
      if (item.productId == row.productId) {
        row.selected = !row.selected;
      }
    });
  }

  segmentChanged(ev: any) {
    console.log("this.searchMode: ", this.searchMode);
    const loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    loading.present();

    if (this.searchMode == "popular") {
      this.productsArr = _.orderBy(this.productsArr, "rating", "desc");
    } else if (this.searchMode == "latest") {
      this.productsArr = _.orderBy(
        this.productsArr,
        "date_created_timestamp",
        "asc"
      );
    } else if (this.searchMode == "top") {
      this.productsArr = _.orderBy(this.productsArr, "orderscount", "desc");
    } else if (this.searchMode == "price") {
      if (this.isLow) {
        this.productsArr = _.orderBy(this.productsArr, "productPrice", "asc");
      } else {
        this.productsArr = _.orderBy(this.productsArr, "productPrice", "desc");
      }
    }
    loading.dismiss();
  }

  getItems(ev) {
    const val = ev.target.value;

    if (val && val.trim() != "") {
      this.productsArr = this.productsArrCopy.filter(item => {
        return item.productName.toLowerCase().indexOf(val.toLowerCase()) > -1;
      });
    } else {
      this.productsArr = this.productsArrCopy;
    }
  }

  onClear() {
    this.productsArr = [];
    this.productsArr = this.productsArrCopy;
  }

  sortPrice() {
    this.isLow = !this.isLow;
    if (this.isLow) {
      this.zone.run(() => {
        this.productsArr = _.orderBy(this.productsArr, "productPrice", "asc");
      });
    } else {
      this.zone.run(() => {
        this.productsArr = _.orderBy(this.productsArr, "productPrice", "desc");
      });
    }
    console.log("sortPrice: ", this.productsArr);
  }

  selectProducts() {
    const selectedProducts: any[] = _.filter(this.productsArr, (row: any) => {
      return row.selected;
    });

    this.viewCtrl.dismiss({
      action: "save",
      products: selectedProducts
    });
  }
}
