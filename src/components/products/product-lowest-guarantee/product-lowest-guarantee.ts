import { Component, ViewEncapsulation } from "@angular/core";

/**
 * Generated class for the ProductLowestGuaranteeComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
	selector: "product-lowest-guarantee",
	encapsulation: ViewEncapsulation.None,
	templateUrl: "product-lowest-guarantee.html"
})
export class ProductLowestGuaranteeComponent {
	constructor() {}
}
