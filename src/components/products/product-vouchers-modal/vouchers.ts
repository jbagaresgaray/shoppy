import { Component, OnInit, ViewChild, AfterViewInit } from "@angular/core";
import {
  NavController,
  NavParams,
  ViewController,
  Content
} from "ionic-angular";
import * as _ from "lodash";

import { ProductsServiceProvider } from "../../../providers/service/products";
import { SellerVouchersService } from "../../../providers/service/seller-vouchers";
import { ServiceProvider } from "../../../providers/service/service";

@Component({
  selector: "page-vouchers-modal",
  templateUrl: "vouchers.html"
})
export class VouchersPage implements OnInit, AfterViewInit {
  variants: any[] = [];
  sellerVouchersArr: any[] = [];
  productVouchersArr: any[] = [];

  image: any = {};
  details: any = {};
  currency: any = {};
  user: any = {};

  enableClaim: boolean;
  @ViewChild(Content) content: Content;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public productService: ProductsServiceProvider,
    public sellerVoucherService: SellerVouchersService,
    public serviceProvider: ServiceProvider
  ) {
    this.variants = navParams.get("variants");
    this.image = navParams.get("image");
    this.details = navParams.get("details");

    this.user = JSON.parse(localStorage.getItem("app.user")) || {};
    this.currency = this.serviceProvider.defaultCurrency();
  }

  ngOnInit() {
    console.log("ionViewDidLoad VouchersPage");
    this.enableClaim = false;
    if (!_.isEmpty(this.user)) {
      if (this.details.userId === this.user.userId) {
        this.enableClaim = false;
      } else {
        this.enableClaim = true;
      }
    }

    this.sellerVoucherService.getSellerVouchers(this.details.userId).then(
      (data: any) => {
        if (data && data.success) {
          console.log("_sellerVouchers: ", data.result);
          this.sellerVouchersArr = data.result;
        }
      },
      error => {
        console.log("error: ", error);
      }
    );

    this.productService.getAllProductVouchers(this.details.productId).then(
      (data: any) => {
        if (data && data.success) {
          console.log("productVouchersArr: ", data.result);
          this.productVouchersArr = data.result;
        }
      },
      error => {
        console.log("error: ", error);
      }
    );
  }

  ngAfterViewInit(){
    if(this.content){
      this.content.resize();
    }
  }

  close() {
    this.viewCtrl.dismiss();
  }
}
