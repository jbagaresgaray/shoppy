import { Component, Input, Output, EventEmitter } from "@angular/core";

import { ServiceProvider } from "../../../providers/service/service";
/**
 * Generated class for the ChatOrderMessageComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
	selector: "chat-order-message",
	templateUrl: "chat-order-message.html"
})
export class ChatOrderMessageComponent {
	@Input() order: any = {};
	@Input() message: any = {};
	@Output() _viewDetails = new EventEmitter<any>();

	currency: any = {};
	minPrice: number;
	maxPrice: number;
	action: string;
	hasVariants: boolean = false;

	constructor(private services: ServiceProvider) {}

	ngOnInit() {
		this.currency = this.services.defaultCurrency();
		console.log("message: ", this.message);
		console.log("order: ", this.order);
		if (this.order) {
			if (this.order.isPrepared === 1 && this.order.isShipped === 0) {
				this.action = "unpaid";
			} else if (
				this.order.isPrepared === 1 &&
				this.order.isShipped === 1
			) {
				this.action = "shipping";
			}
		}
	}

	viewDetails(order) {
		this._viewDetails.emit(order);
	}
}
