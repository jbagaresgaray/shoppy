import { Component,Input } from '@angular/core';

/**
 * Generated class for the ChatDetailMessageComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'chat-detail-message',
  templateUrl: 'chat-detail-message.html'
})
export class ChatDetailMessageComponent {

  @Input() message: any = {};
  @Input() currentUser: any = {};
  @Input() toUser: any = {};

  constructor() {
  }

}
