import { Component, Input, OnInit, ViewEncapsulation } from "@angular/core";
import * as _ from "lodash";
import {
  ModalController,
  LoadingController,
  NavController,
  Events
} from "ionic-angular";

import { ServiceProvider } from "../../../providers/service/service";
import { ShoppingCartProvider } from "../../../providers/service/shopping-cart";

import { ModalComponentFormPage } from "../../../pages/modal-component-form/modal-component-form";
import { ShoppingcartPage } from "../../../pages/shoppingcart/shoppingcart";

@Component({
  selector: "chat-product-message",
  encapsulation: ViewEncapsulation.None,
  templateUrl: "chat-product-message.html"
})
export class ChatProductMessageComponent implements OnInit {
  @Input() product: any = {};
  @Input() message: any = {};

  currency: any = {};
  minPrice: number;
  maxPrice: number;
  hasVariants: boolean = false;

  isMakeOffer: boolean;

  constructor(
    private services: ServiceProvider,
    public modalCtrl: ModalController,
    public loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public events: Events,
    public shoppingCart: ShoppingCartProvider
  ) {}

  ngOnInit() {
    this.currency = this.services.defaultCurrency();
    const vuser = JSON.parse(localStorage.getItem("app.user")) || {};

    console.log("message: ", this.message);

    if (this.product) {
      if (!_.isEmpty(this.product.variants)) {
        this.hasVariants = true;
      }

      this.minPrice = _.minBy(this.product.variants, (row: any) => {
        return row.price;
      });
      this.maxPrice = _.maxBy(this.product.variants, (row: any) => {
        return row.price;
      });

      if (this.message.toUserId == vuser.uuid) {
        this.isMakeOffer = false;
      } else {
        this.isMakeOffer = true;
      }
    }
  }

  makeOffer() {
    const params = {
      action: "make_offer_modal",
      variants: this.product.variants,
      image: this.product.image,
      details: this.product,
      modalInset: true
    };
    console.log("params: ", params);

    const modal = this.modalCtrl.create(ModalComponentFormPage, params, {
      cssClass: "inset-modal"
    });
    modal.present();
  }

  buyNow() {
    const loading = this.loadingCtrl.create({
      dismissOnPageChange: true
    });
    const params = {
      action: "product_variants_modal",
      variants: this.product.variants,
      image: this.product.image,
      details: this.product,
      modalInset: true
    };

    if (_.size(this.product.variants) > 0) {
      const modal = this.modalCtrl.create(ModalComponentFormPage, params, {
        cssClass: "inset-modal"
      });

      modal.onDidDismiss(resp => {
        if (resp) {
          this.events.publish("update:cart");
          setTimeout(() => {
            this.navCtrl.push(ShoppingcartPage);
          }, 300);
        }
      });
      modal.present();
    } else {
      loading.onDidDismiss(() => {
        this.events.publish("update:cart");
        setTimeout(() => {
          this.navCtrl.push(ShoppingcartPage);
        }, 300);
      });
      loading.present().then(() => {
        this.shoppingCart.addItem(this.product, 1);
      });
      setTimeout(() => {
        loading.dismiss();
      }, 600);
    }
  }
}
