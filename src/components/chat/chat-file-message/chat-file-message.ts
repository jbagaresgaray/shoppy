import { Component } from '@angular/core';

/**
 * Generated class for the ChatFileMessageComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'chat-file-message',
  templateUrl: 'chat-file-message.html'
})
export class ChatFileMessageComponent {

  text: string;

  constructor() {
    console.log('Hello ChatFileMessageComponent Component');
    this.text = 'Hello World';
  }

}
