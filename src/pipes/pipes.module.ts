import { NgModule } from '@angular/core';
import { RelativeTimePipe } from './relative-time/relative-time';
import { KeepHtmlPipe } from './keep-html/keep-html';
@NgModule({
	declarations: [RelativeTimePipe,
    KeepHtmlPipe],
	imports: [],
	exports: [RelativeTimePipe,
    KeepHtmlPipe]
})
export class PipesModule {}
