// this is a custom dictionary to make it easy to extend/override
// provide a name for an entry, it can be anything such as 'copyAssets' or 'copyFonts'
// then provide an object with a `src` array of globs and a `dest` string
module.exports = {
    copyAssets: {
        src: ["{{SRC}}/assets/**/*"],
        dest: "{{WWW}}/assets"
    },
    copyIndexContent: {
        src: [
            "{{SRC}}/index.html",
            "{{SRC}}/manifest.json",
            "{{SRC}}/service-worker.js"
        ],
        dest: "{{WWW}}"
    },
    copyFonts: {
        src: [
            "{{ROOT}}/node_modules/ionicons/dist/fonts/**/*",
            "{{ROOT}}/node_modules/ionic-angular/fonts/**/*",
            "{{SRC}}/assets/fonts"
        ],
        dest: "{{WWW}}/assets/fonts"
    },
    copyPolyfills: {
        src: [
            `{{ROOT}}/node_modules/ionic-angular/polyfills/${
                process.env.IONIC_POLYFILL_FILE_NAME
            }`
        ],
        dest: "{{BUILD}}"
    },
    copySwToolbox: {
        src: ["{{ROOT}}/node_modules/sw-toolbox/sw-toolbox.js"],
        dest: "{{BUILD}}"
    },
    copySlickFonts: {
        src: ["{{ROOT}}/node_modules/slick-carousel/slick/fonts/*.*"],
        dest: "{{BUILD}}/fonts"
    },
    copySlick: {
        src: [
            "{{ROOT}}/node_modules/slick-carousel/slick/slick.css",
            "{{ROOT}}/node_modules/slick-carousel/slick/slick-theme.css",
            "{{ROOT}}/node_modules/slick-carousel/slick/ajax-loader.gif"
        ],
        dest: "{{BUILD}}/slick"
    },
    copySlickGIF: {
        src: ["{{ROOT}}/node_modules/slick-carousel/slick/ajax-loader.gif"],
        dest: "{{BUILD}}"
    },
    copyThemify: {
        src: ["{{SRC}}/assets/themify-icons.css"],
        dest: "{{WWW}}/assets"
    },
    copyJquery: {
        src: ["{{ROOT}}/node_modules/jquery/dist/jquery.min.js"],
        dest: "{{BUILD}}/jquery"
    },
    copyCircleProgress: {
        src: [
            "{{ROOT}}/node_modules/jquery-circle-progress/dist/circle-progress.min.js"
        ],
        dest: "{{BUILD}}/jquery-circle-progress"
    },
    copyFontAwesome: {
        src: ["{{ROOT}}/node_modules/font-awesome/fonts/**/*"],
        dest: "{{WWW}}/assets/fonts"
    }
};
