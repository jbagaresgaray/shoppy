var chalk = require("chalk");
var fs = require('fs');
var path = require('path');
var useDefaultConfig = require('@ionic/app-scripts/config/webpack.config.js');

var env = process.env.MY_ENV;
console.log('webpack env: ', env);

useDefaultConfig.prod.resolve.alias = {
  "@app/env": path.resolve(environmentPath('prod'))
};

useDefaultConfig.dev.resolve.alias = {
  "@app/env": path.resolve(environmentPath('dev'))
};


/*if (env !== 'prod' && env !== 'dev') {
  // Default to dev config
  useDefaultConfig[env] = useDefaultConfig.dev;
  useDefaultConfig[env].resolve.alias = {
    "@app/env": path.resolve(environmentPath(env))
  };
}*/

if (env == 'prod') {
  useDefaultConfig[env] = useDefaultConfig.dev;
  useDefaultConfig.prod.resolve.alias = {
    "@app/env": path.resolve(environmentPath('prod'))
  };
} else if (env == 'staging') {
  useDefaultConfig[env] = useDefaultConfig.dev;
  useDefaultConfig[env].resolve.alias = {
    "@app/env": path.resolve(environmentPath('staging'))
  };
} else if (env == 'dev') {
  useDefaultConfig[env] = useDefaultConfig.dev;
  useDefaultConfig.dev.resolve.alias = {
    "@app/env": path.resolve(environmentPath('dev'))
  };
}

function environmentPath(env) {
  console.log('env: ', env);
  var filePath = './src/environments/environment' + '.' + env + '.ts';
  if (!fs.existsSync(filePath)) {
    console.log(chalk.red('\n' + filePath + ' does not exist!'));
  } else {
    console.log('filePath: ', filePath);
    return filePath;
  }
}

module.exports = function() {
  return useDefaultConfig;
};