## How to run with environment

### With the Ionic CLI:

Here the environment supported on this app `prod`,`staging`,`dev` and by default the environment runs on a production environemtn `prod`. To run on different environment below:

## SERVING WITH ENVIRONMENT

PRODUCTION
```bash
$ npm run serve:prod
```

STAGING
```bash
$ npm run serve:staging
```

LOCAL
```bash
$ npm run serve:dev
```

## BUILDING WITH ENVIRONMENT

PRODUCTION
```bash
$ npm run build:prod
```

STAGING
```bash
$ npm run build:staging
```

LOCAL
```bash
$ npm run build:dev
```

## compile and build with cordova on different environment variables:

PRODUCTION
```bash
$ SET MY_ENV=prod
$ ionic cordova run ios
```
OR
```bash
$ SET MY_ENV=prod
$ ionic cordova build ios
```


STAGING
```bash
$ SET MY_ENV=staging
$ ionic cordova run ios
```
OR
```bash
$ SET MY_ENV=staging
$ ionic cordova build ios
```

LOCAL
```bash
$ SET MY_ENV=dev
$ ionic cordova run ios
```
OR
```bash
$ SET MY_ENV=dev
$ ionic cordova build ios
```